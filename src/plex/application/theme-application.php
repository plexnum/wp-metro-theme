<?php

/**
 * Goal of that class to instantiate theme application
 *
 * Example:
 *   $plex_app = Plex_Application_ThemeApplication::run();
 *
 * Class Plex_Application_ThemeApplication
 */
class Plex_Application_ThemeApplication {

	/**
	 * Static instance of the application. Singleton pattern
	 *
	 * @var Plex_Application_ThemeApplication
	 */
	static $app_instance;

	/**
	 * Theme localization domain used by application
	 *
	 * @var string
	 */
	static $theme_localization_domain;

	/**
	 * Singleton instance of theme config bag
	 *
	 * @var Plex_Config_ParamBag
	 */
	private $theme_config_bag;

	/**
	 * Array of widgets grouped by two types: dash and common
	 *
	 * @var array
	 */
	private $widgets;

	/**
	 * Light up. Singleton method to instantiate theme application
	 *
	 * @return Plex_Application_ThemeApplication
	 */
	static function run() {
		if ( null === self::$app_instance ) {
			return new self();
		}
		return self::$app_instance;
	}

	/**
	 * Initialize theme application
	 */
	function __construct() {
		$this->widgets = array( 'dash' => array(), 'common' => array() );
		$this->initialize_theme_config_bag();
		$this->load_helper_functions();
		$this->initialize_widget_configs();
		$this->initialize_navigation_menus();
		$this->initialize_sidebars();
		$this->initialize_theme_supports();

		add_action( 'init', array( $this, 'initialize_post_type_supports' ) );

		add_action( 'after_setup_theme', array( $this, 'load_theme_localization' ) );
		add_action( 'widgets_init', array( $this, 'register_widgets' ) );
		add_action( 'wp_dashboard_setup', array( $this, 'register_dash_widgets' ) );
		// Register plugins
		$this->register_theme_components();
		$this->load_theme_helper_functions();
	}

	/**
	 * Register theme components from theme components configuration file
	 *
	 * @throws Plex_Exception_Common if component configuration file not founded
	 */
	function register_theme_components() {

		try {
			$components             = plex_include_file( $this->theme_config_bag->get( 'patches[components_declaration_file]', null, true ) );
			$plex_component_manager = new Plex_Component_Manager();
			foreach ( $components as $component ) {
				$plex_component_manager->register( $component );
			}
			$plex_component_manager->initialize();
		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}
	}

	/**
	 * Register common group widgets
	 */
	function register_widgets() {
		// register common group widgets
		foreach ( $this->widgets['common'] as $widget ) {

			if ( is_array( $widget ) ) {
				list( $remove_widget, $widget ) = $widget;
				unregister_widget( $remove_widget );
			}

			register_widget( $widget );
		}
	}

	/**
	 * Register dash group widgets
	 */
	function register_dash_widgets() {
		// register dash widgets
		foreach ( $this->widgets['dash'] as $widget ) {
			$widget_instance = new $widget;

			if ( $widget_instance instanceof Plex_Model_SetupableInterface ) {
				wp_add_dashboard_widget( $widget_instance->get_id(),
					$widget_instance->get_name(),
					array( $widget_instance, 'show' ),
					array( $widget_instance, 'setup' )
				);
				continue;
			}

			wp_add_dashboard_widget( $widget_instance->get_id(),
				$widget_instance->get_name(),
				array( $widget_instance, 'show' )
			);

		}
	}

	/**
	 * Initialize widgets from widget configuration file
	 *
	 * @throws Plex_Exception_Common if widget config file not founded
	 */
	function initialize_widget_configs() {

		try {

			$widgets = plex_include_file( $this->theme_config_bag->get( 'patches[widgets_declaration_file]', null, true ) );
			if ( is_array( $widgets ) ) {
				$widget_group = 'common';
				foreach ( $widgets as $widget ) {
					if (
						! is_array( $widget ) &&
						trim( $widget ) == '-'
					) {
						$widget_group = 'dash';
						continue;
					}
					$this->widgets[$widget_group][] = $widget;
				}
			}

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

	}

	/**
	 * Initialize singleton theme configuration bag from configuration file
	 *
	 * @throws Plex_Exception_Common if user config file not founded
	 */
	function initialize_theme_config_bag() {
		// init configurations
		if ( file_exists( PLEX_THEME_DIR . '/theme/configs.php' ) ) {
			$configs = include PLEX_THEME_DIR . '/theme/configs.php';
			// Instantiate singleton config manager class based on defined configurations
			$this->theme_config_bag = Plex_Config_ParamBag::instantiate( $configs );
		}
		else {
			throw new Plex_Exception_Common( 'Theme configuration file not founded' );
		}
	}

	/**
	 * Load theme text domain
	 */
	function load_theme_localization() {
		$domain                          = $this->theme_config_bag->get( 'localization[domain]', false, true );
		$theme_dir                       = $this->theme_config_bag->get( 'patches[theme]', false, true ) . '/languages';
		self::$theme_localization_domain = $domain;
		load_theme_textdomain( $domain, $theme_dir );
	}

	/**
	 * Load theme helper functions from configuration file
	 * @throws Plex_Exception_Common
	 */
	function load_helper_functions() {

		try {

			if ( file_exists( $this->theme_config_bag->get( 'patches[src]', false, true ) . '/plex/helper_functions.php' ) ) {
				require $this->theme_config_bag->get( 'patches[src]', false, true ) . '/plex/helper_functions.php';
			}
			else {
				throw new Plex_Exception_Common( 'Theme helper functions file does not existed' );
			}

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

	}

	/**
	 * Initialize navigation menus from config file
	 * @throws Plex_Exception_Common
	 */
	function initialize_navigation_menus() {

		try {
			// Our theme support nav menus
			add_theme_support( 'nav-menus' );

			if ( function_exists( 'register_nav_menus' ) ) {
				// Load menus declarations from config file
				$menus = plex_include_file( $this->theme_config_bag->get( 'patches[menus_declaration_file]', false, true ) );

				foreach ( $menus as $menu_id => $label ) {
					register_nav_menu( $menu_id, $label );
				}
			}

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

	}

	/**
	 * Initialize sidebars from config file
	 * @throws Plex_Exception_Common
	 */
	function initialize_sidebars() {

		try {

			if ( function_exists( 'register_sidebar' ) ) {
				$sidebars = plex_include_file( $this->theme_config_bag->get( 'patches[sidebars_declaration_file]', false, true ) );

				foreach ( $sidebars as $sidebar ) {
					register_sidebar( $sidebar );
				}
			}

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}
	}

	/**
	 * Load functions which convenient to use in theme templates
	 */
	function load_theme_helper_functions() {
		try {
			plex_include_file( $this->theme_config_bag->get( 'patches[src]', false, true ) . '/plex/template_helper_functions.php', false );
		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}
	}

	/**
	 * Initialize theme supports
	 * @throws Plex_Exception_Common
	 */
	function initialize_theme_supports() {

		try {
			$supports = plex_include_file( $this->theme_config_bag->get( 'patches[theme_support_declaration_file]', false, true ) );

			foreach ( $supports as $name => $arguments ) {
				if ( empty( $arguments ) ) {
					add_theme_support( $name );
				}
				else {
					add_theme_support( $name, $arguments );
				}
			}

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}
	}

	/**
	 * Initialize post type support
	 * @throws Plex_Exception_Common
	 */
	public function  initialize_post_type_supports() {

		try {

			$supports = plex_include_file( $this->theme_config_bag->get( 'patches[post_type_support_declaration_file]', false, true ) );
			foreach ( $supports as $post_type => $support ) {
				if ( isset( $support['add'] ) && is_array( $support['add'] ) ) {
					add_post_type_support( $post_type, $support['add'] );
				}
			}

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}
	}
}