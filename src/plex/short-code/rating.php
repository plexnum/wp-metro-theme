<?php

class Plex_ShortCode_Rating extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'stars' => 'abs_int:0'
	);

	protected $animation = null;

	function __construct() {
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
		parent::__construct();
	}

	function get_tag_name() {
		return 'rating';
	}

	function handle( $atts, $content = null ) {
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		$this->view->add( (array) $atts );
		return $this->view->get_output( '/rating/short-code.php' );
	}

	function form() {
		$this->view->set( 'animation', $this->animation->_form() );
		return $this->view->get_output( '/rating/form.php' );
	}

}