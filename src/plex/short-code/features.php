<?php

class Plex_ShortCode_Features extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'with_links' => 'bool:1'
	);

	protected $animation;

	function __construct() {
		parent::__construct();
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
	}

	function get_tag_name() {
		return 'features';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		return $this->view->get_output( '/features/short-code.php' );
	}

	function form() {
		$this->view->set( 'animation', $this->animation->_form() );
		return $this->view->get_output( '/features/form.php' );
	}

}