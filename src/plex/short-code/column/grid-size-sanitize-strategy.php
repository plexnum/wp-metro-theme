<?php

class Plex_ShortCode_Column_GridSizeSanitizeStrategy extends Plex_Model_AbstractSanitizeStrategy {


	protected $grid_size = 12;

	function get_name() {
		return 'grid_size';
	}

	function sanitize_input( $input, $args = array() ) {
		return $this->sanitize_output( $input, $args );
	}

	function sanitize_output( $output, $args = array() ) {

		$result       = explode( '/', $output );
		$result_count = count( $result );

		if ( $result_count == 1 ) {
			$result = (int) array_shift( $result );

			if ( empty( $result ) ) {
				$result = 1;
			}

			return $result;

		}

		elseif ( $result_count == 2 ) {

			$el1 = (int) array_shift( $result );
			$el2 = (int) array_shift( $result );

			if ( $el1 == 0 ) {
				$el1 = 1;
			}

			if ( $el2 == 0 ) {
				$el2 = 1;
			}

			return $this->grid_size * $el1 / $el2;

		}

		return 1;

	}


}