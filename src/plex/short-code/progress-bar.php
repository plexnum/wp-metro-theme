<?php

class Plex_ShortCode_ProgressBar extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'width'  => 'abs_int:50',
		'type'   => 'in_list(error,success,info,danger,striped,warning):striped',
		'active' => 'bool:1',
		'text'   => 'text_field'
	);

	protected $types;

	protected $animation;

	function __construct() {

		parent::__construct();

		$this->types = array(
			'striped' => __( 'Striped', plex_get_trans_domain() ),
			'error'   => __( 'Error', plex_get_trans_domain() ),
			'success' => __( 'Success', plex_get_trans_domain() ),
			'info'    => __( 'Info', plex_get_trans_domain() ),
			'danger'  => __( 'Danger', plex_get_trans_domain() ),
			'warning' => __( 'Warning', plex_get_trans_domain() )
		);

		$this->animation = new Plex_ShortCode_ShowAnimateMixin();

	}


	function get_tag_name() {
		return 'progress';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		return $this->view->get_output( '/progress/short-code.php' );
	}

	function form() {
		$this->view->set( 'types', $this->types );
		$this->view->set( 'animation', $this->animation->form() );
		return $this->view->get_output( '/progress/form.php' );
	}

}