<?php

class Plex_ShortCode_Label extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'type' => 'in_list(default,success,warning,important,info,inverse)',
		'text' => 'text_field:Default'
	);

	protected $types;

	protected $animation;

	protected $title = '';

	function __construct() {

		parent::__construct();

		$this->types = array(
			'default'   => __( 'Default', plex_get_trans_domain() ),
			'success'   => __( 'Success', plex_get_trans_domain() ),
			'warning'   => __( 'Warning', plex_get_trans_domain() ),
			'important' => __( 'Important', plex_get_trans_domain() ),
			'info'      => __( 'Info', plex_get_trans_domain() ),
			'inverse'   => __( 'Inverse', plex_get_trans_domain() )
		);

		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
		$this->title     = __( 'Label settings', plex_get_trans_domain() );

	}

	function get_tag_name() {
		return 'label';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		return $this->view->get_output( '/label/short-code.php' );
	}

	function form() {
		$this->view->set( 'title', $this->title );
		$this->view->set( 'types', $this->types );
		$this->view->set( 'animation', $this->animation->_form() );
		return $this->view->get_output( '/label/form.php' );
	}

	function preview_settings() {
		return array(
			'text' => __( 'Default', plex_get_trans_domain() )
		);
	}

}