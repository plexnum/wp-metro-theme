<?php

class Plex_ShortCode_Tab extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'id'     => 'text_field',
		'active' => 'bool'
	);

	function get_tag_name() {
		return 'tab';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'content', plex_do_short_code( $content ) );
		return $this->view->get_output( '/tab/short-code.php' );
	}

	function is_manageable() {
		return false;
	}

	function is_customizable() {
		return false;
	}

}