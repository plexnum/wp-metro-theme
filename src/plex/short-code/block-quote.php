<?php

class Plex_ShortCode_BlockQuote extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'author'    => 'text_field',
		'with_icon' => 'bool',
		'content'		=> 'html_data'
	);

	protected $icon_short_code;

	protected $animation;

	function __construct() {
		$this->icon_short_code = new Plex_ShortCode_IconMixin( 'icon', 'icon-quotes-left' );
		$this->animation       = new Plex_ShortCode_ShowAnimateMixin();
		parent::__construct();
	}

	function get_tag_name() {
		return 'block_quote';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'icon', $this->icon_short_code->_handle( $atts, $content ) );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		$this->view->set( 'content', $content );
		return $this->view->get_output( '/block-quote/short-code.php' );
	}

	function form() {
		$this->view->set( 'icon', $this->icon_short_code->_form() );
		$this->view->set( 'animation', $this->animation->_form() );
		return $this->view->get_output( '/block-quote/form.php' );
	}

	function preview_settings() {

		$settings = $this->icon_short_code->preview_settings();

		$cm = plex_get_theme_conf_bag();

		return array_merge( array(
			'with_icon'       => true,
			'author'          => $cm->get( 'preview_author' ),
			'content_preview' => $cm->get( 'preview_medium' ),
			'content'         => $cm->get( 'short_code_content' )
		), $settings );

	}

}