<?php

class Plex_ShortCode_Tabs extends Plex_Model_AbstractShortCode {

	protected $positions = null;

	protected $tabs = null;

	protected $sanitize_rules = array(
		'position' => 'in_list(top,left,right,below):top',
		'*tabs'    => array(
			'id'     => 'text_field',
			'title'  => 'text_field',
			'active' => 'bool'
		)
	);

	protected $animation;

	function __construct() {

		$this->positions = array(
			'top'   => __( 'Top', plex_get_trans_domain() ),
			'left'  => __( 'Left', plex_get_trans_domain() ),
			'right' => __( 'Right', plex_get_trans_domain() ),
			'below' => __( 'Bottom', plex_get_trans_domain() )
		);

		$this->animation = new Plex_ShortCode_ShowAnimateMixin();

		parent::__construct();
	}


	function get_tag_name() {
		return 'tabs';
	}

	function handle( $atts, $content = null ) {

		$content = plex_do_short_code( $this->create_tabs_content( $content ) );
		$this->view->add( (array) $atts );
		$this->view->set( 'content', $content );
		$this->view->set( 'tabs', $this->tabs );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );

		return $this->view->get_output( '/tabs/short-code.php' );
	}

	function form() {
		$this->view->set( 'animation', $this->animation->_form() );
		$this->view->set( 'positions', $this->positions );
		return $this->view->get_output( '/tabs/form.php' );
	}

	function form_sub_templates() {

		return array(
			'plexRadioListTemplate' => $this->view->get_output( '/_sub_templates/radio-list.php' ),
			'plexAddFormView'       => $this->view->get_output( '/_sub_templates/add-form.php' ),
			'plexRadioInput'        => $this->view->get_output( '/_sub_templates/radio-input.php' )
		);

	}


	function preview_settings() {

		$cm = plex_get_theme_conf_bag();

		return array(

			'_preview_content' => $cm->get( 'preview_medium' ),

			'_content'         => $cm->get( 'short_code_content' ),

			'_items'           => array(
				array(
					'title'  => __( 'One', plex_get_trans_domain() ),
					'active' => false
				),
				array(
					'title'  => __( 'Two', plex_get_trans_domain() ),
					'active' => true
				)
			)

		);

	}

	function create_tabs_content( $content ) {

		$this->tabs = array();

		if ( preg_match_all( '#' . get_shortcode_regex() . '#i', $content, $matches ) ) {

			$tab_ids = array();

			if ( isset( $matches[3] ) ) {

				foreach ( $matches[3] as $attrs ) {

					$tab_title = plex_get_string_between( $attrs, 'title="', '"' );

					$tab_active = plex_get_string_between( $attrs, 'active="', '"' );

					$unique_id = uniqid();

					$tab_ids[] = array(
						'id' => $unique_id
					);

					$this->tabs[] = array(
						'id'     => $unique_id,
						'title'  => $tab_title,
						'active' => $tab_active
					);
				}
			}
			return plex_create_short_codes( $matches, $tab_ids );
		}
		return '';
	}


}