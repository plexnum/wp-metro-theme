<?php
class Plex_ShortCode_IconMixin extends Plex_ShortCode_Icon {

	function __construct( $icon_attr = 'icon', $default_icon = 'icon-next' ) {
		parent::__construct( $icon_attr, $default_icon );
		$this->with_animation = false;
	}

}