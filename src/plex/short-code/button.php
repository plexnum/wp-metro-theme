<?php

class Plex_ShortCode_Button extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'block'       => 'bool',
		'size'        => 'html_class',
		'type'        => 'html_class',
		'label'       => 'text_field',
		'link'        => 'url',
		'in_new_page' => 'bool:1'
	);

	protected $sizes;

	protected $colors;

	protected $animation;

	function __construct() {

		parent::__construct();

		$cm = plex_get_theme_conf_bag();

		$this->sizes = $cm->get( 'els_sizes' );

		$this->types = array(
			'default' => __( 'Default', plex_get_trans_domain() ),
			'col2'    => __( 'Default 2', plex_get_trans_domain() ),
			'grey'    => __( 'Opacity button', plex_get_trans_domain() ),
			'success' => __( 'Success', plex_get_trans_domain() ),
			'warning' => __( 'Warning', plex_get_trans_domain() ),
			'info'    => __( 'Info', plex_get_trans_domain() ),
			'inverse' => __( 'Inverse', plex_get_trans_domain() ),
			'danger'  => __( 'Danger', plex_get_trans_domain() ),
		);

		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
	}

	function get_tag_name() {
		return 'button';
	}

	function handle( $atts, $content = null ) {

		$this->view->add( (array) $atts );

		// Set size
		if ( $this->view->get( 'size' ) ) {
			$this->view->set( 'size', 'btn_' . $this->view->get( 'size' ) );
		}

		// Set type
		if ( $this->view->get( 'type' ) ) {
			$this->view->set( 'type', 'btn_' . $this->view->get( 'type' ) );
		}

		// Set block
		if ( $this->view->get( 'block' ) && $this->view->get( 'block' ) != 'false' ) {
			$this->view->set( 'block_cls', 'btn_bloc' );
		}

		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );

		return $this->view->get_output( '/button/short-code.php' );
	}

	function form() {
		$this->view->set( 'animation', $this->animation->_form() );
		$this->view->set( 'sizes', $this->sizes );
		$this->view->set( 'types', $this->types );
		return $this->view->get_output( '/button/form.php' );
	}

	function preview_settings() {

		return array(
			'label' => __( 'Default', plex_get_trans_domain() )
		);

	}

}