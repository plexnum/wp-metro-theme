<?php

class Plex_ShortCode_Column extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'size' => 'grid_size'
	);

	function get_tag_name() {
		return 'column';
	}

	function handle( $atts, $content = null ) {
		Plex_Lib_Sanitizer::register( new Plex_ShortCode_Column_GridSizeSanitizeStrategy() );
		$this->view->add( (array) $atts );
		$this->view->set( 'content', force_balance_tags( plex_do_short_code( $content ) ) );
		return $this->view->get_output( '/column/short-code.php' );
	}

	function is_manageable() {
		return false;
	}

	function is_customizable() {
		return false;
	}

}