<?php

class Plex_ShortCode_Badge extends Plex_ShortCode_Label {

	protected $animation;

	function __construct() {
		parent::__construct();
		$this->title     = __( 'Badge settings', plex_get_trans_domain() );
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
	}

	function get_tag_name() {
		return 'badge';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		return $this->view->get_output( '/badge/short-code.php' );
	}

}