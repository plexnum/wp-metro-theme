<?php

class Plex_ShortCode_Icon extends Plex_Model_AbstractShortCode {

	protected $icon_attr = null;

	protected $default_icon = null;

	protected $sanitize_rules = array(
		'style_mod'    => 'attr',
		'size_mod'     => 'attr',
		'pos_mod'      => 'attr',
		'rotation_mod' => 'attr'
	);

	protected $animation;

	protected $with_animation = true;

	function __construct( $icon_attr = 'type', $default_icon = 'icon-next' ) {
		parent::__construct();
		$this->icon_attr                        = $icon_attr;
		$this->default_icon                     = $default_icon;
		$this->sanitize_rules[$this->icon_attr] = 'text_field';
		$this->animation                        = new Plex_ShortCode_ShowAnimateMixin();
	}


	function get_tag_name() {
		return 'icon';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );

		if ( $this->with_animation == true ) {
			$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		}
		else {
			$this->view->set( 'animation', new Plex_Config_ParamBag() );
		}

		if ( isset( $atts[$this->icon_attr] ) ) {

			$icon = $atts[$this->icon_attr];

			if ( strpos( $atts[$this->icon_attr], 'icon-' ) !== 0 ) {
				$icon = 'icon-' . $icon;
			}

			$this->view->set( 'icon', $icon );

		}

		return $this->view->get_output( '/icon/short-code.php' );
	}

	function form() {
		$config_manager = plex_get_theme_conf_bag();
		$this->view->set( 'icons', $config_manager->get( 'icons' ) );
		$this->view->set( 'icons_size_mod', $config_manager->get( 'icons_size_mod' ) );
		$this->view->set( 'icons_style_mod', $config_manager->get( 'icons_style_mod' ) );
		$this->view->set( 'icons_pos_mod', $config_manager->get( 'icons_position_mod' ) );
		$this->view->set( 'icons_rotation_mod', $config_manager->get( 'icons_rotation_mod' ) );
		$this->view->set( 'icon_attr', $this->icon_attr );
		$this->view->set( 'default_icon', $this->default_icon );

		if ( $this->with_animation == true ) {
			$this->view->set( 'animation', $this->animation->_form() );
		}

		return $this->view->get_output( '/icon/form.php' );
	}

	function preview_settings() {
		return array(
			$this->icon_attr => $this->default_icon
		);
	}

	function preview_layout( $content = '' ) {
		return parent::preview_layout( '<div style="text-align:center;">' . $content . '</div> ' );
	}


}