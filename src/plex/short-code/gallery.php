<?php

class Plex_ShortCode_Gallery extends Plex_Model_AbstractShortCode {

	private $names_mapping = array(
		'scroll__pauseonehover' => 'scroll__pauseOnHover',
		'auto__duration'        => 'auto__timeoutDuration'
	);

	private $sanitization_rules = array(
		'attrs::filter1' => array(
			'order'                 => 'in_list(ASC,DESC,RAND)',
			'link'                  => 'in_list(file,post)',
			'id'                    => 'abs_int',
			'size'                  => 'attr:post_thumbnail',
			'include'               => 'attr',
			'exclude'               => 'attr',
			'orderby'               => 'sql_order_by:post__in',
			'_orderbyRandom'        => 'bool',
			'circular'              => 'bool:1',
			'pagination'            => 'bool',
			'direction'             => 'in_list(left,
																					right,
																					up,
																					down):left',
			'items__visible'        => 'abs_int:1',
			'scroll__items'         => 'abs_int:1',
			'scroll__fx'            => 'in_list(none,
																					scroll,
																				  directscroll,
																				  fade,
																				  crossfade,
																				  cover,
																				  cover-fade,
																				  uncover,
																				  uncover-fade):scroll',
			'scroll__easing'        => 'text_field',
			'scroll__duration'      => 'abs_int:500',
			'scroll__pauseonehover' => 'bool:1',
			'auto__play'            => 'bool',
			'auto__duration'        => 'abs_int:2500',
			'auto__progress'        => 'bool',
			'buttons__enable'       => 'bool:1',
			'buttons__position'     => 'in_list(left-right-middle,
																					left-top,
																					left-bottom,
																					right-top,
																					right-bottom):left-right-middle',
			'ids::filter2'          => 'attr'
		)
	);

	function is_customizable() {
		return false;
	}

	function get_tag_name() {
		return 'gallery';
	}

	function handle( $atts, $content = null ) {

		$animation = new Plex_ShortCode_ShowAnimateMixin();
		$this->view->set( 'animation', $animation->_handle( $atts, $content ) );

		$post = get_post();

		$sanitizer = new Plex_Lib_Sanitizer( $this->sanitization_rules, array(
			'attrs' => $atts
		) );

		$sanitizer->add_filter(
			'filter1',
			array( $this, 'resolve_names' )
		);

		$sanitizer->add_filter(
			'filter1',
			array( $this, 'resolve_attributes' )
		);

		$result = $sanitizer->sanitize_output();
		$attrs  = new Plex_Config_ParamBag( $result['attrs'] );

		$this->resolve_size( $atts );

		$id = $attrs->get( 'id' );

		if ( empty( $id ) ) {
			$id = $post->ID;
		}


		if ( $attrs->get( 'ids' ) ) {
			$attrs->set( 'include', $attrs->get( 'ids' ) );
		}

		if ( $attrs->get( 'order' ) == 'RAND' ) {
			$attrs->set( 'orderby', 'none' );
		}

		if ( $attrs->get( 'include' ) ) {

			$_attachments = get_posts( array(
				'include'        => $attrs->get( 'include' ),
				'post_status'    => 'inherit',
				'post_type'      => 'attachment',
				'post_mime_type' => 'image',
				'order'          => $attrs->get( 'order' ),
				'orderby'        => $attrs->get( 'orderby' ) ) );

			$attachments = array();
			foreach ( $_attachments as $key => $val ) {
				$attachments[$val->ID] = $_attachments[$key];
			}

		}
		elseif ( $attrs->get( 'exclude' ) ) {
			$attachments = get_children(
				array( 'post_parent'    => $id,
							 'exclude'        => $attrs->get( 'exclude' ),
							 'post_status'    => 'inherit',
							 'post_type'      => 'attachment',
							 'post_mime_type' => 'image',
							 'order'          => $attrs->get( 'order' ),
							 'orderby'        => $attrs->get( 'orderby' ) )
			);
		}
		else {
			$attachments = get_children(
				array( 'post_parent'    => $id,
							 'post_status'    => 'inherit',
							 'post_type'      => 'attachment',
							 'post_mime_type' => 'image',
							 'order'          => $attrs->get( 'order' ),
							 'orderby'        => $attrs->get( 'orderby' ) )
			);
		}

		if ( empty( $attachments ) )
			return '';

		if ( is_feed() ) {
			$output = "\n";
			foreach ( $attachments as $att_id => $attachment ) {
				$output .= wp_get_attachment_link( $att_id, $attrs->get( 'size' ), true ) . "\n";
			}
			return $output;
		}

		$this->view->set( 'js_configs',
			esc_js(
				$attrs->get_json( array(
					'link',
					'order',
					'id',
					'size',
					'exclude',
					'include',
					'orderby',
					'ids',
					'_orderbyRandom',
				) )
			)
		);

		$attrs->set( 'rel', uniqid( 'plex' ) );

		$this->view->set( 'attrs', $attrs );
		$this->view->set( 'attachments', $attachments );

		return $this->view->get_output( "/gallery.php" );
	}

	/**
	 * @param $attrubutes Plex_Config_ParamBag
	 */
	function resolve_size( $attrubutes ) {
		$config_manager = plex_get_theme_conf_bag();
		if ( $config_manager->has( 'context_image_size' ) ) {
			$attrubutes->set( 'size', $config_manager->get( 'context_image_size' ) );
		}
	}

	function resolve_names( $attributes ) {
		foreach ( $attributes as $attr_name => $attr_value ) {
			if ( isset( $this->names_mapping[$attr_name] ) ) {
				unset( $attributes[$attr_name] );
				$attributes[$this->names_mapping[$attr_name]] = $attr_value;
			}
		}

		return $attributes;

	}

	function resolve_attributes( $attributes ) {

		foreach ( $attributes as $attr_name => $attr_value ) {

			if ( strpos( $attr_name, '__' ) !== false ) {

				$names_hierarchy = explode( '__', $attr_name );

				// set reference
				$reference =& $attributes;

				// set hierarchy and set reference to deepest element
				foreach ( $names_hierarchy as $name_node ) {
					if ( ! isset( $reference[$name_node] ) ) {
						$reference[$name_node] = array();
					}
					$reference =& $reference[$name_node];
				}

				// asign attribute value
				$reference = $attr_value;

				// clear for yourself
				unset( $reference );
				unset( $attributes[$attr_name] );
			}
		}

		return $attributes;
	}

}