<?php

class Plex_ShortCode_Row extends Plex_Model_AbstractShortCode {

	function get_tag_name() {
		return 'row';
	}

	/**
	 * That short code may include only other short codes
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return string
	 */
	function handle( $atts, $content = null ) {
		return "<div class='row-fluid'>" . plex_do_short_code( $content, true ) . "</div>";
	}

	function form() {
		return $this->view->get_output( '/row/form.php' );
	}

	function preview_layout( $content = '' ) {
		return parent::preview_layout( '<div class="ifremWraper">' . $content . '</div>' );
	}

	function form_sub_templates() {
		return array(
			'plexGridLayout'     => $this->view->get_output( '/row/_grid-layout.php' ),
			'plexGridLayoutItem' => $this->view->get_output( '/row/_grid-layout-item.php' )
		);
	}

	function preview_settings() {

		$grid_img_url = plex_fix_url() . '/grid';

		$config_manager = plex_get_theme_conf_bag();


		return array(

			'_content' => $config_manager->get('short_code_content'),

			'_preview_content' => $config_manager->get('preview_medium'),

			'_layouts' => array(

				array(
					'img'    => $grid_img_url . '/1.2-1.2.png',
					'layout' => array(
						'1/2',
						'1/2'
					)
				),

				array(
					'img' => $grid_img_url . '/1.2-1.4-1.4.png',
					'layout' => array(
						'1/2',
						'1/4',
						'1/4'
					)
				),

				array(
					'img' => $grid_img_url . '/1.4-1.4-1.2.png',
					'layout' => array(
						'1/4',
						'1/4',
						'1/2'
					)
				),

				array(
					'img'    => $grid_img_url . '/1.4-1.4-1.4-1.4.png',
					'layout' => array(
						'1/4',
						'1/4',
						'1/4',
						'1/4'
					)
				),

				array(
					'img'    => $grid_img_url . '/1.4-1.2-1.4.png',
					'layout' => array(
						'1/4',
						'1/2',
						'1/4'
					)
				),


				array(
					'img'    => $grid_img_url . '/1.4-3.4.png',
					'layout' => array(
						'1/4',
						'3/4'
					)
				),

				array(
					'img'    => $grid_img_url . '/3.4-1.4.png',
					'layout' => array(
						'3/4',
						'1/4',
					)
				),

				array(
					'img'    => $grid_img_url . '/1.3-1.3-1.3.png',
					'layout' => array(
						'1/3',
						'1/3',
						'1/3'
					)
				),


				array(
					'img'    => $grid_img_url . '/1.3-2.3.png',
					'layout' => array(
						'1/3',
						'2/3'
					)
				),


				array(
					'img'    => $grid_img_url . '/2.3-1.3.png',
					'layout' => array(
						'2/3',
						'1/3',
					)
				),

				array(
					'img'    => $grid_img_url . '/1.6-1.6-1.6-1.6-1.6-1.6.png',
					'layout' => array(
						'1/6',
						'1/6',
						'1/6',
						'1/6',
						'1/6',
						'1/6',
					)
				),

				array(
					'img'    => $grid_img_url . '/1.6-1.3-1.3-1.6.png',
					'layout' => array(
						'1/6',
						'1/3',
						'1/3',
						'1/6'
					)
				),


				array(
					'img'    => $grid_img_url . '/1.6-2.3-1.6.png',
					'layout' => array(
						'1/6',
						'2/3',
						'1/6'
					)
				),


				array(
					'img'    => $grid_img_url . '/1.2-1.6-1.6-1.6.png',
					'layout' => array(
						'1/2',
						'1/6',
						'1/6',
						'1/6'
					)
				),


				array(
					'img'    => $grid_img_url . '/1.6-1.6-1.6-1.2.png',
					'layout' => array(
						'1/6',
						'1/6',
						'1/6',
						'1/2'
					)
				),


				array(
					'img'    => $grid_img_url . '/1.3-1.6-1.6-1.3.png',
					'layout' => array(
						'1/3',
						'1/6',
						'1/6',
						'1/3'
					)
				),

				array(
					'img'    => $grid_img_url . '/1.6-1.6-1.3-1.6-1.6.png',
					'layout' => array(
						'1/6',
						'1/6',
						'1/3',
						'1/6',
						'1/6'
					)
				),



			)
		);

	}

}