<?php
class Plex_ShortCode_IconBox extends Plex_Model_AbstractShortCode {

	protected $positions = array();

	protected $sanitize_rules = array(
		'with_separator' => 'bool:1',
		'link'           => 'url',
		'position'       => 'in_list(right,left,top):right',
		'in_new_page'    => 'bool:1'
	);

	protected $icon_short_code = null;

	protected $animation = null;

	function get_tag_name() {
		return 'icon_box';
	}

	function __construct() {

		$this->positions = array(
			'right' => __( 'Right', plex_get_trans_domain() ),
			'top'   => __( 'Top', plex_get_trans_domain() ),
			'left'  => __( 'Left', plex_get_trans_domain() ),
		);

		$this->icon_short_code = new Plex_ShortCode_IconMixin();
		$this->animation       = new Plex_ShortCode_ShowAnimateMixin();

		parent::__construct();

	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		$this->view->set( 'icon', $this->icon_short_code->_handle( $atts, $content ) );
		$this->view->set( 'content', plex_do_short_code( $content ) );
		return $this->view->get_output( '/icon-box/short-code.php' );
	}

	function form() {
		$this->view->set( 'animation', $this->animation->_form() );
		$this->view->set( 'icon', $this->icon_short_code->_form() );
		$this->view->set( 'positions', $this->positions );
		return $this->view->get_output( '/icon-box/form.php' );
	}

	function preview_settings() {

		$settings = $this->icon_short_code->preview_settings();

		$cm = plex_get_theme_conf_bag();

		return array_merge( array(
			'content_preview' => $cm->get( 'preview_medium' ),
			'content'         => $cm->get( 'short_code_content' )
		), $settings );

	}

}