<?php

class Plex_ShortCode_Clients extends Plex_Model_AbstractShortCode {

	protected $animation;

	protected $sanitize_rules = array(
		'columns'    => 'abs_int:2',
		'max_items'  => 'abs_int:0',
		'with_links' => 'bool:1'
	);

	function __construct() {
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
		parent::__construct();
	}

	function get_tag_name() {
		return 'clients';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		return $this->view->get_output( '/clients/short-code.php' );
	}

	function form() {
		$this->view->set( 'animation', $this->animation->form() );
		return $this->view->get_output( '/clients/form.php' );
	}

	function preview_layout( $content = '' ) {
		return parent::preview_layout( '<div class="ifremWraper">' . $content . '</div>' );
	}

}