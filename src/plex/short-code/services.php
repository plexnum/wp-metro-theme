<?php

class Plex_ShortCode_Services extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'columns'          => 'abs_int:2',
		'max_items'        => 'abs_int:0',
		'with_links'       => 'bool:1',
		'with_charge_list' => 'bool:1'
	);

	protected $animation = null;

	function __construct() {
		parent::__construct();
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
	}


	function get_tag_name() {
		return 'services';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		return $this->view->get_output( '/services/short-code.php' );
	}

	function form() {
		$this->view->set( 'animation', $this->animation->_form() );
		return $this->view->get_output( '/services/form.php' );
	}

	function preview_layout( $content = '' ) {
		return parent::preview_layout( '<div class="ifremWraper">' . $content . '</div>' );
	}

}