<?php

class Plex_ShortCode_MarkedItem extends Plex_Model_AbstractShortCode {

	protected $sanitized_rules = array(
		'icon' => 'attr',
		'content' => 'html_data'
	);

	function get_tag_name() {
		return 'marked_item';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'content', $content );
		return $this->view->get_output( '/marked-item/short-code.php' );
	}

	function is_customizable() {
		return false;
	}

	function is_manageable() {
		return false;
	}


}