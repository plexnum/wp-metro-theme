<?php

class Plex_ShortCode_MarkedList extends Plex_Model_AbstractShortCode {

	protected $animation;

	function __construct() {
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
		parent::__construct();
	}


	function handle( $atts, $content = null ) {
		$animation = $this->animation->_handle( $atts, $content );
		return '<ul class="icons-ul ' . $animation->get( 'class' ) . '" ' . $animation->get( 'animation_type' ) . '>' . plex_do_short_code( $content, true ) . '</ul>';
	}

	function get_tag_name() {
		return 'marked_list';
	}

	function form() {
		$this->view->set( 'animation', $this->animation->_form() );
		return $this->view->get_output( '/marked-list/form.php' );
	}

	function form_sub_templates() {

		$cm = plex_get_theme_conf_bag();
		$this->view->set( 'icons', $cm->get( 'icons' ) );
		$this->view->set( 'default_icon', 'icon-chetbox_select' );

		return array(
			'plexAddFormWithIconView' => $this->view->get_output( '/_sub_templates/add-form-with-icon.php' ),
			'plexIconListView'        => $this->view->get_output( '/_sub_templates/icon-list.php' ),
			'plexIconInputView'       => $this->view->get_output( '/_sub_templates/icon-input.php' )
		);

	}

	function preview_settings() {

		$cm = plex_get_theme_conf_bag();

		return array(

			'_items' => array(
				array(
					'icon'  => 'icon-chetbox_select',
					'title' => __( 'One', plex_get_trans_domain() ),
				),
				array(
					'icon'  => 'icon-chetbox_select',
					'title' => __( 'Two', plex_get_trans_domain() ),
				),
			)
		);
	}


}