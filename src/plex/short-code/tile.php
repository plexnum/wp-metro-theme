<?php

class Plex_ShortCode_Tile extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'type' => 'in_list(tile_color1,tile_color2)'
	);

	protected $animation;

	protected $types;

	function __construct() {
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
		$this->types     = array(
			'tile_color1' => __( 'Type 1', plex_get_trans_domain() ),
			'tile_color2' => __( 'Type 2', plex_get_trans_domain() )
		);
		parent::__construct();
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'content', plex_do_short_code( $content ) );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		return $this->view->get_output( '/tile/short-code.php' );
	}

	function get_tag_name() {
		return 'tile';
	}

	function form() {
		$this->view->set( 'types', $this->types );
		$this->view->set( 'animation', $this->animation->_form() );
		return $this->view->get_output( '/tile/form.php' );
	}

	function preview_settings() {

		$cm = plex_get_theme_conf_bag();

		return array(
			'header'          => $cm->get( 'preview_author' ),
			'content'         => $cm->get( 'short_code_content' ),
			'content_preview' => $cm->get( 'preview_medium' )
		);

	}


}