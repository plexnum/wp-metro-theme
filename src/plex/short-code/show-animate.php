<?php

class Plex_ShortCode_ShowAnimate extends Plex_Model_AbstractShortCode {

	protected $types;

	protected $sanitize_rules = array(
		'animation_type' => 'attr:rollIn'
	);

	protected $show_setting_header = null;

	function __construct( $show_setting_header = true ) {
		$cm                        = plex_get_theme_conf_bag();
		$this->types               = $cm->get( 'animation_types' );
		$this->show_setting_header = $show_setting_header;
		parent::__construct();

	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'content', plex_do_short_code( $content ) );
		return $this->view->get_output( '/show-animate/short-code.php' );
	}

	function get_tag_name() {
		return 'show_animate';
	}

	function form() {
		$this->view->set( 'show_setting_header', $this->show_setting_header );
		$this->view->set( 'types', $this->types );
		return $this->view->get_output( '/show-animate/form.php' );
	}

	function preview_settings() {

		$cm = plex_get_theme_conf_bag();

		return array(
			'content'         => $cm->get( 'short_code_content' ),
			'content_preview' => $cm->get( 'preview_animate' )
		);
	}

}