<?php

class Plex_ShortCode_Testimonials extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'columns'          => 'abs_int:2',
		'max_items'        => 'abs_int:0',
		'show_as_carousel' => 'bool:1'
	);

	protected $icon;

	protected $animation;

	function __construct() {
		$this->icon      = new Plex_ShortCode_IconMixin( 'icon', 'icon-quotes-left' );
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
		parent::__construct();
	}

	function get_tag_name() {
		return 'testimonials';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'icon', $this->icon->_handle( $atts, $content ) );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		$this->icon->handle( $atts, $content );

		return $this->view->get_output( '/testimonials/short-code.php' );
	}

	function form() {
		$this->view->set( 'icon', $this->icon->_form() );
		$this->view->set( 'animation', $this->animation->_form() );
		return $this->view->get_output( '/testimonials/form.php' );
	}

	function preview_layout( $content = '' ) {
		return parent::preview_layout( '<div class="ifremWraper">' . $content . '</div>' );
	}

	function preview_settings() {
		return $this->icon->preview_settings();
	}

}