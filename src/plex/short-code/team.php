<?php

class Plex_ShortCode_Team extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'columns'           => 'abs_int:3',
		'max_items'         => 'abs_int:0',
		'show_image'        => 'bool:1',
		'show_position'     => 'bool:1',
		'show_desc'         => 'bool:1',
		'show_social_icons' => 'bool:1',
		'with_links'        => 'bool'
	);

	protected $animation;

	function __construct() {
		parent::__construct();
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
	}

	function get_tag_name() {
		return 'team';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		return $this->view->get_output( '/team/short-code.php' );
	}

	function form() {
		$this->view->set( 'animation', $this->animation->form() );
		return $this->view->get_output( '/team/form.php' );
	}

	function preview_layout( $content = '' ) {
		return parent::preview_layout( '<div class="ifremWraper">' . $content . '</div>' );
	}

}