<?php

class Plex_ShortCode_Accordion extends Plex_Model_AbstractShortCode {

	protected $animation;

	function __construct() {
		parent::__construct();
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
	}

	function get_tag_name() {
		return 'accordion';
	}

	function handle( $atts, $content = null ) {
		$animation = $this->animation->_handle( $atts, $content );

		// That short code only allowed to include another short codes
		return "<div class='accordion " . $animation->get( 'class' ) . "' " . $animation->get( 'animation_type' ) . " > " . plex_do_short_code( $content, true ) . "</div > ";
	}

	function form() {
		$this->view->set( 'animation', $this->animation->form() );
		return $this->view->get_output( '/accordion/form.php' );
	}

	function form_sub_templates() {
		return array(
			'plexRadioListTemplate' => $this->view->get_output( '/_sub_templates/radio-list.php' ),
			'plexAddFormView'       => $this->view->get_output( '/_sub_templates/add-form.php' ),
			'plexRadioInput'        => $this->view->get_output( '/_sub_templates/radio-input.php' )
		);
	}

	function preview_settings() {

		$cm = plex_get_theme_conf_bag();

		return array(

			'_preview_content' => $cm->get( 'preview_medium' ),

			'_content'         => $cm->get( 'short_code_content' ),

			'_items'           => array(
				array(
					'title'  => __( 'One', plex_get_trans_domain() ),
					'active' => false
				),
				array(
					'title'  => __( 'Two', plex_get_trans_domain() ),
					'active' => true
				)
			)
		);

	}

}