<?php

class Plex_ShortCode_Collapse extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'id'     => 'text_field',
		'active' => 'bool',
		'title'  => 'text_field'
	);

	function get_tag_name() {
		return 'collapse';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'content', plex_do_short_code( $content ) );
		$this->view->set( 'id', uniqid( 'plex' ) );
		return $this->view->get_output( '/collapse/short-code.php' );
	}

	function is_manageable() {
		return false;
	}

	function is_customizable() {
		return false;
	}

}