<?php

class Plex_ShortCode_Divider extends Plex_Model_AbstractShortCode {

	protected $animation;

	function __construct() {
		parent::__construct();
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
	}

	function get_tag_name() {
		return 'divider';
	}

	function handle( $atts, $content = null ) {
		$animation = $this->animation->_handle( $atts, $content );
		return '<hr class="' . $animation->get( 'class' ) . '" ' . $animation->get( 'animation_type' ) . ' > ';
	}

	function form() {
		return $this->animation->_form();
	}

}