<?php

class Plex_ShortCode_GoogleMap extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'address' => 'text_field',
		'zoom'    => 'abs_int:15'
	);

	protected $animation;

	function __construct() {
		parent::__construct();
		$this->animation = new Plex_ShortCode_ShowAnimateMixin();
	}

	function get_tag_name() {
		return 'google_map';
	}

	function handle( $atts, $content = null ) {

		$this->view->add( (array) $atts );
		$this->view->sanitize();
		$this->view->set( 'map_id', uniqid( 'Plex' ) );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );

		try {

			if ( $this->view->get( 'address' ) ) {


				$coordinates = $this->get_coordinates( $this->view->get( 'address' ) );
				$this->view->set( 'content', $content );
				$this->view->set( 'coordinates', $coordinates );
			}

		} catch ( Plex_Exception_Common $e ) {
			$this->view->set( 'err', $e->getMessage() );
		}

		return $this->view->get_output( '/google-map/short-code.php' );

	}

	function get_coordinates( $address ) {

		$address_hash = md5( $address );

		$coordinates = get_transient( $address_hash );

		if ( $coordinates === false ) {

			$url = add_query_arg( array(
					'address' => urlencode( $address ),
					'sensor'  => 'false'
				), 'http://maps.googleapis.com/maps/api/geocode/json'
			);

			$response = wp_remote_get( $url );

			if ( is_wp_error( $response ) ) {
				throw new Plex_Exception_Common( __( 'Unable to contact Google API service.', plex_get_trans_domain() ) );
			}

			$data = wp_remote_retrieve_body( $response );

			if ( is_wp_error( $data ) ) {
				throw new Plex_Exception_Common( __( 'Unable to contact Google API service.', plex_get_trans_domain() ) );
			}

			if ( $response['response']['code'] == 200 ) {

				$data = json_decode( $data );

				if ( $data->status === 'OK' ) {

					$coordinates = $data->results[0]->geometry->location;

					$cache_value['lat']     = $coordinates->lat;
					$cache_value['lng']     = $coordinates->lng;
					$cache_value['address'] = (string) $data->results[0]->formatted_address;

					// cache coordinates for 3 months
					set_transient( $address_hash, $cache_value, 3600 * 24 * 30 * 3 );
					$data = $cache_value;

				}
				elseif ( $data->status === 'ZERO_RESULTS' ) {
					throw new Plex_Exception_Common( __( 'No location found for the entered address.', plex_get_trans_domain() ) );
				}
				elseif ( $data->status === 'INVALID_REQUEST' ) {
					throw new Plex_Exception_Common( __( 'Invalid request. Did you enter an address?.', plex_get_trans_domain() ) );
				}
				else {
					throw new Plex_Exception_Common( __( 'Something went wrong while retrieving your map, please ensure you have entered the short code correctly.', plex_get_trans_domain() ) );
				}

			}
			else {
				throw new Plex_Exception_Common( __( 'Unable to contact Google API service.', plex_get_trans_domain() ) );
			}

		}
		else {
			// return cached results
			$data = $coordinates;
		}

		return $data;
	}

	function form() {
		$this->view->set( 'animation', $this->animation->_form() );
		return $this->view->get_output( '/google-map/form.php' );
	}

	function preview_settings() {
		return array(
			'address' => __( 'New York', plex_get_trans_domain() )
		);
	}

}