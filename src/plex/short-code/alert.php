<?php

class Plex_ShortCode_Alert extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'type'           => 'in_list(block,error,success,info)',
		'with_close_btn' => 'bool:1'
	);

	protected $types;

	protected $animation;

	function __construct() {

		parent::__construct();

		$this->animation = new Plex_ShortCode_ShowAnimateMixin();

		$this->types = array(
			'block'   => __( 'Block', plex_get_trans_domain() ),
			'error'   => __( 'Error', plex_get_trans_domain() ),
			'success' => __( 'Success', plex_get_trans_domain() ),
			'info'    => __( 'Info', plex_get_trans_domain() )
		);

	}

	function get_tag_name() {
		return 'alert';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		$this->view->set( 'animation', $this->animation->_handle( $atts, $content ) );
		$this->view->set( 'content', $content );
		return $this->view->get_output( '/alert/short-code.php' );
	}

	function form() {
		$this->view->set( 'animation', $this->animation->_form() );
		$this->view->set( 'types', $this->types );
		return $this->view->get_output( '/alert/form.php' );
	}

	function preview_settings() {

		$cm = plex_get_theme_conf_bag();

		return array(
			'content_preview' => $cm->get( 'preview_medium' ),
			'content'         => $cm->get( 'short_code_content' )
		);

	}

}