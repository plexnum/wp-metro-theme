<?php

class Plex_ShortCode_ShowAnimateMixin extends Plex_Model_AbstractShortCode {

	protected $sanitize_rules = array(
		'animation_type' => 'attr:rollIn',
		'use_animation'  => 'bool'
	);

	protected $types = null;

	protected $animation;

	function get_tag_name() {
		return null;
	}

	function __construct() {
		$cm              = plex_get_theme_conf_bag();
		$this->types     = $cm->get( 'animation_types' );
		$this->animation = new Plex_ShortCode_ShowAnimate( false );
		parent::__construct();
	}

	function handle( $atts, $content = null ) {

		$result = array();

		if ( isset( $atts['use_animation'] ) && $atts['use_animation'] != "false" ) {

			$animation_type = 'rollIn';

			if ( isset( $atts['animation_type'] ) ) {
				$animation_type = esc_html( $atts['animation_type'] );
			}

			$result = array(
				'class'          => 'js-animate',
				'animation_type' => 'data-type-animate="' . $animation_type . '"'
			);

		}

		return new Plex_Config_ParamBag( $result );

	}

	function form() {
		$this->view->set( 'animation', $this->animation->form() );
		return $this->view->get_output( '/show-animate-mixin/form.php' );
	}


}