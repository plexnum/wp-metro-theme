<?php

class Plex_ShortCode_Embed extends Plex_Model_AbstractShortCode {

	protected $animation;

	protected $sanitize_rules = array(
		'url' => 'url'
	);

	function __construct() {
		parent::__construct();
	}

	function get_tag_name() {
		return 'plex_embed';
	}

	function handle( $atts, $content = null ) {
		$this->view->add( (array) $atts );
		if($this->view->get('url')) {
			return wp_oembed_get(esc_url($this->view->get('url')));
		}
	}

	function form() {
		return $this->view->get_output( '/embed/form.php' );
	}

	function preview_settings() {
		return array(
			'url' => 'https://soundcloud.com/sugar-hill-records'
		);
	}

}