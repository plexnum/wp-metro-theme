<?php

class Plex_Component_Video_VideoSaveStrategyFactory {

	/**
	 * @param $type
	 * @param $data
	 *
	 * @return Plex_Component_Video_AbstractSaveStrategy
	 * @throws Plex_Exception_Common
	 */
	static function create( $type, $data ) {

		$video_type = ucfirst( str_replace( 'plex', '', $type ) );

		$class = 'Plex_Component_Video_'.$video_type.'SaveStrategy';

		if(class_exists($class)) {
			return new $class($data);
		}
		else {
			throw new Plex_Exception_Common(sprintf('Save strategy factory not founded for %s video type', $type));
		}

	}

}