<?php

abstract class Plex_Component_Video_AbstractSaveStrategy {

	private $data;

	function __construct( $data ) {
		$this->data = $data;
	}

	function save() {
		$save_strategy = new Plex_Component_Image_SaveStrategy($this->data);
		return $save_strategy->save();
	}

}