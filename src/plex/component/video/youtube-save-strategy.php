<?php

class Plex_Component_Video_YoutubeSaveStrategy extends Plex_Component_Video_AbstractSaveStrategy {

	function save() {

		$data = parent::save();

		try {
			// set youtube video id
			$data->set( 'video_id', plex_youtube_get_id_by_url( $data->get( 'url' ) ) );

			// if user not define image herself try to load from youtube server
			if ( ! $data->get( 'image' ) ) {
				$uploader = new Plex_Lib_YoutubeImageUploader( $data->get( 'url' ) );
				$data->add( (array)$uploader->upload() );
			}

		} catch ( Plex_Exception_Common $e ) {
			$data->set( 'error', $e->getMessage() );
		}

		return $data;

	}

}