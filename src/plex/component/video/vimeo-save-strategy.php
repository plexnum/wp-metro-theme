<?php

class Plex_Component_Video_VimeoSaveStrategy extends Plex_Component_Video_AbstractSaveStrategy {

	function save() {

		$data = parent::save();

		try {
			// Set video id
			$vimeo_data = plex_vimeo_get_info_by_url( $data->get( 'url' ) );
			$data->set( 'video_id', $vimeo_data->video_id );

			// If used defined image does not existed load it from Vimeo server
			if ( !$data->get( 'image' ) ) {

				$uploader = new Plex_Lib_VimeoImageUploader(
					$data->get( 'url' )
				);

				$uploaded_info = $uploader->upload();
				$data->add( $uploaded_info );

			}

		} catch ( Plex_Exception_Common $e ) {
			$data->set( 'error', $e->getMessage() );
		}

		return $data;

	}

}