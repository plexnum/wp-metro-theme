<?php

class Plex_Component_GalleryCustomizer extends Plex_Model_AbstractComponent implements Plex_Model_AdminableInterface {

	/**
	 * Component declaration
	 * That method define callbacks that listen to WordPress hooks
	 * @return mixed
	 */
	function declaration() {
		add_action( 'print_media_templates', array( $this, 'print_media_templates' ) );
	}

	/**
	 * Load templates
	 */
	function print_media_templates() {
		$view           = new Plex_View();
		$config_manager = plex_get_theme_conf_bag();

		$view->set( 'direction', array(
			'left'  => __( 'Left', plex_get_trans_domain() ),
			'right' => __( 'Right', plex_get_trans_domain() ),
		) );

		$view->set( 'easing', $config_manager->get( 'animation_fns', array() ) );

		$view->set( 'positions', array(
			'left-right-middle' => __( 'Left Right Middle', plex_get_trans_domain() ),
			'left-top'          => __( 'Left Top', plex_get_trans_domain() ),
			'left-bottom'       => __( 'Left Bottom', plex_get_trans_domain() ),
			'right-top'         => __( 'Right Top', plex_get_trans_domain() ),
			'right-bottom'      => __( 'Right Bottom', plex_get_trans_domain() )
		) );

		$view->set( 'fx', array(
			'none'         => __( 'None', plex_get_trans_domain() ),
			'scroll'       => __( 'Scroll', plex_get_trans_domain() ),
			'directscroll' => __( 'Directscroll', plex_get_trans_domain() ),
			'fade'         => __( 'Fade', plex_get_trans_domain() ),
			'crossfade'    => __( 'Crossfade', plex_get_trans_domain() ),
			'cover'        => __( 'Cover', plex_get_trans_domain() ),
			'cover-fade'   => __( 'Cover-fade', plex_get_trans_domain() ),
			'uncover'      => __( 'Uncover', plex_get_trans_domain() ),
			'uncover-fade' => __( 'Uncover-fade', plex_get_trans_domain() )
		) );

		$animation = new Plex_ShortCode_ShowAnimateMixin();
		$view->set( 'animation', $animation->_form() );

		$view->display( '/view/admin/media-templates.php' );
	}

	function enquire_scripts() {
		wp_enqueue_script( 'plex_admin_gallery_customizer' );
	}

}