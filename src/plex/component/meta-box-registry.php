<?php

class Plex_Component_MetaBoxRegistry extends Plex_Model_AbstractComponent implements Plex_Model_AdminableInterface {

	private $config_manager;

	private $meta_boxes;

	function __construct() {
		$this->config_manager = plex_get_theme_conf_bag();
	}

	/**
	 * @return mixed|void
	 * @throws Plex_Exception_Common
	 */
	function declaration() {

		try {
			$this->meta_boxes = plex_include_file( $this->config_manager->get( 'patches[meta_boxes_declaration_file]', false, true ) );
		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

		add_action( 'add_meta_boxes', array( $this, 'register_meta_boxes' ) );
	}

	function register_meta_boxes() {

		foreach ( $this->meta_boxes as $meta_box ) {

			$screens = $meta_box->screen();

			if ( ! is_array( $screens ) ) {
				$screens = array( $screens );
			}

			foreach ( $screens as $screen ) {
				add_meta_box( $meta_box->get_id(),
					$meta_box->get_title(),
					array( $meta_box, '_show' ),
					$screen,
					$meta_box->context(),
					$meta_box->priority()
				);
			}

		}

	}
}