<?php

class Plex_Component_ThemeCustomizer extends Plex_Model_AbstractComponent {

	/**
	 * @var WP_Customize_Manager
	 */
	private $manager = null;

	private $config_manager;

	private $preview_css = null;


	function __construct() {
		$this->config_manager = plex_get_theme_conf_bag();
	}

	function declaration() {
		// Register before save action hook
		add_action( 'customize_save', array( $this, 'customize_save' ) );
		// Register settings, sections and controls for customizer manager
		add_action( 'customize_register', array( $this, 'register' ) );
		add_action( 'customize_controls_print_footer_scripts', array( $this, 'print_scripts' ) );
		add_action( 'customize_preview_init', array( $this, 'apply_css_to_preview' ) );
	}

	function enquire_css_to_preview() {
		wp_add_inline_style( 'plex_style_settings', $this->preview_css );
	}

	function apply_css_to_preview( $wp_customizer ) {
		$this->preview_css = $this->resolve_settings_to_css( $wp_customizer );
		add_action( 'wp_enqueue_scripts', array( $this, 'enquire_css_to_preview' ), 999 );
	}

	/**
	 * @param $wp_customizer WP_Customize_Manager
	 */
	function customize_save( $wp_customizer ) {
		$resolved = $this->resolve_settings_to_css( $wp_customizer );
		set_theme_mod( 'style_settings', $resolved );
	}

	/**
	 * @param $wp_customize
	 *
	 * @throws InvalidArgumentException
	 * @throws Plex_Exception_Common
	 */
	function register( $wp_customize ) {
		$this->manager = $wp_customize;

		// Load section hierarchy
		try {
			$sections = plex_include_file( $this->config_manager->get( 'patches[theme_customizer_declaration_file]', false, true ) );
		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

		foreach ( $sections as $section ) {

			$options = array();

			if ( isset( $section['options'] ) ) {
				$options = $section['options'];
				unset( $section['options'] );
			}

			// Create section
			$section = new Plex_Component_ThemeCustomizer_Section(
				$section
			);

			if ( ! $section->get_id() ) {
				throw new Plex_Exception_Common( 'Section id not defined for theme customizer option' );
			}

			// Add section to manager
			if ( ! $this->manager->get_section( $section->get_id() ) ) {
				$this->manager->add_section(
					$section->get_id(),
					$section->get_configs()->all()
				);
			}
			else {
				$section_instance        = $this->manager->get_section( $section->get_id() );
				$section_instance->title = $section->get_title();

				if ( is_array( $section->get_delete() ) ) {

					foreach ( $section->get_delete() as $delete ) {

						if ( $this->manager->get_setting( $delete ) ) {
							$this->manager->remove_setting( $delete );
						}

						if ( $this->manager->get_control( $delete ) ) {
							$this->manager->remove_control( $delete );
						}

					}

				}

			}

			if ( is_array( $options ) ) {

				foreach ( $options as $option ) {

					// Create option
					$option = new Plex_Component_ThemeCustomizer_Option(
						$option,
						$this->manager
					);

					$setting = $option->build_setting();

					$this->manager->add_setting( $setting );

					$control = $option->get_control();

					// resolve control from contol builder
					if ( $control instanceof Plex_Component_ThemeCustomizer_ControlBuilder ) {
						$control = $control->build( $this->manager, $option->get_id() );
					}

					// For Plex_Component_ThemeCustomizer_Control set view with sanitize rules
					if ( $control instanceof Plex_Component_ThemeCustomizer_Control ) {


						$sanitizer = new Plex_Lib_Sanitizer(
							$option->get_output_sanitize_rules(), array()
						);

						$view = new Plex_View();
						$view->set_sanitizer( $sanitizer );

						$control->set_view( $view );

					}

					// Provide settings for control
					if ( $control instanceof WP_Customize_Control ) {

						$control->section = $section->get_id();

						// Set settings which control depend to
						$control->settings = array(
							'default' => $this->manager->get_setting( $option->get_id() )
						);

						$this->manager->add_control( $control );
					}
					else {

						$control = array_merge( $control,
							array(
								'section'  => $section->get_id(),
								'settings' => $option->get_id()
							)
						);

						// Add control to theme customizer
						$this->manager->add_control( $option->get_id(), $control );
					}
				}
			}
		}
	}

	function print_scripts() {

		$resolver = new Plex_Lib_CssFileResolver(
			array(),
				$this->config_manager->get( 'patches[assets][css]', '', true ) . '/style-settings.css',
			true
		);

		$resolved = $resolver->resolve();

		echo plex_script_wrapper(
			'var plexDefaultStyleSettings = ' . json_encode( $resolver->get_default_settings() ) . ';',
			'plexDefaultStyleSettings'
		);

		echo plex_script_template_wrapper(
			$resolved,
			'plexStyleSettings'
		);

	}

	function resolve_settings_to_css( $wp_customizer ) {

		$settings        = $wp_customizer->settings();
		$settings_values = array();
		foreach ( $settings as $setting_key => $setting ) {

			$setting_value = $setting->post_value();

			$settings_values[$setting_key] = $setting_value;

			if ( $setting instanceof Plex_Component_ThemeCustomizer_Model_AbstractSetting ) {

				$resolver = $setting->get_resolver();

				if ( $resolver instanceof Plex_Component_ThemeCustomizer_Model_AbstractResolver ) {
					$resolver->set_theme_setting( $setting );
					$resolver->set_settings( $settings_values );
					$resolver->set_key( $setting_key );
					$settings_values[$setting_key] = $resolver->resolve();
				}

			}
		}

		$resolver = new Plex_Lib_CssFileResolver(
			$settings_values,
				$this->config_manager->get( 'patches[assets][css]', '', true ) . '/style-settings.css'
		);

		return $resolver->resolve();

	}


}