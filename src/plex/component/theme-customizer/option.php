<?php

class Plex_Component_ThemeCustomizer_Option implements Plex_Model_SanitizableInterface {

	protected $manager;

	protected $defaults = array(
		'id'                    => null,
		'control'               => null,
		'setting'               => null,
		'sanitize_rules'        => null,
		'input_sanitize_rules'  => null,
		'output_sanitize_rules' => null,
		'sanitize_strategy'     => null
	);

	protected $configs = array();

	function __construct( $configs, $manager ) {

		$this->configs = new Plex_Config_ParamBag(
			array_merge( $this->defaults, $configs )
		);
		$this->manager = $manager;

	}

	function get_id() {
		return $this->configs->get( 'id' );
	}

	function get_configs() {
		return $this->configs;
	}

	function get_setting() {
		return $this->configs->get( 'setting', array() );
	}

	function build_setting() {

		if($this->is_multiple_value()) {

			return new Plex_Component_ThemeCustomizer_MultipleSetting(
				$this->manager,
				$this->get_id(),
				array_merge((array)$this->get_setting(), array(
					'sanitize_callback'    => array( $this, 'input_sanitize_callback' ),
					'sanitize_js_callback' => array( $this, 'js_sanitize_callback' )
				))
			);

		}

		$setting = $this->get_setting();

		if( $sanitize_strategy = $this->get_sanitize_strategy() ) {

			$setting = array_merge(
				(array) $setting,
				array(
					'sanitize_callback'    => array( $sanitize_strategy, 'sanitize_input' ),
					'sanitize_js_callback' => array( $sanitize_strategy, 'sanitize_output' )
				)
			);

		}

		return new Plex_Component_ThemeCustomizer_Setting(
			$this->manager,
			$this->get_id(),
			$setting
		);

	}

	function get_control() {
		return $this->configs->get( 'control' );
	}

	function get_sanitize_strategy() {
		return $this->configs->get( 'sanitize_strategy' );
	}

	function get_output_sanitize_rules() {
		if ( ! $this->configs->get( 'output_sanitize_rules' ) ) {
			return $this->configs->get( 'sanitize_rules', array() );
		}

		return $this->configs->get( 'output_sanitize_rules' );
	}

	function get_input_sanitize_rules() {

		if ( ! $this->configs->get( 'input_sanitize_rules' ) ) {
			return $this->configs->get( 'sanitize_rules', array() );
		}

		return $this->configs->get( 'input_sanitize_rules' );
	}

	function input_sanitize_callback( $data ) {
		$data      = (array) json_decode( $data );
		$sanitizer = new Plex_Lib_Sanitizer( $this->get_input_sanitize_rules(), $data );
		apply_filters($this->get_id().'_input_extend_sanitizer', $sanitizer);
		return $sanitizer->sanitize_input();
	}

	function js_sanitize_callback( $data ) {
		$sanitizer = new Plex_Lib_Sanitizer( $this->get_output_sanitize_rules(), (array) $data );
		apply_filters($this->get_id().'_output_extend_sanitizer', $sanitizer);
		return esc_js( json_encode( $sanitizer->sanitize_output() ) );
	}

	/**
	 * If sanitize rules defined that means setting is multiple
	 * @return bool
	 */
	function is_multiple_value() {
		if ( $this->configs->get( 'sanitize_rules' ) ||
				( $this->configs->get( 'input_sanitize_rules' ) &&
						$this->configs->get( 'output_sanitize_rules' ) )
		) {
			return true;
		}

		return false;

	}


}