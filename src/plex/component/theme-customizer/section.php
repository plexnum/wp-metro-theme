<?php

class Plex_Component_ThemeCustomizer_Section {

	protected $defaults = array(
		'id'            => null,
		'title'         => null,
		'description'   => null,
		'priority'      => 10,
		'capability'    => 'edit_theme_options',
		'theme_support' => '',
		'delete'				=> array()
	);

	protected $configs = array();

	protected $delete = array();

	function __construct( $configs ) {

		$this->configs = new Plex_Config_ParamBag(
			array_merge( $this->defaults, $configs )
		);


		$this->delete = $this->configs->get('delete');
		$this->configs->remove('delete');

	}

	function get_configs() {
		return $this->configs;
	}

	function get_id() {
		return $this->configs->get('id');
	}

	function get_title() {
		return $this->configs->get('title');
	}

	function get_delete() {
		return $this->delete;
	}

}