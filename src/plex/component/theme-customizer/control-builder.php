<?php

class Plex_Component_ThemeCustomizer_ControlBuilder {

	private $control_class;

	private $options;

	function __construct($control_class, $options) {
		$this->control_class  = $control_class;
		$this->options				= $options;
	}

	function build($manager, $id) {
		$class = $this->control_class;
		return new $class($manager, $id, $this->options);
	}

}