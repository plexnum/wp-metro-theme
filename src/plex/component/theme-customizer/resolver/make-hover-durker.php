<?php

class Plex_Component_ThemeCustomizer_Resolver_MakeHoverDurker extends Plex_Component_ThemeCustomizer_Model_AbstractResolver {

	function resolve() {

		$setting = $this->get_setting();
		$val     = $setting->post_value();

		$rgb = plex_rgb_difference(
			plex_convert_to_rgb( $val ),
			plex_convert_to_rgb( '111111' )
		);

		$this->settings[$setting->get_id() . '__hover'] = 'rgb(' . $rgb['r'] . ',' . $rgb['g'] . ',' . $rgb['b'] . ')';
		return $setting->post_value();

	}

}