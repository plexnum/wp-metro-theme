<?php

class Plex_Component_ThemeCustomizer_Resolver_SubLayerOpacityResolver extends Plex_Component_ThemeCustomizer_Model_AbstractResolver {

	function resolve() {

		$setting = $this->get_setting();
		$val     = (int) $setting->post_value();

		$this->settings['plex_site_sublayer_bgc_op__ie']  = 'filter: alpha(opacity=' . $val . ')';
		$this->settings['plex_site_sublayer_bgc_op__ie1'] = '"progid:DXImageTransform.Microsoft.Alpha(Opacity=' . $val . ')"';
		$this->settings['plex_site_sublayer_bgc_op__ie2'] = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' . $val . ')';

		return $val / 100;

	}

}