<?php

class Plex_Component_ThemeCustomizer_Resolver_FontsResolver extends Plex_Component_ThemeCustomizer_Model_AbstractResolver {

	function resolve() {
		$setting = $this->get_setting();

		$font = str_replace( '+', ' ', $setting->post_value() );

		if ( preg_match(
			'/(?:(.*?):(\d*)([^"]*))|(.*)/',
			$font, $matches )
		) {

			if ( $matches[4] ) {
				return $matches[4];
			}

			if ( $matches[2] ) {
				$this->settings[$setting->get_id() . '_' . 'weight'] = $matches[2];
			}

			if ( $matches[3] ) {
				$this->settings[$setting->get_id() . '_' . 'style'] = $matches[3];
			}

			return $matches[1];

		}

		return $setting->get_default();

	}

}