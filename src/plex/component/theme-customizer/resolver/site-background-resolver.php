<?php

class Plex_Component_ThemeCustomizer_Resolver_SiteBackgroundResolver extends Plex_Component_ThemeCustomizer_Model_AbstractResolver {

	function resolve() {
		$setting = $this->get_setting();
		$val     = $setting->post_value();

		$rgb     = plex_convert_to_rgb( $val );

		$this->settings[$setting->get_id() . '__rad']    = 'radial-gradient(circle closest-corner, rgba(' . $rgb['r'] . ', ' . $rgb['g'] . ', ' . $rgb['b'] . ', 0.4), ' . $val . ')';
		$this->settings[$setting->get_id() . '__webkit'] = '-webkit-' . $this->settings[$setting->get_id() . '__rad'];
		$this->settings[$setting->get_id() . '__moz']    = '-moz-' . $this->settings[$setting->get_id() . '__rad'];
		$this->settings[$setting->get_id() . '__ms']     = '-ms-' . $this->settings[$setting->get_id() . '__rad'];
		$this->settings[$setting->get_id() . '__o']      = '-o-' . $this->settings[$setting->get_id() . '__rad'];

		return $val;
	}

}