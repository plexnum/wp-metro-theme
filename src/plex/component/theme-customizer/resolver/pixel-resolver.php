<?php

class Plex_Component_ThemeCustomizer_Resolver_PixelResolver extends Plex_Component_ThemeCustomizer_Model_AbstractResolver {

	function resolve() {
		$setting = $this->get_setting();
		return $setting->post_value() . 'px';
	}

}