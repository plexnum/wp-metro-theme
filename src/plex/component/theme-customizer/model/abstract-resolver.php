<?php

abstract class Plex_Component_ThemeCustomizer_Model_AbstractResolver {

	protected $setting;

	protected $settings;

	protected $key;

	function set_theme_setting($setting) {
		$this->setting = $setting;
	}

	function set_settings(&$settings) {
		$this->settings =& $settings;
	}

	function set_key($key) {
		$this->key = $key;
	}

	/**
	 * @return WP_Customize_Setting
	 */
	function get_setting() {
		return $this->setting;
	}

	function get_theme_settings() {
		return $this->settings;
	}

	function get_key() {
		return $this->key;
	}

	abstract function resolve();

}