<?php

abstract class Plex_Component_ThemeCustomizer_Model_AbstractSetting extends WP_Customize_Setting {

	public $resolver = null;

	public $args = array();

	public function __construct( $manager, $id, $args = array() ) {

		if ( isset( $args['resolver'] ) ) {
			$this->resolver = $args['resolver'];
		}

		$this->args = $args;

		parent::__construct( $manager, $id, $args );

	}

	function get_arguments() {
		return new Plex_Config_ParamBag($this->args, true);
	}

	function get_argument( $name, $default ) {

		if ( isset( $this->args[$name] ) ) {
			return $this->args[$name];
		}

		return $default;
	}

	function set_argument( $name, $value ) {
		$this->args[$name] = $value;
	}

	function get_resolver() {
		return $this->resolver;
	}

	function set_resolver( $resolver ) {
		$this->resolver = $resolver;
	}

	function get_default() {
		return $this->default;
	}

	function get_id() {
		return $this->id;
	}

}