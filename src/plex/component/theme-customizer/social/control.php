<?php

class Plex_Component_ThemeCustomizer_Social_Control extends Plex_Component_ThemeCustomizer_Control {

	function render_content() {

		$path_prefix = $this->view->get_patch_prefix();

		$this->view->set_patch_prefix( '/view/meta-box' );

		$show_strategy = new Plex_Component_Social_ShowStrategy(
			$this->view,
			(array)$this->value(),
			false
		);

		$this->view->set( 'meta_box', $show_strategy->show() );
		$this->view->set_patch_prefix( $path_prefix );
		$this->view->display('/social/form.php');

	}


}