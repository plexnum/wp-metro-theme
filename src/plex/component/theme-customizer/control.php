<?php

class Plex_Component_ThemeCustomizer_Control extends WP_Customize_Control {

	/**
	 * @var Plex_View
	 */
	protected $view;

	/**
	 * @param $view Plex_View
	 */
	function set_view( $view ) {
		$this->view = $view;
		$this->view->set_patch_prefix('/view/theme-customizer');
		$this->view->set( 'id', $this->id );
		$this->view->set( 'link', $this->get_link() );
	}

}