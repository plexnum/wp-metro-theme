<?php

class Plex_Component_ThemeCustomizer_MultipleSetting extends Plex_Component_ThemeCustomizer_Model_AbstractSetting {

	public function sanitize( $value ) {
		return apply_filters( "customize_sanitize_{$this->id}", $value, $this );
	}

}