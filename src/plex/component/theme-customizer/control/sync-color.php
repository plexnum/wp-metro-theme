<?php

class Plex_Component_ThemeCustomizer_Control_SyncColor extends WP_Customize_Color_Control {

	protected $header;

	public function render_content() {
		$this_default = $this->setting->default;
		$default_attr = '';
		if ( $this_default ) {
			if ( false === strpos( $this_default, '#' ) )
				$this_default = '#' . $this_default;
			$default_attr = ' data-default-color="' . esc_attr( $this_default ) . '"';
		}
		// The input's value gets set by JS. Don't fill it.
		?>

		<?php if ( $this->header == true ): ?>
			<h3><?php echo $this->header; ?></h3>
			<hr>
		<?php endif; ?>

		<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>

			<div class="customize-control-content">
				<input class="color-picker-hex" type="text" maxlength="7" placeholder="<?php esc_attr_e( 'Hex Value' ); ?>"<?php echo $default_attr; ?> <?php $this->link(); ?> />
			</div>
		</label>
	<?php
	}

}