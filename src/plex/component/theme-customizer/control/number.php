<?php

class Plex_Component_ThemeCustomizer_Control_Number extends WP_Customize_Control {

	protected $min = 0;

	protected $max = 1000;

	protected function render_content() {
		?>
		<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<input type="number" max="<?php echo (int)$this->max; ?>" min="<?php echo (int)$this->min; ?>" value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />
		</label>
	<?php
	}


}