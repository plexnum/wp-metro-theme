<?php

class Plex_Component_ThemeCustomizer_Control_ColorScheme extends Plex_Component_ThemeCustomizer_Control {

	protected $schemes = array();

	protected function render_content() {

		/* @var $setting Plex_Component_ThemeCustomizer_Model_AbstractSetting */
		$setting   = $this->settings['default'];
		$arguments = $setting->get_arguments();

		$this->schemes = $arguments->get( 'schemes', array() );

		foreach ( $this->schemes as $key => $scheme ) {
			$this->choices[$key] = array(
				'name'  => $scheme['name'],
				'color' => $scheme['color']
			);
		}

		add_action( 'customize_controls_print_footer_scripts', array(
			$this,
			'enquire_color_schemes_settings'
		) );

		$this->view->set( 'label', $this->label );
		$this->view->set( 'value', $this->value() );
		$this->view->set( 'choices', $this->choices );
		$this->view->display( '/schemes/form.php' );

		//parent::render_content();

	}

	function enquire_color_schemes_settings() {
		echo plex_script_wrapper(
			'var plexColorSchemesSettings = ' . json_encode( $this->schemes ),
			'plexColorSchemesSettings'
		);
	}


}