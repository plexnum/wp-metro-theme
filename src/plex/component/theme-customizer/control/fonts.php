<?php

class Plex_Component_ThemeCustomizer_Control_Fonts extends WP_Customize_Control {

	protected $header = false;

	protected function render_content() {
		?>

		<?php if ( $this->header == true ): ?>
			<h3><?php echo $this->header; ?></h3>
			<hr>
		<?php endif; ?>

		<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<input type="text" class="-font-selector" value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />
		</label>
	<?php

	}


}