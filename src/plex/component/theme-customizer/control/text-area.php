<?php

class Plex_Component_ThemeCustomizer_Control_TextArea extends WP_Customize_Control {

	protected function render_content() {
		?>
		<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<textarea class="widefat" <?php $this->link(); ?>><?php wp_kses_data($this->value()); ?></textarea>
		</label>
	<?php

	}


}