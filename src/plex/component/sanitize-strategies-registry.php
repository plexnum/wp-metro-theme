<?php

class Plex_Component_SanitizeStrategiesRegistry extends Plex_Model_AbstractComponent {

	private $config_manager;

	function __construct() {
		$this->config_manager = plex_get_theme_conf_bag();
	}

	/**
	 * Component declaration
	 * That method define callbacks that listen to WordPress hooks
	 * @return mixed
	 */
	function declaration() {
		$this->register();
	}

	/**
	 * Register sanitize strategies
	 * @throws Plex_Exception_Common
	 */
	function register() {

		try {
			$strategies = plex_include_file( $this->config_manager->get( 'patches[sanitize_strategies_declaration_file]', false, true ) );

			if ( is_array( $strategies ) ) {
				Plex_Lib_Sanitizer::multiple_register($strategies);
			}

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

	}

}