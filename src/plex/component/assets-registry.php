<?php

class Plex_Component_AssetsRegistry extends Plex_Model_AbstractComponent {

	private $enquires_types = array();

	/**
	 * @return mixed|void
	 * @throws Plex_Exception_Common
	 */
	function declaration() {

		$config_manager = plex_get_theme_conf_bag();

		try {

			$assets = plex_include_file(
				$config_manager->get( 'patches[assets_declaration_file]', false, true )
			);

			$manager = new Plex_Component_AssetsRegistry_Manager( $assets );

			$manager->addRegister( new Plex_Component_AssetsRegistry_ScriptRegistry() );
			$manager->addRegister( new Plex_Component_AssetsRegistry_StyleRegistry() );
			$manager->addEnquire( new Plex_Component_AssetsRegistry_ScriptEnquire() );
			$manager->addEnquire( new Plex_Component_AssetsRegistry_StyleEnquire() );

			add_action('wp_enqueue_scripts', array($manager, 'register'));
			add_action('wp_enqueue_scripts', array($manager, 'enquire'));

			if(is_admin()) {
				add_action('admin_enqueue_scripts', array($manager, 'register'));
				add_action('admin_enqueue_scripts', array($manager, 'enquire'));
				add_action('customize_controls_enqueue_scripts', array($manager, 'register'));
				add_action('customize_controls_enqueue_scripts', array($manager, 'enquire'));
			}

			add_action('customize_preview_init', array($manager, 'register'));
			add_action('customize_preview_init', array($manager, 'enquire'));

			add_action('plex_short_code_preview_enquire_scripts', array($manager, 'register'));
			add_action('plex_short_code_preview_enquire_scripts', array($manager, 'enquire'));

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

	}

	function add_enquires( $asset_name, $asset_type, $asset_options ) {

		$enquires_in = $asset_options->get( 'enquires_in', false );

		if ( $enquires_in ) {

			if ( ! is_array( $enquires_in ) ) {
				$enquires_in = array( $enquires_in );
			}

			foreach ( $enquires_in as $enquire_in ) {

				if ( ! isset( $this->enquires_types[$asset_type][$enquire_in] ) ) {
					$this->enquires_types[$asset_type][$enquire_in] = array();
				}

				array_push(
					$this->enquires_types[$asset_type][$enquire_in],
					$asset_name
				);

			}

		}

	}

	static function create_initializer( $assert_type ) {

		$assert_type = ucfirst( $assert_type );

		$class = 'Plex_Component_AssetsRegistry_' . $assert_type . 'Initializer';

		if ( class_exists( $class ) ) {
			return new $class();
		}
		else {
			throw new Plex_Exception_Common( sprintf( 'Asset initializer not founded for %s asset type', $type ) );
		}

	}

}