<?php

class Plex_Component_ThemeResolver extends Plex_Model_AbstractComponent {
	/**
	 * Component declaration
	 * That method define callbacks that listen to WordPress hooks
	 * @return mixed
	 */
	function declaration() {
		add_filter( 'comment_form_default_fields', array( $this, 'resolve_comment_form_fields' ) );
		add_filter( 'comment_form_defaults', array( $this, 'resolve_comment_form_defaults' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'resolve_comment_reply_script' ) );
		add_filter( 'next_post_link', array( $this, 'resolve_next_post_link' ), 10, 4 );
		add_filter( 'previous_post_link', array( $this, 'resolve_prev_post_link' ), 10, 4 );
		add_filter( 'embed_oembed_html', array( $this, 'resolve_oembed_frame' ), 99, 4 );
		add_filter( 'oembed_result', array( $this, 'resolve_oembed_frame' ), 99, 4 );
		add_theme_support( 'automatic-feed-links' );
		add_action( 'wp_enqueue_scripts', array( $this, 'add_theme_style_settings' ), 990 );
		add_action( 'wp_enqueue_scripts', array( $this, 'add_media_element' ), 990 );
		add_filter( 'wp_video_shortcode', array( $this, 'make_video_responsive' ) );
		add_filter( 'wp_audio_shortcode', array( $this, 'add_animation_to_audio' ) );
		$this->set_content_width();

	}

	function set_content_width() {

		global $content_width;

		if ( ! isset( $content_width ) ) {
			$content_width = 1200;
		}

	}

	function add_animation_to_audio( $html ) {
		return '<div class="js-animate">' . $html . '</div>';
	}

	function make_video_responsive( $html ) {
		return '<div class="js-animate">' . str_replace( '<video', '<video style="width: 100%; height: 100%;"', $html ) . '</div>';
	}

	function add_media_element() {

		$plex_cm = plex_get_theme_conf_bag();

		if ( ! wp_script_is( 'wp-mediaelement', 'registered' ) ) {

			$plex_asset_js = $plex_cm->get( 'urls[assets][js]', '', true );

			wp_register_script( 'plex_jquery_media-element', $plex_asset_js . '/plugin/mediaelementjs/build/mediaelement-and-player.min.js', array(
				'jquery'
			) );

			wp_enqueue_script( 'plex_jquery_media-element_init', $plex_asset_js . '/media-element.js', array(
				'plex_jquery_media-element',
				'jquery'
			), false, true );

			wp_register_style( 'plex_media-element', $plex_asset_js . '/plugin/mediaelementjs/build/mediaelementplayer.min.css', array(
				'plex_style'
			) );

			wp_enqueue_style( 'plex_media-element-styles', $plex_asset_js . '/plugin/mediaelementjs/worpres-defalt-mejs.css', array(
				'plex_media-element'
			) );


		}
		else {
			wp_enqueue_script( 'wp-mediaelement' );
			wp_enqueue_style( 'wp-mediaelement' );
		}

	}

	function add_theme_style_settings() {
		wp_add_inline_style( 'plex_style_settings', get_theme_mod( 'style_settings' ) );
	}

	function resolve_oembed_frame( $html, $url, $attr = array() ) {

		$attrs = array();

		if ( strpos( $url, 'soundcloud' ) !== false ) {
			$attrs[] = 'data-not-height="true"';
		}

		// if content cached
		if ( strpos( $html, 'proportion-ifrem' ) !== false ) {
			return $html;
		}

		return '<div class="proportion-ifrem js-proportion-ifrem js-animate" ' . join( ' ', $attrs ) . '>' . $html . '</div>';
	}

	function resolve_next_post_link( $output, $format, $link, $post ) {

		if ( empty( $output ) ) {
			return $output;
		}

		return '<a href="' . esc_url( get_permalink( $post ) ) . '" class="icon_in-bubble icon-next icon_lage" rel="next"></a>';
	}

	function resolve_prev_post_link( $output, $format, $link, $post ) {

		if ( empty( $output ) ) {
			return $output;
		}

		return '<a href="' . esc_url( get_permalink( $post ) ) . '" class="icon_in-bubble icon-prev icon_lage" rel="prev"></a>';
	}

	function resolve_comment_reply_script() {
		if ( is_singular() ) {
			wp_enqueue_script( "comment-reply" );
		}
	}

	function resolve_comment_form_defaults( $defaults ) {
		if ( is_array( $defaults ) ) {
			$defaults['comment_field']        = '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="4" aria-required="true"></textarea></p>';
			$defaults['comment_notes_before'] = '';
			$defaults['comment_notes_after']  = '';
		}

		return $defaults;
	}

	function resolve_comment_form_fields() {

		$commenter   = wp_get_current_commenter();
		$req         = get_option( 'require_name_email' );
		$aria_req    = ( $req ? " aria-required='true'" : '' );
		$plex_format = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';
		$html5       = 'html5' === $plex_format;

		$fields = array(
			'author' => '<p class="comment-form-author">
			<input id="author" name="author" placeholder="' . __( 'Name', plex_get_trans_domain() ) . ( $req ? ' *' : '' ) . '" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
			'email'  => '<p class="comment-form-email">
			<input id="email" placeholder="' . __( 'Email', plex_get_trans_domain() ) . ( $req ? ' *' : '' ) . '" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
			'url'    => '<p class="comment-form-url">
			<input id="url" name="url" placeholder="' . __( 'Website', plex_get_trans_domain() ) . '" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>',
		);

		return $fields;


	}

}