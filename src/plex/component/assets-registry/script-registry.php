<?php

class Plex_Component_AssetsRegistry_ScriptRegistry implements Plex_Component_AssetsRegistry_RegistryInterface {

	function get_type() {
		return 'scripts';
	}

	function register( $asset_name, $asset_options ) {
		wp_register_script(
			$asset_name,
			$asset_options->get( 'src' ),
			$asset_options->get( 'deps', array() ),
			$asset_options->get( 'ver', false ),
			$asset_options->get( 'in_footer', true )
		);
	}

}