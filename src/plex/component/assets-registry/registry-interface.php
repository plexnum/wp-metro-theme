<?php

interface Plex_Component_AssetsRegistry_RegistryInterface {

	function get_type();

	function register( $asset_name, $asset_options );

}