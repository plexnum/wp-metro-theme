<?php

interface Plex_Component_AssetsRegistry_EnquireInterface {

	function get_type();

	function enquire( $name );

}