<?php

class Plex_Component_AssetsRegistry_ScriptEnquire implements Plex_Component_AssetsRegistry_EnquireInterface {

	function get_type() {
		return 'scripts';
	}

	function enquire( $name ) {
		wp_enqueue_script( $name );
	}

}