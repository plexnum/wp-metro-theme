<?php

class Plex_Component_AssetsRegistry_StyleEnquire implements Plex_Component_AssetsRegistry_EnquireInterface {

	function get_type() {
		return 'styles';
	}

	function enquire( $name ) {
		wp_enqueue_style( $name );
	}

}