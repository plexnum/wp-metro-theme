<?php

class Plex_Component_AssetsRegistry_StyleRegistry implements Plex_Component_AssetsRegistry_RegistryInterface {

	function get_type() {
		return 'styles';
	}

	function register( $asset_name, $asset_options ) {

		wp_register_style(
			$asset_name,
			$asset_options->get( 'src' ),
			$asset_options->get( 'deps', array() ),
			$asset_options->get( 'ver', false ),
			$asset_options->get( 'media', 'all' )
		);

	}


}