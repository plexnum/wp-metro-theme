<?php

class Plex_Component_AssetsRegistry_AssetsHook {

	private $name;

	private $asset_names;

	private $action;

	function __construct( $hook_name, $asset_names, $action ) {
		$this->name        = $hook_name;
		$this->asset_names = $asset_names;
		$this->action      = $action;
	}

	function create() {

		if(current_filter() == $this->name)
		{
			$this->callback();
		}
		else {
			add_action(
				$this->name,
				array( $this, 'callback' )
			);
		}

	}

	function callback() {
		foreach ( $this->asset_names as $asset_name ) {
			call_user_func( $this->action, $asset_name );
		}
	}

}