<?php

class Plex_Component_AssetsRegistry_Manager {

	static $registered = false;

	static $enquired = false;

	private $assets = array();

	private $registers = array();

	private $enquires = array();

	private $assets_to_enquire = array();

	function __construct( $assets ) {
		$this->assets = $assets;
	}

	function addRegister( $register ) {
		$this->registers[$register->get_type()] = $register;
	}

	function addEnquire( $enquire ) {
		$this->enquires[$enquire->get_type()] = $enquire;
	}

	function register() {

		if(self::$registered) {
			return;
		}

		foreach ( $this->assets as $type => $assets ) {
			if ( isset( $this->registers[$type] ) ) {

				$register = $this->registers[$type];

				foreach ( $assets as $asset_name => $asset_options ) {

					if ( ! $asset_options instanceof Plex_Config_ParamBag ) {
						$asset_options = new Plex_Config_ParamBag( $asset_options );
					}

					$asset_options->set( 'type', $type );

					// if src does not existed
					if ( $asset_options->get( 'src', false ) ) {
						$register->register( $asset_name, $asset_options );
					}

					$this->add_assets_to_enquire( $asset_name, $asset_options );

				}

			}
		}

		self::$registered = true;

	}

	function enquire() {

		if(self::$enquired) {
			return;
		}

		foreach ( $this->assets_to_enquire as $type => $hooks ) {
			if ( isset( $this->enquires[$type] ) ) {
				$enquire = $this->enquires[$type];

				foreach ( $hooks as $hook_name => $asset_names ) {

					$hook = new Plex_Component_AssetsRegistry_AssetsHook(
						$hook_name,
						$asset_names,
						array( $enquire, 'enquire' )
					);

					$hook->create();
				}

			}
		}

		self::$enquired = true;

	}

	private function add_assets_to_enquire( $asset_name, $asset_options ) {

		if ( $hooks = $asset_options->get( 'hooks' ) ) {

			if ( ! is_array( $hooks ) ) {
				$hooks = array( $hooks );
			}

			foreach ( $hooks as $hook ) {
				if ( ! isset( $this->assets_to_enquire[$asset_options->get( 'type' )][$hook] ) ) {
					$this->assets_to_enquire[$asset_options->get( 'type' )][$hook] = array();
				}

				array_push( $this->assets_to_enquire[$asset_options->get( 'type' )][$hook], $asset_name );

			}
		}
	}

}