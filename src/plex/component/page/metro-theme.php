<?php

class Plex_Component_Page_MetroTheme extends Plex_Model_AbstractPage implements Plex_Model_AdminableInterface {

	/**
	 * Get title of the page
	 * @return string
	 */
	function get_title() {
		return __( 'Metro theme', plex_get_trans_domain() );
	}

	/**
	 * Get menu title of the page
	 * @return string
	 */
	function get_menu_title() {
		return __( 'Metro', plex_get_trans_domain() );
	}

	/**
	 * Get slug of the page
	 * @return mixed
	 */
	function get_slug() {
		return basename( __FILE__ );
	}

	/**
	 * Show callback of the page
	 * @return mixed
	 */
	function show() {
		return null;
	}

	function declaration() {
		parent::declaration();
		add_action( 'admin_menu', array( $this, 'change_name' ), 1000 );
	}

	function change_name() {

		global $submenu;

		if ( isset( $submenu[$this->get_slug()][0] ) ) {

			unset( $submenu[$this->get_slug()][0] );

			// Add theme customizer link
			array_push( $submenu[$this->get_slug()], array(
				__( 'Theme Customizer', plex_get_trans_domain() ),
				'manage_options',
				'/wp-admin/customize.php',
				__( 'Theme Customizer', plex_get_trans_domain() ),
			) );

		}

	}

}