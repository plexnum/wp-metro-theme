<?php

class Plex_Component_Page_Settings extends Plex_Model_AbstractSubPage {

	private $sanitization_rules = array(
		'plex_settings' => array(
			'twitter_consumer_key'        => 'text_field',
			'twitter_consumer_secret'     => 'text_field',
			'twitter_access_token'        => 'text_field',
			'twitter_access_token_secret' => 'text_field',
			'twitter_cache_expire'        => 'abs_int:500',
			//'google_api_key'              => 'text_field'
		)
	);

	function get_theme_settings() {
		return array_keys( $this->sanitization_rules );
	}

	function get_parent_slug() {
		return 'metro-theme.php';
	}


	/**
	 * Get title of the page
	 * @return string
	 */
	function get_title() {
		return __( 'Settings', plex_get_trans_domain() );
	}

	/**
	 * Get menu title of the page
	 * @return string
	 */
	function get_menu_title() {
		return __( 'Settings', plex_get_trans_domain() );
	}

	/**
	 * Get slug of the page
	 * @return mixed
	 */
	function get_slug() {
		return basename( __FILE__ );
	}

	/**
	 * Show callback of the page
	 * @return mixed
	 */
	function show() {

		$sanitizer = new Plex_Lib_Sanitizer(
			$this->sanitization_rules,
			array()
		);

		$this->view->set_sanitizer( $sanitizer );
		$this->view->set( 'page_slug', $this->get_slug() );
		$this->view->set( 'settings', $this->get_theme_settings() );
		$this->view->set( 'plex_settings', get_option( 'plex_settings' ) );
		$this->view->display( '/admin/settings.php' );
	}

	/**
	 * Get unique name of the plugin
	 * @return mixed
	 */
	function get_name() {
		return 'plex_plugin_page_twitter-setting';
	}

	function declaration() {
		parent::declaration();
		add_action( 'admin_init', array( $this, 'define_settings' ) );
	}

	function define_settings() {

		foreach ( $this->get_theme_settings() as $setting ) {
			// Register settings
			register_setting(
				$setting,
				$setting,
				array( $this, $setting . '_sanitize' )
			);

		}

	}

	function plex_settings_sanitize( $raw_data ) {
		return $this->sanitize( 'plex_settings', $raw_data );
	}

	function sanitize( $setting_name, $raw_data ) {
		$sanitizer = new Plex_Lib_Sanitizer(
			$this->sanitization_rules[$setting_name],
			$raw_data
		);
		return $sanitizer->sanitize_input();
	}

}