<?php

class Plex_Component_JsLocalization extends Plex_Model_AbstractComponent {

	/**
	 * Component declaration
	 * That method define callbacks that listen to WordPress hooks
	 * @return mixed
	 */
	function declaration() {
		add_action('wp_enqueue_scripts', array($this, 'include_localization'), 20);
		add_action('admin_enqueue_scripts', array($this, 'include_localization'), 20);
		add_action('customize_controls_enqueue_scripts', array($this, 'include_localization'), 20);
	}

	function include_localization() {

		$config_manager    = plex_get_theme_conf_bag();
		$localization_file = $config_manager->get( 'patches[js_localization_declaration_file]', null, true );

		try {

			$localizations = plex_include_file( $localization_file );

			wp_localize_script(
				'plex_admin',
				'localization',
				$localizations['admin']
			);

			wp_localize_script(
				'plex_main',
				'localization',
				$localizations['theme']
			);

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

	}



}