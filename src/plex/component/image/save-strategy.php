<?php

class Plex_Component_Image_SaveStrategy {

	private $data;

	function __construct( $data ) {
		$this->data = $data;
	}

	function save() {

		if ( ! $this->data instanceof Plex_Config_ParamBag ) {
			$this->data = new Plex_Config_ParamBag( $this->data );
		}

		// If attachment id existed then rebuild attachment
		if ( $this->data->get( 'attachment_id' ) ) {

			try {

				$attachment = new Plex_Lib_Attachment(
					$this->data->get( 'attachment_id' )
				);

				$attachment->rebuild();

				$this->data->add(
					$attachment->get_thumbnail_info(
						$this->data->get( 'size', 'full' )
					)
				);

			} catch ( Plex_Exception_Common $e ) {
				$this->data->set( 'error', $e->getMessage() );
			}

		}

		return $this->data;

	}

}