<?php
class Plex_Component_Manager {

	private $components;

	function __construct() {
		$this->components = array();
	}

	/**
	 * @param array $components
	 */
	function register_array( array $components ) {
		foreach ( $components as $component ) {
			$this->register( $component );
		}
	}

	/**
	 * @param Plex_Model_ComponentInterface $component
	 */
	function register( Plex_Model_ComponentInterface $component ) {
		$this->components[$component->get_name()] = $component;
	}

	/**
	 * @param $component
	 *
	 * @throws Plex_Exception_Common
	 */
	function unregister( $component ) {
		if ( $component instanceof Plex_Model_ComponentInterface ) {
			unset( $this->components[$component->get_name()] );
		}
		elseif ( is_scalar( $component ) && isset( $this->components[$component] ) ) {
			unset( $this->components[$component] );
		}

		throw new Plex_Exception_Common( 'Component not found' );
	}

	/**
	 * Initialize all registered components
	 */
	function initialize() {
		foreach ( $this->components as $component ) {

			// Do not load components of AdminableInterface in public part
			if ( $component instanceof Plex_Model_AdminableInterface && ! is_admin() ) {
				continue;
			}

			// Do not load components of PubliclyInterface in admin part
			if ( $component instanceof Plex_Model_PublicalyInterface && is_admin() ) {
				continue;
			}

			// Declare component
			$component->declaration();
		}
	}
}
