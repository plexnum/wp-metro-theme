<?php

class Plex_Component_CommentResolver {

	static function resolve( $comment, $args, $depth ) {

		extract( $args, EXTR_SKIP );

		$view = new Plex_View( array(), true );

		$view->set( 'args', $args );
		$view->set( 'comment', $comment );
		$view->set( 'depth', $depth );
		$view->display( '/view/templates/_partials/comment.php' );

	}

	static function end_resolve() {
		echo '</li>';
	}

}