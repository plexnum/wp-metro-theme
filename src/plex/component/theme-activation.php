<?php

/**
 * Class Plex_Component_ThemeActivation
 */
class Plex_Component_ThemeActivation extends Plex_Model_AbstractComponent {

	private $config_manager;

	private $option_activation_key;

	function __construct() {
		$this->config_manager        = plex_get_theme_conf_bag();
		$this->option_activation_key = "theme_is_actived_" . $this->config_manager->get( 'theme_option_identity' );
	}

	function declaration() {
		$this->register_activation_function( array( $this, 'initialize' ) );
		$this->register_deactivation_function( array( $this, 'deactivation' ) );
	}

	function initialize() {

		flush_rewrite_rules();

		$default = new Plex_Config_ParamBag( plex_get_default_theme_opts() );

		foreach ( $this->config_manager->get( 'option_identities', array() ) as $id ) {
			$opts = get_option( $this->config_manager->get( $id ) );

			if ( ! $opts ) {
				add_option( $id, $default->get( $id, array() ) );
			}
		}

	}

	function deactivation() {
		// delete activation key
		delete_option( $this->option_activation_key );
	}

	function register_activation_function( $callback ) {

		$option_key = $this->option_activation_key;
		if ( ! get_option( $option_key ) ) {
			call_user_func( $callback );
			update_option( $option_key, 1 );
		}

	}

	function register_deactivation_function( $callback ) {
		add_action( 'switch_theme', $callback );
	}

}