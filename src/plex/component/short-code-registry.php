<?php

class Plex_Component_ShortCodeRegistry extends Plex_Model_AbstractComponent {

	private $config_manager;

	private $short_codes = array();

	private $short_codes_array = array();

	function __construct() {
		$this->config_manager = plex_get_theme_conf_bag();
	}

	function declaration() {

		try {

			$short_codes = plex_include_file( $this->config_manager->get( 'patches[short_codes_declaration_file]', false, true ) );

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

		/**
		 * @var $code Plex_Model_AbstractShortCode
		 */
		foreach ( $short_codes as $code ) {

			$this->short_codes[$code->get_tag_name()] = $code;

			// ADD array representation of short codes
			if ( $code->is_manageable() ) {
				$this->short_codes_array[] = $code->to_array();
			}

			// REGISTER short code
			add_shortcode( $code->get_tag_name(), array( $code, '_handle' ) );

		}

		add_action( 'wp_ajax_plex_get_short_codes', array( $this, 'ajax_get_short_codes' ) );
		add_action( 'wp_ajax_plex_do_short_code', array( $this, 'ajax_do_short_code' ) );

		add_action( 'print_media_templates', array( $this, 'render_templates' ) );

	}

	/**
	 * Render templates for short code builder
	 */
	function render_templates() {

		$short_codes_builder_view = new Plex_View();
		$short_codes_builder_view->display( 'view/short-code/_system/builder.php' );

		$sub_tpls_names = array();

		/**
		 * @var $code Plex_Model_AbstractShortCode
		 */
		foreach ( $this->short_codes as $code ) {

			$tpl = $code->_form();

			if ( $tpl ) {
				echo plex_script_template_wrapper( $tpl, 'plex_' . $code->get_tag_name() );
			}

			// Additional templates
			foreach ( $code->form_sub_templates() as $tpl_name => $tpl ) {

				if(!isset($sub_tpls_names[$tpl_name])) {
					echo plex_script_template_wrapper( $tpl, $tpl_name );
				}

				$sub_tpls_names[$tpl_name] = true;

			}

		}

	}

	/**
	 * Do short code
	 */
	function ajax_do_short_code() {

		header( 'Content-type: application/json' );

		// if short code founded
		if ( isset( $this->short_codes[$_POST['shortcode_tag']] )
				&& isset( $_POST['shortcode'] )
		) {

			/**
			 * @var $short_code Plex_Model_AbstractShortCode
			 */
			$short_code = $this->short_codes[$_POST['shortcode_tag']];

			$short_code_str = '';

			if ( isset( $_POST['shortcode'] ) ) {
				$short_code_str = wp_kses_stripslashes( wp_filter_post_kses( $_POST['shortcode']) );
			}

			do_action( 'plex_short_code_preview_enquire_scripts' );
			do_action( 'plex_short_code_preview_enquire_scripts_' . $_POST['shortcode_tag'] );

			echo json_encode( array(
					'success' => true,
					'data'    => $short_code->preview_layout( do_shortcode( $short_code_str ) )
				)
			);

		}
		else {

			echo json_encode( array(
				'success' => false
			) );

		}

		wp_die();
	}

	/**
	 * Get customizable shortcodes
	 */
	function ajax_get_short_codes() {

		header( 'Content-type: application/json' );

		echo json_encode( array(
			'success' => true,
			'data'    => $this->short_codes_array
		) );

		wp_die();
	}


}