<?php

class Plex_Component_Social_ShowStrategy {

	protected $output;

	protected $view;

	protected $data;

	/**
	 * @param      $view Plex_View
	 * @param      $data
	 * @param bool $output
	 */
	function __construct($view, $data, $output = true) {
		$this->output = $output;
		$this->view = $view;
		$this->data = $data;
	}

	function show() {

		$config_manager     = plex_get_theme_conf_bag();
		$social_el_template = new Plex_View();

		$social_el_template->set_patch_prefix(
			$this->view->get_patch_prefix()
		);

		$social_el_template->set( 'icon', '<%= icon %>' );
		$social_el_template->set( 'link', '<%= link %>' );
		$social_el_template->set( 'key', '<%= key %>' );

		$this->view->set( 'icons', $config_manager->get( 'social-icons' ) );

		$this->view->add( (array)$this->data );

		$this->view->set(
			'template',
			plex_script_template_wrapper(
				$social_el_template->get_output( '/_social-element.php' ),
				'plexSocialElement'
			)
		);

		if($this->output == true) {
			$this->view->display( '/social.php' );
		}
		else {
			return $this->view->get_output('/social.php');
		}

	}

}