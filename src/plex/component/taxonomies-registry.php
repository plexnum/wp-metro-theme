<?php

class Plex_Component_TaxonomiesRegistry extends Plex_Model_AbstractComponent {
	/**
	 * Component declaration
	 * That method define callbacks that listen to WordPress hooks
	 * @return mixed
	 */
	function declaration() {
		add_action( 'init', array( $this, 'register_taxonomies' ) );
	}


	function register_taxonomies() {

		register_taxonomy( 'project_category', 'project', array(

			'hierarchical' => true,

			'rewrite' => array(
				'with_front' => false,
				'slug'       => 'project-categories'
			),
			'labels'  => array(
				'name'                       => __( 'Project categories', plex_get_trans_domain() ),
				'singular_name'              => __( 'Project category', plex_get_trans_domain() ),
				'edit_item'                  => __( 'Edit project category', plex_get_trans_domain() ),
				'update_item'                => __( 'Update project category', plex_get_trans_domain() ),
				'add_new_item'               => __( 'Add new project category', plex_get_trans_domain() ),
				'new_item_name'              => __( 'New project category', plex_get_trans_domain() ),
				'all_items'                  => __( 'All project categories', plex_get_trans_domain() ),
				'search_items'               => __( 'Search project categories', plex_get_trans_domain() ),
				'popular_items'              => __( 'Popular project categories', plex_get_trans_domain() ),
				'separate_items_with_commas' => __( 'Separate project categories with commas', plex_get_trans_domain() ),
				'add_or_remove_items'        => __( 'Add or remove project categories', plex_get_trans_domain() ),
				'choose_from_most_used'      => __( 'Choose from the most popular project categories', plex_get_trans_domain() ),
			)

		) );

	}

}