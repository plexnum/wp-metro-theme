<?php

class Plex_Component_PostTypeRegistry extends Plex_Model_AbstractComponent {

	private $config_manager;

	function __construct() {
		$this->config_manager = plex_get_theme_conf_bag();
	}

	function declaration() {
		add_action( 'init', array( $this, 'register' ) );
	}

	function register() {

		try {
			$types = plex_include_file( $this->config_manager->get( 'patches[custom_types_declaration_file]', false, true ) );
		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

		foreach ( $types as $type ) {
			register_post_type( $type['id'], $type['settings'] );
		}
	}
}