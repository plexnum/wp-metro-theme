<?php
class Plex_Component_ThumbnailSizesRegistery extends Plex_Model_AbstractComponent {

	static $sizes = array(

		'plex_testimonial'     => array(
			'width'  => 60,
			'height' => 60,
			'crop'   => true,
			'label'  => '60X60'
		),

		'post_thumbnail'       => array(
			'width'  => 1200,
			'height' => 0,
			'crop'   => false
		),

		'plex_gallery'         => array(
			'width'  => 1200,
			'height' => 768,
			'crop'   => true,
			'label'  => '1200X768'
		),

		'plex_gallery_on_main' => array(
			'width'  => 460,
			'height' => 300,
			'crop'   => true,
			'label'  => '460X300'
		),
		'plex_tile_140_150'    => array(
			'width'  => 140,
			'height' => 150,
			'crop'   => true,
			'label'  => '140X150'
		),
		'plex_tile_300_150'    => array(
			'width'  => 300,
			'height' => 150,
			'crop'   => true,
			'label'  => '300X150'
		),
		'plex_tile_140_320'    => array(
			'width'  => 140,
			'height' => 320,
			'crop'   => true,
			'label'  => '300X320'
		),
		'plex_tile_300_320'    => array(
			'width'  => 300,
			'height' => 320,
			'crop'   => true,
			'label'  => '300X320'
		),
		'plex_tile_300_490'    => array(
			'width'  => 300,
			'height' => 490,
			'crop'   => true,
			'label'  => '300X490'
		),
		'plex_tile_460_320'    => array(
			'width'  => 460,
			'height' => 320,
			'crop'   => true,
			'label'  => '460X320'
		),
		'plex_tile_460_490'    => array(
			'width'  => 460,
			'height' => 490,
			'crop'   => true,
			'label'  => '460X490'
		)
	);

	function declaration() {
		$this->resolve_sizes();
		add_action( 'after_setup_theme', array( $this, 'add_sizes' ) );
		add_filter( 'image_size_names_choose', array( $this, 'add_image_size_names' ) );
	}

	function resolve_sizes() {
		if ( isset( self::$sizes['post_thumbnail'] ) ) {
			set_post_thumbnail_size(
				self::$sizes['post_thumbnail']['width'],
				self::$sizes['post_thumbnail']['height'],
				self::$sizes['post_thumbnail']['crop']
			);

			unset( self::$sizes['post_thumbnail'] );

		}
	}

	function add_sizes() {
		foreach ( self::$sizes as $size_name => $options ) {
			add_image_size( $size_name, $options['width'], $options['height'], $options['crop'] );
		}
	}

	function add_image_size_names( $image_sizes ) {

		$labels = array();
		foreach ( self::$sizes as $size_name => $opts ) {
			if ( isset( $opts['label'] ) ) {
				$labels[$size_name] = $opts['label'];
			}
		}

		return array_merge( $image_sizes, $labels );

	}
}