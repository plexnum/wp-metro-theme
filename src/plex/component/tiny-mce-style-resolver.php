<?php

class Plex_Component_TinyMceStyleResolver extends Plex_Model_AbstractComponent {
	/**
	 * Component declaration
	 * That method define callbacks that listen to WordPress hooks
	 * @return mixed
	 */
	function declaration() {
		add_action( 'admin_init', array( $this, 'resolve_editor_styles' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'resolve_styles' ) );
		add_filter( 'mce_buttons_2', array( $this, 'activate_style_drop_down' ) );
	}

	function resolve_editor_styles() {
		$cm = plex_get_theme_conf_bag();
		add_editor_style( $cm->get( 'urls[assets][css]', false, true ) . '/editor-styles.css' );
	}

	function activate_style_drop_down( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}


	function resolve_styles( $settings ) {

		$style_formats = array(
			array(
				'title'   => 'Page header',
				'block'   => 'h2',
				'classes' => 'page-header'
			)
		);

		$settings['style_formats'] = json_encode( $style_formats );

		return $settings;
	}


}