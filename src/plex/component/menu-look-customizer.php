<?php

class Plex_Component_MenuLookCustomizer extends Plex_Model_AbstractComponent implements Plex_Model_PublicalyInterface {

	function declaration() {
		add_filter( 'nav_menu_css_class', array( $this, 'change_main_nav_menu_classes' ), 0, 3 );
	}

	/**
	 * Change classes of main location menu
	 *
	 * @param $classes
	 * @param $item
	 * @param $args
	 *
	 * @return array
	 */
	function change_main_nav_menu_classes( $classes, $item, $args ) {

		if ( $args->theme_location == 'main' ) {

			$result = array(
				'item'
			);

			if ( in_array( 'current-menu-item', $classes ) ) {
				$result[] = 'active';
			}

			return $result;

		}

		return $classes;
	}
}