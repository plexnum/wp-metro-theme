<?php

class Plex_Lib_YoutubeImageUploader implements Plex_Model_UploaderInterface {

	private $url;

	private $upload_to;

	function __construct( $url, $upload_to = 'plex-video' ) {
		$this->url       = $url;
		$this->upload_to = $upload_to;
	}

	/**
	 * Upload
	 * @return array
	 * @throws Plex_Exception_Common
	 */
	function upload() {

		$data = array();

		try {

			$youtube_id = plex_youtube_get_id_by_url( $this->url );

			if ( ! $youtube_id ) {
				throw new Plex_Exception_Common( __( 'Youtube url have not valid format', plex_get_trans_domain() ) );
			}

			$remote_uploader = new Plex_Lib_RemoteImgUploader( 'http://img.youtube.com/vi/' . $youtube_id . '/0.jpg', $this->upload_to, $youtube_id );
			$result          = $remote_uploader->upload();
			$data['image']   = $result['url'];
			$data['width']   = 460;
			$data['height']  = 360;

		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}
		return $data;

	}

}