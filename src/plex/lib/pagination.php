<?php

/**
 * Deprecated becouse themeforester check
 * Class Plex_Lib_Pagination
 */
class Plex_Lib_Pagination {

	protected $max_pages_to_show = 10;

	protected $show_from_left;

	protected $show_from_right;

	protected $current_page;

	protected $max_pages;

	protected $posts_on_page;

	protected $pages = array();

	function __construct( WP_Query $wp_query ) {

		$this->current_page = $wp_query->get( 'paged' );

		if ( $this->current_page == 0 ) {

			$this->current_page = 1;

		}

		$this->max_pages = $wp_query->max_num_pages;

		$this->posts_on_page = $wp_query->get( 'post_per_page' );

		$this->set_show_from_directions();

		$this->set_pages();

		$this->view = new Plex_View();

	}

	function paginate( $template = '/view/pagenation/default.php' ) {

		$this->view->add( array(
			'max_pages'    => $this->max_pages,
			'current_page' => $this->current_page,
			'pages'        => $this->pages
		) );

		if($this->max_pages > 1) {
			$this->view->display( $template );
		}

	}

	function set_show_from_directions() {
		$this->show_from_right = (int) ( $this->max_pages_to_show / 2 );
		$this->show_from_left  = $this->max_pages_to_show - $this->show_from_right;
	}

	function set_pages() {

		$start = $this->current_page - $this->show_from_left;
		if ( $start <= 0 ) {
			$start = 1;
		}

		$end = $this->current_page + $this->show_from_right;

		if ( $end > $this->max_pages ) {
			$end = $this->max_pages;
		}

		for ( $i = $start; $i <= $end; $i ++ ) {
			$this->pages[] = array(
				'name' => $i,
				'url'  => get_pagenum_link( $i )
			);
		}

	}
}