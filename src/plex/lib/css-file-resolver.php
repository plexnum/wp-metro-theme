<?php
/**
 *
 * Purpose of that class to resolve css file placeholders to values provided values
 *
 * Example:
 *
 * mycss.css looks like
 *
 *  .selector1 {
 *      comment plex-var myProperty1
 *      border: "1px black solid",
 *
 *      comment plex-var myProperty2
 *      font-size: 16px;
 *  }
 *
 * $property_map = array(
 *      'myProperty1' => 'myValue1',
 *      'myProperty2' => 'myValue2'
 * );
 *
 * $path_to_css_file = __FILE__.'/css/mycss.css';
 *
 * $resolver = new Plex_Lib_CssFileResolver($property_map, $path_to_css_file);
 * $result = $resolver->resolve();
 *
 * Class Plex_Lib_CssFileResolver
 */
class Plex_Lib_CssFileResolver {

	private $properties_maps;

	private $css_source_file;

	private $create_template;

	private $default_settings = array();

	function __construct( $properties_maps, $css_source_file, $create_template = false ) {
		$this->properties_maps = $properties_maps;
		$this->css_source_file = $css_source_file;
		$this->create_template = $create_template;
	}

	function resolve() {

		if ( file_exists( $this->css_source_file ) and is_readable( $this->css_source_file ) ) {

			$config_css_source = file_get_contents( $this->css_source_file );

			// Resolve configurations
			$config_css_source_resolved = preg_replace_callback( '#/\*\s+plex-var(.*?)\*/(.*?):(.*?)(?:;|})#msi', array( $this, 'match_handler' ), $config_css_source );

			return $config_css_source_resolved;

		}
		else {
			throw new Plex_Exception_Common( "Setting css file does not existed or not readable" );
		}
	}

	function match_handler( $matches ) {
		$source = array_shift( $matches );

		$matches = array_map( 'trim', $matches );
		list( $match_key, $property, $value ) = $matches;

		$this->default_settings[$match_key] = $value;

		if ( isset( $this->properties_maps[$match_key] ) || $this->create_template == true ) {

			if ( $this->create_template == true ) {
				$property_val = '<%= '.$match_key.' %>';
			}
			else {
				$property_val = $this->properties_maps[$match_key];
			}

			return "/* plex-var " . $match_key . " */\n" . $property . ':' . $property_val . ';';

		}

		return $source;
	}

	function get_default_settings() {
		return $this->default_settings;
	}

}
