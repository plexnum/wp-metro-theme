<?php

class Plex_Lib_VimeoImageUploader implements Plex_Model_UploaderInterface {

	private $url;

	private $upload_to;

	function __construct( $url, $upload_to = 'plex-video' ) {
		$this->url       = $url;
		$this->upload_to = $upload_to;
	}

	/**
	 * Upload 640 to 360 px image
	 * @return array
	 * @throws Plex_Exception_Common
	 */
	function upload() {
		try {
			$data            = array();
			$result          = plex_vimeo_get_info_by_url( $this->url );
			$url             = str_replace( '_1280', '_640', $result->thumbnail_url );
			$remote_uploader = new Plex_Lib_RemoteImgUploader( $url, $this->upload_to, $result->video_id );
			$result          = $remote_uploader->upload();
			$data['image']   = $result['url'];
			$data['width']   = 640;
			$data['height']  = 360;
		} catch ( Plex_Exception_Common $e ) {
			throw new Plex_Exception_Common( $e->getMessage() );
		}

		return $data;
	}

}