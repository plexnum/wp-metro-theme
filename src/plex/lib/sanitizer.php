<?php

class Plex_Lib_Sanitizer {

	static $sanitize_strategies = array();

	private $rules;

	private $before_filters = array();

	private $filter = array();


	public function __construct( $rules, $data = array() ) {
		$this->rules = $rules;
		$this->data  = $data;
	}

	public function set_data( $data ) {
		$this->data = $data;
	}

	public function set_rules( $rules ) {
		$this->rules = $rules;
	}

	public function sanitize_input() {
		return $this->sanitize( $this->rules, $this->data, 'input' );
	}

	public function sanitize_output() {
		return $this->sanitize( $this->rules, $this->data, 'output' );
	}

	public function add_before_filter( $name, $callable ) {

		if ( ! isset( $this->before_filters[$name] ) ) {
			$this->filter[$name] = array();
		}

		array_push( $this->before_filters[$name], $callable );
	}

	public function get_before_filter( $name ) {

		if ( isset( $this->before_filters[$name] ) ) {
			return $this->before_filters[$name];
		}
		else {
			return array();
		}

	}

	public function get_filter( $name ) {

		if ( isset( $this->filter[$name] ) ) {
			return $this->filter[$name];
		}
		else {
			return array();
		}

	}

	public function add_filter( $name, $callable ) {

		if ( ! isset( $this->filter[$name] ) ) {
			$this->filter[$name] = array();
		}

		array_push( $this->filter[$name], $callable );
	}

	public function sanitize( $rules, $data, $type = 'input' ) {

		$result = null;

		if ( is_array( $rules ) ) {

			if ( ! $data instanceof Plex_Config_ParamBag ) {

				if( $data instanceof stdClass ) {
					$data = (array) $data;
				}

				if ( ! is_array( $data ) ) {
					$data = array();
				}

				$data = new Plex_Config_ParamBag( $data );
			}

			foreach ( $rules as $rule_key => $rule ) {

				$rule_key = Plex_Lib_Sanitizer_RuleKey::factory_method( $rule_key );

				// if rule key is iterative with leading *
				if ( $rule_key->is_iterative() ) {
					$iterative_data   = (array)$data->get( $rule_key->get_name(), array() );
					$iterative_result = array();
					if ( is_array( $iterative_data ) ) {
						foreach ( $iterative_data as $key => $data_item ) {

							// apply before filter
							$filtered_data = $this->filter_result(
								$rule_key->get_filters(),
								$data_item,
								$type,
								true
							);

							// assign data and apply filter
							$iterative_result[$key] = $this->filter_result(
								$rule_key->get_filters(),
								$this->sanitize( $rule, $filtered_data, $type ),
								$type
							);

						}
					}

					$result[$rule_key->get_name()] = $iterative_result;
				}
				else {

					// apply before filters
					$filtered_data = $this->filter_result(
						$rule_key->get_filters(),
						$data->get( $rule_key->get_name(), null ),
						$type,
						true
					);

					// assign result and apply filters
					$result[$rule_key->get_name()] = $this->filter_result(
						$rule_key->get_filters(),
						$this->sanitize( $rule, $filtered_data, $type ),
						$type
					);

				}

			}

			return $result;
		}


		// Solve concrete problem
		$rule       = Plex_Lib_Sanitizer_Rule::factory_method( $rules );
		$sanitizers = $rule->get_sanitize_strategies();

		if ( $data === null && $rule->get_default() ) {
			$data = $rule->get_default();
		}

		foreach ( $sanitizers as $sanitizer ) {

			$sanitize_strategy = self::get_sanitize_strategy( $sanitizer['name'] );

			if ( $type == 'input' ) {
				$data = $sanitize_strategy->sanitize_input( $data, $sanitizer['args'] );
			}

			if ( $type == 'output' ) {
				$data = $sanitize_strategy->sanitize_output( $data, $sanitizer['args'] );
			}

		}

		return $data;
	}


	private function filter_result( $filters, $result, $type = 'input', $before = false ) {

		foreach ( $filters as $filter_name ) {

			if ( $before == true ) {
				$filter_callbacks = $this->get_before_filter( $filter_name );
			}
			else {
				$filter_callbacks = $this->get_filter( $filter_name );
			}

			foreach ( $filter_callbacks as $filter_callback ) {
				if (
					$filter_callback != null &&
					is_callable( $filter_callback )
				) {
					$result = call_user_func( $filter_callback, $result, $type );
				}
			}

		}

		return $result;

	}

	static function multiple_register( $strategies ) {
		foreach ( $strategies as $strategy ) {
			self::register( $strategy );
		}
	}

	/**
	 * @param $strategy Plex_Model_AbstractSanitizeStrategy
	 */
	static function register( $strategy ) {
		self::$sanitize_strategies[$strategy->get_name()] = $strategy;
	}

	/**
	 * @param $strategy Plex_Model_AbstractSanitizeStrategy
	 */
	static function unregister( $strategy ) {
		self::$sanitize_strategies[$strategy->get_name()] = $strategy;
	}

	/**
	 * @param $name
	 *
	 * @return Plex_Model_AbstractSanitizeStrategy
	 * @throws Plex_Exception_Common
	 */
	static function get_sanitize_strategy( $name ) {
		if ( ! isset( self::$sanitize_strategies[$name] ) ) {
			throw new Plex_Exception_Common( sprintf( 'Sanitize strategy %s not founded', $name ) );
		}
		return self::$sanitize_strategies[$name];
	}

}