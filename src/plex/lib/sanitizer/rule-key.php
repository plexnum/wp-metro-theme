<?php

class Plex_Lib_Sanitizer_RuleKey {

	private $name;

	private $is_iterative;

	private $filters;

	function __construct( $name, $is_iterative, $filters = null ) {
		$this->name           = $name;
		$this->is_iterative   = $is_iterative;
		$this->filters = $filters;
	}

	function get_filters() {
		return $this->filters;
	}

	function get_name() {
		return $this->name;
	}

	function is_iterative() {
		return $this->is_iterative;
	}

	static function factory_method( $str_key ) {

		// is iterative
		if ( strpos( $str_key, '*' ) === 0 ) {
			$str_key      = substr( $str_key, 1 );
			$is_iterative = true;
		}
		else {
			$is_iterative = false;
		}

		// output filters
		$filters = explode( '::', $str_key );
		$str_key = array_shift( $filters );

		return new Plex_Lib_Sanitizer_RuleKey( $str_key, $is_iterative, $filters );

	}
}