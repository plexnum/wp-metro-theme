<?php

class Plex_Lib_Sanitizer_Rule {

	private $sanitize_strategies;

	private $default = null;

	function __construct( $sanitize_strategies, $default ) {
		$this->sanitize_strategies = $sanitize_strategies;
		$this->default             = $default;
	}

	function get_default() {
		return $this->default;
	}

	function get_sanitize_strategies() {
		return $this->sanitize_strategies;
	}

	static function factory_method( $str_rule ) {

		$default = null;

		$sanitize_strategies = array();

		$pattern = '#(?:(?:\||^)(\w+)(?:\(([\w\s-,]+)\))?)(?::([\w\s]+)$)?#ms';

		// Set sanitize_strategies
		if ( preg_match_all( $pattern, $str_rule, $matches ) ) {

			if ( ! empty( $matches[1] ) ) {

				foreach ( $matches[1] as $key => $sanitizer ) {

					$args = array();

					if ( ! empty( $matches[2][$key] ) ) {
						$args = array_map( 'trim', explode( ',', $matches[2][$key] ) );
					}

					$sanitize_strategies[] = array(
						'name' => $sanitizer,
						'args' => $args
					);
				}
			}

			// Set default value
			if ( ! empty( $matches[3] ) ) {
				$result = array_pop( $matches[3] );
				if ( ! empty( $result ) ) {
					$default = $result;
				}
			}

		}

		return new Plex_Lib_Sanitizer_Rule( $sanitize_strategies, $default );

	}

}