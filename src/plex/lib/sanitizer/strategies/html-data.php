<?php

class Plex_Lib_Sanitizer_Strategies_HtmlData extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'html_data';
	}

	function sanitize_input( $input, $args = array() ) {
		return wp_kses_data( $input );
	}

	function sanitize_output( $output, $args = array() ) {
		return wp_kses_data( $output );
	}

}