<?php

class Plex_Lib_Sanitizer_Strategies_Attr extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'attr';
	}

	function sanitize_input( $input, $args = array() ) {
		return sanitize_text_field( $input );
	}

	function sanitize_output( $output, $args = array() ) {
		return esc_attr( $output );
	}

}