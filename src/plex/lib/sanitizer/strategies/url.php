<?php

class Plex_Lib_Sanitizer_Strategies_Url extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'url';
	}

	function sanitize_input( $input, $args = array() ) {
		return esc_url_raw( $input );
	}

	function sanitize_output( $output, $args = array() ) {
		return esc_url( $output );
	}

}