<?php

class Plex_Lib_Sanitizer_Strategies_Date extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'date';
	}

	function sanitize_input( $input, $args = array() ) {

		try {
			$date_time = new DateTime($input);
		}
		catch(Exception $e) {
			$date_time = new DateTime();
		}

		return $date_time->format('m/d/Y');

	}

	function sanitize_output( $output, $args = array() ) {
		return esc_html( $output );
	}


}