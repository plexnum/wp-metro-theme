<?php

class Plex_Lib_Sanitizer_Strategies_Color extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'color';
	}

	function sanitize_input( $input, $args = array() ) {
		return sanitize_hex_color( $input );
	}

	function sanitize_output( $output, $args = array() ) {
		return sanitize_hex_color( $output );
	}


}