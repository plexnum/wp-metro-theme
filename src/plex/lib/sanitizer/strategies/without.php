<?php

class Plex_Lib_Sanitizer_Strategies_Without extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'without';
	}

	function sanitize_input( $input, $args = array() ) {
		return $input;
	}

	function sanitize_output( $output, $args = array() ) {
		return $output;
	}


}