<?php

class Plex_Lib_Sanitizer_Strategies_Post extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'post';
	}

	function sanitize_input( $input, $args = array() ) {
		return wp_kses_post( $input );
	}

	function sanitize_output( $output, $args = array() ) {
		return wp_kses_post( $output );
	}

}