<?php

class Plex_Lib_Sanitizer_Strategies_TextField extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'text_field';
	}

	function sanitize_input( $input, $args = array() ) {
		return sanitize_text_field($input);
	}

	function sanitize_output( $output, $args = array() ) {
		return esc_html( $output );
	}

}