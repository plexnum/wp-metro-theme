<?php

class Plex_Lib_Sanitizer_Strategies_SqlOrderBy extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'sql_order_by';
	}

	function sanitize_input( $input, $args = array() ) {
		return sanitize_sql_orderby( $input );
	}

	function sanitize_output( $output, $args = array() ) {
		return $this->sanitize_input( $output, $args );
	}

}