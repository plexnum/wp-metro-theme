<?php

class Plex_Lib_Sanitizer_Strategies_Bool extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'bool';
	}

	function sanitize_input( $input, $args = array() ) {

		if ( $input === "false" ) {
			$input = false;
		}
		return (bool) $input;

	}

	function sanitize_output( $output, $args = array() ) {
		return $this->sanitize_input( $output, $args );
	}

}