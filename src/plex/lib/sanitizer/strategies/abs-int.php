<?php

class Plex_Lib_Sanitizer_Strategies_AbsInt extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'abs_int';
	}

	function sanitize_input( $input, $args = array() ) {
		return absint($input);
	}

	function sanitize_output( $output, $args = array() ) {
		return absint($output);
	}

}