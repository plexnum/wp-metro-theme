<?php

class Plex_Lib_Sanitizer_Strategies_InList extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'in_list';
	}

	function sanitize_input( $input, $args = array() ) {

		$possible_values = $args;

		if ( ! is_array( $possible_values ) ) {
			$possible_values = array();
		}

		if ( in_array( $input, $possible_values ) ) {
			return $input;
		}

		return array_shift( $possible_values );

	}

	function sanitize_output( $output, $args = array() ) {
		return $this->sanitize_input( $output, $args );
	}

}