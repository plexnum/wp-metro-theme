<?php

class Plex_Lib_Sanitizer_Strategies_HtmlClass extends Plex_Model_AbstractSanitizeStrategy {

	function get_name() {
		return 'html_class';
	}

	function sanitize_input( $input, $args = array() ) {
		return sanitize_html_class( $input );
	}

	function sanitize_output( $output, $args = array() ) {
		return esc_attr( $output );
	}

}