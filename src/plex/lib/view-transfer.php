<?php

class Plex_Lib_ViewTransfer extends Plex_Config_ParamBag {

	static protected $instance;

	public static function get_transfer( array $parameters = array() ) {
		if ( ! self::$instance ) {
			self::$instance = new self( $parameters, true );
		}
		return self::$instance;
	}

}