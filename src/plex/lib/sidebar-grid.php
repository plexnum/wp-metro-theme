<?php

class Plex_Lib_SidebarGrid {

	private $content_width = 12;

	private $classes_of_sidebars = array();

	private $active_all_sidebars = false;

	private $content_item_widht = null;

	function __construct( $sidebar_settings ) {

		$sidebar_settings = (array) $sidebar_settings;

		$count = count( $sidebar_settings );

		foreach ( (array) $sidebar_settings as $sidebar_name => $options ) {

			if ( is_active_sidebar( $sidebar_name ) ) {

				if ( ! empty( $options['active'] ) && ! empty( $options['width'] ) ) {

					$count -= 1;

					$this->content_width -= (int) $options['width'];
					$this->classes_of_sidebars[$sidebar_name] = 'span' . (int) $options['width'];
				}

			}

		}

		$this->active_all_sidebars = ! (bool) $count;

	}

	function the_content_class() {
		echo 'span' . $this->content_width;
	}

	function the_sidebar_class( $name ) {

		if ( isset( $this->classes_of_sidebars[$name] ) ) {
			echo ' ' . $this->classes_of_sidebars[$name] . ' ';
		}

	}

	function is_active_sidebar( $name ) {

		if ( isset( $this->classes_of_sidebars[$name] ) ) {
			return true;
		}

		return false;
	}

	function the_js() {
		if ( ! $this->active_all_sidebars ) {
			echo ' js-sade-bar ';
		}
	}

	function calculate_number_of_items( $item_width ) {
		$result = $this->content_width / $item_width;
		return (int) $result;
	}

	function calculate_fluid_number_of_items( $item_width ) {
		return (int) ( 12 / $item_width );
	}


}