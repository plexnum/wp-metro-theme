<?php

class Plex_Lib_TwitterCacheStrategy {

	static $hash_prefix = 'plex_twitter_cache_';

	static $possible_hashes = array(
		'get_tweets'
	);

	private $expire_time;

	/**
	 * @param $expire_time in seconds
	 */
	function __construct( $expire_time ) {
		$this->expire_time = $expire_time;
	}

	/**
	 * @param $hash
	 *
	 * @return false or result
	 */
	function get_cached( $hash ) {
		return get_transient( self::$hash_prefix . $hash );
	}

	/**
	 * Cache only if result not empty
	 * and errors not presented
	 *
	 * @param $hash
	 * @param $result
	 *
	 * @return mixed
	 */
	function cache( $hash, $result ) {

		if ( ! empty( $result ) && !isset($result->errors)) {
			set_transient( self::$hash_prefix . $hash, $result, $this->expire_time );
		}
		return $result;
	}

	/**
	 * Clear all cache
	 */
	static function clear_all() {
		foreach ( self::$possible_hashes as $hash ) {
			delete_transient( self::$hash_prefix . $hash );
		}
	}

}