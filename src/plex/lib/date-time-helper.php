<?php

class Plex_Lib_DateTimeHelper {

	static function time_ago($date_str) {

		$date = new DateTime($date_str);
		$now 		 = new DateTime();
		$interval = $date->diff($now);

		if($interval->d != 0) return $interval->d . ' ' .__('days ago', plex_get_trans_domain());
		if($interval->h != 0) return $interval->h . ' ' .__('hours ago', plex_get_trans_domain());
		if($interval->i != 0) return $interval->i . ' ' .__('minutes ago', plex_get_trans_domain());

	}

}