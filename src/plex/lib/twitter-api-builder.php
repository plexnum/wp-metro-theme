<?php
class Plex_Lib_TwitterApiBuilder {

	public function build( $options = array() ) {

		// Include twitteroauth.php if not required before
		if ( ! class_exists( 'TwitterOAuth' ) ) {
			require 'twitter-connection/twitteroauth.php';
		}

		// options
		$default_options = get_option( 'plex_settings', array() );

		$options         = new Plex_Config_ParamBag( array_merge( $default_options, $options ) );

		$connection = new TwitterOAuth( $options->get( 'twitter_consumer_key', '' ),
																		$options->get( 'twitter_consumer_secret', '' ),
																		$options->get( 'twitter_access_token', '' ),
																		$options->get( 'twitter_access_token_secret', '' ) );

		$cache_strategy = new Plex_Lib_TwitterCacheStrategy( $options->get('twitter_cache_expire') );
		return new Plex_Lib_TwitterApi( $connection, $cache_strategy );

	}

}