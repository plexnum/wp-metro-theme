<?php

/**
 * Class Plex_Lib_Attachment
 */
class Plex_Lib_Attachment {

	private $id;

	private $file;

	private $meta;

	function __construct( $id ) {

		$attachment_meta_data = wp_get_attachment_metadata( $id );

		if ( empty( $attachment_meta_data ) ) {
			throw new Plex_Exception_Common( sprintf( __( 'Attachment does not exist for id %s', plex_get_trans_domain() ), $id ) );
		}

		$upload_dir_info = wp_upload_dir( null );

		$this->file = $upload_dir_info['basedir'] . '/' . $attachment_meta_data['file'];
		$this->id   = $id;
		$this->meta = wp_generate_attachment_metadata( $this->id, $this->file );

		if ( empty( $this->meta ) ) {
			throw new Plex_Exception_Common( sprintf( __( 'Attachment does not exist for id %s', plex_get_trans_domain() ), $id ) );
		}

	}

	/**
	 * Rebuild attachment
	 * @return bool
	 * @throws Plex_Exception_Common
	 */
	function rebuild() {

		if ( empty( $this->id ) || empty( $this->meta ) ) {
			throw new Plex_Exception_Common( __( "Attachment does not exist and can't be rebuild", plex_get_trans_domain() ) );
		}

		wp_update_attachment_metadata( $this->id, $this->meta );

		if ( ! $this->is_rebuilded_successfully( $this->meta ) ) {
			$ext = 'unknown';

			if ( preg_match( '/(?=.)\w+$/i', $this->file, $match ) ) {
				$ext = $match[0];
			}

			throw new Plex_Exception_Common( sprintf( __( 'Your system does not support GD or Imagick library for %s extension. For proper images resizing highly recommend to install one of them', plex_get_trans_domain() ), $ext ) );
		}

		return true;
	}

	/**
	 * Get thumbnail info
	 *
	 * @param $name
	 *
	 * @return array
	 * @throws Plex_Exception_Common
	 */
	function get_thumbnail_info( $name ) {
		$data = array();

		$sized_img_data = wp_get_attachment_image_src( $this->id, $name );
		if ( isset( $sized_img_data[0] ) ) {
			$data['image']  = $sized_img_data[0];
			$data['width']  = $sized_img_data[1];
			$data['height'] = $sized_img_data[2];
			$data['alt']    = get_post_meta( $this->id, '_wp_attachment_image_alt', true );
		}
		else {
			throw new Plex_Exception_Common( sprintf( __( "Thumbnail %s doesn't exist for attachment id %s" ), $name, $this->id ) );
		}

		return $data;
	}

	private function is_rebuilded_successfully( $meta_data ) {

		foreach ( Plex_Component_ThumbnailSizesRegistery::$sizes as $size_name => $size ) {

			if ( $meta_data['width'] > $size['width'] || $meta_data['height'] > $size['height'] ) {
				if ( ! isset( $meta_data['sizes'][$size_name] ) ) {
					return false;
				}
			}

		}
		return true;
	}

}