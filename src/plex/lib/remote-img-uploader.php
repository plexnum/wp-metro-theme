<?php

class Plex_Lib_RemoteImgUploader {

	static $allowed_mime_types = array(
		'jpg' => 'image/jpeg',
	);

	private $url;

	private $upload_to;

	private $file_name;

	/**
	 * @param $url           - image url
	 * @param $upload_to     - relative to WP upload directory without leading slash
	 * @param $as_file_name  - file name without extension
	 */
	function __construct( $url, $upload_to, $as_file_name ) {
		$this->url       = $url;
		$this->upload_to = $upload_to;
		$this->file_name = $as_file_name;

	}

	/**
	 * Upload
	 * @return array
	 * @throws Plex_Exception_Common
	 */
	function upload() {

		$result = wp_remote_get( $this->url );

		if ( ! is_wp_error( $result ) ) {

			$result = new Plex_Config_ParamBag( $result );

			$this->check_mine_type( $result );

			$dir = wp_upload_dir();

			$this->upload_to = str_replace( '..', '', $this->upload_to );

			$upload_to = $dir['basedir'] . '/' . $this->upload_to;

			if ( ! is_dir( $upload_to ) ) {
				@mkdir( $upload_to, 0777 );
			}

			if ( is_writable( $upload_to ) ) {

				$ext = $this->get_extension_by_content_type( $result->get( 'headers[content-type]', false, true ) );

				$file = $this->file_name . '.' . $ext;

				$upload_file_path = $upload_to . '/' . $file;
				$upload_url_path  = $dir['baseurl'] . '/' . $this->upload_to . '/' . $file;

				file_put_contents( $upload_file_path, $result->get( 'body' ) );

				return array(
					'file' => $upload_file_path,
					'url'  => $upload_url_path
				);

			}

			throw new Plex_Exception_Common( sprintf( __( 'Directory %s not writable', plex_get_trans_domain() ), $this->upload_to ) );
		}
		else {
			throw new Plex_Exception_Common( $result->get_error_message() );
		}
	}

	private function check_mine_type( $result ) {
		if ( ! in_array( $result->get( 'headers[content-type]', false, true ), self::$allowed_mime_types ) ) {
			throw new Plex_Exception_Common( sprintf( __( 'Remote uploader does not support %s mime type', plex_get_trans_domain() ), $result->get( 'headers[content-type]', false, true ) ) );
		}
	}

	private function get_extension_by_content_type( $content_type ) {
		return array_search( $content_type, self::$allowed_mime_types );
	}

}