<?php
class Plex_Lib_TwitterApi {

	private $connection;

	private $cache_strategy;

	function __construct( $connection, $cache_strategy ) {

		$this->connection     = $connection;
		$this->cache_strategy = $cache_strategy;

	}

	function get_tweets( $options = array() ) {

		$result = array();

		if ( ( $result = $this->cache_strategy->get_cached( 'get_tweets' ) ) === false ) {

			$api_result = $this->connection->get( "statuses/home_timeline", $options );

			if($this->is_error_result($api_result))
			{
				$result['errors'] = $api_result->errors;
				return $result;
			}

			return $this->cache_strategy->cache( 'get_tweets', apply_filters('plex_twitter-api_get_tweets', $this->connection->get( "statuses/home_timeline", $options )) );

		}
		return $result;

	}

	function is_error_result($result) {
		if(isset($result->errors))
		{
			return true;
		}
		else {
			return false;
		}
	}

}