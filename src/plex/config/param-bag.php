<?php

/**
 * Goal of that class to manage all types of configurations
 * Class Plex_Config_ParamBag
 */
class Plex_Config_ParamBag implements \IteratorAggregate, \Countable {

	/**
	 * Singleton instance
	 * @var Plex_Config_ParamBag
	 */
	static protected $instance;

	/**
	 * Parameter storage.
	 *
	 * @var array
	 */
	protected $parameters;


	protected $deep = null;

	/**
	 * Singleton interface
	 *
	 * @param array $parameters
	 *
	 * @return Plex_Config_ParamBag
	 */
	public static function instantiate( array $parameters = array() ) {
		if ( ! self::$instance ) {
			self::$instance = new self( $parameters );
		}
		return self::$instance;
	}

	/**
	 * @param array $parameters
	 * @param bool  $deep
	 */
	public function __construct( array $parameters = array(), $deep = null ) {
		$this->parameters = $parameters;
		$this->deep       = $deep;
	}

	/**
	 * Returns the parameters.
	 *
	 * @return array An array of parameters
	 *
	 * @api
	 */
	public function all() {
		return $this->parameters;
	}

	/**
	 * Returns the parameter keys.
	 *
	 * @return array An array of parameter keys
	 *
	 * @api
	 */
	public function keys() {
		return array_keys( $this->parameters );
	}

	/**
	 * Replaces the current parameters by a new set.
	 *
	 * @param array   $parameters An array of parameters
	 *
	 * @api
	 */
	public function replace( array $parameters = array() ) {
		$this->parameters = $parameters;
	}

	/**
	 * Adds parameters.
	 *
	 * @param array   $parameters An array of parameters
	 *
	 * @api
	 */
	public function add( array $parameters = array() ) {
		$this->parameters = array_merge( $this->parameters, $parameters );
	}

	/**
	 * Returns a parameter by name.
	 *
	 * @param string  $path    The key
	 * @param mixed   $default The default value if the parameter key does not exist
	 * @param boolean $deep    If true, a path like foo[bar] will find deeper items
	 *
	 * @return mixed
	 *
	 * @throws \InvalidArgumentException
	 *
	 * @api
	 */
	public function get( $path, $default = null, $deep = false ) {

		if ( $this->deep !== null ) {
			$deep = (bool) $this->deep;
		}

		if ( ! $deep || false === $pos = strpos( $path, '[' ) ) {
			return array_key_exists( $path, $this->parameters ) ? $this->parameters[$path] : $default;
		}

		$root = substr( $path, 0, $pos );
		if ( ! array_key_exists( $root, $this->parameters ) ) {
			return $default;
		}

		$value       = $this->parameters[$root];
		$current_key = null;
		for ( $i = $pos, $c = strlen( $path ); $i < $c; $i ++ ) {
			$char = $path[$i];

			if ( '[' === $char ) {
				if ( null !== $current_key ) {
					throw new \InvalidArgumentException( sprintf( 'Malformed path. Unexpected "[" at position %d.', $i ) );
				}

				$current_key = '';
			}
			elseif ( ']' === $char ) {
				if ( null === $current_key ) {
					throw new \InvalidArgumentException( sprintf( 'Malformed path. Unexpected "]" at position %d.', $i ) );
				}

				if ( ! is_array( $value ) || ! array_key_exists( $current_key, $value ) ) {
					return $default;
				}

				$value       = $value[$current_key];
				$current_key = null;
			}
			else {
				if ( null === $current_key ) {
					throw new \InvalidArgumentException( sprintf( 'Malformed path. Unexpected "%s" at position %d.', $char, $i ) );
				}

				$current_key .= $char;
			}
		}

		if ( null !== $current_key ) {
			throw new \InvalidArgumentException( sprintf( 'Malformed path. Path must end with "]".' ) );
		}

		return $value;
	}

	/**
	 * Sets a parameter by name.
	 *
	 * @param string  $key   The key
	 * @param mixed   $value The value
	 *
	 * @api
	 */
	public function set( $key, $value ) {
		$this->parameters[$key] = $value;
	}

	/**
	 * Returns true if the parameter is defined.
	 *
	 * @param string  $key The key
	 *
	 * @return Boolean true if the parameter exists, false otherwise
	 *
	 * @api
	 */
	public function has( $key ) {
		return array_key_exists( $key, $this->parameters );
	}

	/**
	 * Removes a parameter.
	 *
	 * @param string  $key The key
	 *
	 * @api
	 */
	public function remove( $key ) {
		unset( $this->parameters[$key] );
	}

	/**
	 * Returns the alphabetic characters of the parameter value.
	 *
	 * @param string  $key     The parameter key
	 * @param mixed   $default The default value if the parameter key does not exist
	 * @param boolean $deep    If true, a path like foo[bar] will find deeper items
	 *
	 * @return string The filtered value
	 *
	 * @api
	 */
	public function get_alpha( $key, $default = '', $deep = false ) {
		return preg_replace( '/[^[:alpha:]]/', '', $this->get( $key, $default, $deep ) );
	}

	/**
	 * Returns the alphabetic characters and digits of the parameter value.
	 *
	 * @param string  $key     The parameter key
	 * @param mixed   $default The default value if the parameter key does not exist
	 * @param boolean $deep    If true, a path like foo[bar] will find deeper items
	 *
	 * @return string The filtered value
	 *
	 * @api
	 */
	public function get_alnum( $key, $default = '', $deep = false ) {
		return preg_replace( '/[^[:alnum:]]/', '', $this->get( $key, $default, $deep ) );
	}

	/**
	 * Returns the digits of the parameter value.
	 *
	 * @param string  $key     The parameter key
	 * @param mixed   $default The default value if the parameter key does not exist
	 * @param boolean $deep    If true, a path like foo[bar] will find deeper items
	 *
	 * @return string The filtered value
	 *
	 * @api
	 */
	public function get_digits( $key, $default = '', $deep = false ) {
		// we need to remove - and + because they're allowed in the filter
		return str_replace( array( '-', '+' ), '', $this->filter( $key, $default, $deep, FILTER_SANITIZE_NUMBER_INT ) );
	}

	/**
	 * Returns the parameter value converted to integer.
	 *
	 * @param string  $key     The parameter key
	 * @param mixed   $default The default value if the parameter key does not exist
	 * @param boolean $deep    If true, a path like foo[bar] will find deeper items
	 *
	 * @return integer The filtered value
	 *
	 * @api
	 */
	public function get_int( $key, $default = 0, $deep = false ) {
		return (int) $this->get( $key, $default, $deep );
	}

	public function get_bool( $key, $default = 0, $deep = false ) {
		$result = $this->get( $key, $default, $deep );
		if ( $result === 'true' ) {
			return true;
		}

		if ( $result === 'false' ) {
			return false;
		}

		return (bool) $result;
	}

	/**
	 * Filter key.
	 *
	 * @param string  $key     Key.
	 * @param mixed   $default Default = null.
	 * @param boolean $deep    Default = false.
	 * @param integer $filter  FILTER_* constant.
	 * @param mixed   $options Filter options.
	 *
	 * @see http://php.net/manual/en/function.filter-var.php
	 *
	 * @return mixed
	 */
	public function filter( $key, $default = null, $deep = false, $filter = FILTER_DEFAULT, $options = array() ) {
		$value = $this->get( $key, $default, $deep );

		// Always turn $options into an array - this allows filter_var option shortcuts.
		if ( ! is_array( $options ) && $options ) {
			$options = array( 'flags' => $options );
		}

		// Add a convenience check for arrays.
		if ( is_array( $value ) && ! isset( $options['flags'] ) ) {
			$options['flags'] = FILTER_REQUIRE_ARRAY;
		}

		return filter_var( $value, $filter, $options );
	}

	/**
	 * Returns an iterator for parameters.
	 *
	 * @return \ArrayIterator An \ArrayIterator instance
	 */
	public function getIterator() {
		return new \ArrayIterator( $this->parameters );
	}

	/**
	 * Returns the number of parameters.
	 *
	 * @return int The number of parameters
	 */
	public function count() {
		return count( $this->parameters );
	}

	public function get_json( $exclude = array() ) {

		$attributes = $this->all();

		foreach ( $exclude as $item ) {

			$reference =& $attributes;

			if ( is_array( $item ) ) {

				$count = count( $item );

				for ( $i = 0; $i < $count; $i ++ ) {

					if ( isset( $reference[$item[$i]] ) ) {

						// if last key then delete
						if ( $i == ( $count - 1 ) ) {
							unset( $reference[$item[$i]] );
							break;
						}

						$reference =& $reference[$item[$i]];
					}
					else {
						break;
					}

				}
			}
			elseif ( isset( $reference[$item] ) ) {
				unset( $reference[$item] );
			}
		}

		unset( $reference );
		return json_encode( $attributes );
	}
}
