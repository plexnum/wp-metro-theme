<?php
class Plex_Widget_Search extends WP_Widget_Search {

	function widget( $args, $instance ) {

		if ( $args['id'] == 'front-page-sidebar' ) {
			plex_wrap_before_metro_widget(2);
			parent::widget( $args, array() );
			plex_wrap_after_metro_widget();
		}
		else {

			plex_wrap_before_widget( $args, 'div', 'search-wd' );
			parent::widget( $args, array() );
			plex_wrap_after_widget( $args, 'div' );

		}
	}

	function form( $instance ) {
	}

}