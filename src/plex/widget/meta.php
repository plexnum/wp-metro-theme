<?php

class Plex_Widget_Meta extends WP_Widget_Meta {

	function widget( $args, $instance ) {

		if ( $args['id'] == 'front-page-sidebar' ) {
			plex_wrap_before_metro_widget();
			parent::widget( $args, $instance );
			plex_wrap_after_metro_widget();
		}
		else {
			plex_wrap_before_widget( $args, 'div', 'meta-wd' );
			parent::widget( $args, $instance );
			plex_wrap_after_widget( $args, 'div' );
		}
	}

}