<?php

class Plex_Widget_Text extends WP_Widget_Text {

	function widget( $args, $instance ) {

		if ( $args['id'] == 'front-page-sidebar' ) {
			plex_wrap_before_metro_widget();
			parent::widget( $args, $instance );
			plex_wrap_after_metro_widget();
		}
		else {
			parent::widget( $args, $instance );
		}

	}

}