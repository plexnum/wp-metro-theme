<?php

class Plex_Widget_Categories extends WP_Widget_Categories {


	function widget( $args, $instance ) {

		if ( $args['id'] == 'front-page-sidebar' ) {
			plex_wrap_before_metro_widget(2);
			parent::widget( $args, $instance );
			plex_wrap_after_metro_widget();
		}
		else {

			plex_wrap_before_widget( $args, 'div', 'categories-list' );
			parent::widget( $args, $instance );
			plex_wrap_after_widget( $args, 'div' );
		}
	}

}