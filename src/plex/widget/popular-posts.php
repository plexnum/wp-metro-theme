<?php

class Plex_Widget_PopularPosts extends Plex_Model_AbstractWidget {

	protected $sanitize_rules = array(
		'title'          => 'text_field',
		'number_to_show' => 'abs_int:5'
	);

	/**
	 * Get widget name
	 * @return string
	 */
	function get_name() {
		return __( 'Plex Popular Posts', 'widget_name', plex_get_trans_domain() );
	}

	/**
	 * Get widget description
	 * @return string
	 */
	function get_desc() {
		return __( 'Plex Popular Posts', 'widget_desc', plex_get_trans_domain() );
	}

	/**
	 * @param $new_instance
	 * @param $old_instance
	 *
	 * @return mixed
	 */
	function save( $new_instance, $old_instance ) {
		wp_cache_delete( $this->get_id(), 'widget' );
		return $new_instance;
	}

	function form( $instance ) {
		$this->view->add( (array) $instance );
		$this->view->display( '/popular-posts/form.php' );
	}

	function widget( $args, $instance ) {

		if ( $args['id'] == 'front-page-sidebar' ) {
			plex_wrap_before_metro_widget();
			$this->_widget( $args, $instance );
			plex_wrap_after_metro_widget();
		}
		else {
			plex_wrap_before_widget( $args, 'div' );
			$this->_widget( $args, $instance );
			plex_wrap_after_widget( $args, 'div' );
		}

	}

	function _widget( $args, $instance ) {

		$cache = wp_cache_get( $this->get_id(), 'widget' );

		if ( ! is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( ! isset( $cache[$args['widget_id']] ) ) {
			$this->view->add( (array) $instance );
			$cache[$args['widget_id']] = $this->view->get_output( '/popular-posts/widget.php' );
			wp_cache_set( $this->get_id(), $cache, 'widget' );
		}

		echo $cache[$args['widget_id']];

	}


}