<?php

class Plex_Widget_TwitterFeedsWidget extends Plex_Model_AbstractWidget {

	protected $sanitize_rules = array(
		'display_items' => 'abs_int:5'
	);

	/**
	 * Get widget name
	 * @return string
	 */
	function get_name() {
		return __( 'Plex Twitter Feeds', plex_get_trans_domain() );
	}

	/**
	 * Get widget description
	 * @return string
	 */
	function get_desc() {
		return __( 'Plex Twitter Feeds', plex_get_trans_domain() );
	}

	/**
	 * Display setting form of widget
	 *
	 * @param array $instance
	 *
	 * @return string|void
	 */
	function form( $instance ) {
		$this->view->add( $instance );
		$this->view->display( '/twitter-feeds/form.php' );
	}

	function save( $new_instance, $old_instance ) {
		Plex_Lib_TwitterCacheStrategy::$hash_prefix = $this->id;
		Plex_Lib_TwitterCacheStrategy::clear_all();
		return $new_instance;
	}

	function widget( $args, $instance ) {

		// Cache each widget separately
		Plex_Lib_TwitterCacheStrategy::$hash_prefix = $this->id;

		$twitter_api_builder = new Plex_Lib_TwitterApiBuilder();
		$api                 = $twitter_api_builder->build();

		add_filter( 'plex_twitter-api_get_tweets', array( $this, 'filter_tweets' ) );

		$result = $api->get_tweets( array(
			'count'            => $instance['display_items'],
			'include_rts'      => true,
			'exclude_replies'  => false,
			'include_entities' => true
		) );

		if ( is_array( $result ) ) {
			$this->view->set( 'tweets', $result );
		}

		$this->view->set( 'widget', $this->view->get_output( '/twitter-feeds/widget.php' ) );

		// For front page
		if ( $args['id'] === 'front-page-sidebar' ) {
			plex_wrap_before_widget( $args, 'div', 'span4 inch2 js-tile tile_twitter js-animate' );
			$this->view->display( '/twitter-feeds/widget.php' );
			plex_wrap_after_widget( $args, 'div' );
		}
		else {
			plex_wrap_before_widget( $args, 'div', 'tile_twitter' );
			$this->view->display( '/twitter-feeds/widget.php' );
			plex_wrap_after_widget( $args, 'div' );
		}

	}

	/**
	 * Filter callback to filter twitter api for widget needs
	 *
	 * @param $tweets
	 *
	 * @return array
	 */
	function filter_tweets( $tweets ) {

		$result = array();

		if ( is_array( $tweets ) ) {
			foreach ( $tweets as $tweet ) {
				$result[] = array(
					'text'        => make_clickable( $tweet->text ),
					'created_at'  => $tweet->created_at,
					'screen_name' => $tweet->user->screen_name
				);
			}
		}

		return $result;

	}

}