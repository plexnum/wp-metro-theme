<?php

class Plex_Widget_Metro_ShortCode extends Plex_Widget_Metro_AbstractWidget {

	protected $sanitize_rules = array(
		'content' => 'post'
	);

	/**
	 * Get widget name
	 * @return string
	 */
	function get_name() {
		return __( 'Plex Metro ShortCode Widget', plex_get_trans_domain() );
	}

	/**
	 * Get widget description
	 * @return string
	 */
	function get_desc() {
		return __( 'Plex Metro ShortCode Widget', plex_get_trans_domain() );
	}

	/**
	 * @param $new_instance
	 * @param $old_instance
	 *
	 * @return mixed
	 */
	function save( $new_instance, $old_instance ) {
		return $new_instance;
	}

	function form( $instance ) {

		$this->view->set( 'metro', parent::form( $instance ) );
		$this->view->add( (array) $instance );

		if ( is_array( $instance ) ) {
			$this->view->add( $instance );
		}

		$this->view->display( '/metro/short-code/form.php' );

	}

	function display( $args, $instance ) {
		$this->view->set( 'args', $args );
		$this->view->display('/metro/short-code/front-page.php');
	}


}