<?php

class Plex_Widget_Metro_YoutubeVideo extends Plex_Widget_Metro_AbstractVideo {
	/**
	 * Get widget name
	 * @return string
	 */
	function get_name() {
		return __( 'Plex Metro Youtube Video', plex_get_trans_domain() );
	}

	/**
	 * Get widget description
	 * @return string
	 */
	function get_desc() {
		return __( 'Plex Metro Youtube Video', plex_get_trans_domain() );
	}

	function save( $new_instance, $old_instance ) {

		$save_strategy = Plex_Component_Video_VideoSaveStrategyFactory::create(
			'youtube',
			$new_instance
		);

		return $save_strategy->save();

	}

	function get_popup_view() {
		return $this->view->get_output( '/metro/video/youtube-popup.php' );
	}


}