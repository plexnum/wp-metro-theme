<?php

abstract class Plex_Widget_Metro_AbstractWidget extends Plex_Model_AbstractWidget {

	protected $sizes = array(
		'plex_tile_140_150' => array( 'label' => '2X2', 'cls' => 'span2 inch2' ),
		'plex_tile_300_150' => array( 'label' => '4X2', 'cls' => 'span4 inch2' ),
		'plex_tile_140_320' => array( 'label' => '2X4', 'cls' => 'span2 inch4' ),
		'plex_tile_300_320' => array( 'label' => '4X4', 'cls' => 'span4 inch4' ),
		'plex_tile_300_490' => array( 'label' => '4X6', 'cls' => 'span4 inch6' ),
		'plex_tile_460_320' => array( 'label' => '6X4', 'cls' => 'span6 inch4' ),
		'plex_tile_460_490' => array( 'label' => '6X4', 'cls' => 'span6 inch6' )
	);

	protected $color_types = array();


	function __construct() {

		parent::__construct();

		$this->sanitize_rules['size']       = 'attr';
		$this->sanitize_rules['color_type'] = 'attr';

		$this->color_types = array(
			'tile-type'  => __( 'Default', plex_get_trans_domain() ),
			'tile-type1' => __( 'Color 1', plex_get_trans_domain() ),
			'tile-type2' => __( 'Color 2', plex_get_trans_domain() )
		);

	}

	function decide_size_css_classes( $size_key ) {
		$default = 'span2 inch2';
		if ( isset( $this->sizes[$size_key] ) ) {
			return $this->sizes[$size_key]['cls'];
		}

		if ( $size_key === false ) {
			return '';
		}

		return $default;
	}

	function get_sizes() {
		return $this->sizes;
	}

	function get_color_types() {
		return $this->color_types;
	}

	abstract function display( $args, $instance );

	function widget( $args, $instance ) {

		$this->view->add( (array) $instance );

		if ( $args['id'] == 'front-page-sidebar' ) {
			$this->before_widget( $args );
			$this->display( $args, $instance );
			$this->after_widget( $args );
		}
		else {
			plex_wrap_before_widget( $args );
			$this->display( $args, $instance );
			plex_wrap_after_widget( $args );
		}

	}

	function form( $instance ) {
		$this->view->add( (array) $instance );
		$this->view->set( 'sizes', $this->get_sizes() );
		$this->view->set( 'color_types', $this->get_color_types() );
		return $this->view->get_output( '/metro/form.php' );
	}

	function before_widget( $args, $tag = 'div', $classes = '', $html_args = '' ) {
		plex_wrap_before_widget( $args, $tag, $this->decide_size_css_classes(
			$this->view->get( 'size', 'plex_tile_140_140' )
		) . ' js-tile js-animate ' . $classes, $html_args );
	}

	function after_widget( $args, $tag = 'div' ) {
		plex_wrap_after_widget( $args, $tag );
	}

}