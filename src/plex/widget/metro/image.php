<?php

/**
 *
 * Image widget specific to metro theme
 *
 * Class Plex_Widget_Metro_Image
 */
class Plex_Widget_Metro_Image extends Plex_Widget_Metro_AbstractWidget {

	protected $sanitize_rules = array(
		'url'              => 'url',
		'image'            => 'url',
		'attachment_id'    => 'abs_int',
		'header'           => 'text_field',
		'description'      => 'html_data',
		'author'           => 'text_field',
		'with_date'        => 'bool',
		'with_description' => 'bool',
		'date'             => 'date',
		'width'            => 'abs_int',
		'height'           => 'abs_int',
		'alt'              => 'text_field',
		'error'            => 'text_field',
		'in_new_page'      => 'bool'
	);

	/**
	 * Get widget name
	 * @return mixed
	 */
	function get_name() {
		return __( 'Plex Metro Image', plex_get_trans_domain() );
	}

	/**
	 * Get widget description
	 * @return string
	 */
	function get_desc() {
		return __( 'Plex Metro Image', plex_get_trans_domain() );
	}

	function form( $instance ) {
		$this->view->set( 'metro', parent::form( $instance ) );
		$this->view->add( (array) $instance );
		$this->view->display( '/metro/image/form.php' );
	}

	/**
	 * @param $new_instance
	 * @param $old_instance
	 *
	 * @return mixed|void
	 */
	function save( $new_instance, $old_instance ) {
		$save_strategy = new Plex_Component_Image_SaveStrategy( $new_instance );
		return $save_strategy->save();

	}

	function display( $args, $instance ) {
		$this->view->set( 'args', $args );
		$this->view->display('/metro/image/front-page.php');
	}

	function add_styles() {
		wp_enqueue_style( 'jquery_ui_flick' );
	}

	function add_scripts() {
		wp_enqueue_media();
		wp_enqueue_script( 'jquery-ui-datepicker' );
	}

}