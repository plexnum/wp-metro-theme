<?php

/**
 * Icon widget specific to metro theme
 *
 * Class Plex_Widget_Metro_Icon
 */
class Plex_Widget_Metro_Icon extends Plex_Widget_Metro_AbstractWidget {

	private $config_manager;

	protected $sanitize_rules = array(
		'header'      => 'text_field',
		'url'         => 'url',
		'icon'        => 'text_field',
		'in_new_page' => 'bool'
	);

	function __construct() {
		$this->config_manager = plex_get_theme_conf_bag();
		parent::__construct();
	}

	function get_name() {
		return __( 'Metro Icon', plex_get_trans_domain() );
	}

	function get_desc() {
		return __( 'Metro Icon', plex_get_trans_domain() );
	}

	function form( $instance ) {

		if ( is_array( $instance ) ) {
			$this->view->add( $instance );
		}

		$this->view->set( 'icons', $this->config_manager->get( 'icons' ) );
		$this->view->display( '/metro/icon/form.php' );

	}

	function save( $new_instance, $old_instance ) {
		return $new_instance;
	}

	function display( $args, $instance ) {
		$this->view->set( 'args', $args );
		$this->view->display('/metro/icon/front-page.php');
	}

	function add_styles() {
		wp_enqueue_style( 'plex_icons' );
	}
}