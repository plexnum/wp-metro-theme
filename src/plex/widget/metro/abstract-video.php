<?php

abstract class Plex_Widget_Metro_AbstractVideo extends Plex_Widget_Metro_AbstractWidget {

	protected $sanitize_rules = array(
		'url'              => 'url',
		'attachment_id'    => 'abs_int',
		'image'            => 'url',
		'with_description' => 'bool',
		'header'           => 'text_field',
		'description'      => 'html_data',
		'author'           => 'text_field',
		'date'             => 'date',
		'width'            => 'abs_int',
		'height'           => 'abs_int',
		'error'            => 'text_field',
		'video_id'         => 'attr',
		'link'             => 'url',
		'in_new_page'      => 'bool'
	);

	function form( $instance ) {
		$this->view->add( (array) $instance );
		$this->view->set( 'metro', parent::form( $instance ) );
		$this->view->display( '/metro/video/form.php' );
	}

	function display( $args, $instance ) {
		$this->view->set( 'args', $args );
		$this->view->set( 'widget_id', $args['widget_id'] );
		$this->view->set( 'popup_view', $this->get_popup_view() );
		$this->view->display('/metro/video/front-page.php');
	}

	function add_styles() {
		wp_enqueue_style( 'jquery_ui_flick' );
	}

	function add_scripts() {
		wp_enqueue_script( 'jquery-ui-datepicker' );
	}

	abstract function get_popup_view();

}