<?php

class Plex_Widget_TagCloud extends WP_Widget_Tag_Cloud {

	function widget( $args, $instance ) {

		if ( $args['id'] == 'front-page-sidebar' ) {
			plex_wrap_before_metro_widget();
			parent::widget( $args, $instance );
			plex_wrap_after_metro_widget();
		}
		else {
			plex_wrap_before_widget( $args, 'div', 'tag-cloud' );
			parent::widget( $args, $instance );
			plex_wrap_after_widget( $args, 'div' );
		}
	}

}