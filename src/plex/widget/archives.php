<?php

class Plex_Widget_Archives extends WP_Widget_Archives {

	function widget( $args, $instance ) {

		if ( $args['id'] == 'front-page-sidebar' ) {
			plex_wrap_before_metro_widget();
			parent::widget( $args, $instance );
			plex_wrap_after_metro_widget();
		}
		else {
			plex_wrap_before_widget( $args, 'div', 'archive-list' );
			parent::widget( $args, $instance );
			plex_wrap_after_widget( $args, 'div' );
		}

	}

}