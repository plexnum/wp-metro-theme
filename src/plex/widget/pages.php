<?php
class Plex_Widget_Pages extends WP_Widget_Pages {

	function widget( $args, $instance ) {

		if ( $args['id'] == 'front-page-sidebar' ) {
			plex_wrap_before_metro_widget();
			parent::widget( $args, $instance );
			plex_wrap_after_metro_widget();
		}
		else {
			plex_wrap_before_widget( $args, 'div', 'page-list' );
			parent::widget( $args, $instance );
			plex_wrap_after_widget( $args, 'div' );
		}

	}

}