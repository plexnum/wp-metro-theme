<?php

class Plex_Widget_Gallery extends Plex_Model_AbstractWidget {

	protected $sanitize_rules = array(
		'short_code' => 'html_data'
	);

	/**
	 * Get widget name
	 * @return mixed
	 */
	function get_name() {
		return __( 'Plex Gallery', plex_get_trans_domain() );
	}

	function get_desc() {
		return __( 'Plex Gallery', plex_get_trans_domain() );
	}

	function form( $instance ) {
		$this->view->add( (array) $instance );
		$this->view->display( '/gallery/form.php' );

	}

	function save( $new_instance, $old_instance ) {
		return $new_instance;
	}

	function widget( $args, $instance ) {
		if ( $args['id'] === 'front-page-sidebar' ) {
			$this->view->add( $instance );
			$this->view->display( '/gallery/front-page.php' );
		}
		else {
			$this->view->add( $instance );
			plex_wrap_before_widget( $args, 'div' );
			$this->view->display( '/gallery/second-page.php' );
			plex_wrap_after_widget( $args, 'div' );
		}
	}

}