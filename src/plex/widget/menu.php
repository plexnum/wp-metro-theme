<?php

class Plex_Widget_Menu extends WP_Nav_Menu_Widget {

	function widget( $args, $instance ) {

		if ( $args['id'] == 'front-page-sidebar' ) {
			plex_wrap_before_metro_widget();
			parent::widget( $args, $instance );
			plex_wrap_after_metro_widget();
		}
		else {
			parent::widget( $args, $instance );
		}

	}

}