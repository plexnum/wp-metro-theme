<?php

if ( ! function_exists( 'plex_get_trans_domain' ) ) {

	/**
	 * Get current theme translation domain
	 * @return string
	 */
	function plex_get_trans_domain() {
		return Plex_Application_ThemeApplication::$theme_localization_domain;
	}

}

if ( ! function_exists( 'plex_get_theme_conf_bag' ) ) {

	/**
	 * Get current theme config bag
	 * @return Plex_Config_ParamBag
	 */
	function plex_get_theme_conf_bag() {
		return Plex_Config_ParamBag::instantiate();
	}

}

if ( ! function_exists( 'plex_get_default_theme_opts' ) ) {

	/**
	 * Get default theme options
	 *
	 * @param bool $fresh - fresh copy or cached copy
	 *
	 * @return array
	 * @throws Plex_Exception_FileException
	 */
	function plex_get_default_theme_opts( $fresh = false ) {

		static $configurations;

		// load configs from file
		if ( ! $configurations || $fresh == true ) {

			$config_manager = plex_get_theme_conf_bag();
			$path           = $config_manager->get( 'patches[theme_default_configs]', false, true );

			$configurations = plex_include_file( $path, 'Default theme configurations not existed' );
			if ( ! is_array( $configurations ) ) {
				$configurations = array();
			}
			return $configurations;

		}

		return $configurations;
	}

}

if ( ! function_exists( 'plex_get_theme_opts' ) ) {

	/**
	 * Get current theme options
	 * @return array
	 */
	function plex_get_theme_opts() {
		return get_option( plex_get_theme_opts_id(), array() );
	}

}

if ( ! function_exists( 'plex_get_theme_opts_bag' ) ) {

	/**
	 * Return theme options as ParamBag instance
	 *
	 * @param bool $fresh - fresh copy or cached copy
	 *
	 * @return Plex_Config_ParamBag
	 */
	function plex_get_theme_opts_bag( $fresh = false ) {

		static $cm;

		if ( ! $cm || $fresh == true ) {
			$cm = new Plex_Config_ParamBag( plex_get_theme_opts() );
		}

		return $cm;

	}

}

if ( ! function_exists( 'plex_merge_theme_opts' ) ) {

	/**
	 * Merge theme options
	 *
	 * @param array $options
	 */
	function plex_merge_theme_opts( array $options ) {
		$old_options =& plex_get_theme_opts();
		update_option( plex_get_theme_opts_id(), array_merge( $old_options, $options ) );
	}

}

if ( ! function_exists( 'plex_get_theme_opts_id' ) ) {

	/**
	 * Get theme options id
	 * @return mixed
	 */
	function plex_get_theme_opts_id() {
		$config_manager = plex_get_theme_conf_bag();
		return $config_manager->get( 'option_identities[theme]', false, true );
	}

}

if ( ! function_exists( 'plex_get_twitter_opts_id' ) ) {

	/**
	 * Get twitter options id
	 * @return mixed
	 */
	function plex_get_twitter_opts_id() {
		$config_manager = plex_get_theme_conf_bag();
		return $config_manager->get( 'option_identities[twitter]', false, true );
	}

}

if ( ! function_exists( 'plex_include_file' ) ) {

	/**
	 * Include file but if file does not exist throw an Exception
	 *
	 * @param        $path
	 * @param bool   $return
	 * @param string $exception_message
	 *
	 * @return mixed
	 * @throws Plex_Exception_Common
	 */
	function plex_include_file( $path, $return = true, $exception_message = 'File %s not found' ) {

		if ( file_exists( $path ) ) {

			if ( $return == true ) {
				$result = include $path;
				return $result;
			}
			else {
				require $path;
			}

		}
		else {
			throw new Plex_Exception_Common( sprintf( $exception_message, basename( $path ) ) );
		}
	}

}

if ( ! function_exists( 'plex_class_name_to_identity' ) ) {

	/**
	 * Take from class name format identity format
	 *
	 * @param $class_name
	 *
	 * @return string
	 */
	function plex_class_name_to_identity( $class_name ) {
		return strtolower( preg_replace( '/((?<!_|^)[A-Z])/', '-$0', $class_name ) );
	}

}

/**
 * Get image url
 */
if ( ! function_exists( 'plex_get_imgs_dir_url' ) ) {

	/**
	 * Get images dir url
	 * @return mixed
	 */
	function plex_get_imgs_dir_url() {
		$config_manager = plex_get_theme_conf_bag();
		return $config_manager->get( 'urls[assets][images]', false, true );
	}

}

if ( ! function_exists( 'plex_imgs_url' ) ) {

	/**
	 * Short name function for get images dir url
	 * @return mixed
	 */
	function plex_imgs_url() {
		return plex_get_imgs_dir_url();
	}

}

if ( ! function_exists( 'plex_e_imgs_url' ) ) {

	/**
	 * Short name function for get images dir url and echo it
	 */
	function plex_e_imgs_url() {
		echo plex_get_imgs_dir_url();
	}

}

if ( ! function_exists( 'plex_get_fixture_dir_url' ) ) {

	/**
	 * Get fixture dir url
	 * @return mixed
	 */
	function plex_get_fixture_dir_url() {
		$config_manager = plex_get_theme_conf_bag();
		return $config_manager->get( 'urls[assets][fixtures]', false, true );
	}

}

if ( ! function_exists( 'plex_fix_url' ) ) {

	/**
	 * Short name function for get fixture dir url
	 * @return mixed
	 */
	function plex_fix_url() {
		return plex_get_fixture_dir_url();
	}

}

if ( ! function_exists( 'plex_e_fix_url' ) ) {

	/**
	 * Short name function for get fixture dir url and echo it
	 */
	function plex_e_fix_url() {
		echo plex_get_fixture_dir_url();
	}

}


if ( ! function_exists( 'plex_build_control' ) ) {

	/**
	 * Used in theme customizer declaration file to defer loading of ThemeCustomizer Control
	 * to resolve it dependencies on initialization step of Plex_Component_ThemeCustomizer_Initializer
	 *
	 * @param $control_class
	 * @param $options
	 *
	 * @return Plex_Component_ThemeCustomizer_ControlBuilder
	 */
	function plex_build_control( $control_class, $options = array() ) {
		return new Plex_Component_ThemeCustomizer_ControlBuilder( $control_class, $options );
	}

}

if ( ! function_exists( 'plex_get_upload_file_by_url' ) ) {

	/**
	 * @param $url
	 *
	 * @return bool|mixed
	 */
	function plex_get_upload_file_by_url( $url ) {

		$url = str_replace( '..', '', $url );

		$upload_dir_info = wp_upload_dir( null );

		if ( strpos( $url, $upload_dir_info['baseurl'] ) !== false ) {
			return str_replace( $upload_dir_info['baseurl'], $upload_dir_info['basedir'], $url );
		}

		return false;

	}

}

if ( ! function_exists( 'plex_youtube_get_id_by_url ' ) ) {

	/**
	 * Get youtube video id by url
	 *
	 * @param $url
	 *
	 * @return bool
	 */
	function plex_youtube_get_id_by_url( $url ) {

		static $cache;

		if ( isset( $cache[$url] ) ) return $cache[$url];

		$pattern = '#^(?:https?://)?'; # Optional URL scheme. Either http or https.
		$pattern .= '(?:www\.)?'; #  Optional www subdomain.
		$pattern .= '(?:'; #  Group host alternatives:
		$pattern .= 'youtu\.be/'; #    Either youtu.be,
		$pattern .= '|youtube\.com'; #    or youtube.com
		$pattern .= '(?:'; #    Group path alternatives:
		$pattern .= '/embed/'; #      Either /embed/,
		$pattern .= '|/v/'; #      or /v/,
		$pattern .= '|/watch\?v='; #      or /watch?v=,
		$pattern .= '|/watch\?.+&v='; #      or /watch?other_param&v=
		$pattern .= ')'; #    End path alternatives.
		$pattern .= ')'; #  End host alternatives.
		$pattern .= '([\w-]{11})'; # 11 characters (Length of Youtube video ids).
		$pattern .= '(?:.+)?$#x'; # Optional other ending URL parameters.
		preg_match( $pattern, $url, $matches );

		if ( isset( $matches[1] ) ) {
			$cache[$url] = $matches[1];
			return $cache[$url];
		}
		else {
			return false;
		}

	}

}

if ( ! function_exists( 'plex_vimeo_get_id_by_url ' ) ) {

	/**
	 * Get information about video from vimeo server
	 *
	 * @param $url
	 *
	 * @return mixed
	 * @throws Plex_Exception_Common
	 */
	function plex_vimeo_get_info_by_url( $url ) {

		static $cache;

		if ( isset( $cache[$url] ) ) return $cache[$url];

		$response = wp_remote_get( 'http://vimeo.com/api/oembed.json?url=' . esc_url( $url ) );

		if ( is_wp_error( $response ) ) {
			throw new Plex_Exception_Common( __( "Can't connect to vimeo server", plex_get_trans_domain() ) );
		}

		$result = json_decode( wp_remote_retrieve_body( $response ) );

		if ( ! isset( $result->video_id ) ) {
			throw new Plex_Exception_Common( __( 'Vimeo url have not valid format', plex_get_trans_domain() ) );
		}

		$cache[$url] = $result;

		return $cache[$url];
	}

}

if ( ! function_exists( 'plex_get_assets_url ' ) ) {

	function plex_get_assets_url() {
		$cm = plex_get_theme_conf_bag();
		return $cm->get( 'urls[assets_root]', '', true );
	}

}


if ( ! function_exists( 'plex_get_post_icon' ) ) {

	function plex_get_post_icon( $post_id = null, $default_icon = '' ) {

		$post_id = ( null === $post_id ) ? get_the_ID() : $post_id;

		$plex_icon = get_post_meta( $post_id, '_plex_icon', true );

		if ( ! empty( $plex_icon['icon'] ) ) {
			return $plex_icon['icon'];
		}

		return $default_icon;
	}

}


if ( ! function_exists( 'plex_get_post_gallery_short_code' ) ) {

	function plex_get_post_gallery_short_code( $post_id = null ) {

		$post_id = ( null === $post_id ) ? get_the_ID() : $post_id;

		$plex_short_code = get_post_meta( $post_id, '_plex_featured_gallery', true );

		if ( ! empty( $plex_short_code['short_code'] ) ) {
			return $plex_short_code['short_code'];
		}

		return '';

	}

}

if ( ! function_exists( 'plex_get_post_video' ) ) {

	function plex_get_post_video( $post_id = null ) {

		$post_id = ( null === $post_id ) ? get_the_ID() : $post_id;

		$plex_video_data = new Plex_Config_ParamBag(
			get_post_meta( $post_id, '_plex_featured_video', true )
		);

		$plex_video_type = $plex_video_data->get( 'type' );

		$plex_video_data = new Plex_Config_ParamBag(
			$plex_video_data->get( $plex_video_type, array() )
		);

		$plex_video_data->set( 'type', $plex_video_type );

		if ( ! $plex_video_data->get( 'short_code' ) &&
				! $plex_video_data->get( 'video_id' )
		) {
			return false;
		}

		return $plex_video_data;
	}

}

if ( ! function_exists( ( 'plex_get_post_audio' ) ) ) {

	function plex_get_post_audio( $post_id = null ) {

		$post_id = ( null === $post_id ) ? get_the_ID() : $post_id;

		$plex_audio_data = new Plex_Config_ParamBag(
			get_post_meta( $post_id, '_plex_featured_audio', true )
		);

		$plex_audio_type = $plex_audio_data->get( 'type' );

		$plex_audio_data = new Plex_Config_ParamBag(
			$plex_audio_data->get( $plex_audio_type, array() )
		);

		$plex_audio_data->set( 'type', $plex_audio_type );

		if ( ! $plex_audio_data->get( 'audio_id' )
				&& ! $plex_audio_data->get( 'short_code' )
		) {
			return false;
		}

		return $plex_audio_data;

	}

}

if ( ! function_exists( 'plex_wrap_before_widget' ) ) {


	function plex_wrap_before_widget( &$args, $element = '', $class = '', $html_args = '' ) {

		if ( ! is_array( $args ) ) {
			return;
		}

		if ( isset( $args['after_widget'] ) ) {
			$args['_after_widget'] = $args['after_widget'];
			$args['after_widget']  = '';
		}

		if ( isset( $args['before_widget'] ) ) {
			echo $args['before_widget'];
			$args['before_widget'] = '';
		}

		if ( $class ) {
			$class = 'class="' . $class . '"';
		}

		if ( $element ) {
			echo "<" . $element . " " . $html_args . " " . $class . ">";
		}

	}

}


if ( ! function_exists( 'plex_wrap_after_widget' ) ) {

	function plex_wrap_after_widget( &$args, $element = "" ) {

		if ( ! is_array( $args ) ) {
			return;
		}

		if ( isset( $args['_after_widget'] ) ) {
			echo $args['_after_widget'];
			$args['_after_widget'] = '';
		}

		if ( $element ) {
			echo " </" . $element . ">";
		}

	}

}

if ( ! function_exists( 'plex_script_template_wrapper' ) ) {

	function plex_script_template_wrapper( $html, $id ) {
		return plex_script_wrapper( $html, $id, 'text/template' );
	}

}

if ( ! function_exists( 'plex_script_wrapper' ) ) {

	function plex_script_wrapper( $html, $id, $type = 'text/javascript' ) {
		$result = "<script type='" . $type . "' id='" . $id . "'>";
		$result .= $html;
		$result .= "</script>";
		return $result;
	}

}

if ( ! function_exists( 'plex_get_attachment_image' ) ) {

	function plex_get_attachment_image( $post_id = null, $size ) {
		$post_id = ( null === $post_id ) ? get_the_ID() : $post_id;

		$img = wp_get_attachment_image_src( $post_id, $size );
		if ( $img ) {
			return $img[0];
		}
		return '';
	}

}

if ( ! function_exists( 'plex_do_short_code' ) ) {

	function plex_do_short_code( $content, $without_html = false ) {

		if ( $without_html == true ) {

			preg_match_all( '#' . get_shortcode_regex() . '#i', $content, $matches );

			$content = '';

			if ( isset( $matches[0] ) && is_array( $matches[0] ) ) {
				$content = join( '', $matches[0] );
			}

		}

		$content = preg_replace( '#^<\/p>|^<br \/>|<p>$#', '', $content );

		return do_shortcode( shortcode_unautop( $content ) );
	}

}

if ( ! function_exists( 'plex_create_short_codes' ) ) {

	function plex_create_short_codes( $matches, $attributes = array(), $include_old_attrs = true ) {

		$plex_short_codes = '';

		foreach ( array( 2, 3, 4 ) as $plex_num ) {
			if ( ! isset( $matches[$plex_num] ) && ! is_array( $matches[$plex_num] ) ) {
				return $plex_short_codes;
			}
		}

		foreach ( $matches[2] as $key => $tag ) {

			$plex_attributes_str = '';
			$plex_old_attributes = '';

			// Only for specific tag
			if ( isset( $attributes[$key] ) && is_array( $attributes[$key] ) ) {

				foreach ( $attributes[$key] as $attr_name => $attr_val ) {
					$plex_attributes_str .= $attr_name . '="' . $attr_val . '" ';
				}

			}

			// For all tags
			foreach ( $attributes as $attr_name => $attr_val ) {

				if ( ! is_array( $attr_val ) ) {
					$plex_attributes_str .= $attr_name . '="' . $attr_val . '" ';
				}

			}

			if ( isset( $matches[3][$key] ) && $include_old_attrs == true ) {
				$plex_old_attributes = $matches[3][$key];
			}

			$plex_short_codes .= '[' . $tag . ' ' . $plex_old_attributes . ' ' . $plex_attributes_str . ']' . $matches[5][$key] . '[/' . $tag . ']';

		}

		return $plex_short_codes;

	}

}

if ( ! function_exists( 'plex_get_string_between' ) ) {

	function plex_get_string_between( $string, $start, $end ) {
		$string = " " . $string;
		$ini    = strpos( $string, $start );
		if ( $ini == 0 ) return "";
		$ini += strlen( $start );
		$len = strpos( $string, $end, $ini ) - $ini;
		return substr( $string, $ini, $len );
	}

}

if ( ! function_exists( 'plex_get_theme_mod' ) ) {

	/**
	 * Return result of option. if result is empty than return a default value. 0 not counted
	 * as empty result
	 *
	 * @param      $option  option name
	 * @param null $default default value
	 *
	 * @return int|null
	 */
	function plex_get_theme_mod( $option, $default = null ) {

		$plex_result = get_theme_mod( $option, $default );

		if ( ! is_int( $plex_result ) && empty( $plex_result ) ) {
			return $default;
		}

		return $plex_result;

	}

}

if ( ! function_exists( 'plex_convert_to_rgb' ) ) {

	function plex_convert_to_rgb( $hex ) {

		$hex = str_replace( "#", "", $hex );

		return array(
			'r' => hexdec( substr( $hex, 0, 2 ) ),
			'g' => hexdec( substr( $hex, 2, 2 ) ),
			'b' => hexdec( substr( $hex, 4, 2 ) )
		);
	}

}

if ( ! function_exists( 'plex_rgb_difference' ) ) {

	function plex_rgb_difference( $rgb1, $rgb2 ) {

		return array(
			'r' => ( $rgb1['r'] > $rgb2['r'] ) ? $rgb1['r'] - $rgb2['r'] : 0,
			'g' => ( $rgb1['g'] > $rgb2['g'] ) ? $rgb1['g'] - $rgb2['g'] : 0,
			'b' => ( $rgb1['b'] > $rgb2['b'] ) ? $rgb1['b'] - $rgb2['b'] : 0,
		);

	}

}

if ( ! function_exists( 'wp_get_audio_extensions' ) ) {

	/**
	 * Return a filtered list of WP-supported audio formats
	 *
	 * Copy from wordpress 3.6 source code to support audio short code in wordpress 3.5
	 *
	 * @return array
	 */
	function wp_get_audio_extensions() {
		return apply_filters( 'wp_audio_extensions', array( 'mp3', 'ogg', 'wma', 'm4a', 'wav' ) );
	}

}


if ( ! function_exists( 'wp_audio_shortcode' ) ) {

	/**
	 * The Audio shortcode.
	 *
	 * This implements the functionality of the Audio Shortcode for displaying
	 * WordPress mp3s in a post.
	 *
	 * Copy from wordpress 3.6 source code to support audio short code in wordpress 3.5
	 *
	 * @param array $attr Attributes of the shortcode.
	 *
	 * @return string HTML content to display audio.
	 */
	function wp_audio_shortcode( $attr ) {
		$post_id = get_post() ? get_the_ID() : 0;

		static $instances = 0;
		$instances ++;

		$audio = null;

		$default_types = wp_get_audio_extensions();
		$defaults_atts = array(
			'src'      => '',
			'loop'     => '',
			'autoplay' => '',
			'preload'  => 'none'
		);
		foreach ( $default_types as $type ) {
			$defaults_atts[$type] = '';
		}

		$atts = shortcode_atts( $defaults_atts, $attr, 'audio' );
		extract( $atts );

		$primary = false;
		if ( ! empty( $src ) ) {
			$type = wp_check_filetype( $src, wp_get_mime_types() );
			if ( ! in_array( $type['ext'], $default_types ) )
				return sprintf( '<a class="wp-embedded-audio" href="%s">%s</a>', esc_url( $src ), esc_html( $src ) );
			$primary = true;
			array_unshift( $default_types, 'src' );
		}
		else {
			foreach ( $default_types as $ext ) {
				if ( ! empty( $$ext ) ) {
					$type = wp_check_filetype( $$ext, wp_get_mime_types() );
					if ( $type['ext'] === $ext )
						$primary = true;
				}
			}
		}

		if ( ! $primary ) {
			$audios = get_attached_media( 'audio', $post_id );
			if ( empty( $audios ) )
				return;

			$audio = reset( $audios );
			$src   = wp_get_attachment_url( $audio->ID );
			if ( empty( $src ) )
				return;

			array_unshift( $default_types, 'src' );
		}

		$library = apply_filters( 'wp_audio_shortcode_library', 'mediaelement' );
		if ( 'mediaelement' === $library && did_action( 'init' ) ) {
			wp_enqueue_style( 'wp-mediaelement' );
			wp_enqueue_script( 'wp-mediaelement' );
		}

		$atts = array(
			'class'    => apply_filters( 'wp_audio_shortcode_class', 'wp-audio-shortcode' ),
			'id'       => sprintf( 'audio-%d-%d', $post_id, $instances ),
			'loop'     => $loop,
			'autoplay' => $autoplay,
			'preload'  => $preload,
			'style'    => 'width: 100%',
		);

		// These ones should just be omitted altogether if they are blank
		foreach ( array( 'loop', 'autoplay', 'preload' ) as $a ) {
			if ( empty( $atts[$a] ) )
				unset( $atts[$a] );
		}

		$attr_strings = array();
		foreach ( $atts as $k => $v ) {
			$attr_strings[] = $k . '="' . esc_attr( $v ) . '"';
		}

		$html = '';
		if ( 'mediaelement' === $library && 1 === $instances )
			$html .= "<!--[if lt IE 9]><script>document.createElement('audio');</script><![endif]-->\n";
		$html .= sprintf( '<audio %s controls="controls">', join( ' ', $attr_strings ) );

		$fileurl = '';
		$source  = '<source type="%s" src="%s" />';
		foreach ( $default_types as $fallback ) {
			if ( ! empty( $$fallback ) ) {
				if ( empty( $fileurl ) )
					$fileurl = $$fallback;
				$type = wp_check_filetype( $$fallback, wp_get_mime_types() );
				$html .= sprintf( $source, $type['type'], esc_url( $$fallback ) );
			}
		}

		if ( 'mediaelement' === $library )
			$html .= wp_mediaelement_fallback( $fileurl );
		$html .= '</audio>';

		return apply_filters( 'wp_audio_shortcode', $html, $atts, $audio, $post_id, $library );
	}

	add_shortcode( 'audio', apply_filters( 'wp_audio_shortcode_handler', 'wp_audio_shortcode' ) );

}

if ( ! function_exists( 'wp_get_video_extensions' ) ) {

	/**
	 * Return a filtered list of WP-supported video formats
	 *
	 * Copy from wordpress 3.6 source code to support audio short code in wordpress 3.5
	 *
	 * @return array
	 */
	function wp_get_video_extensions() {
		return apply_filters( 'wp_video_extensions', array( 'mp4', 'm4v', 'webm', 'ogv', 'wmv', 'flv' ) );
	}

}

if ( ! function_exists( 'wp_video_shortcode' ) ) {

	/**
	 * The Video shortcode.
	 *
	 * This implements the functionality of the Video Shortcode for displaying
	 * WordPress mp4s in a post.
	 *
	 * Copy from wordpress 3.6 source code to support video short code in wordpress 3.5
	 *
	 * @param array $attr Attributes of the shortcode.
	 *
	 * @return string HTML content to display video.
	 */
	function wp_video_shortcode( $attr ) {
		global $content_width;
		$post_id = get_post() ? get_the_ID() : 0;

		static $instances = 0;
		$instances ++;

		$video = null;

		$default_types = wp_get_video_extensions();
		$defaults_atts = array(
			'src'      => '',
			'poster'   => '',
			'loop'     => '',
			'autoplay' => '',
			'preload'  => 'metadata',
			'height'   => 360,
			'width'    => empty( $content_width ) ? 640 : $content_width,
		);

		foreach ( $default_types as $type ) {
			$defaults_atts[$type] = '';
		}

		$atts = shortcode_atts( $defaults_atts, $attr, 'video' );
		extract( $atts );

		$w = $width;
		$h = $height;
		if ( is_admin() && $width > 600 )
			$w = 600;
		elseif ( ! is_admin() && $w > $defaults_atts['width'] )
			$w = $defaults_atts['width'];

		if ( $w < $width )
			$height = round( ( $h * $w ) / $width );

		$width = $w;

		$primary = false;
		if ( ! empty( $src ) ) {
			$type = wp_check_filetype( $src, wp_get_mime_types() );
			if ( ! in_array( $type['ext'], $default_types ) )
				return sprintf( '<a class="wp-embedded-video" href="%s">%s</a>', esc_url( $src ), esc_html( $src ) );
			$primary = true;
			array_unshift( $default_types, 'src' );
		}
		else {
			foreach ( $default_types as $ext ) {
				if ( ! empty( $$ext ) ) {
					$type = wp_check_filetype( $$ext, wp_get_mime_types() );
					if ( $type['ext'] === $ext )
						$primary = true;
				}
			}
		}

		if ( ! $primary ) {
			$videos = get_attached_media( 'video', $post_id );
			if ( empty( $videos ) )
				return;

			$video = reset( $videos );
			$src   = wp_get_attachment_url( $video->ID );
			if ( empty( $src ) )
				return;

			array_unshift( $default_types, 'src' );
		}

		$library = apply_filters( 'wp_video_shortcode_library', 'mediaelement' );
		if ( 'mediaelement' === $library && did_action( 'init' ) ) {
			wp_enqueue_style( 'wp-mediaelement' );
			wp_enqueue_script( 'wp-mediaelement' );
		}

		$atts = array(
			'class'    => apply_filters( 'wp_video_shortcode_class', 'wp-video-shortcode' ),
			'id'       => sprintf( 'video-%d-%d', $post_id, $instances ),
			'width'    => absint( $width ),
			'height'   => absint( $height ),
			'poster'   => esc_url( $poster ),
			'loop'     => $loop,
			'autoplay' => $autoplay,
			'preload'  => $preload,
		);

		// These ones should just be omitted altogether if they are blank
		foreach ( array( 'poster', 'loop', 'autoplay', 'preload' ) as $a ) {
			if ( empty( $atts[$a] ) )
				unset( $atts[$a] );
		}

		$attr_strings = array();
		foreach ( $atts as $k => $v ) {
			$attr_strings[] = $k . '="' . esc_attr( $v ) . '"';
		}

		$html = '';
		if ( 'mediaelement' === $library && 1 === $instances )
			$html .= "<!--[if lt IE 9]><script>document.createElement('video');</script><![endif]-->\n";
		$html .= sprintf( '<video %s controls="controls">', join( ' ', $attr_strings ) );

		$fileurl = '';
		$source  = '<source type="%s" src="%s" />';
		foreach ( $default_types as $fallback ) {
			if ( ! empty( $$fallback ) ) {
				if ( empty( $fileurl ) )
					$fileurl = $$fallback;
				$type = wp_check_filetype( $$fallback, wp_get_mime_types() );
				// m4v sometimes shows up as video/mpeg which collides with mp4
				if ( 'm4v' === $type['ext'] )
					$type['type'] = 'video/m4v';
				$html .= sprintf( $source, $type['type'], esc_url( $$fallback ) );
			}
		}
		if ( 'mediaelement' === $library )
			$html .= wp_mediaelement_fallback( $fileurl );
		$html .= '</video>';

		$html = sprintf( '<div style="width: %dpx; max-width: 100%%;">%s</div>', $width, $html );
		return apply_filters( 'wp_video_shortcode', $html, $atts, $video, $post_id, $library );
	}

	add_shortcode( 'video', apply_filters( 'wp_video_shortcode_handler', 'wp_video_shortcode' ) );

}

if ( ! function_exists( 'get_attached_media' ) ) {

	/**
	 * Retrieve media attached to the passed post
	 *
	 * Copy from wordpress 3.6 source code to support video short code in wordpress 3.5
	 *
	 * @param string $type (Mime) type of media desired
	 * @param mixed  $post Post ID or object
	 *
	 * @return array Found attachments
	 */
	function get_attached_media( $type, $post = 0 ) {
		if ( ! $post = get_post( $post ) )
			return array();

		$args = array(
			'post_parent'    => $post->ID,
			'post_type'      => 'attachment',
			'post_mime_type' => $type,
			'posts_per_page' => - 1,
			'orderby'        => 'menu_order',
			'order'          => 'ASC',
		);

		$args = apply_filters( 'get_attached_media_args', $args, $type, $post );

		$children = get_children( $args );

		return (array) apply_filters( 'get_attached_media', $children, $type, $post );
	}

}
if ( ! function_exists( 'wp_mediaelement_fallback' ) ) {

	function wp_mediaelement_fallback( $url ) {
		return apply_filters( 'wp_mediaelement_fallback', sprintf( '<a href="%1$s">%1$s</a>', esc_url( $url ) ), $url );
	}

}