<?php

class Plex_MetaBox_ServiceCharge extends Plex_Model_AbstractMetaBox {

	protected $sanitize_rules = array(
		'*items' => array(
			'item' => 'text_field'
		)
	);

	/**
	 * Meta key for saving data
	 * @return mixed
	 */
	function get_meta_key() {
		return '_plex_service_charge';
	}

	/**
	 * Get meta box title
	 * @return mixed
	 */
	function get_title() {
		return __('Included in service charge', plex_get_trans_domain());
	}

	/**
	 * @param $post
	 * @param $meta_data
	 *
	 * @return mixed
	 */
	function show( $post, $meta_data ) {

		$service_charge_item = new Plex_View();

		$service_charge_item->set_patch_prefix(
			$this->view->get_patch_prefix()
		);

		$service_charge_item->set('item', '<%= item %>');
		$this->view->add((array)$meta_data);
		$this->view->set(
			'template',
			plex_script_template_wrapper(
				$service_charge_item->get_output('/_service-charge-item.php'),
				'plexServiceChargeItem'
			)
		);

		$this->view->display('/service-charge.php');
	}

	/**
	 * Each meta box should implement that method
	 * and return data for save in one of these formats: Plex_Config_ParamBag or array
	 *
	 * @param $post_id
	 *
	 * @return mixed
	 */
	function save( $post_id ) {

		$post_data = new Plex_Config_ParamBag( $_POST );

		return array(
			'items' => $post_data->get( 'plex_service_charge', array() )
		);

	}

	function screen() {
		return array(
			'service'
		);
	}

}