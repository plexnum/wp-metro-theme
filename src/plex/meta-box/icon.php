<?php

class Plex_MetaBox_Icon extends Plex_Model_AbstractMetaBox {

	protected $config_manager;

	protected $sanitize_rules = array(
		'icon' => 'text_field'
	);

	function __construct() {
		$this->config_manager = plex_get_theme_conf_bag();
		parent::__construct();
	}

	function get_title() {
		return __( 'Icon', plex_get_trans_domain() );
	}

	function get_meta_key() {
		return '_plex_icon';
	}

	function screen() {
		return array(
			'post',
			'page'
		);
	}

	function context() {
		return 'side';
	}

	function enquire_styles() {
		wp_enqueue_style( 'plex_icons' );
	}

	function show( $post, $post_meta ) {
		$this->view->add( $post_meta );
		$this->view->set( 'icons', $this->config_manager->get( 'icons' ) );
		$this->view->display( '/icon.php' );
	}

	function save( $post_id ) {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			die( __( 'Permission denied', plex_get_trans_domain() ) );
		}

		$post_data = new Plex_Config_ParamBag( $_POST );

		return $post_data->get( 'plex_icon', array() );
	}

}