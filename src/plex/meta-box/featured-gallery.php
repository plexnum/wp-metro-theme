<?php

class Plex_MetaBox_FeaturedGallery extends Plex_Model_AbstractMetaBox {

	protected $sanitize_rules = array(
		'short_code' => 'html_data'
	);

	protected $size = 'plex_gallery';

	function get_meta_key() {
		return '_plex_featured_gallery';
	}

	function get_title() {
		return __( 'Featured Gallery', plex_get_trans_domain() );
	}

	function screen() {
		return array(
			'post',
			'project'
		);
	}

	function context() {
		return 'side';
	}

	function show( $post, $meta_data ) {
		$this->view->add( $meta_data );
		$this->view->set( 'size', $this->size );
		$this->view->display( '/featured-gallery.php' );
	}

	function save( $post_id ) {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			die( __( 'Permission denied', plex_get_trans_domain() ) );
		}

		$post_data = new Plex_Config_ParamBag( $_POST );
		$data      = new Plex_Config_ParamBag( (array) $post_data->get( 'plex_fg', array() ) );

		return $data;
	}


}