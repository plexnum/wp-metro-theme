<?php

class Plex_MetaBox_FeaturedAudio extends Plex_Model_AbstractMetaBox {

	protected $sanitize_rules = array(
		'type'               => 'in_list(plexAudio,
											 							 plexAudioShortCode):plexAudio',
		'plexAudio'          => array(
			'url'           => 'text_field',
			'audio_id'      => 'abs_int',
			'image'         => 'url',
			'width'         => 'abs_int',
			'height'        => 'abs_int',
			'attachment_id' => 'abs_int',
			'error'         => 'text_field'
		),
		'plexAudioShortCode' => array(
			'short_code'    => 'text_field',
			'image'         => 'url',
			'width'         => 'abs_int',
			'height'        => 'abs_int',
			'attachment_id' => 'abs_int',
			'error'         => 'text_field'
		),
		'error'              => 'text_field'
	);

	function get_meta_key() {
		return '_plex_featured_audio';
	}

	function get_title() {
		return __( 'Featured Audio', plex_get_trans_domain() );
	}

	function screen() {
		return array(
			'post',
			'project'
		);
	}

	function context() {
		return 'side';
	}

	function show( $post, $meta_data ) {
		$this->view->add( $meta_data );
		$this->view->display( '/featured-audio.php' );
	}

	function save( $post_id ) {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			die( __( 'Permission denied', plex_get_trans_domain() ) );
		}

		$post_data = new Plex_Config_ParamBag( $_POST );
		$data      = new Plex_Config_ParamBag( (array) $post_data->get( 'plex_fa', array() ) );

		$url        = $data->get( $data->get( 'type', 'none' ) . '[url]', false, true );
		$short_code = $data->get( $data->get( 'type', 'none' ) . '[short_code]', false, true );

		if ( ! $url && ! $short_code ) return;

		// Save strategy for image
		$save_strategy = new Plex_Component_Image_SaveStrategy(
			$data->get( $data->get( 'type' ) )
		);

		$data->set(
			$data->get( 'type' ),
			$save_strategy->save()
		);

		return $data;

	}

}