<?php

class Plex_MetaBox_Testimonial extends Plex_Model_AbstractMetaBox {

	protected $sanitize_rules = array(
		'author'      => 'text_field',
		'position'    => 'text_field',
		'testimonial' => 'html_data'
	);

	/**
	 * Meta key for saving data
	 * @return mixed
	 */
	function get_meta_key() {
		return '_testimonial';
	}

	/**
	 * Get meta box title
	 * @return mixed
	 */
	function get_title() {
		return __( 'Testimonial', plex_get_trans_domain() );
	}

	/**
	 * @param $post
	 * @param $meta_data
	 *
	 * @return mixed
	 */
	function show( $post, $meta_data ) {
		$this->view->add( (array) $meta_data );
		$this->view->display( '/testimonial.php' );
	}

	/**
	 * Each meta box should implement that method
	 * and return data for save in one of these formats: Plex_Config_ParamBag or array
	 *
	 * @param $post_id
	 *
	 * @return mixed
	 */
	function save( $post_id ) {
		$post_data = new Plex_Config_ParamBag( $_POST );
		return $post_data->get( 'plex_testimonial', array() );
	}

	function screen() {
		return array(
			'testimonial'
		);
	}




}