<?php

class Plex_MetaBox_FeaturedVideo_Sanitizer_Strategies_VideoType extends Plex_Model_AbstractSanitizeStrategy {

	private $posibleTypes = array(
		'plexYoutube',
		'plexVimeo',
		'plexServer',
		'plexShortCode'
	);

	private $default = 'plexYoutube';

	function get_name() {
		return 'video_type';
	}

	function sanitize_input( $input, $args = array() ) {
		if ( in_array( $input, $this->posibleTypes ) ) {
			return $input;
		}
		return $this->default;
	}

	function sanitize_output( $output, $args = array() ) {
		return $this->sanitize_input( $output );
	}

}