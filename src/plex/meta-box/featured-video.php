<?php

class Plex_MetaBox_FeaturedVideo extends Plex_Model_AbstractMetaBox {

	protected $sanitize_rules = array(
		'type'          => 'text_field|video_type:plexYoutube',
		'plexServer'    => array(
			'url'           => 'url',
			'video_id'      => 'abs_int',
			'image'         => 'url',
			'width'         => 'abs_int',
			'height'        => 'abs_int',
			'attachment_id' => 'abs_int',
			'error'         => 'text_field'
		),
		'plexShortCode' => array(
			'short_code'    => 'text_field',
			'image'         => 'url',
			'width'         => 'abs_int',
			'height'        => 'abs_int',
			'attachment_id' => 'abs_int',
			'error'         => 'text_field'
		),
		'plexYoutube'   => array(
			'video_id'      => 'text_field',
			'url'           => 'url',
			'image'         => 'url',
			'width'         => 'abs_int',
			'height'        => 'abs_int',
			'attachment_id' => 'abs_int',
			'error'         => 'text_field'
		),
		'plexVimeo'     => array(
			'video_id'      => 'text_field',
			'url'           => 'url',
			'image'         => 'url',
			'width'         => 'abs_int',
			'height'        => 'abs_int',
			'attachment_id' => 'abs_int',
			'error'         => 'text_field'
		),
		'error'         => 'text_field'
	);

	private $video_types = array(
		'plexYoutube',
		'plexVimeo'
	);

	function get_title() {
		return __( 'Featured Video', plex_get_trans_domain() );
	}

	function screen() {
		return array(
			'post',
			'project'
		);
	}

	function context() {
		return 'side';
	}

	/**
	 * Meta key for saving data
	 * @return mixed
	 */
	function get_meta_key() {
		return '_plex_featured_video';
	}

	/**
	 * @param $post
	 * @param $meta_data
	 *
	 * @return mixed
	 */
	function show( $post, $meta_data ) {
		$this->view->add( $meta_data );
		$this->view->display( '/featured-video.php' );
	}

	/**
	 * Each meta box should implement that method
	 * and return data for save in one of these formats: Plex_Config_ParamBag or array
	 *
	 * @param $post_id
	 *
	 * @return mixed
	 */
	function save( $post_id ) {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			die( __( 'Permission denied', plex_get_trans_domain() ) );
		}

		$post_data = new Plex_Config_ParamBag( $_POST );
		$data      = new Plex_Config_ParamBag( (array) $post_data->get( 'plex_fv', array() ) );

		try {

			$url        = $data->get( $data->get( 'type', 'none' ) . '[url]', false, true );
			$short_code = $data->get( $data->get( 'type', 'none' ) . '[short_code]', false, true );


			if ( ! $url && ! $short_code ) return;

			$save_strategy = Plex_Component_Video_VideoSaveStrategyFactory::create(
				$data->get( 'type' ),
				$data->get( $data->get( 'type' ) )
			);

			$data->set(
				$data->get( 'type' ),
				$save_strategy->save()
			);

		} catch ( Plex_Exception_Common $e ) {
			$data->set( 'error', $e->getMessage() );
		}

		return $data;
	}

	function add_custom_sanitize_strategies() {
		// Add Sanitize Strategy for sanitizing video type
		Plex_Lib_Sanitizer::register(
			new Plex_MetaBox_FeaturedVideo_Sanitizer_Strategies_VideoType()
		);
	}

}