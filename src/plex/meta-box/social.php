<?php

class Plex_MetaBox_Social extends Plex_Model_AbstractMetaBox {

	protected $sanitize_rules = array(
		'*plex_social' => array(
			'link' => 'url',
			'icon' => 'text_field'
		)
	);

	/**
	 * Meta key for saving data
	 * @return mixed
	 */
	function get_meta_key() {
		return '_plex_social';
	}

	/**
	 * Get meta box title
	 * @return mixed
	 */
	function get_title() {
		return __( 'Social', plex_get_trans_domain() );
	}

	/**
	 * @param $post
	 * @param $meta_data
	 *
	 * @return mixed
	 */
	function show( $post, $meta_data ) {

		$show_strategy = new Plex_Component_Social_ShowStrategy(
			$this->view,
			$meta_data
		);
		$show_strategy->show();
	}

	/**
	 * Each meta box should implement that method
	 * and return data for save in one of these formats: Plex_Config_ParamBag or array
	 *
	 * @param $post_id
	 *
	 * @return mixed
	 */
	function save( $post_id ) {

		$post_data = new Plex_Config_ParamBag( $_POST );

		return array(
			'plex_social' => $post_data->get( 'plex_social', array() )
		);

	}

	function screen() {
		return 'team';
	}

}