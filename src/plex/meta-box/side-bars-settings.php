<?php

class Plex_MetaBox_SideBarsSettings extends Plex_Model_AbstractMetaBox {

	protected $sanitize_rules = array(
		'overwrite_sidebars_display_settings' => 'bool',
		'display_left_sidebar'                => 'bool',
		'display_right_sidebar'               => 'bool'
	);


	/**
	 * Meta key for saving data
	 * @return mixed
	 */
	function get_meta_key() {
		return 'sidebars_settings';
	}

	/**
	 * Get meta box title
	 * @return mixed
	 */
	function get_title() {
		return __( 'Sidebars', plex_get_trans_domain() );
	}

	/**
	 * @param $post
	 * @param $meta_data
	 *
	 * @return mixed
	 */
	function show( $post, $meta_data ) {
		$this->view->add( (array) $meta_data );
		$this->view->display( '/sidebars-settings.php' );
	}

	/**
	 * Each meta box should implement that method
	 * and return data for save in one of these formats: Plex_Config_ParamBag or array
	 *
	 * @param $post_id
	 *
	 * @return mixed
	 */
	function save( $post_id ) {
		$post_data = new Plex_Config_ParamBag( $_POST );
		$data      = new Plex_Config_ParamBag( (array) $post_data->get( 'plex_sidebars', array() ) );

		if ( ! $data->get( 'overwrite_sidebars_display_settings', false ) ) {
			$data->set('display_left_sidebar', 1);
			$data->set('display_right_sidebar', 1);
		}

		return $data;
	}

	function screen() {
		return array(
			'page',
			'post',
			'project'
		);
	}

}