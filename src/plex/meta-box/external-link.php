<?php

class Plex_MetaBox_ExternalLink extends Plex_Model_AbstractMetaBox {

	protected $sanitize_rules = array(
		'link'        => 'url',
		'in_new_page' => 'bool'
	);

	/**
	 * Meta key for saving data
	 * @return mixed
	 */
	function get_meta_key() {
		return '_plex_external_link';
	}

	/**
	 * Get meta box title
	 * @return mixed
	 */
	function get_title() {
		return __( 'External link', plex_get_trans_domain() );
	}

	/**
	 * @param $post
	 * @param $meta_data
	 *
	 * @return mixed
	 */
	function show( $post, $meta_data ) {
		$this->view->add( (array) $meta_data );
		$this->view->display( '/external-link.php' );
	}

	/**
	 * Each meta box should implement that method
	 * and return data for save in one of these formats: Plex_Config_ParamBag or array
	 *
	 * @param $post_id
	 *
	 * @return mixed
	 */
	function save( $post_id ) {
		$post_data = new Plex_Config_ParamBag( $_POST );
		return $post_data->get( 'plex_external_link', array() );
	}

	function screen() {

		return array(
			'client',
			'feature',
			'service'
		);

	}

}