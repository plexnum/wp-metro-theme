<?php

class Plex_MetaBox_Team extends Plex_Model_AbstractMetaBox {

	protected $sanitize_rules = array(
		'position' => 'text_field',
		'desc'     => 'html_data'
	);


	/**
	 * Meta key for saving data
	 * @return mixed
	 */
	function get_meta_key() {
		return '_plex_team';
	}

	/**
	 * Get meta box title
	 * @return mixed
	 */
	function get_title() {
		return __( 'Team meta data', plex_get_trans_domain() );
	}

	/**
	 * @param $post
	 * @param $meta_data
	 *
	 * @return mixed
	 */
	function show( $post, $meta_data ) {
		$this->view->add( (array) $meta_data );
		$this->view->display( '/team.php' );
	}

	/**
	 * Each meta box should implement that method
	 * and return data for save in one of these formats: Plex_Config_ParamBag or array
	 *
	 * @param $post_id
	 *
	 * @return mixed
	 */
	function save( $post_id ) {
		$post_data = new Plex_Config_ParamBag( $_POST );
		return $post_data->get('plex_team', array());
	}

	function screen() {
		return 'team';
	}

}