<?php
/**
 * Purpose of that class to autoload classes from files
 * Example:
 *
 * $autoloader = new Plex_AutoLoader_Loader();
 * $autoloader->register_prefixes(array(
 * 'Test_' => "/test"
 * ));
 *
 * $instance = new Test_MyVendor_MySuperClass();
 * autoloader will be search in: /test/my-vendor/my-super-class.php file to
 * instantiate Test_MyVendor_MySuperClass class
 */
class Plex_AutoLoader_Loader {

	/**
	 * List of registered prefixes
	 * @var array
	 */
	private $prefixes;

	/**
	 * Register prefixes
	 *
	 * @param array $classes
	 */
	function register_prefixes( array $classes ) {
		foreach ( $classes as $prefix => $locations ) {
			$this->prefixes[$prefix] = (array) $locations;
		}
	}

	/**
	 * Register php autoloader
	 */
	function register() {
		spl_autoload_register( array( $this, 'load_class' ) );
	}

	/**
	 * Load file corresponding to the class name
	 *
	 * @param $class class name
	 *
	 * @return bool
	 */
	function load_class( $class ) {
		if ( $file = $this->find_file( $class ) ) {
			require $file;
			return true;
		}
	}

	/**
	 * Find file that should be loaded by class name
	 *
	 * @param $class class name
	 *
	 * @return string
	 */
	function find_file( $class ) {
		$normalizedClass = mb_strtolower( preg_replace( '/((?<!_|^)[A-Z])/', '-$0', $class ) );
		$normalizedClass = str_replace( '_', DIRECTORY_SEPARATOR, $normalizedClass ) . '.php';
		foreach ( $this->prefixes as $prefix => $dirs ) {
			if ( 0 !== strpos( $class, $prefix ) ) {
				continue;
			}

			foreach ( $dirs as $dir ) {
				$file = $dir . DIRECTORY_SEPARATOR . $normalizedClass;
				if ( is_file( $file ) ) {
					return $file;
				}
			}
		}
	}
}
