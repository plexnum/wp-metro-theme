<?php

/**
 * Class Plex_Model_ThemeCustomizerOptionInterface
 */
interface Plex_Model_ThemeCustomizerOptionInterface {

	/**
	 * Get unique id of option that will be used in setting and in control
	 * of native theme customizer manager
	 * @return string
	 */
	function get_id();

	/**
	 * Should return section identity that option belongs to
	 * @return mixed
	 */
	function section();

	/**
	 * Array of setting configuration
	 * @return mixed
	 */
	function setting();

	/**
	 * Array of control configuration
	 * @return mixed
	 */
	function control();

}