<?php

interface Plex_Model_ShortCodeInterface {

	function get_tag_name();

	function _handle( $atts, $content = null );

	function _form();

	function _form_sub_templates();

	function get_icon();

	function is_customizable();

	function is_manageable();

	function get_desc();

	function preview_layout();

	function to_array();

}