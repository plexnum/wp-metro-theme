<?php
/**
 * Goal of that class to present theme customizer section
 * Class Plex_Model_AbstractThemeCustomizerSection
 */
abstract class Plex_Model_AbstractThemeCustomizerSection implements Plex_Model_ThemeCustomizerSectionInterface {

	/**
	 * Get title of section
	 * @return string
	 */
	abstract function get_title();

	/**
	 * Get description of section
	 * @return mixed
	 */
	abstract function get_description();

	/**
	 * Get priority of section
	 * @return int
	 */
	function get_priority() {
		return 10;
	}

	/**
	 * Get capability of section
	 * @return string
	 */
	function get_capability() {
		return 'edit_theme_options';
	}

	/**
	 * Get theme support of section
	 * @return string
	 */
	function get_theme_support() {
		return '';
	}

	/**
	 * Get unique id of section
	 * @return string
	 */
	function get_id() {
		return plex_class_name_to_identity( get_class( $this ) );
	}

	/**
	 * Get configs of section
	 * @return array
	 */
	function configs() {
		return array(
			'priority'       => $this->get_priority(),
			'capability'     => $this->get_capability(),
			'theme_supports' => $this->get_theme_support(),
			'title'          => $this->get_title(),
			'description'    => $this->get_description(),
		);
	}
}