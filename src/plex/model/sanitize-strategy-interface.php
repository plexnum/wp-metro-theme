<?php

interface Plex_Model_SanitizeStrategyInterface {

	function get_name();

	function sanitize_input( $input, $args = array() );

	function sanitize_output( $output, $args = array() );

}