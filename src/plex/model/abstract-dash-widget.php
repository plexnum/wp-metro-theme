<?php

/**
 * All dash type widgets should extend from that class
 * Class Plex_Model_AbstractDashWidget
 */
abstract class Plex_Model_AbstractDashWidget {

	/**
	 * @var Plex_View
	 */
	protected $view;

	/**
	 * get an id of widget
	 *
	 * @return string
	 */
	function get_id() {
		return plex_class_name_to_identity(get_class($this));
	}

	/**
	 * get a name of widget
	 *
	 * @return string
	 */
	abstract function get_name();

	/**
	 * show widget. From that method we should display content of our widget
	 *
	 * @return null
	 */
	abstract function show();

	public function __construct() {
		$this->view = new Plex_View();
		$this->view->set_patch_prefix( 'view/widget' );
	}
}
