<?php
/**
 * Each theme component should realize that interface
 * Class Plex_Model_ComponentInterface
 */
interface Plex_Model_ComponentInterface {

	/**
	 * Component declaration
	 * That method define callbacks that listen to WordPress hooks
	 * @return mixed
	 */
	function declaration();

	/**
	 * Get unique name of the component
	 * @return mixed
	 */
	function get_name();
}
