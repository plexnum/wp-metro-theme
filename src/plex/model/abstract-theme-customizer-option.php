<?php
/**
 * Goal of that class to present theme customizer option. Which
 * contains 2 parts: setting and control of native theme customizer manager.
 * Class Plex_Model_AbstractThemeCustomizerOption
 */
abstract class Plex_Model_AbstractThemeCustomizerOption implements Plex_Model_ThemeCustomizerOptionInterface {

	/**
	 * Identity of section that option belongs to
	 * @var string
	 */
	private $section_identity;

	/**
	 * Native theme customizer manager
	 * @var WP_Customize_Manager
	 */
	private $customizer;

	/**
	 * Set customizer manager
	 *
	 * @param $customizer
	 */
	function set_customizer( $customizer ) {
		$this->customizer = $customizer;
	}

	/**
	 * Get customizer manager
	 * @return WP_Customize_Manager
	 */
	function get_customizer() {
		return $this->customizer;
	}

	/**
	 * Get unique id of option that will be used in setting and in control
	 * of native theme customizer manager
	 * @return string
	 */
	function get_id() {
		return plex_class_name_to_identity( get_class( $this ) );
	}

	/**
	 * Should return section identity that option belongs to
	 * @return string
	 */
	function section() {
		return $this->section_identity;
	}

	/**
	 * Set section identity
	 *
	 * @param $name
	 */
	function set_section_identity( $name ) {
		$this->section_identity = $name;
	}

}