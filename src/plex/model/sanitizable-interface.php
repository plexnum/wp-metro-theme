<?php

interface Plex_Model_SanitizableInterface {

	function get_output_sanitize_rules();

	function get_input_sanitize_rules();

}