<?php

abstract class Plex_Model_AbstractOptionPage extends Plex_Model_AbstractPage {

	function declaration() {
		parent::declaration();
		$id = basename($this->get_slug(), '.php');
		add_action('admin_print_styles-appearance_page_'.$id, array($this, 'enquire_styles'));
		add_action('admin_print_scripts-appearance_page_'.$id, array($this, 'enquire_scripts'));
		add_action('admin_head-appearance_page_'.$id, array($this, 'admin_head'));
		add_action('load-appearance_page_'.$id, array($this, 'load'));
	}

	function define_page() {
		add_options_page( $this->get_title(),
			$this->get_menu_title(),
			$this->get_capability(),
			$this->get_slug(),
			array( $this, 'show' ) );
	}

}