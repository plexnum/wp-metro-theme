<?php

/**
 * Abstract admin page class
 * Class Plex_Model_AbstractPage
 */
abstract class Plex_Model_AbstractPage extends Plex_Model_AbstractComponent implements Plex_Model_AdminableInterface {

	function __construct() {
		$this->view = new Plex_View( array(), true );
		$this->view->set_patch_prefix( 'view' );
	}

	/**
	 * Get title of the page
	 * @return string
	 */
	abstract function get_title();

	/**
	 * Get menu title of the page
	 * @return string
	 */
	abstract function get_menu_title();

	/**
	 * Get icon url of the page
	 * @return string
	 */
	function get_icon_url() {
		return '';
	}

	/**
	 * Get position of the page
	 * @return null
	 */
	function get_position() {
		return null;
	}

	/**
	 * Get capability of the page
	 * @return string
	 */
	function get_capability() {
		return 'manage_options';
	}

	/**
	 * Get slug of the page
	 * @return mixed
	 */
	abstract function get_slug();

	/**
	 * Show callback of the page
	 * @return mixed
	 */
	abstract function show();

	function declaration() {
		add_action( 'admin_menu', array( $this, 'define_page' ) );
		$this->define_ajax();
	}

	/**
	 * Enquire styles for specific page
	 */
	function enquire_styles() {
	}

	/**
	 * Enquire scripts for specific page
	 */
	function enquire_scripts() {
	}

	/**
	 * When display a header
	 */
	function admin_head() {
	}

	/**
	 * When page is loaded
	 */
	function load() {
	}

	/**
	 * Define page with class settings
	 */
	function define_page() {

		add_menu_page( $this->get_title(),
			$this->get_menu_title(),
			$this->get_capability(),
			$this->get_slug(),
			array( $this, 'show' ),
			$this->get_icon_url(),
			$this->get_position()
		);
	}

	/**
	 * Define ajax actions
	 */
	function define_ajax() {
	}

}