<?php

abstract class Plex_Model_AbstractComponent implements Plex_Model_ComponentInterface {

	/**
	 * Get unique name of the component
	 * @return mixed
	 */
	function get_name() {
		return plex_class_name_to_identity(get_class($this));
	}

}