<?php

abstract class Plex_Model_AbstractMetaBox implements Plex_Model_SanitizableInterface {

	protected $sanitize_rules = array();

	protected $output_sanitize_rules = array();

	protected $input_sanitize_rules = array();

	/**
	 * Meta key for saving data
	 * @return mixed
	 */
	abstract function get_meta_key();

	/**
	 * Get meta box title
	 * @return mixed
	 */
	abstract function get_title();

	/**
	 * @param $post
	 * @param $meta_data
	 *
	 * @return mixed
	 */
	abstract function show( $post, $meta_data );

	/**
	 * Each meta box should implement that method
	 * and return data for save in one of these formats: Plex_Config_ParamBag or array
	 *
	 * @param $post_id
	 *
	 * @return mixed
	 */
	abstract function save( $post_id );

	function get_id() {
		return plex_class_name_to_identity( get_class( $this ) );
	}

	/**
	 * Priority
	 * @return string
	 */
	function priority() {
		return 'default';
	}

	/**
	 * Context where meta box should be shown: 'normal', 'advanced'
	 * @return mixed
	 */
	function context() {
		return 'normal';
	}

	/**
	 * Post type where meta box shoul be shown
	 * @return string|array post, page, link, ...
	 */
	function screen() {
		return 'page';
	}

	function add_custom_sanitize_strategies() {
	}

	function _save( $post_id ) {

		$this->add_custom_sanitize_strategies();

		if ( isset( $_POST['action'] ) && $_POST['action'] == 'autosave' ) {
			return;
		}


		$raw_data = $this->save( $post_id );

		if (
			! is_array( $raw_data ) &&
			! $raw_data instanceof Plex_Config_ParamBag
		) {
			$raw_data = array();
		}

		$sanitizer = new Plex_Lib_Sanitizer(
			$this->get_input_sanitize_rules(),
			$raw_data
		);

		$data = $sanitizer->sanitize_input();
		update_post_meta( $post_id, $this->get_meta_key(), $data );
	}

	function _show( $post ) {

		$this->add_custom_sanitize_strategies();

		$post_meta = get_post_meta(
			$post->ID,
			$this->get_meta_key(),
			true
		);

		if ( ! is_array( $post_meta ) ) {
			$post_meta = array();
		}

		$this->show( $post, $post_meta );
	}

	function __construct() {

		$this->view = new Plex_View();
		$this->view->set_patch_prefix( 'view/meta-box' );
		$this->view->set_sanitizer(
			new Plex_Lib_Sanitizer( $this->get_output_sanitize_rules() )
		);

		add_action(
			'pre_post_update',
			array( $this, '_save' )
		);

		add_action(
			'admin_print_styles',
			array( $this, 'enquire_styles' )
		);

		add_action(
			'admin_print_scripts',
			array( $this, 'enquire_scripts' )
		);

	}

	function enquire_scripts() {
	}

	function enquire_styles() {
	}

	function get_output_sanitize_rules() {
		if ( empty( $this->output_sanitize_rules ) ) {
			return $this->sanitize_rules;
		}

		return $this->output_sanitize_rules;
	}

	function get_input_sanitize_rules() {
		if ( empty( $this->input_sanitize_rules ) ) {
			return $this->sanitize_rules;
		}

		return $this->input_sanitize_rules;
	}

}