<?php

abstract class Plex_Model_AbstractSanitizer {

	abstract function sanitize( $data );

}