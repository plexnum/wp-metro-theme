<?php

/**
 * All views should extend that class
 * Class Plex_Model_AbstractView
 */
abstract class Plex_Model_AbstractView extends Plex_Config_ParamBag {

	/**
	 * Prefix path of template that view used to display yourself
	 * @var string
	 */
	protected $patch_prefix = '';

	protected $sanitizer = null;


	/**
	 * Set template path prefix
	 *
	 * @param $patch_prefix
	 */
	public function set_patch_prefix( $patch_prefix ) {
		$this->patch_prefix = $patch_prefix;
	}

	/**
	 * Get template path prefix
	 * @return string
	 */
	public function get_patch_prefix() {
		return $this->patch_prefix;
	}

	public function sanitize() {

		// Sanitize view output
		if ( $this->sanitizer ) {
			$this->sanitizer->set_data( $this );
			$sanitized_output = $this->sanitizer->sanitize_output();
			if ( is_array( $sanitized_output ) ) {
				$this->add( $sanitized_output );
			}
		}

	}

	/**
	 * Display view use template php file
	 *
	 * @param string $template_name
	 */
	public function display( $template_name ) {

		$this->sanitize();

		$template_location = locate_template( $this->patch_prefix . $template_name );
		require $template_location;
	}

	public function get_output( $template_name ) {
		ob_start();
		$this->display( $template_name );
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}

	public function set_sanitizer( $sanitizer ) {
		$this->sanitizer = $sanitizer;
	}

	/**
	 * @return Plex_Lib_Sanitizer
	 */
	public function get_sanitizer() {
		return $this->sanitizer;
	}

}
