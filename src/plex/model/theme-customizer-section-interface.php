<?php

interface Plex_Model_ThemeCustomizerSectionInterface {

	/**
	 * Get unique id of section
	 * @return string
	 */
	function get_id();

	/**
	 * Get configs of section
	 * @return array
	 */
	function configs();

}