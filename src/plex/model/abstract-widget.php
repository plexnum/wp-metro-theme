<?php

/**
 * Each widget should extends from that abstract class
 * Class Plex_Model_AbstractWidget
 */
abstract class Plex_Model_AbstractWidget extends WP_Widget implements Plex_Model_SanitizableInterface {

	protected $sanitize_rules = array();

	protected $output_sanitize_rules = array();

	protected $input_sanitize_rules = array();

	protected $options;

	/**
	 * Widget view
	 * @var Plex_View
	 */
	protected $view;

	/**
	 * Get unique id of widget
	 * @return mixed
	 */
	function get_id() {
		return plex_class_name_to_identity( get_class( $this ) );
	}

	/**
	 * Get widget name
	 * @return string
	 */
	abstract function get_name();


	/**
	 * Get widget description
	 * @return string
	 */
	abstract function get_desc();

	/**
	 * @param $new_instance
	 * @param $old_instance
	 *
	 * @return mixed
	 */
	abstract function save( $new_instance, $old_instance );

	/**
	 * Get widget options
	 * @return array
	 */
	function get_options() {
		return array(
			'classname'   => $this->get_id(),
			'description' => $this->get_desc()
		);
	}

	function update( $new_instance, $old_instance ) {
		$raw_data  = $this->save( $new_instance, $old_instance );
		$sanitizer = new Plex_Lib_Sanitizer( $this->get_input_sanitize_rules(), $raw_data );
		return $sanitizer->sanitize_input();
	}

	/*
	 * Get control options
	 */
	public function get_control_options() {
		return array();
	}

	public function add_styles() {
	}

	public function add_scripts() {
	}

	public function __construct() {
		$this->view         = new Plex_View();
		$this->view->widget = $this;
		$this->view->set_patch_prefix( 'view/widget' );
		$this->view->set_sanitizer( new Plex_Lib_Sanitizer( $this->get_output_sanitize_rules(), array() ) );
		$this->add_assets_hooks();
		parent::__construct( $this->get_id(), $this->get_name(), $this->get_options(), $this->get_control_options() );
	}

	protected function add_assets_hooks() {
		add_action( 'admin_print_styles-widgets.php', array( $this, 'add_styles' ) );
		add_action( 'admin_print_scripts-widgets.php', array( $this, 'add_scripts' ) );
	}

	function get_output_sanitize_rules() {
		if ( empty( $this->output_sanitize_rules ) ) {
			return $this->sanitize_rules;
		}

		return $this->output_sanitize_rules;
	}

	function get_input_sanitize_rules() {
		if ( empty( $this->input_sanitize_rules ) ) {
			return $this->sanitize_rules;
		}

		return $this->input_sanitize_rules;
	}

}