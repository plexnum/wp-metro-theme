<?php

interface Plex_Model_ThemeCustomizerSanitizeInterface {

	function sanitize_input( $input );

	function sanitize_output( $output );

}