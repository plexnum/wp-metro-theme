<?php
/**
 * Class Plex_Model_SetupableInterface
 */
interface Plex_Model_SetupableInterface {
	function setup();
}
