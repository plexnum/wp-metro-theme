<?php

abstract class Plex_Model_AbstractShortCode implements Plex_Model_ShortCodeInterface, Plex_Model_SanitizableInterface {


	protected $sanitize_rules = array();

	protected $output_sanitize_rules = array();

	protected $input_sanitize_rules = array();


	function __construct() {
		$this->view = new Plex_View();
		$sanitizer  = new Plex_Lib_Sanitizer( $this->get_output_sanitize_rules() );
		$this->view->set_sanitizer( $sanitizer );
		$this->view->set_patch_prefix( 'view/short-code' );
	}

	abstract function handle( $atts, $content = null );

	function get_output_sanitize_rules() {
		if ( empty( $this->output_sanitize_rules ) ) {
			return $this->sanitize_rules;
		}

		return $this->output_sanitize_rules;
	}

	function get_input_sanitize_rules() {
		if ( empty( $this->input_sanitize_rules ) ) {
			return $this->sanitize_rules;
		}

		return $this->input_sanitize_rules;
	}

	function is_customizable() {
		return true;
	}

	function is_manageable() {
		return true;
	}

	function get_icon() {
		$cm = plex_get_theme_conf_bag();
		return $cm->get( 'urls[assets][fixtures]', false, true ) . '/short-codes/' . $this->get_tag_name() . '.jpg';
	}

	function get_desc() {
		return null;
	}

	function form() {
		return null;
	}

	function form_sub_templates() {
		return array();
	}

	function _form() {
		$this->view->replace();
		return $this->form();
	}

	function _handle( $atts, $content = null ) {
		$this->view->replace();
		return $this->handle( $atts, $content );
	}

	function _form_sub_templates() {
		$this->view->replace();
		return array();
	}


	function preview_layout( $content = '' ) {
		$this->view->set( '_content', $content );
		return $this->view->get_output( '/_system/basic-preview-layout.php' );
	}

	function to_array() {
		return array(
			'id'              => $this->get_tag_name(),
			'icon'            => $this->get_icon(),
			'desc'            => $this->get_desc(),
			'is_customizable' => $this->is_customizable(),
			'settings'        => $this->preview_settings()
		);
	}

	function preview_settings() {
		return array();
	}


}