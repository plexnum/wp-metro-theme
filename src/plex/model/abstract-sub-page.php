<?php

abstract class Plex_Model_AbstractSubPage extends Plex_Model_AbstractPage {

	abstract function get_parent_slug();

	function define_page() {
		add_submenu_page( $this->get_parent_slug(),
			$this->get_title(),
			$this->get_menu_title(),
			$this->get_capability(),
			$this->get_slug(),
			array( $this, 'show' )
		);
	}


}