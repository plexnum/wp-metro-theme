<?php

if ( ! function_exists( 'plex_is_front_static' ) ) {

	/**
	 * Check is front page is static page and not latest blog posts
	 * @return string
	 */
	function plex_is_front_static() {

		if ( 'page' == get_option( 'show_on_front' ) && get_option( 'page_on_front' ) && is_page( get_option( 'page_on_front' ) ) ) {
			return true;
		}
		else {
			return false;
		}
	}

}


if ( ! function_exists( 'plex_is_front_latest_posts' ) ) {

	/**
	 * Check is front page is latest blog posts page and not static page
	 * @return bool
	 */
	function plex_is_front_latest_posts() {

		if ( 'posts' == get_option( 'show_on_front' ) && is_home() ) {
			return true;
		}
		else {
			return false;
		}

	}

}

if ( ! function_exists( 'plex_load_partial' ) ) {

	/**
	 * Load partial
	 *
	 * @param        $partial
	 * @param null   $preferable_template
	 * @param string $fall_back_extension
	 */
	function plex_load_partial( $partial, $preferable_template = null, $fall_back_extension = '.php' ) {

		$templates_to_load = array();

		if ( ! empty( $preferable_template ) ) {
			$templates_to_load[] = $preferable_template;
		}

		$templates_to_load[] = $partial;

		foreach ( $templates_to_load as $key => $tpl ) {
			$path_info = pathinfo( $tpl );

			if ( ! isset( $path_info['extension'] ) ) {
				$templates_to_load[$key] .= $fall_back_extension;
			}
		}

		locate_template( $templates_to_load, true, false );

	}

}

if ( ! function_exists( 'plex_the_comment_count' ) ) {

	function plex_the_comment_count( $post = 0 ) {

		$post = get_post( $post );

		if ( isset( $post->comment_count ) ) {
			echo $post->comment_count;
		}

	}
}


if ( ! function_exists( 'plex_get_logo' ) ) {

	function plex_get_logo() {
		$plex_logo = get_theme_mod( 'plex_logo_image' );
		if ( empty( $plex_logo ) ) {
			return plex_fix_url() . '/logo.png';
		}

		return esc_attr( $plex_logo );
	}

}


if ( ! function_exists( 'plex_get_post_format_featured_image' ) ) {


	function plex_get_post_format_featured_image( $size = 'post-thumbnain', $css_class = 'js-imgCentr' ) {
		$plex_post_format = get_post_format();
		$plex_fn_name     = 'plex_get_post_' . $plex_post_format;
		$plex_data        = new Plex_Config_ParamBag();

		if ( function_exists( $plex_fn_name ) ) {
			$plex_data = call_user_func( $plex_fn_name );
		}
		elseif ( $plex_post_format = 'gallery' ) {
			$plex_short_code = plex_get_post_gallery_short_code();

			if ( preg_match( '/(?<=ids=")\d+/i', $plex_short_code, $plex_matches ) ) {
				$plex_data->set( 'attachment_id', $plex_matches[0] );
			}

		}

		return plex_get_featured_image( $plex_data, $size, $css_class );

	}

}

if ( ! function_exists( 'plex_get_featured_image' ) ) {

	function plex_get_featured_image( $data, $size = 'post-thumbnail', $css_class = 'js-imgCentr' ) {

		if ( $data->get( 'attachment_id' ) ) {

			return wp_get_attachment_image( $data->get( 'attachment_id' ),
				$size,
				false,
				array(
					'class' => $css_class
				)
			);

		}

		elseif ( $data->get( 'image' ) ) {
			return '<img src="' . $data->get( 'image' ) . '"
			 class="' . $css_class . '"
			 width="' . $data->get( 'width' ) . '"
			 height="' . $data->get( 'height' ) . '" />';
		}

		elseif ( has_post_thumbnail() ) {
			return get_the_post_thumbnail( null, $size, array(
				'class' => $css_class
			) );
		}
		else {
			return false;
		}

	}

}


if ( ! function_exists( 'plex_the_featured_image' ) ) {

	function plex_the_featured_image( $data, $size = 'post-thumbnail' ) {
		$result = plex_get_featured_image( $data, $size );
		if ( $result !== false ) {
			echo $result;
		}
	}

}

if ( ! function_exists( 'plex_show_in_row' ) ) {

	function plex_show_in_row( $arguments ) {

		global $wp_query;

		$args = new Plex_Config_ParamBag( $arguments );
		$args->set( 'count', $wp_query->post_count );

		if ( ! $args->get( 'iterator' ) ) {
			throw new Plex_Exception_Common( 'Iterator does not existed' );
		}

		if ( $args->get( 'iterator' ) == 1 ) {
			echo $args->get( 'start' );
		}

		plex_load_partial( $args->get( 'view' ) );

		$els = (int) $args->get( 'els' );

		if ( $els == 0 ) {
			$els = 1;
		}

		if ( ( $args->get( 'iterator' ) % $els == 0 )
				&& ( $args->get( 'iterator' ) != $args->get( 'count' ) )
		) {

			echo $args->get( 'end' );
			echo $args->get( 'between' );
			echo $args->get( 'start' );

		}


		if ( $args->get( 'iterator' ) == $args->get( 'count' ) ) {
			echo $args->get( 'end' );
		}


	}

}


if ( ! function_exists( 'plex_show_sidebar' ) ) {


	/**
	 * Show sidebar
	 *
	 * @param $id
	 * @param $sidebar_grid Plex_Lib_SidebarGrid
	 */
	function plex_the_sidebar( $id, $sidebar_grid ) {

		if ( $sidebar_grid->is_active_sidebar( $id ) ) {
			$plex_view = new Plex_View();
			$plex_view->set( 'id', $id );
			$plex_view->set( 'sidebar_grid', $sidebar_grid );
			$plex_view->display( 'view/templates/_partials/sidebar.php' );
		}

	}

}

if ( ! function_exists( 'plex_get_external_link' ) ) {

	function plex_get_external_link() {

		$plex_external_link = new Plex_Config_ParamBag(
			(array) get_post_meta( get_the_ID(), '_plex_external_link', true )
		);

		if ( $plex_external_link->get( 'in_new_page' ) ) {
			$plex_external_link->set( 'target', 'target="_blank"' );
		}

		return $plex_external_link;

	}

}

if ( ! function_exists( 'plex_get_default_post_icon' ) ) {

	function plex_get_default_post_icon() {

		$plex_cm = plex_get_theme_conf_bag();
		return $plex_cm->get( 'default_post_icon[' . get_post_type() . ']', false, true );

	}

}

if ( ! function_exists( 'plex_get_gallery_img' ) ) {

	function plex_the_gallery_img( $id, $attrs, $rel = null ) {

		if ( 'post' === $attrs->get( 'link' ) ) {

			$plex_image_output = wp_get_attachment_image(
				$id,
				$attrs->get( 'size' ),
				false,
				'class=js-imgCentr'
			);

			echo '<a href="' . get_attachment_link( $id ) . '" rel="' . $attrs->get( 'rel' ) . '" class="img-link">' . $plex_image_output . '</a>';

		}
		else {

			$plex_image_output = wp_get_attachment_image(
				$id,
				$attrs->get( 'size' ),
				false,
				'class=js-imgCentr'
			);

			echo '<a href="' . plex_get_attachment_image( $id, 'full' ) . '" rel="' . $attrs->get( 'rel' ) . '" class="img-link js-fancybox">' . $plex_image_output . '</a>';

		}

	}

}

function plex_get_link_target( $show ) {

	if ( $show ) {
		return 'target="_blank"';
	}

}

function plex_the_link_target( $show ) {

	if ( $show ) {
		echo 'target="_blank"';
	}

}

function plex_the_convert_terms_to_classes( $terms = array(), $postfix = '' ) {

	if ( $terms == false ) {
		return '';
	}

	$plex_classes = '';

	foreach ( $terms as $term ) {
		$plex_classes .= esc_attr( $postfix . $term->slug ) . ' ';
	}

	echo $plex_classes;
}


function plex_do_excerpt_length_20() {
	return 20;
}

function plex_wrap_before_metro_widget( $header_type = 4 ) {
	echo '<div class="span4 inch' . (int) $header_type . ' js-tile js-animate"><div class="tile_text">
	<div class="bg-main-bg"></div><div class="dis"><div class="scroller">';
}

function plex_wrap_after_metro_widget() {
	echo '<div class="scroller__track"><div class="scroller__bar"></div></div></div></div></div></div>';
}