<?php
if ( !defined('ABSPATH')) exit;
/**
* Template Name: Metro Front Page
*/
?>

<?php plex_load_partial('view/templates/_partials/header'); // Load header partial ?>
<?php plex_load_partial('view/templates/_content/metro-front-page'); ?>
<?php plex_load_partial('view/templates/_partials/footer'); // Load footer partial ?>