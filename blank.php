<?php
/**
 * Template Name: Blank page
 *
 * ========================================================================
 * Blank page
 * ========================================================================
 */

if ( !defined('ABSPATH')) exit;

// Set settings. These will be available for all child templates
$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();
$plex_view_transfer->set(
	'sidebars_settings',
	(array)get_post_meta(get_the_ID(), 'sidebars_settings', true)
);

?>

<?php plex_load_partial('view/templates/_partials/header'); // Load header partial ?>

<?php plex_load_partial('view/templates/_partials/page-header'); ?>

<?php plex_load_partial('view/templates/_content/blank'); ?>

<?php
// Reset to default page query as well as post data
wp_reset_query();
?>

<?php plex_load_partial('view/templates/_partials/footer'); // Load footer partial ?>