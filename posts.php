<?php
/**
 * Template Name: Posts
 */

/**
 * ========================================================================
 * Posts template page
 * ========================================================================
 */

if ( ! defined( 'ABSPATH' ) ) exit;
?>

<?php plex_load_partial( 'view/templates/_partials/header' ); // Load header partial ?>

<?php plex_load_partial( 'view/templates/_partials/page-header' ); ?>

<?php

// Set settings. These will be available for all child templates
$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();
$plex_view_transfer->set(
	'sidebars_settings',
	(array) get_post_meta( get_the_ID(), 'sidebars_settings', true )
);

// Change context from page to posts
global $wp_query;
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$wp_query = new WP_Query( array(
	'post_per_page' => (int) get_option( 'posts_per_page' ),
	'paged'         => (int) $paged,
	'orderby'       => 'date'
) );

?>

<?php plex_load_partial( 'view/templates/_content/posts' ); // Load posts ?>

<?php
// Reset to default page query as well as post data
wp_reset_query();
?>

<?php plex_load_partial( 'view/templates/_partials/footer' ); // Load footer partial ?>

