<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-el">
	<div class="-col-left">
		<div class="-icon-wrap">
			<i class="<?php echo $this->get( 'icon' ); ?> -icon"></i>
		</div>
	</div>
	<div class="-col-right">
		<div class="-actions">
			<a class="button button-primary -action js-social-del" href="#">
				<?php echo __( 'Delete', plex_get_trans_domain() ); ?>
			</a>
		</div>
	</div>
	<div class="-col-center">
		<input name="plex_social[<?php echo $this->get( 'key' ); ?>][link]"
					 value="<?php echo $this->get( 'link' ); ?>"
					 class="-link widefat" type="text">

		<input name="plex_social[<?php echo $this->get( 'key' ); ?>][icon]"
					 value="<?php echo $this->get( 'icon' ); ?>"
					 type="hidden" class="-icon__input">
	</div>
</div>