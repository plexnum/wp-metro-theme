<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-meta-box -context">
	<p>
		<input data-box="sidebarDisplaySettingsBox"
					 type="checkbox"
					 class="checkbox js-toggle-box-trigger"
					 name="plex_sidebars[overwrite_sidebars_display_settings]"
					 id="overwriteSidebarsDisplaySettigs"
				<?php checked( true, $this->get( 'overwrite_sidebars_display_settings', false ) ); ?>>
		<label for="overwriteSidebarsDisplaySettigs">
			<?php _e( 'Overwrite sidebars display settings', plex_get_trans_domain() ); ?>
		</label>
	</p>

	<p style="<?php if(!$this->get( 'overwrite_sidebars_display_settings', false )): ?>display: none<?php endif; ?>" id="sidebarDisplaySettingsBox">

		<input type="checkbox"
					 class="checkbox"
					 name="plex_sidebars[display_left_sidebar]"
					 id="displayLeftSidebar"
				<?php checked( true, $this->get( 'display_left_sidebar', false, true ) ); ?>>

		<label for="displayLeftSidebar">
			<?php _e( 'Display left sidebar', plex_get_trans_domain() ); ?>
		</label><br>

		<input type="checkbox"
					 class="checkbox"
					 name="plex_sidebars[display_right_sidebar]"
					 id="displayRightSidebar"
				<?php checked( true, $this->get( 'display_right_sidebar', false, true ) ); ?>>
		<label for="displayRightSidebar">
			<?php _e( 'Display right sidebar', plex_get_trans_domain() ); ?>
		</label>

	</p>
</div>