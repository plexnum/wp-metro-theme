<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-meta-box -type-changable -block">

	<?php if ( $this->get( 'error' ) ): ?>
		<div class="error-message"><?php echo $this->get( 'error' ); ?></div>
	<?php endif; ?>

	<div class="tabs-wrapper">
		<ul class="wp-tab-bar">
			<li
					<?php if ( $this->get( 'type', 'plexAudio' ) == 'plexAudio' ): ?>
						class="wp-tab-active"
					<?php else: ?>
						class="hide-if-no-js"
					<?php endif; ?>>
				<a href="#plexAudio"><?php _e( 'Server', plex_get_trans_domain() ); ?></a>
			</li>
			<li
					<?php if ( $this->get( 'type' ) == 'plexAudioShortCode' ): ?>
						class="wp-tab-active"
					<?php else: ?>
						class="hide-if-no-js"
					<?php endif; ?>>
				<a href="#plexAudioShortCode"><?php _e( 'Short Code', plex_get_trans_domain() ); ?></a>
			</li>
		</ul>
		<input type="hidden" class="-type"
					 name="plex_fa[type]"
					 value="<?php echo $this->get( 'type', 'plexAudio' ); ?>" />

		<div id="plexAudio" class="wp-tab-panel -context"
				<?php if ( $this->get( 'type', 'plexAudio' ) == 'plexAudio' ): ?>
					style="display: block;"
				<?php else: ?>
					style="display: none;"
				<?php endif; ?>>

			<?php if ( $this->get( 'plexAudio[error]', null, true ) ): ?>
				<div class="error-message"><?php echo $this->get( 'plexAudio[error]', null, true ); ?></div>
			<?php endif; ?>

			<input type="hidden" class="-audio__id"
						 name="plex_fa[plexAudio][audio_id]"
						 value="<?php echo $this->get( 'plexAudio[audio_id]', null, true ); ?>">

			<input type="hidden" class="-image__input"
						 name="plex_fa[plexAudio][image]"
						 value="<?php echo $this->get( 'plexAudio[image]', null, true ); ?>">

			<input type="hidden" class="-image__att-id"
						 name="plex_fa[plexAudio][attachment_id]"
						 value="<?php echo $this->get( 'plexAudio[attachment_id]', null, true ); ?>">


			<p>
				<label><?php echo __( "Audio url: ", plex_get_trans_domain() ); ?></label>
				<input type="text"
							 class="-audio__input widefat"
							 name="plex_fa[plexAudio][url]"
							 value="<?php echo $this->get( 'plexAudio[url]', null, true ); ?>">
			</p>

			<p class="-image__preview-box">
				<?php if ( $this->get( 'plexAudio[image]', null, true ) ): ?>
					<img src="<?php echo $this->get( 'plexAudio[image]', null, true ); ?>" width="100%">
				<?php else: ?>
					<img src="<?php echo plex_fix_url() . '/default-audio-thumbnail.png'; ?>" width="100%">
				<?php endif; ?>
			</p>

		</div>

		<div id="plexAudioShortCode" class="wp-tab-panel -context"
				<?php if ( $this->get( 'type' ) == 'plexAudioShortCode' ): ?>
					style="display: block;"
				<?php else: ?>
					style="display: none;"
				<?php endif; ?>>
			<?php if ( $this->get( 'plexAudioShortCode[error]', null, true ) ): ?>
				<div class="error-message"><?php echo $this->get( 'plexAudioShortCode[error]', null, true ); ?></div>
			<?php endif; ?>

			<input type="hidden" class="-image__input"
						 name="plex_fa[plexAudioShortCode][image]"
						 value="<?php echo $this->get( 'plexAudioShortCode[image]', null, true ); ?>">

			<input type="hidden" class="-image__att-id"
						 name="plex_fa[plexAudioShortCode][attachment_id]"
						 value="<?php echo $this->get( 'plexAudioShortCode[attachment_id]', null, true ); ?>">

			<p>
				<label><?php echo __( "Short code: ", plex_get_trans_domain() ); ?></label>
				<textarea class="widefat"
									name="plex_fa[plexAudioShortCode][short_code]"><?php echo $this->get( 'plexAudioShortCode[short_code]', null, true ); ?></textarea>
			</p>

			<p class="-image__preview-box">
				<?php if ( $this->get( 'plexAudioShortCode[image]', null, true ) ): ?>
					<img src="<?php echo $this->get( 'plexAudioShortCode[image]', null, true ); ?>" width="100%">
				<?php else: ?>
					<img src="<?php echo plex_fix_url() . '/default-audio-thumbnail.png'; ?>" width="100%">
				<?php endif; ?>
			</p>

		</div>

	</div>

</div>