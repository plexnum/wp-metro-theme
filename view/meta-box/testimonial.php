<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-meta-box -testimonial-meta-box">

	<p>
		<label>
			<?php _e( "Author: ", plex_get_trans_domain() ); ?>
		</label>

		<input type="text" class="widefat" name="plex_testimonial[author]" value="<?php echo $this->get( 'author' ); ?>"></input>
	</p>

	<p>
		<label>
			<?php _e( "Job position: ", plex_get_trans_domain() ); ?>
		</label>

		<input type="text" class="widefat" name="plex_testimonial[position]" value="<?php echo $this->get( 'position' ); ?>"></input>
	</p>

	<p>
		<label>
			<?php _e( "Testimonial: ", plex_get_trans_domain() ); ?>
		</label>
		<textarea class="widefat" name="plex_testimonial[testimonial]"><?php echo $this->get( 'testimonial' ); ?></textarea>
	</p>

</div>