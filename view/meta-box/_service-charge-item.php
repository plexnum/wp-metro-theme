<?php if ( !defined('ABSPATH')) exit; ?>

<div class="row-fluid -item">
	<div class="span10">
		<input name="plex_service_charge[<?php echo $this->get( 'key' ); ?>][item]"
					 value="<?php echo $this->get( 'item' ); ?>"
					 class="widefat" type="text">
	</div>
	<div class="span2">
		<div class="-actions">
			<a class="button button-primary -action js-service-charge-item-del" href="#">
				<?php echo __( 'Delete', plex_get_trans_domain() ); ?>
			</a>
		</div>
	</div>
</div>