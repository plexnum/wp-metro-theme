<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-meta-box -service-charge-meta-box bootstrap-namespace">
	<div class="-wp-box">
		<div class="-wp-header">
			<?php echo __( 'Add', plex_get_trans_domain() ); ?>
		</div>
		<div class="-wp-content -add-form -context">
			<div class="-err"></div>
			<label><?php echo __( 'Service charge', plex_get_trans_domain() ); ?></label>
			<input type="text" class="widefat -service-charge-input">
			<a class="button button-primary -action js-service-charge-item-add" href="#"><?php echo __( 'Add', plex_get_trans_domain() ); ?></a>
		</div>
	</div>

	<div class="-service-charge-items">

		<?php $plex_service_charge_items = array_merge((array)$this->get( 'items', array() )); ?>

		<?php foreach ( $plex_service_charge_items as $key => $item ): ?>

			<?php $plex_service_charge_item = new Plex_View();
			$plex_service_charge_item->set_patch_prefix( $this->get_patch_prefix() );
			$plex_service_charge_item->add( (array) $item );
			$plex_service_charge_item->set( 'key', $key );
			$plex_service_charge_item->display( '/_service-charge-item.php' );
			?>

		<?php endforeach; ?>
	</div>

	<?php echo $this->get('template'); ?>

</div>