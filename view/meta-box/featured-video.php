<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-meta-box -type-changable -block">

	<?php if ( $this->get( 'error' ) ): ?>
		<div class="error-message"><?php echo $this->get( 'error' ); ?></div>
	<?php endif; ?>

	<div class="tabs-wrapper">
		<ul class="wp-tab-bar">
			<li
					<?php if ( $this->get( 'type' ) == 'plexYoutube' ): ?>
						class="wp-tab-active"
					<?php else: ?>
						class="hide-if-no-js"
					<?php endif; ?>>
				<a href="#plexYoutube"><?php _e('Youtube', plex_get_trans_domain()); ?></a>
			</li>
			<li
					<?php if ( $this->get( 'type' ) == 'plexVimeo' ): ?>
						class="wp-tab-active"
					<?php else: ?>
						class="hide-if-no-js"
					<?php endif; ?>>
				<a href="#plexVimeo"><?php _e('Vimeo', plex_get_trans_domain()); ?></a>
			</li>
			<li
					<?php if ( $this->get( 'type' ) == 'plexServer' ): ?>
						class="wp-tab-active"
					<?php else: ?>
						class="hide-if-no-js"
					<?php endif; ?>>
				<a href="#plexServer"><?php _e('Server', plex_get_trans_domain()); ?></a>
			</li>
			<li
					<?php if ( $this->get( 'type' ) == 'plexShortCode' ): ?>
						class="wp-tab-active"
					<?php else: ?>
						class="hide-if-no-js"
					<?php endif; ?>>
				<a href="#plexShortCode"><?php _e('ShortCode', plex_get_trans_domain()); ?></a>
			</li>
		</ul>
		<input type="hidden" class="-type"
					 name="plex_fv[type]"
					 value="<?php echo $this->get( 'type', 'plexYoutube' ); ?>" />

			<div id="plexYoutube" class="wp-tab-panel -context"
					<?php if ( $this->get( 'type', 'plexYoutube' ) == 'plexYoutube' ): ?>
						style="display: block;"
					<?php else: ?>
						style="display: none;"
					<?php endif; ?>>

			<?php if ( $this->get( 'plexYoutube[error]', null, true ) ): ?>
				<div class="error-message"><?php echo $this->get( 'plexYoutube[error]', null, true ); ?></div>
			<?php endif; ?>

			<input type="hidden" class="-image__input"
						 name="plex_fv[plexYoutube][image]"
						 value="<?php echo $this->get( 'plexYoutube[image]', null, true ); ?>">

			<input type="hidden" class="-image__att-id"
						 name="plex_fv[plexYoutube][attachment_id]"
						 value="<?php echo $this->get( 'plexYoutube[attachment_id]', null, true ); ?>">

			<p>
				<label><?php echo __( "Youtube url: ", plex_get_trans_domain() ); ?></label>
				<input type="text"
							 class="widefat"
							 name="plex_fv[plexYoutube][url]"
							 value="<?php echo $this->get( 'plexYoutube[url]', null, true ); ?>">
			</p>

			<p class="-image__preview-box">
				<?php if($this->get( 'plexYoutube[image]', null, true )): ?>
					<img src="<?php echo $this->get( 'plexYoutube[image]', null, true ); ?>" width="100%">
				<?php else: ?>
					<img src="<?php echo plex_fix_url().'/default-video-thumbnail.png'; ?>" width="100%">
				<?php endif; ?>
			</p>

		</div>

		<div id="plexVimeo" class="wp-tab-panel -context"
				<?php if ( $this->get( 'type' ) == 'plexVimeo' ): ?>
					style="display: block;"
				<?php else: ?>
					style="display: none;"
				<?php endif; ?>>

			<?php if ( $this->get( 'plexVimeo[error]', null, true ) ): ?>
				<div class="error-message"><?php echo $this->get( 'plexVimeo[error]', null, true ); ?></div>
			<?php endif; ?>

			<p>
				<input type="hidden" class="-image__input"
							 name="plex_fv[plexVimeo][image]"
							 value="<?php echo $this->get( 'plexVimeo[image]', null, true ); ?>">

				<input type="hidden" class="-image__att-id"
							 name="plex_fv[plexVimeo][attachment_id]"
							 value="<?php echo $this->get( 'plexVimeo[attachment_id]', null, true ); ?>">

				<label><?php echo __( "Vimeo url: ", plex_get_trans_domain() ); ?></label>
				<input type="text"
							 class="widefat"
							 name="plex_fv[plexVimeo][url]"
							 value="<?php echo $this->get( 'plexVimeo[url]', null, true ); ?>">
			</p>

			<p class="-image__preview-box">
				<?php if($this->get( 'plexVimeo[image]', null, true )): ?>
					<img src="<?php echo $this->get( 'plexVimeo[image]', null, true ); ?>" width="100%">
				<?php else: ?>
					<img src="<?php echo plex_fix_url().'/default-video-thumbnail.png'; ?>" width="100%">
				<?php endif; ?>
			</p>

		</div>

		<div id="plexServer" class="wp-tab-panel -context"
				<?php if ( $this->get( 'type' ) == 'plexServer' ): ?>
					style="display: block;"
				<?php else: ?>
					style="display: none;"
				<?php endif; ?>>


			<?php if ( $this->get( 'plexServer[error]', null, true ) ): ?>
				<div class="error-message"><?php echo $this->get( 'plexServer[error]', null, true ); ?></div>
			<?php endif; ?>

			<input type="hidden" class="-image__input"
						 name="plex_fv[plexServer][image]"
						 value="<?php echo $this->get( 'plexServer[image]', null, true ); ?>">

			<input type="hidden" class="-image__att-id"
						 name="plex_fv[plexServer][attachment_id]"
						 value="<?php echo $this->get( 'plexServer[attachment_id]', null, true ); ?>">

			<input type="hidden" class="-video__id"
						 name="plex_fv[plexServer][video_id]"
						 value="<?php echo $this->get( 'plexServer[video_id]', null, true ); ?>">

			<p>
				<label><?php echo __( "Video url: ", plex_get_trans_domain() ); ?></label>
				<input type="text"
						   class="widefat -video__input"
							 name="plex_fv[plexServer][url]"
							 value="<?php echo $this->get( 'plexServer[url]', null, true ); ?>">
			</p>
			<p class="-image__preview-box">
				<?php if($this->get( 'plexServer[image]', null, true )): ?>
					<img src="<?php echo $this->get( 'plexServer[image]', null, true ); ?>" width="100%">
				<?php else: ?>
					<img src="<?php echo plex_fix_url().'/default-video-thumbnail.png'; ?>" width="100%">
				<?php endif; ?>
			</p>
		</div>

		<div id="plexShortCode" class="wp-tab-panel -context"
				<?php if ( $this->get( 'type' ) == 'plexShortCode' ): ?>
					style="display: block;"
				<?php else: ?>
					style="display: none;"
				<?php endif; ?>>

			<?php if ( $this->get( 'plexShortCode[error]', null, true ) ): ?>
				<div class="error-message"><?php echo $this->get( 'plexShortCode[error]', null, true ); ?></div>
			<?php endif; ?>

			<input type="hidden" class="-image__input"
						 name="plex_fv[plexShortCode][image]"
						 value="<?php echo $this->get( 'plexShortCode[image]', null, true ); ?>">

			<input type="hidden" class="-image__att-id"
						 name="plex_fv[plexShortCode][attachment_id]"
						 value="<?php echo $this->get( 'plexShortCode[attachment_id]', null, true ); ?>">

			<p>
				<label><?php echo __( "Short code: ", plex_get_trans_domain() ); ?></label>
				<textarea class="widefat"
									name="plex_fv[plexShortCode][short_code]"><?php echo $this->get( 'plexShortCode[short_code]', null, true ); ?></textarea>
			</p>

			<p class="-image__preview-box">
				<?php if($this->get( 'plexShortCode[image]', null, true )): ?>
					<img src="<?php echo $this->get( 'plexShortCode[image]', null, true ); ?>" width="100%">
				<?php else: ?>
					<img src="<?php echo plex_fix_url().'/default-video-thumbnail.png'; ?>" width="100%">
				<?php endif; ?>
			</p>
		</div>

	</div>
</div>