<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-meta-box -client-meta-box">

	<p>
		<label>
			<?php _e( "External link: ", plex_get_trans_domain() ); ?>
		</label>
		<input type="text"
					 class="widefat"
					 name="plex_external_link[link]"
					 value="<?php echo $this->get( 'link' ); ?>">
	</p>

	<p>
		<input type="checkbox"
					 class="checkbox"
					 name="plex_external_link[in_new_page]"
					 id="showExternalLinkInNewPage"
				<?php checked( true, $this->get( 'in_new_page', true ) ); ?>>
		<label for="showExternalLinkInNewPage">
			<?php _e( 'Show in new page', plex_get_trans_domain() ); ?>
		</label>
	</p>

</div>