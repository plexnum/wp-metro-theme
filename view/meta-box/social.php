<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-meta-box -social-meta-box bootstrap-namespace">

	<div class="-wp-box">
		<div class="-wp-header">
			<?php echo __( 'Add', plex_get_trans_domain() ); ?>
		</div>
		<div class="-wp-content -add-form -social-add-form -context">
			<div class="-err"></div>
			<div class="-icons-selection -icons-selection-social">
				<?php foreach ( $this->get( 'icons', array() ) as $plex_icon ): ?>
					<i class="<?php echo $plex_icon; ?>"></i>
				<?php endforeach; ?>
			</div>
			<label><?php echo __( 'Link', plex_get_trans_domain() ); ?></label>
			<input type="text" class="widefat -link">
			<input type="hidden" class="-icon__input">
			<a class="button button-primary -action js-social-add" href="#"><?php echo __( 'Add', plex_get_trans_domain() ); ?></a>
		</div>
	</div>

	<div class="-icon-list">

		<?php $plex_social = array_merge((array)$this->get( 'plex_social', array() )); ?>

		<?php foreach ( $plex_social as $key => $plex_el ): ?>

			<?php $plex_social_element = new Plex_View();
			$plex_social_element->set_patch_prefix( $this->get_patch_prefix() );
			$plex_social_element->add( (array) $plex_el );
			$plex_social_element->set( 'key', $key );
			$plex_social_element->display( '/_social-element.php' );
			?>

		<?php endforeach; ?>
	</div>
</div>
<?php echo $this->get( 'template' ); ?>
