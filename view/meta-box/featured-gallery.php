<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-widget-box -context">

	<p>
		<a href="#" class="button-secondary -gallery__create" data-options='{"size":"<?php echo $this->get( 'size', 'plex_gallery' ); ?>"}'>
			<?php _e( "Gallery Builder", plex_get_trans_domain() ); ?>
		</a>
	</p>

	<p>
		<label>
			<?php _e( "Place From Short Code: ", plex_get_trans_domain() ); ?>
		</label>
		<textarea class="widefat -gallery__short-code"
							name="plex_fg[short_code]"><?php echo $this->get( 'short_code' ); ?></textarea>
	</p>

</div>