<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-meta-box -team-meta-box">

	<p>
		<label>
			<?php _e( "Job position: ", plex_get_trans_domain() ); ?>
		</label>

		<input type="text" class="widefat" name="plex_team[position]" value="<?php echo $this->get( 'position' ); ?>"></input>
	</p>

	<p>
		<label>
			<?php _e( "Description: ", plex_get_trans_domain() ); ?>
		</label>
		<textarea class="widefat" name="plex_team[desc]"><?php echo $this->get( 'desc' ); ?></textarea>
	</p>

</div>