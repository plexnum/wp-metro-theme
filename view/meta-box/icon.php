<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-meta-box -context">
	<div class="-icons-selection">

		<?php foreach ( $this->get( 'icons', array() ) as $icon ): ?>
			<i class="<?php echo $icon; ?>
				<?php if ( $this->get( 'icon' ) == $icon ): ?>-selected<?php endif; ?>">
			</i>
		<?php endforeach; ?>

		<input type="hidden" class="-icon__input"
					 name="plex_icon[icon]"
					 value="<?php echo $this->get( 'icon' ); ?>">
	</div>
</div>