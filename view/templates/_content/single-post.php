<?php if ( !defined('ABSPATH')) exit; ?>
<?php

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();

$plex_sidebar_grid = new Plex_Lib_SidebarGrid( array(
	'post-left-sidebar'  => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_left_sidebar]', true ),
		'width'  => 3
	),
	'post-right-sidebar' => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_right_sidebar]', true ),
		'width'  => 3
	)
) );

?>

<div class="row">

	<?php plex_the_sidebar( 'post-left-sidebar', $plex_sidebar_grid ); ?>

	<div class="<?php $plex_sidebar_grid->the_content_class(); ?>">
		<ul class="blog-list">
			<?php if ( have_posts() ): ?>
				<?php while ( have_posts() ) :
					the_post(); ?>

					<li id="post-<?php the_ID(); ?>" <?php post_class( array(
						'blog-item'
					) ); ?>>

						<div class="blog-item__content content">

							<?php the_content(); ?>

							<div class="aligncenter pagination-post">
								<?php wp_link_pages( array(
									'before'      => '',
									'after'       => '',
									'link_before' => '<span>',
									'link_after'  => '</span>'
								) ); ?>
							</div>

							<?php $plex_social = get_theme_mod( 'plex_social', array() ); ?>

							<?php $plex_tags = get_the_tags(); ?>

							<?php if ( ! empty( $plex_social['plex_social'] ) || ! empty( $plex_tags ) ): ?>
								<hr>
								<div class="blog-item__footer">
									<?php if ( ! empty( $plex_social['plex_social'] ) ): ?>
										<div class="social">
											<h3><?php echo __( 'Social Media', plex_get_trans_domain() ); ?></h3>
											<?php foreach ( $plex_social['plex_social'] as $plex_icon ): ?>
												<a target="_blank" href="<?php echo esc_url( $plex_icon['link'] ); ?>" class="icon_cub  icon_lage <?php echo esc_attr( $plex_icon['icon'] ); ?>"></a>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
									<?php if (
										! empty( $plex_social['plex_social'] ) &&
										! empty( $plex_tags )
									): ?>
										<div class="split"></div>
									<?php endif; ?>
									<?php if ( ! empty( $plex_tags ) ): ?>
										<div class="blog-item__tagcloud">
											<h3> <?php echo __( 'Tags:', plex_get_trans_domain() ); ?></h3>

											<div class="tagcloud">
												<?php the_tags( '', ' ', '' ); ?>
											</div>
										</div>
									<?php endif; ?>
								</div>
							<?php endif; ?>

							<div class="bg-main-bg"></div>
						</div>
						<?php comments_template( '/view/templates/_partials/comments.php' ); ?>
					</li>
				<?php endwhile; ?>
			<?php endif; ?>
		</ul>
	</div>

	<?php plex_the_sidebar( 'post-right-sidebar', $plex_sidebar_grid ); ?>

</div>