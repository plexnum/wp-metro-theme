<?php if ( !defined('ABSPATH')) exit; ?>
<h4 class="-blog-name"><?php echo bloginfo('name'); ?></h4>
<p class="-blog-desc">
	<?php echo bloginfo('description'); ?>
</p>

<?php if ( is_active_sidebar( 'front-page-sidebar' ) ) : ?>
	<div class="row grid-metro js-grid-metro">
		<?php dynamic_sidebar( 'front-page-sidebar' ); ?>
	</div>
<?php endif; ?>