<?php if ( !defined('ABSPATH')) exit; ?>
<?php if ( has_post_thumbnail() ) : ?>
	<div class="blog-item__img">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail(); ?>
		</a>
	</div>
<?php endif; ?>