<?php if ( !defined('ABSPATH')) exit; ?>
<?php $plex_featured_gallery = plex_get_post_gallery_short_code( get_the_ID() ); ?>

<?php if ( ! empty( $plex_featured_gallery ) ): ?>
	<div class="blog-item__gallery" >
		<?php echo do_shortcode( wp_kses_data( $plex_featured_gallery ) ); ?>
	</div>
<?php endif; ?>