<?php if ( !defined('ABSPATH')) exit; ?>
<?php $plex_featured_video = plex_get_post_video( get_the_ID() ); ?>
<?php if ( $plex_featured_video ): ?>

	<div class="blog-item__video">

		<?php if ( $plex_featured_video->get( 'type' ) == 'plexYoutube' ): ?>
			<?php echo wp_oembed_get( esc_url( $plex_featured_video->get( 'url' ) ) ); ?>
		<?php endif; ?>

		<?php if ( $plex_featured_video->get( 'type' ) == 'plexVimeo' ): ?>
			<?php echo wp_oembed_get( esc_url( $plex_featured_video->get( 'url' ) ) ); ?>
		<?php endif; ?>

		<?php if ( $plex_featured_video->get( 'type' ) == 'plexServer' ): ?>
			<?php echo do_shortcode( '[video src="' . esc_url( $plex_featured_video->get( 'url' ) ) . '"][/video]' ); ?>
		<?php endif; ?>

		<?php if ( $plex_featured_video->get( 'type' ) == 'plexShortCode' ): ?>
			<?php echo do_shortcode( wp_kses_data( $plex_featured_video->get( 'short_code' ) ) ); ?>
		<?php endif; ?>

	</div>

<?php endif; ?>