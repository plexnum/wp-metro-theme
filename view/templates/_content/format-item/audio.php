<?php if ( !defined('ABSPATH')) exit; ?>
<?php $plex_featured_audio = plex_get_post_audio( get_the_ID() ); ?>

<?php if ( $plex_featured_audio ): ?>

	<div class="blog-item__audio">

		<?php if ( $plex_featured_audio->get( 'type' ) == 'plexAudio' ): ?>
			<?php echo do_shortcode( '[audio src="' . esc_url( $plex_featured_audio->get( 'url' ) ) . '"][/audio]' ); ?>

		<?php elseif ( $plex_featured_audio->get( 'type' ) == 'plexAudioShortCode' ): ?>
			<?php echo apply_filters( 'the_content', $plex_featured_audio->get( 'short_code' ) ); ?>
		<?php endif; ?>

	</div>

<?php endif; ?>