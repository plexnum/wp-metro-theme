<?php if ( !defined('ABSPATH')) exit; ?>
<?php

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();

$plex_sidebar_grid = new Plex_Lib_SidebarGrid( array(
	'posts-left-sidebar'  => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_left_sidebar]', true ),
		'width'  => 3
	),
	'posts-right-sidebar' => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_right_sidebar]', true ),
		'width'  => 3
	)
) );

?>

<div class="row">

	<?php plex_the_sidebar( 'posts-left-sidebar', $plex_sidebar_grid ); ?>

	<div class="<?php $plex_sidebar_grid->the_content_class(); ?>">
		<?php if ( have_posts() ): ?>
			<?php $i = 1; ?>

			<?php while ( have_posts() ) :
				the_post(); ?>

				<?php plex_show_in_row( array(
				'els'      => $plex_sidebar_grid->calculate_fluid_number_of_items(
					$plex_view_transfer->get( 'el_width', 6 )
				),
				'start'    => '<div class="from-blog"><div class="row-fluid">',
				'end'      => '</div></div>',
				'between'  => '<hr class="hr-min js-animate">',
				'view'     => 'view/templates/_partials/post-magazine-item/item',
				'iterator' => $i
			) ); ?>

				<?php $i ++; endwhile; ?>

			<?php plex_load_partial( 'view/templates/_partials/pagenation' ); ?>
		<?php endif; ?>
	</div>

	<?php plex_the_sidebar( 'posts-right-sidebar', $plex_sidebar_grid ); ?>

</div>