<?php if ( !defined('ABSPATH')) exit; ?>
<?php

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();

$plex_sidebar_type = $plex_view_transfer->get( 'sidebar_type', 'search' );

$plex_sidebar_grid = new Plex_Lib_SidebarGrid( array(
	$plex_sidebar_type . '-left-sidebar'  => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_left_sidebar]', true ),
		'width'  => 3
	),
	$plex_sidebar_type . '-right-sidebar' => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_right_sidebar]', true ),
		'width'  => 3
	)
) );

?>

<div class="row">

	<?php plex_the_sidebar( $plex_sidebar_type . '-left-sidebar', $plex_sidebar_grid ); ?>

	<div class="<?php $plex_sidebar_grid->the_content_class(); ?>">
		<div class="from-blog">
			<?php if ( have_posts() ): ?>
				<?php $plex_divider = ''; ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php echo $plex_divider; ?>
					<?php plex_load_partial( 'view/templates/_partials/post-search-item/item' ); ?>
					<?php $plex_divider = '<hr class="js-animate hr-min">'; ?>
				<?php endwhile; ?>
			<?php else: ?>
				<?php echo do_shortcode( '[alert type="info" with_close_btn="false"]<h2>' . $plex_view_transfer->get( 'not_founded' ) . '</h2>[/alert]' ); ?>
			<?php endif; ?>
		</div>

		<?php plex_load_partial( 'view/templates/_partials/pagenation' ); ?>

	</div>

	<?php plex_the_sidebar( $plex_sidebar_type . '-right-sidebar', $plex_sidebar_grid ); ?>

</div>