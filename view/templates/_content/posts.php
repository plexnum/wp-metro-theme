<?php if ( !defined('ABSPATH')) exit; ?>
<?php

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();

$plex_sidebar_type = $plex_view_transfer->get( 'sidebar_type', 'posts' );

$plex_sidebar_grid = new Plex_Lib_SidebarGrid( array(
	$plex_sidebar_type . '-left-sidebar'  => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_left_sidebar]', true ),
		'width'  => 3
	),
	$plex_sidebar_type . '-right-sidebar' => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_right_sidebar]', true ),
		'width'  => 3
	)
) );

?>

<div class="row">

	<?php plex_the_sidebar( $plex_sidebar_type . '-left-sidebar', $plex_sidebar_grid ); ?>

	<div class="<?php $plex_sidebar_grid->the_content_class(); ?>">
		<ul class="blog-list">
			<?php if ( have_posts() ): ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<li id="post-<?php the_ID(); ?>" <?php post_class( array(
						'blog-item',
						'js-animate'
					) ); ?>>

						<?php plex_load_partial( '/view/templates/_partials/post-item/header' ); ?>

						<?php plex_load_partial( 'view/templates/_content/format-item/default',
								'view/templates/_content/format-item/' . get_post_format() ); // Load post
						?>

						<div class="blog-item__content content">
							<div class="bg-main-bg"></div>
							<?php plex_load_partial( '/view/templates/_partials/post-item/excerpt' ); ?>
						</div>
					</li>
				<?php endwhile; ?>
			<?php endif; ?>
		</ul>

		<?php plex_load_partial( 'view/templates/_partials/pagenation' ); ?>

	</div>

	<?php plex_the_sidebar( $plex_sidebar_type . '-right-sidebar', $plex_sidebar_grid ); ?>

</div>