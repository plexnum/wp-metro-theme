<?php if ( !defined('ABSPATH')) exit; ?>
<?php if ( has_post_thumbnail() ) : ?>
	<div class="cont tile_img tile_hover">

		<?php the_post_thumbnail( 'post-thumbnail', array(
			'class' => 'js-imgCentr'
		) ); ?>

		<div class="dis">
			<a href="<?php the_permalink(); ?>" class="icon-link icon_big icon_in-bubble"></a>
			<?php if ( has_post_thumbnail() ) : ?>
				<a href="<?php echo plex_get_attachment_image( get_post_thumbnail_id( get_the_ID() ), 'full' ); ?>" class="icon-image icon_big icon_in-bubble js-fancybox"></a>
			<?php endif; ?>
		</div>

	</div>
<?php else: ?>
	<?php plex_load_partial( 'view/templates/_content/project-item-preview/empty-media' ); ?>
<?php endif; ?>