<?php if ( !defined('ABSPATH')) exit; ?>
<?php $plex_featured_video = plex_get_post_video( get_the_ID() ); ?>

<?php if ( $plex_featured_video ): ?>

	<div class="cont tile_img tile_hover">

		<?php plex_the_featured_image( $plex_featured_video ); ?>

		<div id="plexVideo<?php echo get_the_ID(); ?>"
				 data-width="800"
				 style="display: none;">
			<?php plex_load_partial( 'view/templates/_content/format-item/video' ); ?>
		</div>

		<div class="dis">
			<a href="<?php the_permalink(); ?>" class="icon-link icon_big icon_in-bubble"></a>
			<a href="#plexVideo<?php echo get_the_ID(); ?>" class="icon-play-2 icon_big icon_in-bubble js-fancybox-media"></a>
		</div>

	</div>

<?php else: ?>
	<?php plex_load_partial( 'view/templates/_content/project-item-preview/empty-media' ); ?>
<?php endif; ?>