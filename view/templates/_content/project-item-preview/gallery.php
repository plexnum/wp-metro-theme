<?php if ( !defined('ABSPATH')) exit; ?>
<?php $plex_gallery_short_code = plex_get_post_gallery_short_code( get_the_ID() ); ?>

<?php if ( ! empty( $plex_gallery_short_code ) ): ?>
	<div class="cont tile_carousel tile_hover js-gallery-context" data-gallery-override='{
		"auto"      : {
			"play"	: true
		},
		"buttons": {
			"enable" : false
		}
	}'>

		<?php echo do_shortcode( wp_kses_data( plex_get_post_gallery_short_code( get_the_ID() ) ) ); ?>

		<div class="dis">
			<a href="<?php the_permalink(); ?>" class="icon-link icon_big icon_in-bubble"></a>
			<a href="<?php the_permalink(); ?>" class="icon-images icon_big icon_in-bubble js-gallery-trigger"></a>
		</div>

	</div>
<?php else: ?>
	<?php plex_load_partial( 'view/templates/_content/project-item-preview/empty-media' ); ?>
<?php endif; ?>