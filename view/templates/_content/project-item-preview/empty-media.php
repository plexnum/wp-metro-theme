<?php if ( !defined('ABSPATH')) exit; ?>
<a href="<?php the_permalink(); ?>" class="cont tile_text-bgc">
	<div class="dis">
		<div class="scroller">
			<?php the_title(); ?>
			<div class="scroller__track">
				<div class="scroller__bar"></div>
			</div>
		</div>
	</div>
</a>