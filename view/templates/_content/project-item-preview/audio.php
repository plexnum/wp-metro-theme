<?php if ( !defined('ABSPATH')) exit; ?>
<?php
$plex_featured_audio = plex_get_post_audio( get_the_ID() );
?>

<?php if ( $plex_featured_audio ): ?>

	<div class="cont tile_img tile_hover">

		<?php plex_the_featured_image( $plex_featured_audio ); ?>

		<?php if ( $plex_featured_audio->get( 'type' ) == 'plexAudioShortCode' ): ?>
			<div id="plexAudio<?php echo get_the_ID(); ?>" style="display: none;">
				<?php plex_load_partial( 'view/templates/_content/format-item/audio' ); ?>
			</div>
		<?php else: ?>
			<div id="plexAudio<?php echo get_the_ID(); ?>"
					 data-width="282"
					 style="display: none; width:282px; height: 50px">
				<?php plex_load_partial( 'view/templates/_content/format-item/audio' ); ?>
			</div>
		<?php endif; ?>

		<div class="dis">
			<a href="<?php the_permalink(); ?>" class="icon-link icon_big icon_in-bubble "></a>
			<a href="#plexAudio<?php echo get_the_ID(); ?>" class="icon-headphones icon_big icon_in-bubble js-fancybox-media"></a>
		</div>

	</div>

<?php else: ?>
	<?php plex_load_partial( 'view/templates/_content/project-item-preview/empty-media' ); ?>
<?php endif; ?>