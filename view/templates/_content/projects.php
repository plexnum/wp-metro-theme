<?php if ( !defined('ABSPATH')) exit; ?>
<?php

$plex_transfer = Plex_Lib_ViewTransfer::get_transfer();

$plex_categories = get_categories( array(
	'taxonomy' => 'project_category'
) );

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();

$plex_sidebar_grid = new Plex_Lib_SidebarGrid( array(
	'projects-left-sidebar'  => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_left_sidebar]', true ),
		'width'  => 3
	),
	'projects-right-sidebar' => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_right_sidebar]', true ),
		'width'  => 3
	)
) );

?>

<div class="row">

	<?php plex_the_sidebar( 'projects-left-sidebar', $plex_sidebar_grid ); ?>

	<div class="<?php $plex_sidebar_grid->the_content_class(); ?>">

		<?php _e( 'Filter', plex_get_trans_domain() ); ?>:
		<ul class="filter-portfolio js-filter-portfolio">
			<li class="active">
				<a class="btn btn_grey" data-filter="*" href="#"><?php _e( 'all', plex_get_trans_domain() ); ?>
					<span class="bg-main-bg"></span>
				</a>
			</li>
			<?php foreach ( $plex_categories as $plex_category ): ?>
				<li>
					<a class="btn btn_grey" data-filter=".<?php esc_attr_e( 'fi-' . $plex_category->slug ); ?>" href="#"><?php esc_html_e( $plex_category->name ); ?>
						<span class="bg-main-bg"></span>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>

		<div class="puzzles puzzles_col<?php echo $plex_transfer->get( 'col', 2 ); ?> js-grid-portfolio">
			<div class="grid-sizer"></div>

			<?php if ( have_posts() ): ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<div class="item <?php plex_the_convert_terms_to_classes(
						get_the_terms( null, 'project_category' ),
						'fi-'
					); ?> js-animate">

						<div class="wrap">
							<?php plex_load_partial( 'view/templates/_content/project-item-preview/default',
									'view/templates/_content/project-item-preview/' . get_post_format() ); // Load post
							?>
						</div>

					</div>

				<?php endwhile; ?>
			<?php endif; ?>
		</div>

	</div>

	<?php plex_the_sidebar( 'projects-right-sidebar', $plex_sidebar_grid ); ?>

</div>