<?php if ( !defined('ABSPATH')) exit; ?>
<?php

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();

$plex_sidebar_grid = new Plex_Lib_SidebarGrid( array(
	'page-left-sidebar'  => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_left_sidebar]', true ),
		'width'  => 3
	),
	'page-right-sidebar' => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_right_sidebar]', true ),
		'width'  => 3
	)
) );

?>

<div class="row">

	<?php plex_the_sidebar( 'page-left-sidebar', $plex_sidebar_grid ); ?>

	<div class="<?php $plex_sidebar_grid->the_content_class(); ?>">
		<ul class="blog-list">
			<?php if ( have_posts() ): ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<li id="post-<?php the_ID(); ?>" <?php post_class( array(
						'blog-item'
					) ); ?>>
						<div class="blog-item__content content">
							<?php the_content(); ?>
							<div class="bg-main-bg"></div>
						</div>
					</li>
				<?php endwhile; ?>
			<?php endif; ?>
		</ul>
	</div>

	<?php plex_the_sidebar( 'page-right-sidebar', $plex_sidebar_grid ); ?>

</div>