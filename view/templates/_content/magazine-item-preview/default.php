<?php if ( !defined('ABSPATH')) exit; ?>
<?php if ( has_post_thumbnail() ) : ?>

	<a href="<?php echo plex_get_attachment_image( get_post_thumbnail_id( get_the_ID() ), 'full' ); ?>" class="cont js-fancybox">
		<?php if ( has_post_thumbnail() ) : ?>

			<?php the_post_thumbnail( 'post-thumbnail', array(
				'class' => 'js-imgCentr'
			) ); ?>

		<?php endif; ?>
		<i class="icon-image"></i>
	</a>

<?php else: ?>
	<a href="<?php the_permalink(); ?>" class="cont">
		<i class="icon-image"></i>
	</a>
<?php endif; ?>