<?php if ( !defined('ABSPATH')) exit; ?>
<?php $plex_featured_video = plex_get_post_video( get_the_ID() ); ?>

<?php if ( $plex_featured_video ): ?>

	<a class="cont js-fancybox-media" href="#plexVideo<?php echo get_the_ID(); ?>">
		<?php plex_the_featured_image( $plex_featured_video ); ?>
		<i class="icon-play-2 icon_in-bubble"></i>
	</a>

	<div id="plexVideo<?php echo get_the_ID(); ?>"
			 data-width="800"
			 style="display: none;">
		<?php plex_load_partial( 'view/templates/_content/format-item/video' ); ?>
	</div>

<?php else: ?>
	<a href="<?php the_permalink(); ?>" class="cont">
		<i class="icon-play-2 icon_in-bubble"></i>
	</a>
<?php endif; ?>