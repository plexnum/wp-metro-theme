<?php if ( !defined('ABSPATH')) exit; ?>
<?php $plex_featured_audio = plex_get_post_audio( get_the_ID() ); ?>

<?php if ( $plex_featured_audio ): ?>

	<a class="cont js-fancybox-media" href="#plexAudio<?php echo get_the_ID(); ?>">
		<?php plex_the_featured_image( $plex_featured_audio ); ?>
		<i class="icon-headphones icon_in-bubble"></i>
	</a>

	<?php if ( $plex_featured_audio->get( 'type' ) == 'plexAudioShortCode' ): ?>
		<div id="plexAudio<?php echo get_the_ID(); ?>" style="display: none;">
			<?php plex_load_partial( 'view/templates/_content/format-item/audio' ); ?>
		</div>
	<?php else: ?>
		<div id="plexAudio<?php echo get_the_ID(); ?>"
				 data-width="282"
				 style="display: none; width:282px; height: 50px">
			<?php plex_load_partial( 'view/templates/_content/format-item/audio' ); ?>
		</div>
	<?php endif; ?>

<?php else: ?>
	<a href="<?php the_permalink(); ?>" class="cont">
		<i class="icon-headphones icon_in-bubble"></i>
	</a>
<?php endif; ?>