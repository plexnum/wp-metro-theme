<?php if ( !defined('ABSPATH')) exit; ?>
<?php $plex_featured_gallery = plex_get_post_gallery_short_code( get_the_ID() ); ?>
<?php if ( ! empty( $plex_featured_gallery ) ): ?>
	<div class="cont js-gallery-context">
		<?php echo do_shortcode( wp_kses_data( $plex_featured_gallery ) ); ?>
		<a href="<?php the_permalink(); ?>" class="js-gallery-trigger icon_wrap">
			<i class="icon-images "></i>
		</a>
	</div>
<?php endif; ?>