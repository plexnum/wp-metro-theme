<?php if ( !defined('ABSPATH')) exit; ?>
<?php

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();

$plex_sidebar_grid = new Plex_Lib_SidebarGrid( array(
	'project-left-sidebar'  => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_left_sidebar]', true ),
		'width'  => 3
	),
	'project-right-sidebar' => array(
		'active' => $plex_view_transfer->get( 'sidebars_settings[display_right_sidebar]', true ),
		'width'  => 3
	)
) );

?>

<div class="row">

	<?php plex_the_sidebar( 'project-left-sidebar', $plex_sidebar_grid ); ?>

	<div class="<?php $plex_sidebar_grid->the_content_class(); ?>">
		<ul class="blog-list">
			<?php if ( have_posts() ): ?>
				<?php while ( have_posts() ) :
					the_post(); ?>


					<?php wp_link_pages( array(
					'before'      => '',
					'after'       => '',
					'link_before' => '<span>',
					'link_after'  => '</span>'
				) ); ?>

					<li id="post-<?php the_ID(); ?>" <?php post_class( array(
						'blog-item'
					) ); ?>>

						<div class="blog-item__content content">

							<?php the_content(); ?>

							<?php $plex_social = get_theme_mod( 'plex_social', array() ); ?>

							<?php if ( ! empty( $plex_social['plex_social'] ) ): ?>
								<hr class="js-animate">
								<div class="blog-item__footer js-animate">
									<?php $plex_social = get_theme_mod( 'plex_social', array() ); ?>
									<div class="social">
										<h3><?php echo __( 'Social Media', plex_get_trans_domain() ); ?></h3>
										<?php foreach ( $plex_social['plex_social'] as $plex_icon ): ?>
											<a target="_blank" href="<?php echo esc_url( $plex_icon['link'] ); ?>" class="icon_cub icon_lage <?php echo esc_attr( $plex_icon['icon'] ); ?>"></a>
										<?php endforeach; ?>
									</div>
								</div>
							<?php endif; ?>

							<div class="bg-main-bg"></div>
						</div>
					</li>
				<?php endwhile; ?>
			<?php endif; ?>
		</ul>
	</div>

	<?php plex_the_sidebar( 'project-right-sidebar', $plex_sidebar_grid ); ?>

</div>