<?php if ( !defined('ABSPATH')) exit; ?>
<div class="sidebar
<?php $this->get( 'sidebar_grid' )->the_sidebar_class( $this->get( 'id' ) ); ?>
<?php $this->get( 'sidebar_grid')->the_js(); ?>">

	<div class="sidebar_icon ">
		<a href="#" class="js-sade-bar-toggler"><i class="icon-equalizer icon_in-bubble"></i></a>
	</div>

	<div class="sidebar__wrap">
		<?php dynamic_sidebar( $this->get('id') ); ?>
	</div>
</div>