<?php if ( !defined('ABSPATH')) exit; ?>
<div class="pagination js-animate">
	<?php

	global $wp_query;

	$plex_big = 999999999;

	echo paginate_links( array(
		'base'    => str_replace( $plex_big, '%#%', esc_url( get_pagenum_link( $plex_big ) ) ),
		'format'  => '?paged=%#%',
		'current' => max( 1, get_query_var( 'paged' ) ),
		'total'   => $wp_query->max_num_pages
	) );

	?>

</div>