<?php if ( !defined('ABSPATH')) exit; ?>
<li <?php comment_class( $this->get( 'args[has_children]' ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">

		<div class="comment-author vcard">
			<?php if ( $this->get( 'args[avatar_size]' ) != 0 ): ?>
				<?php echo get_avatar( $this->get('comment'), $this->get('avatar_size') ); ?>
			<?php endif; ?>
			<?php printf( __( '<cite class="fn">%s</cite> <span class="says">says:</span>', plex_get_trans_domain() ), get_comment_author_link() ) ?>
		</div>

		<?php if ( $this->get( 'comment' )->comment_approved == '0' ): ?>
			<em class="comment-awaiting-moderation">
				<?php _e( 'Your comment is awaiting moderation.', plex_get_trans_domain() ) ?>
			</em>
			<br />
		<?php endif; ?>

		<?php comment_text() ?>

		<div class="reply">
			<?php comment_reply_link(
				array_merge( $this->get( 'args' ),
					array(
						'depth'     => $this->get( 'depth' ),
						'max_depth' => $this->get( 'args[max_depth]' )
					)
				)
			); ?>
		</div>

		<div class="comment-meta commentmetadata">
			<a href="<?php echo htmlspecialchars( get_comment_link( $this->get( 'comment' )->comment_ID ) ) ?>">
				<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', plex_get_trans_domain() ), get_comment_date(), get_comment_time() ) ?></a><?php edit_comment_link( __( '(Edit)', plex_get_trans_domain() ), '&nbsp;&nbsp;', '' );
			?>
		</div>

	</div>