<?php if ( !defined('ABSPATH')) exit; ?>
<?php if ( get_comments_number() > 0 ): ?>
	<div class="comment-box js-animate">
		<div class="bg-main-bg"></div>
		<h2 class="page-header">
			<?php comments_number(); ?>
		</h2>

		<ol class="commentlist">
			<?php
			wp_list_comments( array(
				'reverse_top_level' => false, //Show the latest comments at the top of the list
				'callback'          => array( 'Plex_Component_CommentResolver', 'resolve' ),
				'end-callback'      => array( 'Plex_Component_CommentResolver', 'end_resolve')
			) );
			?>
		</ol>

		<div class="pagination">
			<?php
			paginate_comments_links();
			?>
		</div>
	</div>
<?php endif; ?>
<?php if ( comments_open() ): ?>
	<div class="feedback mt20 js-animate">
		<div class="bg-main-bg"></div>
		<h2 class="page-header"><?php echo _e( 'Add comment', plex_get_trans_domain() ); ?></h2>
		<?php comment_form(); ?>
	</div>
<?php endif; ?>