<?php if ( !defined('ABSPATH')) exit; ?>
<div class="blog-item__header">
	<div class="bg-main-bg"></div>
	<time>
		<span class="dey"><?php echo get_the_date('d'); ?></span>
		<span class="mon"><?php echo get_the_date('M'); ?></span>
	</time>
	<h3>
		<?php if(!is_single()): ?>
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Link to %s', plex_get_trans_domain() ), the_title_attribute( 'echo=0' ) ) ); ?>"><?php the_title(); ?></a>
		<?php else: ?>
			<?php the_title(); ?>
		<?php endif; ?>
	</h3>
	<div class="blog-item__prop">
		<i class="icon-user"></i><?php the_author(); ?><span class="split">|</span>
		<i class="icon-pencil"></i><?php the_category(' / '); ?><span class="split">|</span>
		<i class="icon-bubbles"></i><?php plex_the_comment_count(); ?> <?php _e('Comments', plex_get_trans_domain()); ?>
	</div>
</div>