<?php if ( !defined('ABSPATH')) exit; ?>
<div class="separator__wrap  separator_right">
	<?php if ( get_the_excerpt() ): ?>

		<div class="separator__content">
			<?php the_excerpt(); ?>
		</div>

		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Link to %s', plex_get_trans_domain() ), the_title_attribute( 'echo=0' ) ) ); ?>" class="separator">
			<i class="icon-next icon_lage icon_in-bubble"></i>
		</a>
	<?php endif; ?>
</div>
