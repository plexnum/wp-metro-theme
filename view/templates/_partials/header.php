<?php if ( !defined('ABSPATH')) exit; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php wp_title( '|', true, 'right' ); ?> <?php echo bloginfo( 'name' ); ?></title>
	<meta name="description" content="<?php bloginfo( 'description' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link href='http://fonts.googleapis.com/css?family=<?php echo esc_attr( get_theme_mod( 'plex_header_font', 'Maven+Pro' ) ); ?>|<?php echo esc_attr( get_theme_mod( 'plex_text_font', 'Open+Sans' ) ); ?>' rel='stylesheet'
				type='text/css'>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" type="text/css" media="screen" />

	<script type="text/javascript">
		window.defaultTypeAnimate = 'fadeInUp';
		window.intervalAnimate = 100;
	</script>

	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

</head>

<?php if (is_front_page()): ?>
<body <?php body_class( 'bg-imag' ); ?>>
<div class="fake-bg" style="background-image: url(<?php echo esc_attr( plex_get_theme_mod( 'background_image', plex_get_fixture_dir_url() . '/bgi-index-1.jpg' ) ); ?>);"></div>
<?php else: ?>
<body <?php body_class(); ?>>
<?php endif; ?>

<!--[if lt IE 7]>
<p class="chromeframe"><?php _e('You are using an <strong>outdated</strong> browser. Please upgrade your browser .', plex_get_trans_domain() ); ?></p>
<![endif]-->

<div class="menu-box ">
	<div class="bg-main-bg"></div>

	<div class="menu-btn-box">
		<span class="js-menu-toggler icon-menu icon_in-bubble"></span>
		<?php if ( is_single() ): ?>
			<?php previous_post_link( '%link', '%title', true ); ?>
			<?php next_post_link( '%link', '%title', true ); ?>
		<?php endif; ?>
	</div>

	<?php if (has_nav_menu( 'main' )): ?>
	<div class="menu-wrap">
		<div class="scroller">
			<?php wp_nav_menu( array(
				'theme_location' => 'main',
				'container'      => false,
				'items_wrap'     => '<ul class="menu js-menu">%3$s</ul>',
				'depth'          => 2,
				'link_after'     => '<i class="icon-next icon_in-bubble"></i>'
			) ); ?>

			<?php endif; ?>
			<div class="scroller__track">
				<div class="scroller__bar"></div>
			</div>
		</div>
	</div>
</div>

<div class="container-wrap">
	<div class="container">
		<?php if (! is_404()): ?>
		<a itemprop="url" class="logo-main" href="#">
			<img itemprop="logo" src="<?php echo plex_get_logo(); ?>" alt="" />
		</a>
		<?php endif; ?>