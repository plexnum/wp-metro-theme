<?php if ( !defined('ABSPATH')) exit; ?>
<div class=" js-animate">
	<div class="row">
		<div class="span3">
			<div class="preview-type-post">
				<?php plex_load_partial( 'view/templates/_content/magazine-item-preview/default',
						'view/templates/_content/magazine-item-preview/' . get_post_format() ); // Load post
				?>
				<div class="bg-main-bg"></div>
			</div>
		</div>
		<div class="span9">
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<time><?php echo get_the_date();
					_e( ' by ', plex_get_trans_domain() );
					the_author() ?></time>
				<h3>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h3>

				<?php plex_load_partial( '/view/templates/_partials/post-item/excerpt' ); ?>

			</div>
		</div>
	</div>

</div>