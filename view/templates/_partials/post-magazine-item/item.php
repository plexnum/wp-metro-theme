<?php if ( !defined('ABSPATH')) exit; ?>
<?php $plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer(); ?>

<div class="span<?php echo $plex_view_transfer->get( 'el_width', 6 ); ?> js-animate">

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="preview-type-post">
			<?php plex_load_partial( 'view/templates/_content/magazine-item-preview/default',
					'view/templates/_content/magazine-item-preview/' . get_post_format() ); // Load post
			?>
			<div class="bg-main-bg"></div>
		</div>

		<time><?php echo get_the_date();
			_e( ' by ', plex_get_trans_domain() );
			the_author() ?></time>

		<h3>
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		</h3>

		<?php plex_load_partial( '/view/templates/_partials/post-item/excerpt' ); ?>

	</div>

</div>