<?php if ( !defined('ABSPATH')) exit; ?>
<?php

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();

if ( is_page() ) {

	if ( ! $plex_view_transfer->has( 'page_header' ) ) {
		$plex_view_transfer->set( 'page_header', get_the_title() );
	}

	if ( ! $plex_view_transfer->has( 'page_icon' ) ) {
		$plex_view_transfer->set( 'page_icon', plex_get_post_icon( get_the_ID(), 'icon-pencil' ) );
	}


	if ( ! $plex_view_transfer->has( 'page_excerpt' ) ) {
		$plex_view_transfer->set( 'page_excerpt', get_the_excerpt() );
	}

}

?>

<header>
	<?php if ( is_front_page() || $plex_view_transfer->get( 'page_header', false ) ): ?>
		<div class="bg-main-bg"></div>

		<h1>
			<i class="<?php echo esc_attr( $plex_view_transfer->get( 'page_icon' ) ); ?>"></i>

			<?php if ( $plex_view_transfer->has( 'page_header' ) ): ?>
				<?php echo esc_html( $plex_view_transfer->get( 'page_header' ) ); ?>
			<?php elseif ( is_front_page() ): ?>
				<?php bloginfo( 'name' ); ?>
			<?php endif; ?>
		</h1>

		<?php if ( $plex_view_transfer->has( 'page_excerpt' ) ): ?>

			<?php $plex_top_excerpt = trim( wp_kses_data( $plex_view_transfer->get( 'page_excerpt' ) ) ); ?>

			<?php if ( ! empty( $plex_top_excerpt ) ): ?>
				<p><?php echo $plex_top_excerpt; ?></p>
			<?php endif; ?>

		<?php elseif ( is_front_page() ): ?>
			<?php $plex_top_excerpt = trim( get_bloginfo( 'description', 'display' ) ); ?>

			<?php if ( $plex_top_excerpt ): ?>
				<p><?php echo wp_kses_data( $plex_top_excerpt ); ?></p>
			<?php endif; ?>
		<?php endif; ?>

	<?php endif; ?>
</header>