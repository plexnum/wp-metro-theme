<?php if ( !defined('ABSPATH')) exit; ?>
</div>
</div>
<footer>
	<div class="bg-main-bg"></div>
	<div class="container -blog-copyrights">
		<?php echo get_theme_mod('plex_copyright', 'METROBIZZ &copy; 2013 | Privacy policy'); ?>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>