<?php if ( !defined('ABSPATH')) exit; ?>

<ul>
	<li><span><span class="bg-main-bg"></span><?php printf("Page %1s of %2s", $this->get('current_page'), $this->get('max_pages')); ?></span></li>
	<?php foreach($this->get('pages', array()) as $pages): ?>
			<li <?php if($pages['name'] == $this->get('current_page')): ?>class="active"<?php endif; ?>><a href="<?php echo $pages['url']; ?>"><span class="bg-main-bg"></span><?php echo $pages['name']; ?></a></li>
		<?php endforeach; ?>
</ul>