<?php

if ( !defined('ABSPATH')) exit;

// Obtain data
$plex_testimonial = new Plex_Config_ParamBag(
	(array) get_post_meta( get_the_ID(), '_testimonial', true )
);

$plex_transfer = Plex_Lib_ViewTransfer::get_transfer();

?>

<?php if ( ! $plex_transfer->get( 'show[show_as_carousel]' ) ): ?>
	<div class="span<?php echo $plex_transfer->get( 'el_width' ); ?> <?php echo $plex_transfer->get( 'show[animation]' )->get( 'class' ); ?>" <?php echo $plex_transfer->get( 'show[animation]' )->get( 'animation_type' ); ?>>
<?php endif; ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class( array( "testimonial__wrap" ) ); ?>>
		<div class="testimonial">
			<blockquote class="blockquote_icon">
				<?php echo $plex_transfer->get( 'icon' ); ?>
				<p><?php echo esc_html( $plex_testimonial->get( 'testimonial' ) ); ?></p>
			</blockquote>
			<i class="testimonial__arrow"></i>

			<div class="testimonial__person">

				<?php the_post_thumbnail( 'plex_testimonial', 'class=pull-left' ); ?>

				<?php if ( $plex_testimonial->get( 'author' ) ): ?>
					<h3><?php echo esc_html( $plex_testimonial->get( 'author' ) ); ?></h3>
				<?php endif; ?>

				<?php if ( $plex_testimonial->get( 'position' ) ): ?>
					<h4><?php echo esc_html( $plex_testimonial->get( 'position' ) ); ?></h4>
				<?php endif; ?>

			</div>
		</div>
	</div>

<?php if ( ! $plex_transfer->get( 'show[show_as_carousel]' ) ): ?>
	</div>
<?php endif; ?>