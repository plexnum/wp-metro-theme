<?php

if ( !defined('ABSPATH')) exit;

global $wp_query;

if ( $this->get( 'max_items' ) <= 0 ) {
	$this->set( 'max_items', - 1 );
}

$wp_query = new WP_Query( 'post_type=testimonial&posts_per_page=' . $this->get( 'max_items' ) );
?>


<?php if ( have_posts() ): ?>

	<?php $i = 1;

	$plex_transfer = Plex_Lib_ViewTransfer::get_transfer();
	$plex_transfer->set( 'el_width', (int) ( 12 / $this->get( 'columns' ) ) );
	$plex_transfer->set( 'show', array(
		'show_as_carousel' => $this->get( 'show_as_carousel' ),
		'animation'        => $this->get( 'animation' )
	) );

	$plex_transfer->set( 'icon', $this->get( 'icon' ) );

	?>

	<?php if ( $this->get( 'show_as_carousel' ) ): // Show as carousel ?>

		<div class="carousel_arrow  carousel_arrow-right-top <?php echo $this->get( 'animation' )->get( 'class' ); ?>" <?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>>
			<ul class="carousel js-carousel" data-config='
                                {
                                "scroll":{
                                  "onBefore": null
                                },
                                "auto": false,
                                "scroll": {
                                    "fx": "directscroll"
                                },
                                "items": {
                                    "visible": 1
                                },
                                "style": {
                                   "paddingItem": [40, 0, 0, 0]
                                },
                                "buttons": {
                                    "enable": true,
                                    "size": "mid"
                                    }
                                }
                                '>
				<?php while ( have_posts() ) :
					the_post(); ?>

					<li>
						<?php plex_load_partial( 'view/short-code/testimonials/_item' ); ?>
					</li>

				<?php endwhile; ?>


			</ul>
		</div>

	<?php else: // Show in grid ?>

		<?php while ( have_posts() ) :
			the_post(); ?>

			<?php plex_show_in_row( array(
			'els'      => $this->get( 'columns' ),
			'start'    => '<div class="row-fluid">',
			'end'      => '</div>',
			'between'  => '',
			'view'     => 'view/short-code/testimonials/_item',
			'iterator' => $i,
			'args'     => array()
		) ); ?>

			<?php $i ++; endwhile; ?>


	<?php endif; ?>

<?php endif; ?>

<?php
// Reset to default page query as well as post data
wp_reset_query();
?>