<?php if ( !defined('ABSPATH')) exit; ?>
<?php if ( $this->get( 'show_setting_header' ) ): ?>
	<h3><?php _e( 'Show Animation Settings', plex_get_trans_domain() ); ?></h3>
<?php endif; ?>
<label class="setting">
	<span><?php _e( 'Animation type', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the type of show animation", plex_get_trans_domain() ); ?>">?</i></span>
	<select class="columns" name="animation_type"
					data-setting="animation_type">
		<?php foreach ( $this->get( 'types' ) as $plex_type_key => $plex_type_val ): ?>
			<option value="<?php echo $plex_type_key; ?>">
				<?php echo $plex_type_val; ?>
			</option>
		<?php endforeach; ?>
	</select>
</label>