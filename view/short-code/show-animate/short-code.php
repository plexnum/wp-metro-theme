<?php if ( !defined('ABSPATH')) exit; ?>
<div class="js-animate" data-type-animate="<?php echo $this->get('animation_type'); ?>">
	<?php echo $this->get('content'); ?>
</div>