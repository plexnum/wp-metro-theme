<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">

	<h3><?php _e( 'Services Settings', plex_get_trans_domain() ); ?></h3>
	<label class="setting">
		<span><?php _e( 'Columns', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the number of columns in row", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="number" min="1" max="6" name="columns" data-setting="columns" value="2" />
	</label>

	<label class="setting">
		<span><?php _e( 'Max items', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines max number of items to show. Set 0 to show all items", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="number" min="0" name="max_items" data-setting="max_items" value="0" />
	</label>

	<label class="setting">
		<span><?php _e( 'With links', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether show services with links or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="with_links" checked="checked" />
	</label>

	<label class="setting">
		<span><?php _e( 'Charge list', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show charge list or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="with_charge_list" checked="checked" />
	</label>

	<?php echo $this->get( 'animation' ); ?>

</div>