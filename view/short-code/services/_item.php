<?php
if ( !defined('ABSPATH')) exit;

// Obtain data
$plex_service_charge = new Plex_Config_ParamBag(
	(array) get_post_meta( get_the_ID(), '_plex_service_charge', true )
);

$plex_transfer = Plex_Lib_ViewTransfer::get_transfer();
$plex_external_link = plex_get_external_link();
?>


<div class="<?php echo 'span' . $plex_transfer->get( 'el_width' ); ?>">
	<div id="post-<?php the_ID(); ?>" <?php post_class( array(
		$plex_transfer->get( 'show[animation]' )->get( 'class' )
	) ); ?> <?php echo $plex_transfer->get( 'show[animation]' )->get( 'animation_type' ); ?>>
		<p>
			<?php if ( $plex_transfer->get( 'show[with_links]' ) ): ?>
				<a href="<?php echo esc_url( $plex_external_link->get( 'link' ) ); ?>" class="btn btn_big  btn_grey btn_bloc "><span class="bg-main-bg"></span>
					<?php the_title(); ?>
				</a>
			<?php else: ?>
				<span class="btn btn_big  btn_grey btn_bloc "><span class="bg-main-bg"></span>
					<?php the_title(); ?>
			</span>
			<?php endif; ?>
		</p>

		<div class="separator__wrap separator_right">

			<div class="separator__content">
				<?php the_content(); ?>
			</div>

			<?php if ( $plex_transfer->get( 'show[with_links]' ) ): ?>
				<a href="<?php echo esc_url( $plex_external_link->get( 'link' ) ); ?>" class=" separator">
					<i class="icon-next  icon_in-bubble icon_lage "></i>
				</a>
			<?php endif; ?>
		</div>
		<?php if ( $plex_transfer->get( 'show[with_charge_list]' ) ): ?>
			<ul class="list-marker">
				<?php foreach ( $plex_service_charge->get( 'items', array() ) as $plex_service_charge_item ): ?>
					<li><i class="icon-chetbox_select"></i><?php echo esc_html( $plex_service_charge_item['item'] ); ?></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>