<?php

if ( !defined('ABSPATH')) exit;

global $wp_query;

if ( $this->get( 'max_items' ) <= 0 ) {
	$this->set( 'max_items', - 1 );
}

$wp_query = new WP_Query( array(
	'post_type'      => 'service',
	'posts_per_page' => $this->get( 'max_items' ),
	'post_status'    => 'publish',
	'orderby'       => 'date'
) );

?>


<?php if ( have_posts() ): ?>

	<?php $i = 1;

	$plex_transfer = Plex_Lib_ViewTransfer::get_transfer();
	$plex_transfer->set( 'el_width', (int) ( 12 / $this->get( 'columns' ) ) );
	$plex_transfer->set( 'show', array(
		'with_links'       => $this->get( 'with_links' ),
		'with_charge_list' => $this->get( 'with_charge_list' ),
		'animation'        => $this->get( 'animation' )
	) );
	?>

	<?php while ( have_posts() ) :
		the_post(); ?>

		<?php plex_show_in_row( array(
		'els'      => $this->get( 'columns' ),
		'start'    => '<div class="row-fluid">',
		'end'      => '</div>',
		'between'  => '',
		'view'     => 'view/short-code/services/_item',
		'iterator' => $i,
		'args'     => array()
	) ); ?>

		<?php $i ++; endwhile; ?>

<?php else: ?>
	<?php echo do_shortcode( '[alert type="error"]' . __( 'No Services Found', plex_get_trans_domain() ) . '[/alert]' ); ?>
<?php endif; ?>


<?php
// Reset to default page query as well as post data
wp_reset_query();
?>