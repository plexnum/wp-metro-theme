<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">
	<h3><?php _e( 'Grid Settings', plex_get_trans_domain() ); ?></h3>
	<div class="grid-layout-place-holder"></div>
	<input class="-content" type="hidden" data-setting="content">
	<input class="-content-preview" type="hidden" data-setting="content_preview">
</div>