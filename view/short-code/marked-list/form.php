<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">
	<h3><?php _e( 'Marked list settings', plex_get_trans_domain() ); ?></h3>
	<div class="marked-list-place-holder"></div>
	<?php echo $this->get( 'animation' ); ?>
</div>