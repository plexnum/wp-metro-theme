<?php if ( !defined('ABSPATH')) exit; ?>
<div class="accordion-group">
	<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse" href="#<?php echo $this->get( 'id' ); ?>">
			<i class="icon-caret-down"></i>
			<?php echo $this->get( 'title' ); ?>
		</a>
	</div>
	<div id="<?php echo $this->get( 'id' ); ?>" class="accordion-body collapse <?php if ( $this->get( 'active' ) ): ?>in<?php endif; ?>" style="height: <?php if ( $this->get( 'active' ) ): ?>auto<?php else: ?>0<?php endif; ?>">
		<div class="accordion-inner">
			<?php echo $this->get( 'content' ); ?>
		</div>
	</div>
</div>