<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">

	<h3><?php echo $this->get( 'title' ); ?></h3>

	<label class="setting">
	<span><?php _e( 'Type', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the type", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="type"
						data-setting="type">
			<?php foreach ( $this->get( 'types' ) as $plex_type_key => $plex_type_val ): ?>
				<option value="<?php echo $plex_type_key; ?>">
					<?php echo $plex_type_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<label class="setting">
	<span><?php _e( 'Text', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the text", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="text" name="text" data-setting="text" value="<?php _e( 'Default', plex_get_trans_domain() ); ?>">
	</label>

	<?php echo $this->get( 'animation' ); ?>
</div>