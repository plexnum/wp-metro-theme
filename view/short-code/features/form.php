<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">

	<h3><?php _e( 'Features Settings', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
	<span><?php _e( 'With links', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines whether show features with links or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="with_links" checked="checked" />
	</label>

	<?php echo $this->get( 'animation' ); ?>
</div>
