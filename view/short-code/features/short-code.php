<?php

if ( !defined('ABSPATH')) exit;

global $wp_query;

if ( $this->get( 'max_items' ) <= 0 ) {
	$this->set( 'max_items', - 1 );
}

$wp_query = new WP_Query( array(
	'post_type'      => 'feature',
	'posts_per_page' => $this->get( 'max_items' ),
	'post_status'    => 'publish',
	'orderby'       => 'date'
) );

?>

<?php if ( have_posts() ): ?>

	<?php $i = 1; ?>

	<?php while ( have_posts() ) :
		the_post(); ?>

		<?php $plex_external_link = plex_get_external_link(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class( array(
			$this->get( 'animation' )->get( 'class' )
		) ); ?> <?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>>

			<h3 class="separator__wrap separator_left">

				<span class="number">
						<i class="icon_in-bubble icon_big"><span><?php echo $i; ?></span></i>
					</span>

				<div class="separator__content">
					<?php the_title(); ?>
				</div>

			</h3>


			<div class="separator__wrap separator_right">

				<div class="separator__content">
					<?php the_content(); ?>
				</div>

				<?php if ( $this->get( 'with_links' ) ): ?>

					<a href="<?php echo esc_url( $plex_external_link->get( 'link' ) ); ?>"
						 class="separator"
							<?php echo $plex_external_link->get( 'target' ); ?>>
						<i class="icon-next  icon_in-bubble icon_lage "></i>
					</a>

				<?php endif; ?>

			</div>

		</div>
		<br>

		<?php $i ++; endwhile; ?>

<?php else: ?>
	<?php echo do_shortcode( '[alert type="error"]' . __( 'No Features Found', plex_get_trans_domain() ) . '[/alert]' ); ?>
<?php endif; ?>

<?php
// Reset to default page query as well as post data
wp_reset_query();
?>