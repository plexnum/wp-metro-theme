<?php if ( !defined('ABSPATH')) exit; ?>
<!DOCTYPE html>
<html>
<head>
	<?php wp_print_styles(); ?>
	<?php wp_print_head_scripts(); ?>
</head>
<body style="padding-bottom: 0; ">
<div class="container-wrap" style="min-height: 400px">

		<div class="container">
			<div style="margin-top: 20px">
				<?php echo $this->get( '_content', '' ); ?>
			</div>
		</div>
</div>
<?php wp_print_footer_scripts(); ?>
</body>
</html>