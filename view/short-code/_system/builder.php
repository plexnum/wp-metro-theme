<?php if ( !defined('ABSPATH')) exit; ?>
<script type="text/html" id="plexShortCodesListTpl">
	<div class="-short-codes">
	</div>
</script>

<script type="text/html" id="plexShortCodeListItemTpl">
	<li class="-item">
		<div class="-thumbnail">
			<img src="<%= icon %>" draggable="false">
		</div>
	</li>
</script>

<script type="text/html" id="plexShortCodeEditTpl">
</script>

<?php echo $this->get('tab_template'); ?>