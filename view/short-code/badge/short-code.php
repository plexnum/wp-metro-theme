<?php if ( !defined('ABSPATH')) exit; ?>
<span class="badge badge-<?php echo $this->get('type'); ?>
	<?php echo $this->get('animation')->get('class'); ?>"
		<?php echo $this->get('animation')->get('animation_type'); ?>>
	<?php echo $this->get('text'); ?>
</span>