<?php if ( !defined('ABSPATH')) exit; ?>
<i class="<?php echo $this->get( 'icon' ); ?>
	<?php echo $this->get( 'size_mod' ); ?>
	<?php echo $this->get( 'style_mod' ); ?>
	<?php echo $this->get( 'pos_mod' ); ?>
	<?php echo $this->get( 'rotation_mod' ); ?>
	<?php echo $this->get( 'animation' )->get( 'class' ); ?>"
		<?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>><?php if ( $this->get( 'style_mod' ) == 'icon_cub' ): ?><?php echo '<span class="bg-main-bg"></span>'; ?><?php endif; ?></i>