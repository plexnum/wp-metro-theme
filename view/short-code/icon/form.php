<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">
	<h3><?php echo __( 'Icon settings', plex_get_trans_domain() ); ?></h3>

	<div class="-icons-selection">
		<?php foreach ( $this->get( 'icons', array() ) as $plex_icon ): ?>

			<?php if ( $plex_icon == $this->get( 'default_icon' ) ): ?>
				<i class="<?php echo $plex_icon; ?> -selected"></i>
			<?php else: ?>
				<i class="<?php echo $plex_icon; ?>"></i>
			<?php endif; ?>

		<?php endforeach; ?>
	</div>

	<label class="setting">
	<span><?php _e( 'Size', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the size of icon", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="size_mod"
						data-setting="size_mod">
			<?php foreach ( $this->get( 'icons_size_mod' ) as $plex_mod_key => $plex_mod_val ): ?>
				<option value="<?php echo $plex_mod_key; ?>">
					<?php echo $plex_mod_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>


	<label class="setting">
	<span><?php _e( 'Style', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the style of icon", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="style_mod"
						data-setting="style_mod">
			<?php foreach ( $this->get( 'icons_style_mod' ) as $plex_mod_key => $plex_mod_val ): ?>
				<option value="<?php echo $plex_mod_key; ?>">
					<?php echo $plex_mod_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<label class="setting">
	<span><?php _e( 'Rotation', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the rotation of icon", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="rotation_mod"
						data-setting="rotation_mod">
			<?php foreach ( $this->get( 'icons_rotation_mod' ) as $plex_mod_key => $plex_mod_val ): ?>
				<option value="<?php echo $plex_mod_key; ?>">
					<?php echo $plex_mod_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<label class="setting">
	<span><?php _e( 'Position', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the position of icon", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="pos_mod"
						data-setting="pos_mod">
			<?php foreach ( $this->get( 'icons_pos_mod' ) as $plex_mod_key => $plex_mod_val ): ?>
				<option value="<?php echo $plex_mod_key; ?>">
					<?php echo $plex_mod_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<input type="hidden" class="-icon__input" data-setting="<?php echo $this->get( 'icon_attr' ); ?>">

	<?php echo $this->get( 'animation' ); ?>
</div>
