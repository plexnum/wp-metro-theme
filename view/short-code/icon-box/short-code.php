<?php if ( !defined('ABSPATH')) exit; ?>
<div class="separator__wrap  separator_<?php echo $this->get( 'position', 'right' ); ?> <?php echo $this->get( 'animation' )->get( 'class' ); ?>" <?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>>

	<?php if ( $this->get( 'position' ) == 'right' ): ?>
		<div class="separator__content">
			<?php echo $this->get( 'content' ); ?>
		</div>
	<?php endif; ?>

	<?php if ( $this->get( 'link' ) ): ?>
		<a href="<?php echo $this->get( 'link' ); ?>" class="<?php if ( $this->get( 'with_separator' ) ): ?>separator<?php endif; ?> icon_wrap" <?php plex_the_link_target( $this->get( 'in_new_page' ) ); ?>>
			<?php echo $this->get( 'icon' ); ?>
		</a>
	<?php else: ?>
		<div class="<?php if ( $this->get( 'with_separator' ) ): ?>separator<?php endif; ?> icon_wrap">
			<?php echo $this->get( 'icon' ); ?>
		</div>
	<?php endif; ?>

	<?php if ( $this->get( 'position' ) != 'right' ): ?>
		<div class="separator__content">
			<?php echo $this->get( 'content' ); ?>
		</div>
	<?php endif; ?>

</div>