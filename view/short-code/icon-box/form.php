<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-context">
	<h3><?php echo __( 'Icon box setting', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
	<span><?php _e( 'Separator', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show separator or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="with_separator" checked="checked" />
	</label>

	<label class="setting">
	<span><?php _e( 'Link', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the external link", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="text" name="link" data-setting="link">
	</label>

	<label class="setting">
	<span><?php _e( 'In new...', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show link in new page or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="in_new_page" checked="checked" />
	</label>

	<label class="setting">
	<span><?php _e( 'Position', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the position of icon relatively to icon box", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="position"
						data-setting="position">
			<?php foreach ( $this->get( 'positions' ) as $plex_pos_key => $plex_pos_val ): ?>
				<option value="<?php echo $plex_pos_key; ?>">
					<?php echo $plex_pos_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<input class="-content" type="hidden" data-setting="content">
	<textarea rows="10" class="-content-preview js-synchronize widefat" data-setting="content_preview"></textarea>
	<?php echo $this->get( 'icon' ); ?>
	<?php echo $this->get( 'animation' ); ?>
</div>