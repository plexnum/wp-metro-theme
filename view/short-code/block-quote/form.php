<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">
	<h3><?php _e( 'Block quote settings', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
		<span><?php _e( 'Author', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the author of quote", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="text" name="author" data-setting="author">
	</label>

	<label class="setting">
		<span><?php _e( 'With icon', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show quote with icon or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="with_icon" />
	</label>

	<input class="-content" type="hidden" data-setting="content">
	<textarea rows="10" class="-content-preview js-synchronize widefat" data-setting="content_preview"></textarea>
	<?php echo $this->get( 'icon' ); ?>
	<?php echo $this->get( 'animation' ); ?>
</div>