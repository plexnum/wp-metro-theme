<?php if ( !defined('ABSPATH')) exit; ?>
<blockquote class="<?php if( $this->get('with_icon')): ?>blockquote_icon<?php endif; ?> <?php echo $this->get('animation')->get('class'); ?>" <?php echo $this->get('animation')->get('animation_type'); ?>>
	<?php if ( $this->get( 'author' ) ): ?>
		<h3 class="author"><?php echo $this->get( 'author' ); ?></h3>
	<?php endif; ?>

	<?php if( $this->get('with_icon')): ?>
		<?php echo $this->get('icon'); ?>
	<?php endif; ?>

	<p>
		<?php echo $this->get( 'content' ); ?>
	</p>
</blockquote>