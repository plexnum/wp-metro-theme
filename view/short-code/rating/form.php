<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">
	<h3><?php _e( "Rating setting", plex_get_trans_domain() ); ?></h3>
	<label class="setting">
	<span><?php _e( 'Stars', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines number of rating stars", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="stars"
						data-setting="stars">
			<?php for ( $i = 0; $i <= 5; $i ++ ): ?>
				<option value="<?php echo $i; ?>">
					<?php echo $i; ?>
				</option>
			<?php endfor; ?>
		</select>
	</label>
	<?php echo $this->get( 'animation' ); ?>
</div>