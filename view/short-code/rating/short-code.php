<?php if ( !defined('ABSPATH')) exit; ?>
<div class="rating <?php echo $this->get('animation')->get('class'); ?>" <?php echo $this->get('animation')->get('animation_type'); ?>>
	<?php for ( $i = 5;
							$i > 0;
							$i -- ): ?>
		<?php if ( $i == $this->get( 'stars' ) ): ?>
			<span class="icon-star active"></span>
		<?php else: ?>
			<span class="icon-star"></span>
		<?php endif; ?>
	<?php endfor; ?>
</div>