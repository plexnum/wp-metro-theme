<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-context">
	<h3><?php echo __( 'Embed setting', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
	<span><?php _e( 'Url', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the url of the embedded resource", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="text" name="url" data-setting="url">
	</label>

</div>