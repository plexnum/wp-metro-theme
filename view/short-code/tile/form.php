<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">
	<h3><?php _e( 'Tile settings', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
	<span><?php _e( 'Type', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the type of tile", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="type"
						data-setting="type">
			<?php foreach ( $this->get( 'types' ) as $plex_type_key => $plex_type_val ): ?>
				<option value="<?php echo $plex_type_key; ?>">
					<?php echo $plex_type_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<input class="-content" type="hidden" data-setting="content">
	<textarea rows="10" class="-content-preview js-synchronize widefat" data-setting="content_preview"></textarea>
	<?php echo $this->get( 'animation' ); ?>
</div>