<?php if ( !defined('ABSPATH')) exit; ?>
<div class="<?php echo $this->get( 'animation' )->get( 'class' ); ?> <?php echo $this->get( 'type' ); ?>"
		<?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>>
		<?php echo $this->get( 'content' ); ?>
</div>