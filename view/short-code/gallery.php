<?php if ( !defined('ABSPATH')) exit; ?>
<div style="height: 100%" class="carousel_arrow <?php echo $this->get( 'animation' )->get( 'class' ); ?>"
		<?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>>

	<ul class="carousel js-carousel" data-config='<?php echo $this->get( 'js_configs' ); ?>'>

		<?php foreach ( $this->get( 'attachments', array() ) as $id => $attachment ): ?>
			<li>
				<?php plex_the_gallery_img( $id, $this->get( 'attrs' ) ); ?>

				<?php if ( trim( $attachment->post_excerpt ) ): ?>
					<div class="gallery-caption">
						<?php echo $attachment->post_excerpt; ?>
					</div>
				<?php endif; ?>
			</li>
		<?php endforeach; ?>

	</ul>

</div>