<?php if ( !defined('ABSPATH')) exit; ?>
<?php wp_print_scripts( 'google_maps_api' ); ?>

<?php if ( ! $this->get( 'err' ) ): ?>

	<div class="map_wrap <?php echo $this->get( 'animation' )->get( 'class' ); ?>"
			<?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>>
		<div class="map" id="<?php echo esc_attr( $this->get( 'map_id' ) ); ?>"></div>
		<div class="map-content hidden"><?php echo $this->get( 'content' ); ?></div>
	</div>

	<script type="text/javascript">

		(function ($) {

			function runMap<?php echo $this->get( 'map_id' ) ; ?>() {

				var $this = $('#<?php echo $this->get( 'map_id' ) ; ?>'),
						$parent = $this.parent(),
						$content = $('.map-content', $parent);

				var location = new google.maps.LatLng(
								"<?php echo $this->get('coordinates[lat]', false, true); ?>",
								"<?php echo $this->get('coordinates[lng]', false, true); ?>"
						),
						mapOptions = {
							zoom     : <?php echo $this->get('zoom'); ?>,
							center   : location,
                            scrollwheel: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						},

						map = new google.maps.Map(
								$this[0],
								mapOptions
						),

						marker = new google.maps.Marker({
							position: location,
							map     : map
						});

				if ($content.html()) {
					var infoWindow = new google.maps.InfoWindow({
						content : $content.html(),
						position: location,
						map     : map
					});
					marker.setIcon(" ")
				}

			}

			runMap<?php echo $this->get( 'map_id' ) ; ?>();

		})(jQuery);


	</script>

<?php else: ?>
	<?php echo do_shortcode( '[alert type="error"]' . $this->get( 'err' ) . '[/alert]' ); ?>
<?php endif; ?>