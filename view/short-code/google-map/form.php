<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">
	<h3><?php echo __( 'Google Map Settings', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
		<span><?php _e( 'Address', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the address of google map", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="text" name="address" data-setting="address">
	</label>

	<label class="setting">
	<span><?php _e( 'Zoom', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the zoom of google map", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="number" min="0" max="100" name="zoom" data-setting="zoom" value="15" />
	</label>

	<input class="-content" type="hidden" data-setting="content">
	<textarea rows="10" class="-content-preview js-synchronize widefat" data-setting="content_preview"></textarea>

	<?php echo $this->get( 'animation' ); ?>

</div>