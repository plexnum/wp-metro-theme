<?php if ( !defined('ABSPATH')) exit; ?>
<div class="progress progress-<?php echo $this->get( 'type' ); ?> <?php if ( $this->get( 'active' ) ): ?>active<?php endif; ?> <?php echo $this->get( 'animation' )->get( 'class' ); ?>" <?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>>
	<div class="bar" style="width: <?php echo $this->get( 'width' ); ?>%;">
		<?php if ( $this->get( 'text' ) ): ?>
			<span><?php echo $this->get( 'text' ); ?></span>
		<?php endif; ?>
	</div>
</div>