<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">

	<h3><?php _e( 'Progress bar settings', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
		<span><?php _e( 'Width', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the width of progress bar from 0 to 100", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="number" min="0" max="100" name="width" data-setting="width" value="50" />
	</label>

	<label class="setting">
	<span><?php _e( 'Type', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the type of progress bar", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="type"
						data-setting="type">
			<?php foreach ( $this->get( 'types' ) as $plex_type_key => $plex_type_val ): ?>
				<option value="<?php echo $plex_type_key; ?>">
					<?php echo $plex_type_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<label class="setting">
		<span><?php _e( 'Active', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show progress bar as active or not. Only work for striped type", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="active" checked="true" />
	</label>

	<label class="setting">
		<span><?php _e( 'Text', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the text of progress bar", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="text" data-setting="text" />
	</label>

	<?php echo $this->get( 'animation' ); ?>

</div>