<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">
	<h3><?php _e( 'Tabs settings', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
	<span><?php _e( 'Position', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the position of tabs", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="position"
						data-setting="position">
			<?php foreach ( $this->get( 'positions' ) as $plex_pos_key => $plex_pos_val ): ?>
				<option value="<?php echo $plex_pos_key; ?>">
					<?php echo $plex_pos_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<br style="clear: both;">

	<input class="-content" type="hidden" data-setting="content">
	<input class="-content-preview" type="hidden" data-setting="content_preview">

	<div class="tabs-place-holder"></div>

	<?php echo $this->get( 'animation' ); ?>

</div>