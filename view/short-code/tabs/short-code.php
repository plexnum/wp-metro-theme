<?php if ( !defined('ABSPATH')) exit; ?>
<div class="tabs-<?php echo $this->get( 'position' ); ?> <?php echo $this->get( 'animation' )->get( 'class' ); ?>" <?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>>

	<?php if ( $this->get( 'position' ) == 'below' ): ?>
		<div class="tab-content">
			<?php echo $this->get( 'content' ); ?>
		</div>
	<?php endif; ?>


	<ul class="nav nav-tabs ">
		<?php foreach ( $this->get( 'tabs', array() ) as $plex_tab ): ?>
			<li <?php if ($plex_tab['active']): ?>class="active"<?php endif; ?>>
				<a href="#<?php echo $plex_tab['id']; ?>" data-toggle="tab"><?php echo $plex_tab['title']; ?></a></li>
		<?php endforeach; ?>
	</ul>

	<?php if ( $this->get( 'position' ) != 'below' ): ?>
		<div class="tab-content">
			<?php echo $this->get( 'content' ); ?>
		</div>
	<?php endif; ?>

</div>