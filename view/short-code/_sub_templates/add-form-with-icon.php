<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-wp-box">
	<div class="-wp-header">
		<?php echo __( 'Add', plex_get_trans_domain() ); ?>
	</div>
	<div class="-wp-content -add-form -context">
		<div class="-err"></div>
		<div class="-icons-selection">
			<?php foreach ( $this->get( 'icons', array() ) as $plex_icon ): ?>

				<?php if($plex_icon == $this->get('default_icon')): ?>
					<i class="<?php echo $plex_icon; ?> -selected"></i>
				<?php else: ?>
					<i class="<?php echo $plex_icon; ?>"></i>
				<?php endif; ?>

			<?php endforeach; ?>
		</div>
		<label><?php echo __( 'Title', plex_get_trans_domain() ); ?></label>
		<input type="text" name="title" class="widefat -title" style="width: 100%;">
		<input type="hidden" class="-icon__input" name="icon" value="<?php echo $this->get('default_icon'); ?>" >
		<a class="button button-primary -action js-add" href="#"><?php echo __( 'Add', plex_get_trans_domain() ); ?></a>
	</div>
</div>