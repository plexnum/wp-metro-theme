<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-el">

	<div class="-col-left">
		<div class="-icon-wrap">
			<i class="<%= icon %> -icon"></i>
		</div>
	</div>

	<div class="-col-right">
		<div class="-actions">
			<a class="button button-primary -action js-del" href="#">
				<?php echo __( 'Delete', plex_get_trans_domain() ); ?>
			</a>
		</div>
	</div>

	<div class="-col-center">
		<input name="<%= input_name %>"
					 value="<%= input_value %>"
					 class="-input widefat" type="text">
	</div>

</div>