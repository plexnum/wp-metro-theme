<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-wp-box">
	<div class="-wp-header">
		<?php echo __( 'Add', plex_get_trans_domain() ); ?>
	</div>
	<div class="-wp-content -add-form -context">
		<div class="-err"></div>
		<label><?php echo __( 'Title', plex_get_trans_domain() ); ?></label>
		<input type="text" name="title" class="widefat" style="width: 100%;">
		<a class="button button-primary -action js-add" href="#"><?php echo __( 'Add', plex_get_trans_domain() ); ?></a>
	</div>
</div>