<?php if ( !defined('ABSPATH')) exit; ?>

<div class="-radio-input">

	<div class="-col-left">
		<input class="js-active -radio" type="radio" name="active" <% if ( active && active != "false" ) { %> checked="checked" <% } %>>
	</div>

	<div class="-col-right">
		<div class="-actions">
			<a class="button button-primary -action js-del" href="#">
				<?php echo __( 'Delete', plex_get_trans_domain() ); ?>
			</a>
		</div>
	</div>

	<div class="-col-center">
		<input type="text" name="<%= input_name %>" value="<%= input_value %>" class="widefat -input">
	</div>

</div>