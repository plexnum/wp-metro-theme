<?php if ( !defined('ABSPATH')) exit; ?>
<div class="tab-pane fade in <?php if($this->get('active')): ?>active<?php endif; ?>" id="<?php echo $this->get('id'); ?>">
	<?php echo $this->get('content'); ?>
</div>