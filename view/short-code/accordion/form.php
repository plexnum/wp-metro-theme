<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">
	<h3><?php _e( 'Accordion settings', plex_get_trans_domain() ); ?></h3>

	<div class="collapses-place-holder"></div>
	<?php echo $this->get( 'animation' ); ?>
</div>