<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">
	<h3><?php _e( 'Team Settings', plex_get_trans_domain() ); ?></h3>
	<label class="setting">
	<span><?php _e( 'Columns', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the number of columns in row", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="number" min="1" max="6" name="columns" data-setting="columns" value="3" />
	</label>

	<label class="setting">
	<span><?php _e( 'Max items', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines max number of items to show. Set 0 to show all items", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="number" min="0" name="max_items" data-setting="max_items" value="0" />
	</label>

	<label class="setting">
	<span><?php _e( 'Social icons', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show social icons or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="show_social_icons" checked="checked" />
	</label>

	<label class="setting">
	<span><?php _e( 'Image', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show team member image or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="show_image" checked="checked" />
	</label>

	<label class="setting">
	<span><?php _e( 'Position', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show team member job position or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="show_position" checked="checked" />
	</label>

	<label class="setting">
	<span><?php _e( 'Description', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show team member description or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="show_desc" checked="checked" />
	</label>

	<?php echo $this->get( 'animation' ); ?>
</div>