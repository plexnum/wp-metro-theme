<?php
if ( !defined('ABSPATH')) exit;
// Obtain data
$plex_team = new Plex_Config_ParamBag(
	(array) get_post_meta( get_the_ID(), '_plex_team', true )
);

$plex_social = new Plex_Config_ParamBag(
	(array) get_post_meta( get_the_ID(), '_plex_social', true )
);

$plex_transfer = Plex_Lib_ViewTransfer::get_transfer();
$plex_el_width = $plex_transfer->get( 'el_width' );

?>

<div class="span<?php echo $plex_el_width; ?>
	<?php echo $plex_transfer->get( 'show[animation]' )->get( 'class' ); ?>"
	<?php echo $plex_transfer->get( 'show[animation]' )->get( 'animation_type' ); ?>>

	<?php if ( $plex_transfer->get( 'show[image]' ) ): ?>
		<p>
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail(); ?>
			<?php endif; ?>
		</p>
	<?php endif; ?>

	<h3>
		<?php if ( $plex_transfer->get( 'show[with_links]' ) ): ?>
			<a href="<?php echo the_permalink(); ?>"> <?php the_title(); ?>
				<?php if ( $plex_transfer->get( 'show[position]' ) ): ?>
					<br><?php echo $plex_team->get( 'position', '' ); ?>
				<?php endif; ?>
			</a>
		<?php else: ?>
			<?php the_title(); ?>
			<?php if ( $plex_transfer->get( 'show[position]' ) ): ?>
				<br><?php echo $plex_team->get( 'position', '' ); ?>
			<?php endif; ?>
		<?php endif; ?>
	</h3>

	<?php if ( $plex_transfer->get( 'show[desc]' ) ): ?>
		<p class="js-cont"><?php echo $plex_team->get( 'desc', '' ); ?></p>
	<?php endif; ?>

	<?php if ( $plex_transfer->get( 'show[social_icons]' ) ): ?>
		<p>
			<?php foreach ( $plex_social->get( 'plex_social', array() ) as $social ): ?>
				<a target="_blank" href="<?php echo $social['link']; ?>" class="<?php echo $social['icon']; ?> icon_cub icon_lage"><span class="bg-main-bg"></span></a>
			<?php endforeach; ?>
		</p>
	<?php endif; ?>

</div>