<?php if ( !defined('ABSPATH')) exit; ?>
<div class="alert alert-<?php echo $this->get( 'type' ); ?>
	<?php echo $this->get( 'animation' )->get( 'class' ); ?>"
		<?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>>

	<?php if ( $this->get( 'with_close_btn' ) ): ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	<?php endif; ?>

	<?php echo $this->get( 'content' ); ?>
</div>