<?php if ( !defined('ABSPATH')) exit; ?>
<br style="clear: both">
<h3><?php _e( 'Show Animation Settings', plex_get_trans_domain() ); ?></h3>
<label class="setting">
	<span><?php _e( 'Use animation', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to use animation or not", plex_get_trans_domain() ); ?>">?</i></span>
	<input type="checkbox" data-setting="use_animation" />
</label>
<?php echo $this->get( 'animation' ); ?>