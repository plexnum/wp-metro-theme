<?php

if ( !defined('ABSPATH')) exit;

$plex_transfer = Plex_Lib_ViewTransfer::get_transfer();
$plex_external_link = plex_get_external_link();

?>

<div class="span<?php echo $plex_transfer->get( 'el_width' ); ?>">

	<div id="post-<?php the_ID(); ?>" <?php post_class( array(
		$plex_transfer->get( 'show[animation]' )->get( 'class' )
	) ); ?> <?php echo $plex_transfer->get( 'show[animation]' )->get( 'animation_type' ); ?>>

		<?php if ( $plex_transfer->get( 'show[with_links]', true ) ): ?>

			<a href="<?php echo esc_url( $plex_external_link->get( 'link' ) ); ?>" <?php echo $plex_external_link->get( 'target' ); ?> class="tile_partners">
				<div class="bg-main-bg"></div>
				<?php if ( has_post_thumbnail() ) : ?>
					<?php the_post_thumbnail(); ?>
				<?php endif; ?>
				<div class="fantom"></div>
			</a>

		<?php else: ?>

			<div class="tile_partners">
				<div class="bg-main-bg"></div>
				<?php if ( has_post_thumbnail() ) : ?>
					<?php the_post_thumbnail(); ?>
				<?php endif; ?>
				<div class="fantom"></div>
			</div>

		<?php endif; ?>

	</div>

</div>