<?php if ( !defined('ABSPATH')) exit; ?>
<a class="btn <?php echo $this->get( 'block_cls' ); ?>
	<?php echo $this->get( 'size' ); ?>
	<?php echo $this->get( 'type' ); ?>
	<?php echo $this->get( 'animation' )->get( 'class' ); ?>"
		<?php echo $this->get( 'animation' )->get( 'animation_type' ); ?>
	 href="<?php echo $this->get( 'link' ); ?>"
		<?php plex_the_link_target( $this->get( 'in_new_page' ) ); ?>>
	<?php echo $this->get( 'label' ); ?>
</a>