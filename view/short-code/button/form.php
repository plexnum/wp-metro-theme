<?php if ( !defined('ABSPATH')) exit; ?>
<div class="-short-code -context">

	<h3><?php echo __( 'Button settings', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
		<span><?php _e( 'Label', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the button label", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="text" name="label" data-setting="label" />
	</label>

	<label class="setting">
		<span><?php _e( 'Link', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the button link", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="text" name="link" data-setting="link" />
	</label>

	<label class="setting">
	<span><?php _e( 'In new...', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show link in new page or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="in_new_page" checked="checked" />
	</label>

	<label class="setting">
	<span><?php _e( 'Type', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the type of button", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="type"
						data-setting="type">
			<?php foreach ( $this->get( 'types' ) as $plex_type_key => $plex_type_val ): ?>
				<option value="<?php echo $plex_type_key; ?>">
					<?php echo $plex_type_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<label class="setting">
	<span><?php _e( 'Size', plex_get_trans_domain() ); ?>
		<i class="-question-mark -tooltip" title="<?php _e( "Determines the size of button", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="size"
						data-setting="size">
			<?php foreach ( $this->get( 'sizes' ) as $plex_size_key => $plex_size_val ): ?>
				<option value="<?php echo $plex_size_key; ?>">
					<?php echo $plex_size_val; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<label class="setting">
		<span><?php _e( 'Block', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show button as block or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="block" />
	</label>

	<?php echo $this->get( 'animation' ); ?>

</div>