<?php if ( !defined('ABSPATH')) exit; ?>

<script type="text/html" id="tmpl-gallery-advanced-settings">
	<h3><?php _e( 'Gallery Settings', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
		<span><?php _e( 'Link To', plex_get_trans_domain() ); ?></span>
		<select class="link-to"
						data-setting="link"
		<# if ( data.userSettings ) { #>
			data-user-setting="urlbutton"
			<# } #>>

				<option value="file" selected>
					<?php esc_attr_e( 'Media File' ); ?>
				</option>

				<option value="post">
					<?php esc_attr_e( 'Attachment Page' ); ?>
				</option>

				</select>
	</label>

	<label class="setting">
		<span><?php _e( 'Rand Order', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Random order", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="_orderbyRandom" />
	</label>

	<h3><?php _e( 'Gallery Advanced Settings', plex_get_trans_domain() ); ?></h3>

	<label class="setting">
		<span><?php _e( 'Circular', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether the gallery should be circular or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" name="circular" data-setting="circular" checked="checked" />
	</label>

	<label class="setting">
		<span><?php _e( 'Pagination', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether the gallery should be paginated", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="pagination" />
	</label>

	<label class="setting">
		<span><?php _e( 'Direction', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines direction to scroll the gallery", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="direction"
						data-setting="direction">
			<?php foreach ( $this->get( 'direction', array() ) as $item_key => $item_value ): ?>
				<option value="<?php echo esc_attr( $item_key ); ?>" <?php selected( $item_key, 'left' ); ?>>
					<?php echo esc_html( $item_value ); ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<label class="setting">
		<span><?php _e( 'Visible', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the number of visible items", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="items__visible"
						data-setting="items__visible">
			<?php for ( $i = 1; $i <= 5; $i ++ ): ?>
				<option value="<?php echo esc_attr( $i ); ?>" <?php selected( $i, 1 ); ?>>
					<?php echo esc_html( $i ); ?>
				</option>
			<?php endfor; ?>
			<option value="variable"><?php _e( "Variable", plex_get_trans_domain() ); ?></option>
		</select>
	</label>

	<h4><?php _e( 'Scroll', plex_get_trans_domain() ); ?></h4>

	<label class="setting">
		<span><?php _e( 'Items', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the number of items to scroll", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="number" min="1" name="scroll__items" data-setting="scroll__items" value="1" />
	</label>

	<label class="setting">
		<span><?php _e( 'Effect', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Indicates which effect to use for the transition", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="scroll__fx"
						data-setting="scroll__fx">

			<?php foreach ( $this->get( 'fx', array() ) as $item_key => $item_value ): ?>
				<option value="<?php echo esc_attr( $item_key ); ?>" <?php selected( $item_key, 'scroll' ); ?>>
					<?php echo esc_html( $item_value ); ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<label class="setting">
		<span><?php _e( 'Effect fun', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Indicates which function to use for the transition", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="scroll__easing"
						data-setting="scroll__easing">

			<?php foreach ( $this->get( 'easing', array() ) as $item_key => $item_value ): ?>
				<option value="<?php echo esc_attr( $item_key ); ?>" <?php selected( $item_key, 'linear' ); ?>>
					<?php echo esc_html( $item_value ); ?>
				</option>
			<?php endforeach; ?>
		</select>
	</label>

	<label class="setting">
		<span><?php _e( 'Duration', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the duration of the transition in milliseconds", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="number" min="1" data-setting="scroll__duration" value="500" />
	</label>

	<label class="setting">
		<span><?php _e( 'Pause', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether the gallery should be paused on hover", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="scroll__pauseonehover" checked="checked" />
	</label>

	<h4><?php _e( 'Auto', plex_get_trans_domain() ); ?></h4>

	<label class="setting">
		<span><?php _e( 'Play', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether the carousel should scroll automatically or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="auto__play" />
	</label>

	<label class="setting">
		<span><?php _e( 'Duration', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the duration of the transition in milliseconds only for auto scrolling", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="number" min="1" data-setting="auto__duration" value="2500" />
	</label>

	<label class="setting">
		<span><?php _e( 'Progress', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to show progress bar or not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="auto__progress" />
	</label>

	<h4><?php _e( 'Control buttons', plex_get_trans_domain() ); ?></h4>

	<label class="setting">
		<span><?php _e( 'Enable', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines whether to enable control buttons ot not", plex_get_trans_domain() ); ?>">?</i></span>
		<input type="checkbox" data-setting="buttons__enable" checked="checked" />
	</label>

	<label class="setting">
		<span><?php _e( 'Position', plex_get_trans_domain() ); ?>
			<i class="-question-mark -tooltip" title="<?php _e( "Determines the position of control buttons", plex_get_trans_domain() ); ?>">?</i></span>
		<select class="columns" name="buttons__position"
						data-setting="buttons__position">
			<?php foreach ( $this->get( 'positions', array() ) as $item_key => $item_value ): ?>
				<option value="<?php echo esc_attr( $item_key ); ?>" <?php selected( $item_key, 'left-right-middle' ); ?>>
					<?php echo esc_html( $item_value ); ?>
				</option>
			<?php endforeach; ?>
		</select>

	</label>

	<?php echo $this->get( 'animation' ); ?>


</script>