<?php if ( !defined('ABSPATH')) exit; ?>

<div class="wrap">
	<h2><?php _ex( 'Settings', 'header', plex_get_trans_domain() ); ?></h2>

	<form action="options.php" method="post">
		<?php foreach ( $this->get( 'settings', array() ) as $plex_setting ): ?>
			<?php settings_fields( $plex_setting ); ?>
		<?php endforeach; ?>
		<h3><?php _e( 'Twitter API', plex_get_trans_domain() ); ?></h3>
		<table class="form-table">
			<tbody>
			<tr valign="top">
				<th scope="row">
					<?php _e( 'Twitter Application Consumer Key', plex_get_trans_domain() ); ?>
				</th>
				<td>
					<input style="width:100%"
								 name="plex_settings[twitter_consumer_key]"
								 value="<?php echo $this->get( 'plex_settings[twitter_consumer_key]' ); ?>"></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e( 'Twitter Application Consumer Secret', plex_get_trans_domain() ); ?></th>
				<td>
					<input style="width:100%"
								 name="plex_settings[twitter_consumer_secret]"
								 value="<?php echo $this->get( 'plex_settings[twitter_consumer_secret]' ); ?>">
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e( 'Account Access Token', plex_get_trans_domain() ); ?></th>
				<td>
					<input style="width:100%"
								 name="plex_settings[twitter_access_token]"
								 value="<?php echo $this->get( 'plex_settings[twitter_access_token]' ); ?>">
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e( 'Account Access Token Secret', plex_get_trans_domain() ); ?></th>
				<td>
					<input style="width:100%"
								 name="plex_settings[twitter_access_token_secret]"
								 value="<?php echo $this->get( 'plex_settings[twitter_access_token_secret]' ); ?>">
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e( 'Cache Duration in seconds', plex_get_trans_domain() ); ?></th>
				<td>
					<input style="width:100%"
								 name="plex_settings[twitter_cache_expire]"
								 value="<?php echo $this->get( 'plex_settings[twitter_cache_expire]' ); ?>">
				</td>
			</tr>
			</tbody>
		</table>

		<!-- <h3><?php _e( 'Google API', plex_get_trans_domain() ); ?></h3>

		<table class="form-table">
			<tbody>
			<tr valign="top">
				<th scope="row">
					<?php _e( 'Google API Key', plex_get_trans_domain() ); ?>
				</th>
				<td>
					<input style="width:100%"
								 name="plex_settings[google_api_key]"
								 value="<?php echo $this->get( 'plex_settings[google_api_key]' ); ?>">
				</td>
			</tr>
			</tbody>
		</table> -->

		<input name="Submit" type="submit" value="Save Changes" / >
	</form>
</div>
