<div class="-cust-block -color-scheme-cust-blk">

	<label>
		<span class="customize-control-title"><?php echo esc_html( $this->get( 'label' ) ); ?></span>
	</label>

	<ul class="-color-schemes">
		<li class="custom <?php if ( $this->get( 'value' ) == 'custom' ): ?>-selected<?php endif; ?>">
			<div class="color-box" data-key="" style="">
			</div>
			<div class="lable-box">
				Custom
			</div>
		</li>


		<?php foreach ( $this->get( 'choices' ) as $key => $choice ): ?>
			<li data-key="<?php echo esc_attr( $key ); ?>" class="js-el <?php if ( $key == $this->get( 'value' ) ): ?>-selected<?php endif; ?>">
				<div class="color-box" data-key="" style="background-color: <?php echo esc_attr( $choice['color'] ); ?>;">
				</div>
				<div class="lable-box">
					<?php echo esc_html( $choice['name'] ); ?>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>

	<input type="hidden" class="-color-scheme" <?php echo $this->get( 'link' ); ?>>
</div>