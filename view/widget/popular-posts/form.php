<div class="-widget-box -context">

	<p>
		<label for="<?php echo $this->widget->get_field_id( 'title' ); ?>">
			<?php echo __( "Title: ", plex_get_trans_domain() ); ?>
		</label>
		<input type="text" class="widefat"
					 id="<?php echo $this->widget->get_field_id( 'title' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'title' ); ?>"
					 value="<?php echo $this->get( 'title' ); ?>">
	</p>

	<p>
		<?php echo __("Numbers of posts to show: ", plex_get_trans_domain()); ?>
		<select name="<?php echo $this->widget->get_field_name('number_to_show'); ?>">
			<?php for($i = 1; $i <= 10; $i++): ?>
				<option value="<?php echo $i; ?>" <?php selected($this->get('number_to_show', 5, true), $i); ?>><?php echo $i; ?></option>
			<?php endfor; ?>
		</select>
	</p>


</div>