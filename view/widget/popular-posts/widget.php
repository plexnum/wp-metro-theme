<?php
$plex_query = new WP_Query( array(
		'posts_per_page'      => $this->get( 'number_to_show' ),
		'post_status'         => 'publish',
		'orderby'             => 'comment_count',
		'ignore_sticky_posts' => 1
	)
);
?>

	<h2 class="mt0"><?php echo $this->get( 'title' ); ?></h2>

<?php if ( $plex_query->have_posts() ) : ?>
	<div class="sub-menu">

		<?php $plex_separator = ''; ?>

		<?php while ( $plex_query->have_posts() ) : $plex_query->the_post(); ?>
			<?php echo $plex_separator; ?>
			<div class="row-fluid">

				<?php $plex_featured_img = plex_get_post_format_featured_image( 'plex_testimonial' ); ?>

				<?php if ( $plex_featured_img !== false ): ?>

					<div class="span3">
						<?php echo $plex_featured_img; ?>
					</div>

				<?php endif; ?>
				<div class="<?php if ( $plex_featured_img !== false ): ?>span9<?php else: ?>span12<?php endif; ?>">
					<time>
						<?php echo get_the_date(); ?>
					</time>
					<div class="separator__wrap  separator_right ">
						<div class="separator__content">
							<?php if ( get_the_title() ) the_title();
							else the_ID(); ?>
						</div>
						<a href="<?php the_permalink(); ?>" class=" separator">
							<i class="icon-next  icon_in-bubble icon_lage "></i>
						</a>
					</div>
				</div>
			</div>
			<?php $plex_separator = '<hr>'; ?>
		<?php endwhile; ?>
	</div>
<?php endif; ?>

<?php wp_reset_postdata(); ?>