<div class="-widget-box -context">

	<p>
		<a href="#" class="button-secondary -gallery__create" data-options='{"size":"plex_gallery_on_main"}'>
			<?php _e( "Gallery Builder", plex_get_trans_domain() ); ?>
		</a>
	</p>

	<p>
		<label for="<?php echo $this->widget->get_field_id( 'short_code' ); ?>">
			<?php _e( "Place From Short Code: ", plex_get_trans_domain() ); ?>
		</label>
		<textarea type="text" class="widefat -gallery__short-code"
					 id="<?php echo $this->widget->get_field_id( 'short_code' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'short_code' ); ?>"><?php echo $this->get( 'short_code' ); ?></textarea>
	</p>

</div>