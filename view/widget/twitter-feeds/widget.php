<?php foreach ( $this->get( 'tweets[errors]', array(), true ) as $error ): ?>
	<p><?php echo $error->message; ?></p>
<?php endforeach; ?>
<?php if ( $this->get( 'tweets[errors]', null, true ) == null ): ?>
	<ul class="js-carousel" data-config='
					{
							"auto": {
									"timeoutDuration":  5000
							},
							"items": {
									"width": "284",
									"visible": 1
							},
							"scroll": {
									"fx": "fade" ,
									"pauseOnHover": true
							},
							"next": {
									"button": null
							},
							"prev": {
									"button": null
							}
					}'>
		<?php foreach ( $this->get( 'tweets', array() ) as $tweet ): ?>

			<li>
				<div class="row-fluid">
					<div class="span2">
						<i class="icon-twitter icon_in-bubble icon_big"></i>
					</div>
					<div class="span10">
						<p>
							<a href="https://twitter.com/<?php echo $tweet['screen_name']; ?>" class="author">
								<?php echo '@' . $tweet['screen_name']; ?>
							</a>
							<?php echo $tweet['text']; ?>
						</p>
						<?php
						echo "<time>" . Plex_Lib_DateTimeHelper::time_ago( $tweet['created_at'] ) . "</time>";
						?>
					</div>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>