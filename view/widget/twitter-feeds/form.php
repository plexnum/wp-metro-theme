<p>
		<?php echo __("Twits to display: ", plex_get_trans_domain()); ?>
		<select name="<?php echo $this->widget->get_field_name('display_items'); ?>">
			<?php for($i = 1; $i <= 10; $i++): ?>
				<option value="<?php echo $i; ?>" <?php selected($this->get('display_items', 5, true), $i); ?>><?php echo $i; ?></option>
			<?php endfor; ?>
		</select>
</p>