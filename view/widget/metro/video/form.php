<div class="-widget-box -image-block -context">

	<?php if ( $this->get( 'error', false, true ) ): ?>
		<div class="error-message"><?php echo $this->get( 'error' ); ?></div>
	<?php endif; ?>

	<p>

		<label for="<?php echo $this->widget->get_field_id( 'url' ); ?>">
			<?php _e( "Video url: ", plex_get_trans_domain() ); ?>
		</label>

		<input type="text" class="widefat"
					 id="<?php echo $this->widget->get_field_id( 'url' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'url' ); ?>"
					 value="<?php echo $this->get( 'url' ); ?>">

		<input type="hidden" class="-image__att-id"
					 id="<?php echo $this->widget->get_field_id( 'attachment_id' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'attachment_id' ); ?>"
					 value="<?php echo $this->get( 'attachment_id' ); ?>">

		<input type="hidden" class="-image__input"
					 id="<?php echo $this->widget->get_field_id( 'image' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'image' ); ?>"
					 value="<?php echo $this->get( 'image' ); ?>">
	</p>

	<p class="-image__preview-box">
		<?php if ( $this->get( 'image' ) ): ?>
			<img src="<?php echo $this->get( 'image' ); ?>" width="100%">
		<?php else: ?>
			<img src="<?php echo plex_fix_url() . '/default-video-thumbnail.png'; ?>" width="100%">
		<?php endif; ?>
	</p>

	<?php echo $this->get( 'metro' ); ?>

	<p>
		<input class="checkbox js-toggle-box-trigger"
					 data-box="videoDateBox"
					 type="checkbox"
					 id="<?php echo $this->widget->get_field_id( 'with_description' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'with_description' ); ?>"
					 value="1" <?php checked( true, $this->get( 'with_description' ) ); ?>>

		<label for="<?php echo $this->widget->get_field_id( 'with_description' ); ?>">
			<?php _e( "With description: ", plex_get_trans_domain() ); ?>
		</label>
	</p>

	<div id="videoDateBox"
			<?php if ( ! $this->get( 'with_description' ) ): ?>
				style="display: none;"
			<?php endif; ?>>

		<p>
			<label for="<?php echo $this->widget->get_field_id( 'link' ); ?>">
				<?php echo __( "Link: ", plex_get_trans_domain() ); ?>
			</label>
			<input type="text" class="widefat"
						 id="<?php echo $this->widget->get_field_id( 'link' ); ?>"
						 name="<?php echo $this->widget->get_field_name( 'link' ); ?>"
						 value="<?php echo $this->get( 'link' ); ?>">
		</p>


		<p>
			<input class="checkbox"
						 type="checkbox"
						 id="<?php echo $this->widget->get_field_id( 'in_new_page' ); ?>"
						 name="<?php echo $this->widget->get_field_name( 'in_new_page' ); ?>"
						 value="1" <?php checked( true, $this->get( 'in_new_page' ) ); ?>>

			<label for="<?php echo $this->widget->get_field_id( 'in_new_page' ); ?>">
				<?php _e( "Show in new page: ", plex_get_trans_domain() ); ?>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->widget->get_field_id( 'header' ); ?>">
				<?php echo __( "Header: ", plex_get_trans_domain() ); ?>
			</label>
			<input type="text" class="widefat"
						 id="<?php echo $this->widget->get_field_id( 'header' ); ?>"
						 name="<?php echo $this->widget->get_field_name( 'header' ); ?>"
						 value="<?php echo $this->get( 'header' ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->widget->get_field_id( 'description' ); ?>">
				<?php echo __( "Description: ", plex_get_trans_domain() ); ?>
			</label>
			<textarea class="widefat"
								id="<?php echo $this->widget->get_field_id( 'description' ); ?>"
								name="<?php echo $this->widget->get_field_name( 'description' ); ?>"><?php echo $this->get( 'description' ); ?></textarea>
		</p>

		<p>
			<label for="<?php echo $this->widget->get_field_id( 'author' ); ?>">
				<?php echo __( "Author: ", plex_get_trans_domain() ); ?>
			</label>
			<input type="text" class="widefat"
						 id="<?php echo $this->widget->get_field_id( 'author' ); ?>"
						 name="<?php echo $this->widget->get_field_name( 'author' ); ?>"
						 value="<?php echo esc_html( $this->get( 'author' ) ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->widget->get_field_id( 'date' ); ?>">
				<?php _e( "Date: ", plex_get_trans_domain() ); ?>
			</label>
			<input type="text" class="widefat datepicker"
						 id="<?php echo $this->widget->get_field_id( 'date' ); ?>"
						 name="<?php echo $this->widget->get_field_name( 'date' ); ?>"
						 value="<?php echo $this->get( 'date' ); ?>">
		</p>

		<input type="hidden"
					 name="<?php echo $this->widget->get_field_name( 'width' ); ?>"
					 value="<?php echo $this->get( 'width' ); ?>">

		<input type="hidden"
					 name="<?php echo $this->widget->get_field_name( 'height' ); ?>"
					 value="<?php echo $this->get( 'height' ); ?>">

	</div>
</div>