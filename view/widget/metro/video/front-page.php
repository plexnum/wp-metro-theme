<?php if ( $this->get( 'with_description' ) ): ?>

	<div class="tile_img-dis <?php echo $this->get('color_type'); ?>">

			<img class="js-imgCentr" src="<?php echo $this->get( 'image' ); ?>"
					 alt="<?php echo $this->get( 'header' ); ?>"
					 width="<?php echo $this->get( 'width' ); ?>"
					 height="<?php echo $this->get( 'height' ); ?>" />

		<a class="tile__icon-box js-fancybox-media" href="#<?php esc_attr_e( $this->get( 'widget_id' ) ); ?>">
			<i class="icon-play-2 icon_in-bubble icon_big"></i>
		</a>

		<div id="<?php esc_attr_e( $this->get( 'widget_id' ) ); ?>"
				 data-width="800"
				 style="display: none;"
				 class="blog-item__video">
			<?php echo $this->get( 'popup_view' ); ?>
		</div>

		<div class="dis">

			<span class="bg-main-bg"></span>

			<time>
				<?php if ( $this->get( 'date' ) ): ?>
					<?php echo date_i18n( get_option( 'date_format' ), strtotime( $this->get( 'date' ) ) ); ?>
				<?php endif; ?>

				<?php if ( $this->get( 'author' ) ): ?>
					<?php _e( ' by ', plex_get_trans_domain() ); ?>
					<?php echo $this->get( 'author' ); ?>
				<?php endif; ?>
			</time>
			<?php if ( $this->get( 'header' ) ): ?>
				<h3>
					<?php echo $this->get( 'header' ); ?>
				</h3>
			<?php endif; ?>


			<div class="separator__wrap  separator_right">
				<div class="separator__content">
					<?php echo $this->get( 'description' ); ?>
				</div>

				<?php if ( $this->get( 'link' ) ): ?>
					<a href="<?php echo $this->get( 'link' ); ?>" class=" separator" <?php plex_the_link_target( $this->get( 'in_new_page' ) ); ?>>
						<i class="icon-next icon_lage icon_in-bubble"></i> <span class="separator_fantom"></span>
					</a>
				<?php endif; ?>

			</div>

		</div>

	</div>

<?php else: ?>

	<a class="tile_video js-fancybox-media <?php echo $this->get('color_type'); ?>"
		 href="#<?php esc_attr_e( $this->get( 'widget_id' ) ); ?>">

		<img class="js-imgCentr" src="<?php echo $this->get( 'image' ); ?>"
				 width="<?php echo $this->get( 'width' ); ?>"
				 height="<?php echo $this->get( 'height' ); ?>" />

	<span class="tile__icon-box">
				<i class="icon-play-2 icon_big icon_in-bubble"></i>
	</span>

		<span class="bg-main-bg"></span>

		<div id="<?php esc_attr_e( $this->get( 'widget_id' ) ); ?>"
				 data-width="800"
				 style="display: none;"
				 class="blog-item__video">
			<?php echo $this->get( 'popup_view' ); ?>
		</div>

	</a>

<?php endif; ?>