<div class="-widget-box -context">

	<?php echo $this->get( 'metro' ); ?>

	<p>
		<a href="#0" class="button js-insert-short-code">
			<?php _e( 'Insert Short Code', plex_get_trans_domain() ); ?>
		</a>
	</p>

	<p>
		<label for="<?php echo $this->widget->get_field_id( 'info' ); ?>">
			<?php _e( "Info: ", plex_get_trans_domain() ); ?>
		</label>

		<textarea type="text" rows="15" class="widefat js-short-code-textarea"
							id="<?php echo $this->widget->get_field_id( 'content' ); ?>"
							name="<?php echo $this->widget->get_field_name( 'content' ); ?>"><?php echo $this->get( 'content' ); ?></textarea>
	</p>

</div>