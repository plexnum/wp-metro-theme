<div class="tile_text <?php echo $this->get( 'color_type' ); ?>">
	<span class="bg-main-bg"></span>

	<div class="dis">

		<div class="scroller">
			<?php echo apply_filters( 'the_content', $this->get( 'content' ) ); ?>
			<div class="scroller__track">
				<div class="scroller__bar"></div>
			</div>
		</div>
	</div>

</div>