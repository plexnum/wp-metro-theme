<?php if ( $this->get( 'with_description' ) ): ?>

	<div class="tile_img-dis <?php echo $this->get('color_type'); ?>">

		<span class="bg-main-bg"></span>

		<?php if ( $this->get( 'image' ) ): ?>

			<img class="js-imgCentr" src="<?php echo $this->get( 'image' ); ?>"
					 alt="<?php echo $this->get( 'alt' ); ?>"
					 width="<?php echo $this->get( 'width' ); ?>"
					 height="<?php echo $this->get( 'height' ); ?>" />

		<?php endif; ?>

		<div class="dis">

			<span class="bg-main-bg"></span>

			<time>
				<?php if ( $this->get( 'date' ) && $this->get( 'with_date' ) ): ?>
					<?php echo date_i18n( get_option( 'date_format' ),
						strtotime( $this->get( 'date' ) ) ); ?>
				<?php endif; ?>

				<?php if ( $this->get( 'author' ) ): ?>
					<?php _e( ' by ', plex_get_trans_domain() );
					echo $this->get( 'author' ); ?>
				<?php endif; ?>
			</time>

			<?php if ( $this->get( 'header' ) ): ?>
				<h3>
					<?php echo $this->get( 'header' ); ?>
				</h3>
			<?php endif; ?>

			<div class="separator__wrap  separator_right">

				<div class="separator__content">
					<?php echo $this->get( 'description' ); ?>
				</div>

				<?php if ( $this->get( 'url' ) ): ?>
					<a href="<?php echo $this->get( 'url' ); ?>"
						 title="<?php echo $this->get( 'header' ); ?>"
							<?php plex_the_link_target( $this->get( 'in_new_page' ) ); ?>
						 class="separator">
						<i class="icon-next icon_lage icon_in-bubble"></i>
					</a>
				<?php endif; ?>

			</div>
		</div>

	</div>

<?php elseif ( $this->get( 'with_date' ) ): ?>

	<a href="<?php echo $this->get( 'url' ); ?>"
			<?php plex_the_link_target( $this->get( 'in_new_page' ) ); ?>
		 class="tile_date <?php echo $this->get('color_type'); ?>">

		<span class="bg-main-bg"></span>

		<?php if ( $this->get( 'image' ) ): ?>

			<img class="js-imgCentr" src="<?php echo $this->get( 'image' ); ?>"
					 alt="<?php echo $this->get( 'alt' ); ?>"
					 width="<?php echo $this->get( 'width' ); ?>"
					 height="<?php echo $this->get( 'height' ); ?>" />

		<?php endif; ?>

		<div class="box">

			<span class="bg-main-bg"></span>

			<?php if ( $this->get( 'date' ) ): ?>
				<time class="separator__wrap separator_right">
					<?php $timestamp = strtotime( $this->get( 'date' ) ); ?>
					<div class="separator__content">
						<span class="dey"><?php echo date_i18n( 'd', $timestamp ); ?></span>
						<span class="mon"><?php echo date_i18n( 'M', $timestamp ); ?></span>
					</div>

					<div class="separator">
						<i class="icon-next icon_lage icon_in-bubble"></i>
					</div>

				</time>
			<?php endif; ?>

		</div>

	</a>

<?php else: ?>
	<a href="<?php echo $this->get( 'url' ); ?>"
			<?php plex_the_link_target( $this->get( 'in_new_page' ) ); ?>
		 class="tile_img <?php echo $this->get('color_type'); ?>">

		<?php if ( $this->get( 'image' ) ): ?>

			<img class="js-imgCentr" src="<?php echo $this->get( 'image' ); ?>" alt="<?php echo $this->get( 'alt' ); ?>" width="<?php echo $this->get( 'width' ); ?>" height="<?php echo $this->get( 'height' ); ?>" />

		<?php endif; ?>

	</a>
<?php endif; ?>