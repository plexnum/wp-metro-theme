<div class="-widget-box -image-block -context">

	<?php if ( $this->get( 'error' ) ): ?>
		<div class="error-message"><?php echo $this->get( 'error' ); ?></div>
	<?php endif; ?>

	<p>
		<label for="<?php echo $this->widget->get_field_id( 'url' ); ?>">
			<?php _e( "Url: ", plex_get_trans_domain() ); ?>
		</label>
		<input type="text" class="widefat"
					 id="<?php echo $this->widget->get_field_id( 'url' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'url' ); ?>"
					 value="<?php echo $this->get( 'url' ); ?>">
	</p>

	<p>
		<input class="checkbox"
					 type="checkbox"
					 id="<?php echo $this->widget->get_field_id( 'in_new_page' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'in_new_page' ); ?>"
					 value="1" <?php checked( true, $this->get( 'in_new_page' ) ); ?>>

		<label for="<?php echo $this->widget->get_field_id( 'in_new_page' ); ?>">
			<?php _e( "Show in new page: ", plex_get_trans_domain() ); ?>
		</label>
	</p>

	<p>
		<label for="<?php echo $this->widget->get_field_id( 'image' ); ?>">
			<?php echo __( "Image: ", plex_get_trans_domain() ); ?>
		</label>
		<input type="text" class="-image__input widefat"
					 id="<?php echo $this->widget->get_field_id( 'image' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'image' ); ?>"
					 value="<?php echo $this->get( 'image' ); ?>">
		<input type="hidden" class="-image__att-id"
					 name="<?php echo $this->widget->get_field_name( 'attachment_id' ); ?>"
					 value="<?php echo $this->get( 'attachment_id' ); ?>">
	</p>

	<p class="-image__preview-box">
		<?php if ( $this->get( 'image' ) ): ?>
			<img src="<?php echo $this->get( 'image' ); ?>" width="100%">
		<?php endif; ?>
	</p>

	<?php echo $this->get( 'metro' ); ?>

	<p>
		<input class="checkbox js-toggle-box-trigger"
					 data-box="imgDescBox"
					 type="checkbox"
					 id="<?php echo $this->widget->get_field_id( 'with_description' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'with_description' ); ?>"
					 value="1" <?php checked( true, $this->get( 'with_description' ) ); ?>>

		<label for="<?php echo $this->widget->get_field_id( 'with_description' ); ?>">
			<?php _e( "With description: ", plex_get_trans_domain() ); ?>
		</label>
	</p>

	<div id="imgDescBox"
			<?php if ( ! $this->get( 'with_description' ) ): ?>
				style="display: none;"
			<?php endif; ?>>

		<p>
			<label for="<?php echo $this->widget->get_field_id( 'header' ); ?>">
				<?php echo __( "Header: ", plex_get_trans_domain() ); ?>
			</label>
			<input type="text" class="widefat"
						 id="<?php echo $this->widget->get_field_id( 'header' ); ?>"
						 name="<?php echo $this->widget->get_field_name( 'header' ); ?>"
						 value="<?php echo $this->get( 'header' ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->widget->get_field_id( 'description' ); ?>">
				<?php echo __( "Description: ", plex_get_trans_domain() ); ?>
			</label>
			<textarea class="widefat"
								id="<?php echo $this->widget->get_field_id( 'description' ); ?>"
								name="<?php echo $this->widget->get_field_name( 'description' ); ?>"><?php echo $this->get( 'description' ); ?></textarea>
		</p>

		<p>
			<label for="<?php echo $this->widget->get_field_id( 'author' ); ?>">
				<?php echo __( "Author: ", plex_get_trans_domain() ); ?>
			</label>
			<input type="text" class="widefat"
						 id="<?php echo $this->widget->get_field_id( 'author' ); ?>"
						 name="<?php echo $this->widget->get_field_name( 'author' ); ?>"
						 value="<?php echo esc_html( $this->get( 'author' ) ); ?>">
		</p>

	</div>

	<p>
		<input class="checkbox js-toggle-box-trigger" data-box="imgDateBox"
					 type="checkbox"
					 id="<?php echo $this->widget->get_field_id( 'with_date' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'with_date' ); ?>"
					 value="1"
				<?php checked( true, $this->get( 'with_date' ) ); ?>>
		<label for="<?php echo $this->widget->get_field_id( 'with_date' ); ?>">
			<?php echo __( "With date: ", plex_get_trans_domain() ); ?>
		</label>
	</p>

	<div id="imgDateBox" <?php if ($this->get( 'with_date' ) == false): ?>style="display: none;" <?php endif; ?>>
		<p>
			<label for="<?php echo $this->widget->get_field_id( 'date' ); ?>">
				<?php _e( "Date: ", plex_get_trans_domain() ); ?>
			</label>
			<input type="text" class="widefat datepicker"
						 id="<?php echo $this->widget->get_field_id( 'date' ); ?>"
						 name="<?php echo $this->widget->get_field_name( 'date' ); ?>"
						 value="<?php echo $this->get( 'date' ); ?>">
		</p>
	</div>
</div>