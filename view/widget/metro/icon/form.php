<div class="-widget-box -context">

	<p>
		<label for="<?php echo $this->widget->get_field_id( 'header' ); ?>">
			<?php echo __( "Header: ", plex_get_trans_domain() ); ?>
		</label>
		<input type="text" class="widefat"
					 id="<?php echo $this->widget->get_field_id( 'header' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'header' ); ?>"
					 value="<?php echo $this->get( 'header' ); ?>">
	</p>

	<p>
		<label for="<?php echo $this->widget->get_field_id( 'url' ); ?>">
			<?php echo __( "Url: ", plex_get_trans_domain() ); ?>
		</label>
		<input type="text" class="widefat"
					 id="<?php echo $this->widget->get_field_id( 'url' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'url' ); ?>"
					 value="<?php echo $this->get( 'url' ); ?>">
	</p>

	<p>
		<input class="checkbox"
					 type="checkbox"
					 id="<?php echo $this->widget->get_field_id( 'in_new_page' ); ?>"
					 name="<?php echo $this->widget->get_field_name( 'in_new_page' ); ?>"
					 value="1" <?php checked( true, $this->get( 'in_new_page' ) ); ?>>

		<label for="<?php echo $this->widget->get_field_id( 'in_new_page' ); ?>">
			<?php _e( "Show in new page: ", plex_get_trans_domain() ); ?>
		</label>
	</p>

	<div class="-icons-selection">

		<?php foreach ( $this->get( 'icons', array() ) as $icon ): ?>
			<i class="<?php echo $icon; ?>
				<?php if ( $this->get( 'icon' ) == $icon ): ?>-selected<?php endif; ?>">
			</i>
		<?php endforeach; ?>

		<input type="hidden" class="-icon__input"
					 name="<?php echo $this->widget->get_field_name( 'icon' ); ?>"
					 value="<?php echo $this->get( 'icon' ); ?>">
	</div>

</div>