<a class="tile_basis-linc <?php echo $this->get('color_type'); ?>"
	 href="<?php echo $this->get( 'url' ); ?>"
	 title="<?php echo $this->get('header'); ?>">
	<div class="bg-main-bg"></div>
	<i class="<?php echo $this->get( 'icon' ); ?>"></i>
	<span><?php echo $this->get( 'header' ); ?></span>
</a>