<p>
	<label for="<?php echo $this->widget->get_field_id( 'color_type' ); ?>">
		<?php _e( "Color types: ", plex_get_trans_domain() ); ?>
	</label>
	<select id="<?php echo $this->widget->get_field_id( 'color_type' ); ?>"
					name="<?php echo $this->widget->get_field_name( 'color_type' ); ?>">
		<?php foreach ( $this->get( 'color_types' ) as $color_type_key => $color_type_val ): ?>
			<option value="<?php echo $color_type_key; ?>"
					<?php selected( $this->get( 'color_type', 'default' ), $color_type_key ); ?>>
				<?php echo $color_type_val; ?>
			</option>
		<?php endforeach; ?>
	</select>
</p>

<p>
	<label for="<?php echo $this->widget->get_field_id( 'size' ); ?>">
		<?php _e( "Size: ", plex_get_trans_domain() ); ?>
	</label>
	<select id="<?php echo $this->widget->get_field_id( 'size' ); ?>"
					name="<?php echo $this->widget->get_field_name( 'size' ); ?>">
		<?php foreach ( $this->get( 'sizes' ) as $size_key => $opts ): ?>
			<option value="<?php echo $size_key; ?>"
					<?php selected( $this->get( 'size', 'plex_tile_140_140' ), $size_key ); ?>>
				<?php echo $opts['label']; ?>
			</option>
		<?php endforeach; ?>
	</select>
</p>