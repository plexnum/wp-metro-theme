<?php

/**
 * ========================================================================
 * Tag page
 * ========================================================================
 */

if ( ! defined( 'ABSPATH' ) ) exit;
?>

<?php plex_load_partial( 'view/templates/_partials/header' ); // Load header partial ?>

<?php

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();
$plex_view_transfer->set( 'page_header', single_tag_title( '', false ) );
$plex_view_transfer->set( 'not_founded', __( "Sorry, but tagged posts haven't been found", plex_get_trans_domain() ) );
$plex_view_transfer->set( 'page_icon', 'icon-tag' );
$plex_view_transfer->get( 'sidebar_type', 'filter' );

if ( tag_description() ) {
	$plex_view_transfer->set( 'page_excerpt', tag_description() );
}

?>

<?php plex_load_partial( 'view/templates/_partials/page-header' ); ?>

<?php plex_load_partial( 'view/templates/_content/search' ); // Load posts ?>

<?php plex_load_partial( 'view/templates/_partials/footer' ); // Load footer partial ?>