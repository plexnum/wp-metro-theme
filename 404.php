<?php
/**
 * ========================================================================
 * 404 page
 * ========================================================================
 */

if ( !defined('ABSPATH')) exit; ?>

<?php plex_load_partial( 'view/templates/_partials/header' ); ?>

<div class="  text-center" >
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<h1 class="page-header">
		<?php _e('Error 404', plex_get_trans_domain()); ?>
	</h1>

	<?php _e("We're sorry, but the page you are looking for doesn't exist. <br/>You can go to the <a href='/' class='btn'>homepage</a>", plex_get_trans_domain()); ?>
</div>

<?php plex_load_partial( 'view/templates/_partials/footer' ); // Load footer partial ?>
