<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Denine direct access

define( 'PLEX_THEME_DIR', dirname( __FILE__ ) );

require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';


// autoloader.php goal of that file to register autoloader
// which will be used to autoload files when classes instantiated.
require 'theme/autoloader.php';

// bootstrap theme enviroment
require 'theme/bootstrap.php';