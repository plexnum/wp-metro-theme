## 

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide.

## Sass install

[Sass](http://sass-lang.com) is a preprocessor that adds nested rules, variables, mixins and functions, selector inheritance, and more to css. Sass files compile into well-formatted, standard CSS to use in your site or application.

This task requires you to have [Ruby](http://www.ruby-lang.org/en/downloads/) and [Sass](http://sass-lang.com/download.html) installed. If you're on OS X or Linux you probably already have Ruby installed; test with `ruby -v` in your terminal. When you've confirmed you have Ruby installed, run `gem install sass --pre` to install Sass.

## install requirements

Go to the assets/ folder in your terminal and run the `npm install` comand
 
```
	npm install
```

## Tasks for sass

for convert sass -> css
```
	grunt sass
```
for watching changes
```
	grunt sassw
```

WANING: Before deploy your changes to the web site: 
- open the style.css 
- cut the theme css settings from the first line to the passphrase "set-val end" 
- replace the style-settings.css file to them.

it's necessary for the corect work of the team customizer:

## Tasks for compression js
for convert js -> .min.js
```
	grunt js
```
for watching main.js changes 
```
	grunt jsw
```
