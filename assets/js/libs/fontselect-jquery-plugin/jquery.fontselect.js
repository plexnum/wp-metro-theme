/*
 * jQuery.fontselect - A font selector for the Google Web Fonts api
 * Tom Moor, http://tommoor.com
 * Copyright (c) 2011 Tom Moor
 * MIT Licensed
 * @version 0.1
 */

(function ($) {

	$.fn.fontselect = function (options, args) {

		var __bind = function (fn, me) {
			return function () {
				return fn.apply(me, arguments);
			};
		};

		var fonts = ["ABeeZee", "ABeeZee:italic", "Abel", "Abril+Fatface", "Aclonica", "Acme", "Actor", "Adamina", "Advent+Pro", "Advent+Pro:100", "Advent+Pro:200", "Advent+Pro:300", "Advent+Pro:500", "Advent+Pro:600", "Advent+Pro:700", "Aguafina+Script", "Akronim", "Aladin", "Aldrich", "Alef", "Alef:700", "Alegreya", "Alegreya:italic", "Alegreya:700", "Alegreya:700italic", "Alegreya:900", "Alegreya:900italic", "Alegreya+SC", "Alegreya+SC:italic", "Alegreya+SC:700", "Alegreya+SC:700italic", "Alegreya+SC:900", "Alegreya+SC:900italic", "Alex+Brush", "Alfa+Slab+One", "Alice", "Alike", "Alike+Angular", "Allan", "Allan:700", "Allerta", "Allerta+Stencil", "Allura", "Almendra", "Almendra:italic", "Almendra:700", "Almendra:700italic", "Almendra+Display", "Almendra+SC", "Amarante", "Amaranth", "Amaranth:italic", "Amaranth:700", "Amaranth:700italic", "Amatic+SC", "Amatic+SC:700", "Amethysta", "Anaheim", "Andada", "Andika", "Angkor", "Annie+Use+Your+Telescope", "Anonymous+Pro", "Anonymous+Pro:italic", "Anonymous+Pro:700", "Anonymous+Pro:700italic", "Antic", "Antic+Didone", "Antic+Slab", "Anton", "Arapey", "Arapey:italic", "Arbutus", "Arbutus+Slab", "Architects+Daughter", "Archivo+Black", "Archivo+Narrow", "Archivo+Narrow:italic", "Archivo+Narrow:700", "Archivo+Narrow:700italic", "Arimo", "Arimo:italic", "Arimo:700", "Arimo:700italic", "Arizonia", "Armata", "Artifika", "Arvo", "Arvo:italic", "Arvo:700", "Arvo:700italic", "Asap", "Asap:italic", "Asap:700", "Asap:700italic", "Asset", "Astloch", "Astloch:700", "Asul", "Asul:700", "Atomic+Age", "Aubrey", "Audiowide", "Autour+One", "Average", "Average+Sans", "Averia+Gruesa+Libre", "Averia+Libre", "Averia+Libre:300", "Averia+Libre:300italic", "Averia+Libre:italic", "Averia+Libre:700", "Averia+Libre:700italic", "Averia+Sans+Libre", "Averia+Sans+Libre:300", "Averia+Sans+Libre:300italic", "Averia+Sans+Libre:italic", "Averia+Sans+Libre:700", "Averia+Sans+Libre:700italic", "Averia+Serif+Libre", "Averia+Serif+Libre:300", "Averia+Serif+Libre:300italic", "Averia+Serif+Libre:italic", "Averia+Serif+Libre:700", "Averia+Serif+Libre:700italic", "Bad+Script", "Balthazar", "Bangers", "Basic", "Battambang", "Battambang:700", "Baumans", "Bayon", "Belgrano", "Belleza", "BenchNine", "BenchNine:300", "BenchNine:700", "Bentham", "Berkshire+Swash", "Bevan", "Bigelow+Rules", "Bigshot+One", "Bilbo", "Bilbo+Swash+Caps", "Bitter", "Bitter:italic", "Bitter:700", "Black+Ops+One", "Bokor", "Bonbon", "Boogaloo", "Bowlby+One", "Bowlby+One+SC", "Brawler", "Bree+Serif", "Bubblegum+Sans", "Bubbler+One", "Buda", "Buenard", "Buenard:700", "Butcherman", "Butterfly+Kids", "Cabin", "Cabin:italic", "Cabin:500", "Cabin:500italic", "Cabin:600", "Cabin:600italic", "Cabin:700", "Cabin:700italic", "Cabin+Condensed", "Cabin+Condensed:500", "Cabin+Condensed:600", "Cabin+Condensed:700", "Cabin+Sketch", "Cabin+Sketch:700", "Caesar+Dressing", "Cagliostro", "Calligraffitti", "Cambo", "Candal", "Cantarell", "Cantarell:italic", "Cantarell:700", "Cantarell:700italic", "Cantata+One", "Cantora+One", "Capriola", "Cardo", "Cardo:italic", "Cardo:700", "Carme", "Carrois+Gothic", "Carrois+Gothic+SC", "Carter+One", "Caudex", "Caudex:italic", "Caudex:700", "Caudex:700italic", "Cedarville+Cursive", "Ceviche+One", "Changa+One", "Changa+One:italic", "Chango", "Chau+Philomene+One", "Chau+Philomene+One:italic", "Chela+One", "Chelsea+Market", "Chenla", "Cherry+Cream+Soda", "Cherry+Swash", "Cherry+Swash:700", "Chewy", "Chicle", "Chivo", "Chivo:italic", "Chivo:900", "Chivo:900italic", "Cinzel", "Cinzel:700", "Cinzel:900", "Cinzel+Decorative", "Cinzel+Decorative:700", "Cinzel+Decorative:900", "Clicker+Script", "Coda", "Coda:800", "Coda+Caption", "Codystar", "Codystar:300", "Combo", "Comfortaa", "Comfortaa:300", "Comfortaa:700", "Coming+Soon", "Concert+One", "Condiment", "Content", "Content:700", "Contrail+One", "Convergence", "Cookie", "Copse", "Corben", "Corben:700", "Courgette", "Cousine", "Cousine:italic", "Cousine:700", "Cousine:700italic", "Coustard", "Coustard:900", "Covered+By+Your+Grace", "Crafty+Girls", "Creepster", "Crete+Round", "Crete+Round:italic", "Crimson+Text", "Crimson+Text:italic", "Crimson+Text:600", "Crimson+Text:600italic", "Crimson+Text:700", "Crimson+Text:700italic", "Croissant+One", "Crushed", "Cuprum", "Cuprum:italic", "Cuprum:700", "Cuprum:700italic", "Cutive", "Cutive+Mono", "Damion", "Dancing+Script", "Dancing+Script:700", "Dangrek", "Dawning+of+a+New+Day", "Days+One", "Delius", "Delius+Swash+Caps", "Delius+Unicase", "Delius+Unicase:700", "Della+Respira", "Denk+One", "Devonshire", "Didact+Gothic", "Diplomata", "Diplomata+SC", "Domine", "Domine:700", "Donegal+One", "Doppio+One", "Dorsa", "Dosis", "Dosis:200", "Dosis:300", "Dosis:500", "Dosis:600", "Dosis:700", "Dosis:800", "Dr+Sugiyama", "Droid+Sans", "Droid+Sans:700", "Droid+Sans+Mono", "Droid+Serif", "Droid+Serif:italic", "Droid+Serif:700", "Droid+Serif:700italic", "Duru+Sans", "Dynalight", "EB+Garamond", "Eagle+Lake", "Eater", "Economica", "Economica:italic", "Economica:700", "Economica:700italic", "Electrolize", "Elsie", "Elsie:900", "Elsie+Swash+Caps", "Elsie+Swash+Caps:900", "Emblema+One", "Emilys+Candy", "Engagement", "Englebert", "Enriqueta", "Enriqueta:700", "Erica+One", "Esteban", "Euphoria+Script", "Ewert", "Exo", "Exo:100", "Exo:100italic", "Exo:200", "Exo:200italic", "Exo:300", "Exo:300italic", "Exo:italic", "Exo:500", "Exo:500italic", "Exo:600", "Exo:600italic", "Exo:700", "Exo:700italic", "Exo:800", "Exo:800italic", "Exo:900", "Exo:900italic", "Expletus+Sans", "Expletus+Sans:italic", "Expletus+Sans:500", "Expletus+Sans:500italic", "Expletus+Sans:600", "Expletus+Sans:600italic", "Expletus+Sans:700", "Expletus+Sans:700italic", "Fanwood+Text", "Fanwood+Text:italic", "Fascinate", "Fascinate+Inline", "Faster+One", "Fasthand", "Fauna+One", "Federant", "Federo", "Felipa", "Fenix", "Finger+Paint", "Fjalla+One", "Fjord+One", "Flamenco", "Flamenco:300", "Flavors", "Fondamento", "Fondamento:italic", "Fontdiner+Swanky", "Forum", "Francois+One", "Freckle+Face", "Fredericka+the+Great", "Fredoka+One", "Freehand", "Fresca", "Frijole", "Fruktur", "Fugaz+One", "GFS+Didot", "GFS+Neohellenic", "GFS+Neohellenic:italic", "GFS+Neohellenic:700", "GFS+Neohellenic:700italic", "Gabriela", "Gafata", "Galdeano", "Galindo", "Gentium+Basic", "Gentium+Basic:italic", "Gentium+Basic:700", "Gentium+Basic:700italic", "Gentium+Book+Basic", "Gentium+Book+Basic:italic", "Gentium+Book+Basic:700", "Gentium+Book+Basic:700italic", "Geo", "Geo:italic", "Geostar", "Geostar+Fill", "Germania+One", "Gilda+Display", "Give+You+Glory", "Glass+Antiqua", "Glegoo", "Gloria+Hallelujah", "Goblin+One", "Gochi+Hand", "Gorditas", "Gorditas:700", "Goudy+Bookletter+1911", "Graduate", "Grand+Hotel", "Gravitas+One", "Great+Vibes", "Griffy", "Gruppo", "Gudea", "Gudea:italic", "Gudea:700", "Habibi", "Hammersmith+One", "Hanalei", "Hanalei+Fill", "Handlee", "Hanuman", "Hanuman:700", "Happy+Monkey", "Headland+One", "Henny+Penny", "Herr+Von+Muellerhoff", "Holtwood+One+SC", "Homemade+Apple", "Homenaje", "IM+Fell+DW+Pica", "IM+Fell+DW+Pica:italic", "IM+Fell+DW+Pica+SC", "IM+Fell+Double+Pica", "IM+Fell+Double+Pica:italic", "IM+Fell+Double+Pica+SC", "IM+Fell+English", "IM+Fell+English:italic", "IM+Fell+English+SC", "IM+Fell+French+Canon", "IM+Fell+French+Canon:italic", "IM+Fell+French+Canon+SC", "IM+Fell+Great+Primer", "IM+Fell+Great+Primer:italic", "IM+Fell+Great+Primer+SC", "Iceberg", "Iceland", "Imprima", "Inconsolata", "Inconsolata:700", "Inder", "Indie+Flower", "Inika", "Inika:700", "Irish+Grover", "Istok+Web", "Istok+Web:italic", "Istok+Web:700", "Istok+Web:700italic", "Italiana", "Italianno", "Jacques+Francois", "Jacques+Francois+Shadow", "Jim+Nightshade", "Jockey+One", "Jolly+Lodger", "Josefin+Sans", "Josefin+Sans:100", "Josefin+Sans:100italic", "Josefin+Sans:300", "Josefin+Sans:300italic", "Josefin+Sans:italic", "Josefin+Sans:600", "Josefin+Sans:600italic", "Josefin+Sans:700", "Josefin+Sans:700italic", "Josefin+Slab", "Josefin+Slab:100", "Josefin+Slab:100italic", "Josefin+Slab:300", "Josefin+Slab:300italic", "Josefin+Slab:italic", "Josefin+Slab:600", "Josefin+Slab:600italic", "Josefin+Slab:700", "Josefin+Slab:700italic", "Joti+One", "Judson", "Judson:italic", "Judson:700", "Julee", "Julius+Sans+One", "Junge", "Jura", "Jura:300", "Jura:500", "Jura:600", "Just+Another+Hand", "Just+Me+Again+Down+Here", "Kameron", "Kameron:700", "Karla", "Karla:italic", "Karla:700", "Karla:700italic", "Kaushan+Script", "Kavoon", "Keania+One", "Kelly+Slab", "Kenia", "Khmer", "Kite+One", "Knewave", "Kotta+One", "Koulen", "Kranky", "Kreon", "Kreon:300", "Kreon:700", "Kristi", "Krona+One", "La+Belle+Aurore", "Lancelot", "Lato", "Lato:100", "Lato:100italic", "Lato:300", "Lato:300italic", "Lato:italic", "Lato:700", "Lato:700italic", "Lato:900", "Lato:900italic", "League+Script", "Leckerli+One", "Ledger", "Lekton", "Lekton:italic", "Lekton:700", "Lemon", "Libre+Baskerville", "Libre+Baskerville:italic", "Libre+Baskerville:700", "Life+Savers", "Life+Savers:700", "Lilita+One", "Lily+Script+One", "Limelight", "Linden+Hill", "Linden+Hill:italic", "Lobster", "Lobster+Two", "Lobster+Two:italic", "Lobster+Two:700", "Lobster+Two:700italic", "Londrina+Outline", "Londrina+Shadow", "Londrina+Sketch", "Londrina+Solid", "Lora", "Lora:italic", "Lora:700", "Lora:700italic", "Love+Ya+Like+A+Sister", "Loved+by+the+King", "Lovers+Quarrel", "Luckiest+Guy", "Lusitana", "Lusitana:700", "Lustria", "Macondo", "Macondo+Swash+Caps", "Magra", "Magra:700", "Maiden+Orange", "Mako", "Marcellus", "Marcellus+SC", "Marck+Script", "Margarine", "Marko+One", "Marmelad", "Marvel", "Marvel:italic", "Marvel:700", "Marvel:700italic", "Mate", "Mate:italic", "Mate+SC", "Maven+Pro", "Maven+Pro:500", "Maven+Pro:700", "Maven+Pro:900", "McLaren", "Meddon", "MedievalSharp", "Medula+One", "Megrim", "Meie+Script", "Merienda", "Merienda:700", "Merienda+One", "Merriweather", "Merriweather:300", "Merriweather:300italic", "Merriweather:italic", "Merriweather:700", "Merriweather:700italic", "Merriweather:900", "Merriweather:900italic", "Merriweather+Sans", "Merriweather+Sans:300", "Merriweather+Sans:300italic", "Merriweather+Sans:italic", "Merriweather+Sans:700", "Merriweather+Sans:700italic", "Merriweather+Sans:800", "Merriweather+Sans:800italic", "Metal", "Metal+Mania", "Metamorphous", "Metrophobic", "Michroma", "Milonga", "Miltonian", "Miltonian+Tattoo", "Miniver", "Miss+Fajardose", "Modern+Antiqua", "Molengo", "Molle", "Monda", "Monda:700", "Monofett", "Monoton", "Monsieur+La+Doulaise", "Montaga", "Montez", "Montserrat", "Montserrat:700", "Montserrat+Alternates", "Montserrat+Alternates:700", "Montserrat+Subrayada", "Montserrat+Subrayada:700", "Moul", "Moulpali", "Mountains+of+Christmas", "Mountains+of+Christmas:700", "Mouse+Memoirs", "Mr+Bedfort", "Mr+Dafoe", "Mr+De+Haviland", "Mrs+Saint+Delafield", "Mrs+Sheppards", "Muli", "Muli:300", "Muli:300italic", "Muli:italic", "Mystery+Quest", "Neucha", "Neuton", "Neuton:200", "Neuton:300", "Neuton:italic", "Neuton:700", "Neuton:800", "New+Rocker", "News+Cycle", "News+Cycle:700", "Niconne", "Nixie+One", "Nobile", "Nobile:italic", "Nobile:700", "Nobile:700italic", "Nokora", "Nokora:700", "Norican", "Nosifer", "Nothing+You+Could+Do", "Noticia+Text", "Noticia+Text:italic", "Noticia+Text:700", "Noticia+Text:700italic", "Noto+Sans", "Noto+Sans:italic", "Noto+Sans:700", "Noto+Sans:700italic", "Noto+Serif", "Noto+Serif:italic", "Noto+Serif:700", "Noto+Serif:700italic", "Nova+Cut", "Nova+Flat", "Nova+Mono", "Nova+Oval", "Nova+Round", "Nova+Script", "Nova+Slim", "Nova+Square", "Numans", "Nunito", "Nunito:300", "Nunito:700", "Odor+Mean+Chey", "Offside", "Old+Standard+TT", "Old+Standard+TT:italic", "Old+Standard+TT:700", "Oldenburg", "Oleo+Script", "Oleo+Script:700", "Oleo+Script+Swash+Caps", "Oleo+Script+Swash+Caps:700", "Open+Sans", "Open+Sans:300", "Open+Sans:300italic", "Open+Sans:italic", "Open+Sans:600", "Open+Sans:600italic", "Open+Sans:700", "Open+Sans:700italic", "Open+Sans:800", "Open+Sans:800italic", "Open+Sans+Condensed", "Open+Sans+Condensed:300", "Open+Sans+Condensed:300italic", "Open+Sans+Condensed:700", "Oranienbaum", "Orbitron", "Orbitron:500", "Orbitron:700", "Orbitron:900", "Oregano", "Oregano:italic", "Orienta", "Original+Surfer", "Oswald", "Oswald:300", "Oswald:700", "Over+the+Rainbow", "Overlock", "Overlock:italic", "Overlock:700", "Overlock:700italic", "Overlock:900", "Overlock:900italic", "Overlock+SC", "Ovo", "Oxygen", "Oxygen:300", "Oxygen:700", "Oxygen+Mono", "PT+Mono", "PT+Sans", "PT+Sans:italic", "PT+Sans:700", "PT+Sans:700italic", "PT+Sans+Caption", "PT+Sans+Caption:700", "PT+Sans+Narrow", "PT+Sans+Narrow:700", "PT+Serif", "PT+Serif:italic", "PT+Serif:700", "PT+Serif:700italic", "PT+Serif+Caption", "PT+Serif+Caption:italic", "Pacifico", "Paprika", "Parisienne", "Passero+One", "Passion+One", "Passion+One:700", "Passion+One:900", "Pathway+Gothic+One", "Patrick+Hand", "Patrick+Hand+SC", "Patua+One", "Paytone+One", "Peralta", "Permanent+Marker", "Petit+Formal+Script", "Petrona", "Philosopher", "Philosopher:italic", "Philosopher:700", "Philosopher:700italic", "Piedra", "Pinyon+Script", "Pirata+One", "Plaster", "Play", "Play:700", "Playball", "Playfair+Display", "Playfair+Display:italic", "Playfair+Display:700", "Playfair+Display:700italic", "Playfair+Display:900", "Playfair+Display:900italic", "Playfair+Display+SC", "Playfair+Display+SC:italic", "Playfair+Display+SC:700", "Playfair+Display+SC:700italic", "Playfair+Display+SC:900", "Playfair+Display+SC:900italic", "Podkova", "Podkova:700", "Poiret+One", "Poller+One", "Poly", "Poly:italic", "Pompiere", "Pontano+Sans", "Port+Lligat+Sans", "Port+Lligat+Slab", "Prata", "Preahvihear", "Press+Start+2P", "Princess+Sofia", "Prociono", "Prosto+One", "Puritan", "Puritan:italic", "Puritan:700", "Puritan:700italic", "Purple+Purse", "Quando", "Quantico", "Quantico:italic", "Quantico:700", "Quantico:700italic", "Quattrocento", "Quattrocento:700", "Quattrocento+Sans", "Quattrocento+Sans:italic", "Quattrocento+Sans:700", "Quattrocento+Sans:700italic", "Questrial", "Quicksand", "Quicksand:300", "Quicksand:700", "Quintessential", "Qwigley", "Racing+Sans+One", "Radley", "Radley:italic", "Raleway", "Raleway:100", "Raleway:200", "Raleway:300", "Raleway:500", "Raleway:600", "Raleway:700", "Raleway:800", "Raleway:900", "Raleway+Dots", "Rambla", "Rambla:italic", "Rambla:700", "Rambla:700italic", "Rammetto+One", "Ranchers", "Rancho", "Rationale", "Redressed", "Reenie+Beanie", "Revalia", "Ribeye", "Ribeye+Marrow", "Righteous", "Risque", "Roboto", "Roboto:100", "Roboto:100italic", "Roboto:300", "Roboto:300italic", "Roboto:italic", "Roboto:500", "Roboto:500italic", "Roboto:700", "Roboto:700italic", "Roboto:900", "Roboto:900italic", "Roboto+Condensed", "Roboto+Condensed:300", "Roboto+Condensed:300italic", "Roboto+Condensed:italic", "Roboto+Condensed:700", "Roboto+Condensed:700italic", "Roboto+Slab", "Roboto+Slab:100", "Roboto+Slab:300", "Roboto+Slab:700", "Rochester", "Rock+Salt", "Rokkitt", "Rokkitt:700", "Romanesco", "Ropa+Sans", "Ropa+Sans:italic", "Rosario", "Rosario:italic", "Rosario:700", "Rosario:700italic", "Rosarivo", "Rosarivo:italic", "Rouge+Script", "Ruda", "Ruda:700", "Ruda:900", "Rufina", "Rufina:700", "Ruge+Boogie", "Ruluko", "Rum+Raisin", "Ruslan+Display", "Russo+One", "Ruthie", "Rye", "Sacramento", "Sail", "Salsa", "Sanchez", "Sanchez:italic", "Sancreek", "Sansita+One", "Sarina", "Satisfy", "Scada", "Scada:italic", "Scada:700", "Scada:700italic", "Schoolbell", "Seaweed+Script", "Sevillana", "Seymour+One", "Shadows+Into+Light", "Shadows+Into+Light+Two", "Shanti", "Share", "Share:italic", "Share:700", "Share:700italic", "Share+Tech", "Share+Tech+Mono", "Shojumaru", "Short+Stack", "Siemreap", "Sigmar+One", "Signika", "Signika:300", "Signika:600", "Signika:700", "Signika+Negative", "Signika+Negative:300", "Signika+Negative:600", "Signika+Negative:700", "Simonetta", "Simonetta:italic", "Simonetta:900", "Simonetta:900italic", "Sintony", "Sintony:700", "Sirin+Stencil", "Six+Caps", "Skranji", "Skranji:700", "Slackey", "Smokum", "Smythe", "Sniglet", "Snippet", "Snowburst+One", "Sofadi+One", "Sofia", "Sonsie+One", "Sorts+Mill+Goudy", "Sorts+Mill+Goudy:italic", "Source+Code+Pro", "Source+Code+Pro:200", "Source+Code+Pro:300", "Source+Code+Pro:500", "Source+Code+Pro:600", "Source+Code+Pro:700", "Source+Code+Pro:900", "Source+Sans+Pro", "Source+Sans+Pro:200", "Source+Sans+Pro:200italic", "Source+Sans+Pro:300", "Source+Sans+Pro:300italic", "Source+Sans+Pro:italic", "Source+Sans+Pro:600", "Source+Sans+Pro:600italic", "Source+Sans+Pro:700", "Source+Sans+Pro:700italic", "Source+Sans+Pro:900", "Source+Sans+Pro:900italic", "Special+Elite", "Spicy+Rice", "Spinnaker", "Spirax", "Squada+One", "Stalemate", "Stalinist+One", "Stardos+Stencil", "Stardos+Stencil:700", "Stint+Ultra+Condensed", "Stint+Ultra+Expanded", "Stoke", "Stoke:300", "Strait", "Sue+Ellen+Francisco", "Sunshiney", "Supermercado+One", "Suwannaphum", "Swanky+and+Moo+Moo", "Syncopate", "Syncopate:700", "Tangerine", "Tangerine:700", "Taprom", "Tauri", "Telex", "Tenor+Sans", "Text+Me+One", "The+Girl+Next+Door", "Tienne", "Tienne:700", "Tienne:900", "Tinos", "Tinos:italic", "Tinos:700", "Tinos:700italic", "Titan+One", "Titillium+Web", "Titillium+Web:200", "Titillium+Web:200italic", "Titillium+Web:300", "Titillium+Web:300italic", "Titillium+Web:italic", "Titillium+Web:600", "Titillium+Web:600italic", "Titillium+Web:700", "Titillium+Web:700italic", "Titillium+Web:900", "Trade+Winds", "Trocchi", "Trochut", "Trochut:italic", "Trochut:700", "Trykker", "Tulpen+One", "Ubuntu", "Ubuntu:300", "Ubuntu:300italic", "Ubuntu:italic", "Ubuntu:500", "Ubuntu:500italic", "Ubuntu:700", "Ubuntu:700italic", "Ubuntu+Condensed", "Ubuntu+Mono", "Ubuntu+Mono:italic", "Ubuntu+Mono:700", "Ubuntu+Mono:700italic", "Ultra", "Uncial+Antiqua", "Underdog", "Unica+One", "UnifrakturCook", "UnifrakturMaguntia", "Unkempt", "Unkempt:700", "Unlock", "Unna", "VT323", "Vampiro+One", "Varela", "Varela+Round", "Vast+Shadow", "Vibur", "Vidaloka", "Viga", "Voces", "Volkhov", "Volkhov:italic", "Volkhov:700", "Volkhov:700italic", "Vollkorn", "Vollkorn:italic", "Vollkorn:700", "Vollkorn:700italic", "Voltaire", "Waiting+for+the+Sunrise", "Wallpoet", "Walter+Turncoat", "Warnes", "Wellfleet", "Wendy+One", "Wire+One", "Yanone+Kaffeesatz", "Yanone+Kaffeesatz:200", "Yanone+Kaffeesatz:300", "Yanone+Kaffeesatz:700", "Yellowtail", "Yeseva+One", "Yesteryear", "Zeyada"];

		var settings = {
			style      : 'font-select',
			placeholder: 'Select a font',
			lookahead  : 10,
			api        : 'http://fonts.googleapis.com/css?family='
		};

		var Fontselect = (function () {

			function Fontselect(original, o, args) {

				this.$original = $(original);

				if (o && typeof(o) == 'string') {

					this.options = this.$original.data('options');

					if (this.options) {

						this.$element = this.$original.parent().find('.' + this.options.style);

						if (this.$element.size() > 0) {

							if (this[o] && this[o] instanceof Function) {
								debugger;
								this[o].apply(this, args);
							}
						}
					}

					return;

				}

				this.active = false;
				this.options = o;

				this.$original.data('options', this.options);

				this.setupHtml();
				this.getVisibleFonts();
				this.bindEvents();

				var font = this.$original.val();

				if (font) {
					this.updateSelected();
					this.addFontLink(font);
				}
			}

			Fontselect.prototype.bindEvents = function () {

				$('li', this.$results)
						.click(__bind(this.selectFont, this))
						.mouseenter(__bind(this.activateFont, this))
						.mouseleave(__bind(this.deactivateFont, this));

				$('span', this.$select).click(__bind(this.toggleDrop, this));
				this.$arrow.click(__bind(this.toggleDrop, this));
			};

			Fontselect.prototype.toggleDrop = function (ev) {

				if (this.active) {
					this.$element.removeClass('font-select-active');
					this.$drop.hide();
					clearInterval(this.visibleInterval);

				} else {
					this.$element.addClass('font-select-active');
					this.$drop.show();
					this.moveToSelected();
					this.visibleInterval = setInterval(__bind(this.getVisibleFonts, this), 500);
				}

				this.active = !this.active;
			};

			Fontselect.prototype.selectFont = function () {
				var font = $('li.active', this.$results).data('value');
				this.$original.val(font).change();
				this.updateSelected();
				this.toggleDrop();
			};

			Fontselect.prototype.moveToSelected = function () {

				var $li, font = this.$original.val();

				if (font) {
					$li = $("li[data-value='" + font + "']", this.$results);
				} else {
					$li = $("li", this.$results).first();
				}

				this.$results.scrollTop(0);
				this.$results.scrollTop($li.addClass('active').position().top);
			};

			Fontselect.prototype.activateFont = function (ev) {
				$('li.active', this.$results).removeClass('active');
				$(ev.currentTarget).addClass('active');
			};

			Fontselect.prototype.deactivateFont = function (ev) {

				$(ev.currentTarget).removeClass('active');
			};

			Fontselect.prototype.updateSelected = function () {

				var font = this.$original.val();
				$('span', this.$element).text(this.toReadable(font)).css(this.toStyle(font));
			};

			Fontselect.prototype.setupHtml = function () {

				this.$original.empty().hide();
				this.$element = $('<div>', {'class': this.options.style});
				this.$arrow = $('<div><b></b></div>');
				this.$select = $('<a><span>' + this.options.placeholder + '</span></a>');
				this.$drop = $('<div>', {'class': 'fs-drop'});
				this.$results = $('<ul>', {'class': 'fs-results'});
				this.$original.after(this.$element.append(this.$select.append(this.$arrow)).append(this.$drop));
				this.$drop.append(this.$results.append(this.fontsAsHtml())).hide();
			};

			Fontselect.prototype.fontsAsHtml = function () {

				var l = fonts.length;
				var r, s, h = '';

				for (var i = 0; i < l; i++) {
					r = this.toReadable(fonts[i]);
					s = this.toStyle(fonts[i]);
					h += '<li data-value="' + fonts[i] + '" style="font-family: ' + s['font-family'] + '; font-weight: ' + s['font-weight'] + '">' + r + '</li>';
				}

				return h;
			};

			Fontselect.prototype.toReadable = function (font) {
				return font.replace(/[\+|:]/g, ' ');
			};

			Fontselect.prototype.toStyle = function (font) {
				var t = font.split(':');
				return {'font-family': this.toReadable(t[0]), 'font-weight': (t[1] || 400)};
			};

			Fontselect.prototype.getVisibleFonts = function () {

				if (this.$results.is(':hidden')) return;

				var fs = this;
				var top = this.$results.scrollTop();
				var bottom = top + this.$results.height();

				if (this.options.lookahead) {
					var li = $('li', this.$results).first().height();
					bottom += li * this.options.lookahead;
				}

				$('li', this.$results).each(function () {

					var ft = $(this).position().top + top;
					var fb = ft + $(this).height();

					if ((fb >= top) && (ft <= bottom)) {
						var font = $(this).data('value');
						fs.addFontLink(font);
					}

				});
			};

			Fontselect.prototype.addFontLink = function (font) {

				var link = this.options.api + font + "&text=" + font.replace(/[ ]/g, '');

				if ($("link[href*='" + font + "']").length === 0) {
					$('link:last').after('<link href="' + link + '" rel="stylesheet" type="text/css">');
				}
			};

			return Fontselect;
		})();

		return this.each(function () {

			if (options && typeof(options) == 'string') {
				options = options;
			}
			else if (options) {
				options = $.extend({}, settings, options);
			}
			else {
				options = settings;
			}

			return new Fontselect(this, options, args);
		});

	};
})(jQuery);