(function ($) {

	"use strict";

	plex.type.ShortCodeFrame = plex.type.GalleryFrame.extend({});

	plex.type.shortCodeLiveBuilder.decorateFrame(
			plex.type.ShortCodeFrame
	);

	$(document).on('click', '.js-insert-short-code', function () {

		var $this = $(this),
				$context = $this.parents('.-context');

		var frame = new plex.type.ShortCodeFrame({
			state: 'short-codes-list'
		});


		frame.state('short-code-edit').on("update", function (library) {
			var shortCode = plex.convertModelToShortCode(library),
					val = $('.js-short-code-textarea', $context).val();

			$('.js-short-code-textarea', $context).val(
					val + "\n\n" + shortCode.string()
			);
		});

		frame.state('gallery-edit').on("update", function (selection) {
			var shortCode = wp.media.gallery.shortcode(selection),
					val = $('.js-short-code-textarea', $context).val();

			$('.js-short-code-textarea', $context).val(
					val + "\n\n" + shortCode.string()
			);

		});

		frame.open();

	});


})(jQuery);


