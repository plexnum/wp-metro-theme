(function ($) {

	"use strict";

	wp.customize.Element.synchronizer.val = {
		update : function (to) {

			if (this.element.is('.-color-scheme') && to === 'custom') {
				var $context = this.element.parents('.-cust-block');
				$context.find('.-selected').removeClass('-selected');
				$context.find('.custom ').addClass('-selected');
			}

			if (this.element.is('.wp-color-picker')) {
				// Synchronize color picker
				if (this.element.wpColorPicker('color') != to) {
					this.element.wpColorPicker('color', to);
				}

			}
			else {
				this.element['val'](to);
			}

			// Synchronize font selector
			if (this.element.is('.-font-selector')) {
				this.element.fontselect('updateSelected');
			}


		},
		refresh: function () {
			return this.element['val']();
		}
	};


	$(function () {
		$('.-font-selector').fontselect();
	});

})(jQuery);