(function ($) {

	"use strict";

	var AbstractResolver = wp.customize.Class.extend({
		resolve: function (value, settings) {
			throw "Method should be implemented";
		}
	});

	var MakeHoverDurkerResolver = AbstractResolver.extend({

		resolve: function (value, key, settings) {

			var rgbResult = rgbDifference(
					convertToRGB(value.replace(/^#/, '')),
					convertToRGB('111111')
			);

			settings[key + '__hover'] = 'rgb(' + rgbResult.r + ',' + rgbResult.g + ',' + rgbResult.b + ')';

			return value;

		}

	});


	var SubLayerOpacityResolver = AbstractResolver.extend({

		resolve: function (value, key, settings) {

			settings['plex_site_sublayer_bgc_op__ie'] = 'filter: alpha(opacity=' + value + ')';
			settings['plex_site_sublayer_bgc_op__ie1'] = '"progid:DXImageTransform.Microsoft.Alpha(Opacity=' + value + ')"';
			settings['plex_site_sublayer_bgc_op__ie2'] = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + value + ')';

			return value / 100;
		}

	});

	var PixelResolver = AbstractResolver.extend({

		resolve: function (value, key, settings) {
			return value + 'px';
		}

	});

	var convertToRGB = function (value) {

		return {
			'r': parseInt(value.slice(0, 2), 16),
			'g': parseInt(value.slice(2, 4), 16),
			'b': parseInt(value.slice(4, 6), 16)
		};

	}

	var rgbDifference = function (rgb1, rgb2) {

		return {
			r: (rgb1.r > rgb2.r) ? rgb1.r - rgb2.r : 0,
			g: (rgb1.g > rgb2.g) ? rgb1.g - rgb2.g : 0,
			b: (rgb1.b > rgb2.b) ? rgb1.b - rgb2.b : 0
		};

	}


	var SiteBackgroundResolver = AbstractResolver.extend({

		resolve: function (value, key, settings) {

			var val = value.replace(/^#/, ''),
					rgb = convertToRGB(val);

			settings[key + '__rad'] = 'radial-gradient(circle closest-corner, rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', 0.4), #' + val + ')';
			settings[key + '__webkit' ] = '-webkit-' + settings[key + '__rad'];
			settings[key + '__moz'] = '-moz-' + settings[key + '__rad'];
			settings[key + '__ms'] = '-ms-' + settings[key + '__rad'];
			settings[key + '__o'] = '-o-' + settings[key + '__rad'];

			return value;

		}

	});


	var FontResolver = AbstractResolver.extend({

		resolve: function (font, key, settings) {

			this.createFontLink(font);

			var newFont = font.replace(/\+/g, ' '),
					reg = /(?:(.*?):(\d*)([^"]*))|(.*)/,
					res = newFont.match(reg),
					result = null;

			if (res) {
				// if just a font
				if (res[4]) {
					result = res[4];
				}
				else {
					result = res[1];

					if (res[2]) {
						settings[key + '_' + 'weight'] = res[2];
					}

					if (res[3]) {
						settings[key + '_' + 'style' ] = '"' + res[3] + '"';
					}

				}
			}

			return '"' + result + '"';

		},

		createFontLink: function (font) {

			var fontId = this.fontToId(font);

			if ($('head #' + fontId).size() == 0) {
				$('<link id=' + fontId + ' href="http://fonts.googleapis.com/css?family=' + font + '" rel="stylesheet" type="text/css">').appendTo('head');
			}

		},

		fontToId: function (font) {
			return 'plex' + font.replace(/[^\w]/g, '_');
		}

	});

	var StyleResolver = {

		resolvers: {},

		register: function (settingName, resolver) {

			if (resolver instanceof AbstractResolver) {
				this.resolvers[settingName] = resolver;
			} else {
				throw "Registered style resolver should have AbstractResolver type";
			}

		},

		resolve: function (styles) {

			_.each(this.resolvers, function (resolver, settingName) {

				if (styles[settingName]) {

					styles[settingName] = resolver.resolve(
							styles[settingName],
							settingName,
							styles
					);

				}

			}, this);

			return styles;

		}

	}


	// Register style resolvers
	StyleResolver.register('plex_header_font', new FontResolver());
	StyleResolver.register('plex_text_font', new FontResolver());
	StyleResolver.register('plex_text_font_size', new PixelResolver());
	StyleResolver.register('plex_h1_font_size', new PixelResolver());
	StyleResolver.register('plex_h2_font_size', new PixelResolver());
	StyleResolver.register('plex_h3_font_size', new PixelResolver());
	StyleResolver.register('plex_h4_font_size', new PixelResolver());
	StyleResolver.register('plex_h5_font_size', new PixelResolver());
	StyleResolver.register('plex_h6_font_size', new PixelResolver());
	//StyleResolver.register('plex_site_bgc', new SiteBackgroundResolver());
	StyleResolver.register('plex_site_sublayer_bgc_op', new SubLayerOpacityResolver());
	StyleResolver.register('plex_btn_type1_bgc', new MakeHoverDurkerResolver());
	StyleResolver.register('plex_btn_type2_bgc', new MakeHoverDurkerResolver());
	StyleResolver.register('plex_accordion_bgc', new MakeHoverDurkerResolver());
	StyleResolver.register('plex_link_c', new MakeHoverDurkerResolver());
	StyleResolver.register('plex_link_c_in_content', new MakeHoverDurkerResolver());

	var StyleChanger = {

		changeToCustom: true,

		change: function (newStyles) {

			var customValue = parent.wp.customize.value('plex_color_scheme');

			if (this.changeToCustom == true && customValue.get() != 'custom') {
				customValue.set('custom');
			}

			var newStyles = StyleResolver.resolve(newStyles);

			var tpl = _.template(
							$(parent.document).find('#plexStyleSettings').html()
					),
					setting = $.extend(
							{},
							parent.plexDefaultStyleSettings,
							newStyles
					),
					styleContainer = $('head #plexCssStyleSettings');

			// Create style container if does not existed yet
			if (styleContainer.size() == 0) {

				styleContainer = $('<style type="text/css" id="plexCssStyleSettings"></style>');

				$('#plex_style-css').before(styleContainer);

			}

			styleContainer.html(tpl(setting));

		}

	}

	wp.customize('background_image', function (value) {
		value.bind(function (image) {

			$('.fake-bg').css({
				'background-image': 'url(' + image + ')'
			});

		});
	});


	// Change logo image
	wp.customize('plex_logo_image', function (value) {
		value.bind(function (newValue) {
			$('.logo-main img').attr('src', newValue);
		});
	});

	wp.customize('blogname', function (value) {
		value.bind(function (newValue) {
			$('.-blog-name').html(newValue);
		});

	});

	wp.customize('blogdescription', function (value) {
		value.bind(function (newValue) {
			$('.-blog-desc').html(newValue);
		});
	});


	wp.customize('plex_copyright', function (value) {
		value.bind(function (newValue) {
			$('.-blog-copyrights').html(newValue);
		});
	});


	wp.customize('plex_header_font', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_h1_font_size', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_h2_font_size', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_h3_font_size', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_h4_font_size', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_h5_font_size', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_h6_font_size', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_site_title_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_text_font', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_text_font_size', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_main_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_site_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});


	wp.customize('plex_site_sublayer_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get());
		});

	});

	wp.customize('plex_site_sublayer_bgc_op', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_btn_type1_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_btn_type1_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_btn_type2_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_btn_type2_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_btn_op_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_btn_op_bgc_hover', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_link_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_link_c_in_content', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_twitter_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_twitter_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_menu_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_menu_bgc_hover', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_menu_btn_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_tile_type1_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_tile_type1_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_tile_type2_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_tile_type2_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_tile_icon_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_media_element_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_testimonial_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_testimonial_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_testimonial_quotes_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_blockquote_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_blockquote_quotes_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_blockquote_author_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_blockquote_icon_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_pagination_c_hover', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_pagination_bgc_hover', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_accordion_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_accordion_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_tab_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_tab_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_calendar_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_calendar_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_calendar_today_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_calendar_today_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_progress_bar_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_progress_bar_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_selector_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_selector_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_blog_header_date_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_blog_header_date_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_sticky_post_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_sticky_post_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_sticky_post_date_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_sticky_post_date_c', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_bypostauthor_comment_bgc', function (value) {

		value.bind(function () {
			StyleChanger.change(wp.customize.get())
		});

	});

	wp.customize('plex_color_scheme', function (value) {

		value.bind(function (newScheme) {

			var schemes = parent.plexColorSchemesSettings;

			if (schemes && schemes[newScheme]) {

				var customize = parent.wp.customize;
				var values = customize._value;

				_.each(schemes[newScheme]['settings'], function (item, key) {

					StyleChanger.changeToCustom = false;

					if (values[key]) {
						values[key].set(item);
						values[key].sync();
					}

				});

				setTimeout(function () {
					StyleChanger.changeToCustom = true;
				}, 0);

			}

		});

	});

})(jQuery);