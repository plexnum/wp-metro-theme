(function ($) {

	"use strict";

	/**
	 * ========================================================================
	 * Init namespaces
	 * ========================================================================
	 */

	if (!plex.model)                     plex.model = {};
	if (!plex.collection)                plex.collection = {};
	if (!plex.view)                      plex.view = {};
	if (!plex.type.shortCodeLiveBuilder) plex.type.shortCodeLiveBuilder = {};


	plex.local = {

		data: window.localization || {},

		'get': function (key) {
			if (this.data[key]) {
				return this.data[key];
			}
			else {
				return null;
			}
		}

	};

	var media = wp.media;

	/**
	 * ========================================================================
	 * Helper functions
	 * ========================================================================
	 */

	plex.convertModelToShortCode = function (model, contentAttr) {

		var attrs = model.settings.toJSON(),
				contentAttr = contentAttr || 'content',
				key = null;

		for (key in attrs) {
			if (_.isString(attrs[key]) && !attrs[key]) {
				delete attrs[key];
			}

			if (key.charAt(0) == '_') {
				delete attrs[key];
			}

		}

		var options = {
			tag  : model.get('id'),
			attrs: attrs,
			type : 'single'
		}


		if (attrs['content'] || attrs['content_preview']) {

			options['content'] = attrs[contentAttr];

			// Delete content attributes from options
			if (options.attrs['content_preview']) {
				delete options.attrs['content_preview'];
			}

			if (options.attrs['content']) {
				delete options.attrs['content'];
			}

			delete options['type'];

		}

		return new wp.shortcode(options);

	}

	plex.doShortCodeAjax = function (options) {
		options = options || {};
		options.context = this;
		options.data = _.extend(options.data || {}, {
			action: 'plex_do_short_code'
		});

		return media.ajax(options);
	}


	/**
	 * ========================================================================
	 * Models and collections
	 * ========================================================================
	 */

	plex.model.ShortCode = Backbone.Model.extend({
		constructor: function () {
			this.settings = new Backbone.Model();
			Backbone.Model.prototype.constructor.apply(this, arguments);
		},

		parse: function (response) {
			if (response.settings) {
				this.settings = new Backbone.Model(response.settings);
				delete response.settings;
			}

			return response;

		}


	});

	plex.collection.ShortCodes = Backbone.Collection.extend({

		model: plex.model.ShortCode,

		sync: function (method, collection, options) {

			if ('read' === method) {

				options = options || {};
				options.context = this;
				options.data = _.extend(options.data || {}, {
					action: 'plex_get_short_codes'
				});

				return media.ajax(options);
			}

		}


	});

	/**
	 * ========================================================================
	 * Views
	 * ========================================================================
	 */

	plex.view.ShortCodeListItem = media.View.extend({

		template: _.template($('#plexShortCodeListItemTpl').html()),

		prepare: function () {
			return this.model.toJSON();
		},

		events: {
			'click .-item': 'clickAction'
		},

		clickAction: function () {
			if (this.options.clickAction) {
				this.options.clickAction.call(this);
			}
			return false;
		}

	});

	plex.view.ShortCodesList = media.View.extend({

		template: _.template($('#plexShortCodesListTpl').html()),

		initialize: function () {
			this.collection.on('reset', this.renderItems, this);
			this.collection.fetch({
				reset: true,
				parse: true
			});
		},

		renderItem: function (model) {
			this.views.add('.-short-codes', new plex.view.ShortCodeListItem({
				model      : model,
				clickAction: this.options.clickAction
			}));
		},

		renderItems: function (models) {

			models.each(function (model) {
				this.renderItem(model);
			}, this);
		}

	});

	plex.view.ShortCodeEdit = media.View.extend({
		template: _.template($('#plexShortCodeEditTpl').html())
	});

	plex.view.ShortCodeSettings = media.view.Settings.extend({

		events: {
			'click  button[data-setting]'   : 'updateHandler',
			'change input[data-setting]'    : 'updateHandler',
			'change select[data-setting]'   : 'updateHandler',
			'change	textarea[data-setting]': 'updateHandler'
		},

		className: 'gallery-settings',

		template: null,

		updateHandler: function (e) {
			media.view.Settings.prototype.updateHandler.apply(this, arguments);
		},

		initialize: function () {
			this.template = this.options.template;

			this.model.on('change', this.refreshFrame, this);

			media.view.Settings.prototype.initialize.apply(this, arguments);
		},

		refreshFrame: function () {

			var changedAttrs = this.model.changedAttributes();

			if (changedAttrs['content']) {
				return false;
			}

			var state = this.controller.state(),
					library = state.get('library'),
					shortcode = plex.convertModelToShortCode(library, 'content_preview');

			var promise = plex.doShortCodeAjax({
				data: {
					shortcode    : shortcode.string(),
					shortcode_tag: library.get('id')
				}
			});

			state.set('frameContentPromise', promise);

			var frameView = this.options.editStateContentView.view.views.get()[0];
			frameView.render();
		}

	});


	plex.view.iframeWithContent = media.View.extend({

		className: '-iframe-short-code-preview',

		render: function () {
			this.views.detach();

			var that = this;

			var iframe = $('<iframe/>');

			this.$el.html(iframe);

			that.controller.state().get('frameContentPromise').done(_.bind(function (response) {

				var targetWindow = iframe[0].contentWindow;

				if (targetWindow != null) {
					targetWindow.document.open();
					targetWindow.document.write(response);
					targetWindow.document.close();
				}

			}, this));


			this.views.render();
			return this;
		}
	});


	/**
	 * ========================================================================
	 * States
	 * ========================================================================
	 */

	plex.model.ShortCodesListState = media.controller.State.extend({

		defaults: {
			id: 'short-codes-list'
		},

		initialize: function () {

			if (!this.get('shortCodes')) {
				var shortCodes = new plex.collection.ShortCodes();
				this.set('shortCodes', shortCodes);
			}

		}

	});

	plex.model.ShortCodeEditState = media.controller.State.extend({

		defaults: {
			id: 'short-code-edit'
		},

		initialize: function () {
			if (!this.get('library')) {
				this.set('library', new Backbone.Collection());
			}
		}

	});


	/**
	 * ========================================================================
	 * Frames
	 * ========================================================================
	 */

	plex.type.shortCodeLiveBuilder.decorateFrame = function (frame) {

		var prototype = _.clone(frame.prototype);

		frame.prototype = _.extend(
				frame.prototype,
				{
					'initialize': function () {
						prototype.initialize.apply(this, arguments);

						var editState = this.state('short-code-edit');


						editState.on('settings.view.created.tabs', function (settingView, model) {

							settingView.views.set('.tabs-place-holder', new plex.type.TabsView({
								model     : model.settings,
								collection: new plex.type.TabsCollection()
							}));

						});

						editState.on('settings.view.created.accordion', function (settingView, model) {

							settingView.views.set('.collapses-place-holder', new plex.type.CollapsesView({
								model     : model.settings,
								collection: new plex.type.CollapsesCollection()
							}));

						});

						editState.on('settings.view.created.marked_list', function (settingView, model) {

							settingView.views.set('.marked-list-place-holder', new plex.type.MarkedListView({
								model     : model.settings,
								collection: new plex.type.MarkedListCollection()
							}));

						});

						editState.on('settings.view.created.row', function (settingView, model) {

							settingView.views.set('.grid-layout-place-holder', new plex.type.GridLayoutView({
								model: model.settings
							}));

						});

					},

					'bindHandlers': function () {

						prototype.bindHandlers.apply(this, arguments);
						this.on('menu:create:shortCodeEdit', this.createMenu, this);
						this.on('menu:render:default', this.mainMenu, this);
						this.on('menu:render:shortCodeEdit', this.shortCodeEditMenu, this);
						this.on('content:create:shortCodes', this.shortCodes, this);
						this.on('content:create:shortCodeEdit', this.shortCodeEdit, this);
						this.on('content:render:shortCodeEdit', this.shortCodeEditRender, this);
						this.on('toolbar:render:shortCodeEdit', this.shortCodeEditToolbar, this);
						this.on('toolbar:create:shortCodes', this.shortCodesToolbar, this);
					},

					'createStates': function () {
						prototype.createStates.apply(this, arguments);

						this.states.add(new plex.model.ShortCodesListState({
							title  : plex.local.get('short_code_live_editor'),
							menu   : 'default',
							content: 'shortCodes',
							toolbar: 'shortCodes'
						}));

						this.states.add(new plex.model.ShortCodeEditState({
							title  : plex.local.get('short_code_edit'),
							menu   : 'shortCodeEdit',
							content: 'shortCodeEdit',
							sidebar: 'settings',
							toolbar: 'shortCodeEdit'
						}));

					},

					shortCodesToolbar: function (content) {
						content.view = new media.View();
					},

					shortCodeEditToolbar: function () {
						this.toolbar.set(new media.view.Toolbar({
							controller: this,
							items     : {
								insert: {
									style   : 'primary',
									text    : plex.local.get('insert'),
									priority: 80,
									click   : function () {
										var controller = this.controller,
												state = controller.state();

										controller.close();
										state.trigger('update', state.get('library'));

										// Restore and reset the default state.
										controller.setState(controller.options.state);
										controller.reset();
									}
								}
							}
						}))
					},

					shortCodeEditMenu: function (view) {
						var lastState = this.lastState(),
								previous = lastState && lastState.id,
								frame = this;

						view.set({
							cancel        : {
								text    : plex.local.get('cancel_editing'),
								priority: 20,
								click   : function () {
									if (previous)
										frame.setState(previous);
									else
										frame.close();
								}
							},
							separateCancel: new media.View({
								className: 'separator',
								priority : 40
							})
						});

					},

					shortCodeEdit: function (content) {

						var state = this.state(),
								library = state.get('library');

						content.view = new plex.view.ShortCodeEdit({
							controller: this
						});

						content.view.views.add(new plex.view.iframeWithContent({
							controller: this
						}));

						library.settings = library.settings || new Backbone.Model();

						if ($('#plex_' + library.get('id')).size() > 0) {

							var sidebar = new media.view.Sidebar({
								controller: this
							});

							var settingsView = new plex.view.ShortCodeSettings({
								template            : _.template($('#plex_' + library.get('id')).html()),
								controller          : this,
								model               : library.settings,
								priority            : 40,
								editStateContentView: content
							});

							sidebar.set({
								settings: settingsView
							});

							content.view.views.add(sidebar);

							state.trigger('settings.view.created', settingsView, library);
							state.trigger('settings.view.created.' + library.get('id'), settingsView, library);

						}


					},

					shortCodes: function (content) {

						var state = this.state();
						var that = this;

						content.view = new plex.view.ShortCodesList({
							controller : this,
							collection : state.get('shortCodes'),
							clickAction: function () {

								// RESOLVE STATE
								var state = that.state('short-code-edit');

								state.set('library', this.model);

								if (this.model.get('id') == 'gallery') {
									that.setState('gallery');
									return false;
								}

								if (!this.model.get('is_customizable')) {
									that.close();
									state.trigger('update', state.get('library'));
									// Restore and reset the default state.
									that.setState(that.options.state);
									that.reset();

									return false;
								}

								var shortcode = plex.convertModelToShortCode(this.model, 'content_preview');

								var promise = plex.doShortCodeAjax({
									data: {
										shortcode    : shortcode.string(),
										shortcode_tag: this.model.get('id')
									}
								});

								state.set('frameContentPromise', promise);

								// SET NEW STATE
								that.setState(state);
							}
						});

					}


				}
		);

	};

	plex.type.shortCodeLiveBuilder.decorateFrame(
			media.view.MediaFrame.Post
	);

	/**
	 * ========================================================================
	 * ========================================================================
	 * Customize settings
	 * ========================================================================
	 * ========================================================================
	 */


	plex.instance.ShortCodeFactory = {

		create: function (attrs) {
			return new wp.shortcode(attrs);
		}
	}

	plex.type.ShortCodeBuilder = Backbone.Model.extend({

		constructor: function () {
			this.shortcodes = [];
			Backbone.Model.prototype.constructor.apply(this, arguments);
		},

		addShortCode: function (attrs) {
			this.shortcodes.push(plex.instance.ShortCodeFactory.create(attrs));
		},

		stringify: function () {

			return _.reduce(this.shortcodes, function (memo, code) {

				return memo + code.string() + '<br/>';

			}, '<br/>');
		}

	});


	/**
	 * ========================================================================
	 * MODELS
	 * ========================================================================
	 */

	plex.type.TabModel = Backbone.Model.extend({

		deactivate: function () {
			this.set('active', false);
		},

		parse: function (model) {

			if (!model.active) {
				model.active = false;
			}

			if (!model.title) {
				model.title = "New item";
			}

			return model;

		}

	});

	plex.type.CollapseModel = plex.type.TabModel.extend({});

	plex.type.TabsCollection = Backbone.Collection.extend({
		model: plex.type.TabModel
	});

	plex.type.CollapsesCollection = Backbone.Collection.extend({
		model: plex.type.CollapseModel
	});

	plex.type.MarkedListCollection = Backbone.Collection.extend({});


	/**
	 * ========================================================================
	 * Views
	 * ========================================================================
	 */


	plex.type.GridLayoutItemView = media.View.extend({

		template: _.template($('#plexGridLayoutItem').html()),

		events: {
			'click': 'activate'
		},

		activate: function () {
			var layouts = this.model.get('layout');

			var contentShortCodeBuilder = new plex.type.ShortCodeBuilder(),
					previewShortCodeBuilder = new plex.type.ShortCodeBuilder();

			_.each(layouts, function (size) {

				contentShortCodeBuilder.addShortCode({
					tag    : 'column',
					attrs  : {
						size: size
					},
					content: this.controller.get('_content')
				});

				previewShortCodeBuilder.addShortCode({
					tag    : 'column',
					attrs  : {
						size: size
					},
					content: this.controller.get('_preview_content')
				});

			}, this);

			this.controller.set('content_preview', previewShortCodeBuilder.stringify());
			this.controller.set('content', contentShortCodeBuilder.stringify());

		},

		prepare: function () {
			return this.model.toJSON();
		}

	});


	plex.type.GridLayoutView = media.View.extend({
		template: _.template($('#plexGridLayout').html()),


		initialize: function () {
			this.collection = new Backbone.Collection();
			this.collection.on('reset', this.addAll, this);
			this.collection.reset(this.model.get('_layouts'));
		},

		addOne: function (model) {
			this.views.add('.-grid-layouts', new plex.type.GridLayoutItemView({
				model     : model,
				controller: this.model
			}));
		},

		addAll: function (collection) {
			collection.each(function (model) {
				this.addOne(model);
			}, this);
		},


	});


	plex.type.AddFormView = media.View.extend({

		template: _.template($('#plexAddFormView').html()),

		events: {
			'click .js-add': 'addItem'
		},

		addItem: function () {

			var data = this.$el.serializeForm();

			this.collection.add(data, {
				parse: true
			});

			this.emptyFields();

		},

		emptyFields: function () {
			this.$el.find('input').val('');
		}

	});

	plex.type.AddFormWithIconView = plex.type.AddFormView.extend({

		template: _.template($('#plexAddFormWithIconView').html()),

		emptyFields: function () {
			this.$el.find('.-title').val('');
		}

	});


	plex.type.AbstractRadioInputView = media.View.extend({

		template: _.template($('#plexRadioInput').html()),

		events: {
			'click .js-del'   : 'removeItem',
			'click .js-active': 'active',
			'change .-input'  : 'changeInput'
		},

		initialize: function () {
			this.model.on('change:active', this.render, this);
		},

		removeItem: function () {
			this.model.collection.remove(this.model);
			this.remove();
		},

		changeInput: function () {
			// changeInput should be implemented
		},

		prepare: function () {
			// prepare should be implemented'
		},

		active: function () {
			this.model.collection.invoke('deactivate');
			this.model.set('active', true);
		}

	});

	plex.type.TabView = plex.type.AbstractRadioInputView.extend({

		changeInput: function (e) {
			var $input = $(e.target);
			this.model.set('title', $input.val())
		},

		prepare: function () {
			return _.extend({
				input_name : 'title',
				input_value: this.model.get('title'),
				active     : this.model.get('active')
			});
		}

	});

	plex.type.CollapseView = plex.type.TabView.extend({});


	plex.type.AbstractRadioList = media.View.extend({

		template: _.template($('#plexRadioListTemplate').html()),

		initialize: function () {

			this.items = this.collection;

			this.views.set('.add-form-view-place-holder', new plex.type.AddFormView({
				collection: this.items
			}));

			this.items.on('add', this.addItem, this);
			this.items.on('add', this.refreshFrame, this);
			this.items.on('reset', this.addAll, this);
			this.items.on('remove', this.refreshActive, this);
			this.items.on('remove', this.refreshFrame, this);
			this.items.on('change', this.refreshFrame, this);
			this.items.on('reset', this.refreshFrame, this);

			this.items.reset(this.model.get('_items'));
		},

		refreshFrame: function () {

			var contentShortCodeBuilder = new plex.type.ShortCodeBuilder(),
					previewShortCodeBuilder = new plex.type.ShortCodeBuilder();

			this.items.each(function (item) {

				this.addShortCodes(
						item,
						contentShortCodeBuilder,
						previewShortCodeBuilder
				);

			}, this);

			this.model.set('content_preview', previewShortCodeBuilder.stringify());
			this.model.set('content', contentShortCodeBuilder.stringify());

		},

		refreshActive: function () {

			var refresh = this.items.reduce(function (memo, model) {

				if (memo == false) {
					return memo;
				}

				if (model.get('active') == true) {
					return false;
				}

				return true;

			}, true);


			if (this.items.size() > 0 && refresh == true) {
				this.items.at(0).set('active', true);
			}

		},

		addAll: function (collection) {

			collection.each(function (model) {
				this.addItem(model);
			}, this);

		},

		addItem: function (model) {

			if (this.items.size() == 1) {
				model.set('active', true);
			}

			// addItem should be implemented

		},

		addShortCodes: function (contentShortCodeBuilder, previewShortCodeBuilder) {
			// addShortCodes should be implemented
		}


	});

	plex.type.TabsView = plex.type.AbstractRadioList.extend({

		addItem: function (model) {

			plex.type.AbstractRadioList.prototype.addItem.call(this, model);
			this.views.add('.-radio-list', new plex.type.TabView({
				model: model
			}));

		},

		addShortCodes: function (item, contentShortCodeBuilder, previewShortCodeBuilder) {

			contentShortCodeBuilder.addShortCode({
				tag    : 'tab',
				attrs  : item.toJSON(),
				content: this.model.get('_content')
			});

			previewShortCodeBuilder.addShortCode({
				tag    : 'tab',
				attrs  : item.toJSON(),
				content: this.model.get('_preview_content')
			});

		}

	});

	plex.type.CollapsesView = plex.type.AbstractRadioList.extend({

		addItem: function (model) {
			plex.type.AbstractRadioList.prototype.addItem.call(this, model);

			this.views.add('.-radio-list', new plex.type.CollapseView({
				model: model
			}));

		},

		addShortCodes: function (item, contentShortCodeBuilder, previewShortCodeBuilder) {

			contentShortCodeBuilder.addShortCode({
				tag    : 'collapse',
				attrs  : item.toJSON(),
				content: this.model.get('_content')
			});

			previewShortCodeBuilder.addShortCode({
				tag    : 'collapse',
				attrs  : item.toJSON(),
				content: this.model.get('_preview_content')
			});

		}
	});

	plex.type.IconItemView = media.View.extend({

		template: _.template($('#plexIconInputView').html()),

		events: {
			'click .js-del' : 'removeItem',
			'change .-input': 'changeInput'
		},

		removeItem: function () {
			this.model.collection.remove(this.model);
			this.remove();
		},

		changeInput: function (e) {
			var $input = $(e.target);

			this.model.set('title', $input.val());
		},

		prepare: function () {
			return _.extend({
				input_name : 'title',
				input_value: this.model.get('title'),
				icon       : this.model.get('icon')
			});
		}

	});

	plex.type.AbstractIconListView = media.View.extend({

		template: _.template($('#plexIconListView').html()),

		initialize: function () {

			this.views.set('.add-form-view-place-holder', new plex.type.AddFormWithIconView({
				collection: this.collection
			}));

			this.collection.on('add', this.addItem, this);
			this.collection.on('add', this.refreshFrame, this);
			this.collection.on('reset', this.addAll, this);
			this.collection.on('remove', this.refreshFrame, this);
			this.collection.on('change', this.refreshFrame, this);
			this.collection.on('reset', this.refreshFrame, this);

			this.collection.reset(this.model.get('_items'));

		},

		addItem: function (model) {
			this.views.add('.-icon-list', new plex.type.IconItemView({
				model: model
			}));
		},

		refreshFrame: function () {

			var contentShortCodeBuilder = new plex.type.ShortCodeBuilder(),
					previewShortCodeBuilder = new plex.type.ShortCodeBuilder();

			this.collection.each(function (item) {

				this.addShortCodes(
						item,
						contentShortCodeBuilder,
						previewShortCodeBuilder
				);

			}, this);

			this.model.set('content_preview', previewShortCodeBuilder.stringify());
			this.model.set('content', contentShortCodeBuilder.stringify());

		},

		addShortCodes: function (item, contentShortCodeBuilder, previewShortCodeBuilder) {
			// addShortCodes should be implemented
		},

		addAll: function (collection) {
			collection.each(function (model) {
				this.addItem(model);
			}, this);

		}

	});

	plex.type.MarkedListView = plex.type.AbstractIconListView.extend({

		addShortCodes: function (item, contentShortCodeBuilder, previewShortCodeBuilder) {

			var itemJson = item.toJSON(),
					content = '';
			if(itemJson['title']) {
				content = itemJson['title'];
				delete itemJson['title'];
			}

			contentShortCodeBuilder.addShortCode({
				tag  : 'marked_item',
				attrs: itemJson,
				content: content
			});

			previewShortCodeBuilder.addShortCode({
				tag  : 'marked_item',
				attrs: itemJson,
				content: content
			});

		}

	});

	(function () {

		var initialized = false;

		$(document).on('click', '.insert-media', function () {

			if (initialized == false) {
				var mediaFrameInstance = wp.media.editor.get('content'),
						editState = mediaFrameInstance.state('short-code-edit');

				// Insert shortcode to tinyMCE
				editState.on('update', function (library) {
					var shortcode = plex.convertModelToShortCode(library);
					wp.media.editor.insert(shortcode.string());
				});

				initialized = true;
			}

		});

	})();


})(jQuery);