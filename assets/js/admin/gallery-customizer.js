(function ($) {

	"use strict";

	/**
	 * ========================================================================
	 * Init namespaces
	 * ========================================================================
	 */

	if (!window.plex)                window.plex = {};
	if (!plex.instance)              plex.instance = {};

	var media = wp.media,
			l10n = media.view.l10n;


	media.gallery.defaults.link = 'file'; // should be default for gallery

	// Change gallery settings template
	media.view.Settings.Gallery.prototype.template = wp.media.template('gallery-advanced-settings');

	// Fix WP bag
	media.view.Settings.Gallery.prototype.update = function (key) {

		media.view.Settings.prototype.update.call(this, key);

		var value = this.model.get(key),
				$setting = this.$('[data-setting="' + key + '"]');

		if ($setting.is('input[type="checkbox"]')) {

			if (value == "false") {
				value = false;
			}

			$setting.attr('checked', !!value);

		} else if ($setting.is('input[type="number"]')) {

			if (!$setting.is(':focus')) {
				$setting.val(value);
			}

		}

	}


	plex.type.GalleryFrame = wp.media.view.MediaFrame.Select.extend({

		initialize: function () {

			_.defaults(this.options, {
				multiple: true,
				editing : false,
				state   : 'gallery' // Current state
			});

			media.view.MediaFrame.Select.prototype.initialize.apply(this, arguments);
		},

		createStates: function () {
			var options = this.options;

			// Add the default states.
			this.states.add([
				// Main states.

				new media.controller.Library({
					id        : 'gallery',
					title     : l10n.createGalleryTitle,
					priority  : 40,
					toolbar   : 'main-gallery', // toolbar mode
					filterable: 'uploaded',
					multiple  : 'add',
					editable  : false,

					library: media.query(_.defaults({
						type: 'image'
					}, options.library))

				}),

				// Gallery states.
				new media.controller.GalleryEdit({
					library: options.selection,
					editing: options.editing,
					menu   : 'gallery'
				}),

				new media.controller.GalleryAdd()
			]);

		},

		bindHandlers: function () {
			media.view.MediaFrame.Select.prototype.bindHandlers.apply(this, arguments);

			this.on('menu:create:gallery', this.createMenu, this);
			this.on('toolbar:create:main-gallery', this.createToolbar, this);

			// Region render handlers
			var handlers = {
				menu: {
					'default': 'mainMenu',
					'gallery': 'galleryMenu'
				},

				toolbar: {
					'main-gallery': 'mainGalleryToolbar',
					'gallery-edit': 'galleryEditToolbar',
					'gallery-add' : 'galleryAddToolbar'
				}
			};

			_.each(handlers, function (regionHandlers, region) {
				_.each(regionHandlers, function (callback, handler) {
					this.on(region + ':render:' + handler, this[ callback ], this);
				}, this);
			}, this);
		},

		/**
		 * Set view for menu region of default mode
		 * Rendered in gallery state
		 * @param view
		 */
		mainMenu: function (view) {
			view.set({
				'library-separator': new media.View({
					className: 'separator',
					priority : 100
				})
			});
		},

		/**
		 * Set view for menu region of gallery mode
		 * Rendered in gallery-edit, gallery-library states
		 * @param view
		 */
		galleryMenu           : function (view) {
			var lastState = this.lastState(),
					previous = lastState && lastState.id,
					frame = this;

			view.set({
				cancel        : {
					text    : l10n.cancelGalleryTitle,
					priority: 20,
					click   : function () {
						if (previous)
							frame.setState(previous);
						else
							frame.close();
					}
				},
				separateCancel: new media.View({
					className: 'separator',
					priority : 40
				})
			});
		},

		// Toolbars
		selectionStatusToolbar: function (view) {
			var editable = this.state().get('editable');

			view.set('selection', new media.view.Selection({
				controller: this,
				collection: this.state().get('selection'),
				priority  : -40,

				// If the selection is editable, pass the callback to
				// switch the content mode.
				editable  : editable && function () {
					this.controller.content.mode('edit-selection');
				}
			}).render());
		},


		mainGalleryToolbar: function (view) {
			var controller = this;

			this.selectionStatusToolbar(view);

			view.set('gallery', {
				style   : 'primary',
				text    : l10n.createNewGallery,
				priority: 60,
				requires: { selection: true },

				click: function () {
					var selection = controller.state().get('selection'),
							edit = controller.state('gallery-edit'),
							models = selection.where({ type: 'image' });

					edit.set('library', new media.model.Selection(models, {
						props   : selection.props.toJSON(),
						multiple: true
					}));

					this.controller.setState('gallery-edit');
				}
			});
		},


		galleryEditToolbar: function () {
			var editing = this.state().get('editing');
			this.toolbar.set(new media.view.Toolbar({
				controller: this,
				items     : {
					insert: {
						style   : 'primary',
						text    : editing ? l10n.updateGallery : l10n.insertGallery,
						priority: 80,
						requires: { library: true },

						click: function () {
							var controller = this.controller,
									state = controller.state();

							controller.close();
							state.trigger('update', state.get('library'));

							controller.reset();
							controller.setState('upload');
						}
					}
				}
			}));
		},

		galleryAddToolbar: function () {
			this.toolbar.set(new media.view.Toolbar({
				controller: this,
				items     : {
					insert: {
						style   : 'primary',
						text    : l10n.addToGallery,
						priority: 80,
						requires: { selection: true },

						click: function () {
							var controller = this.controller,
									state = controller.state(),
									edit = controller.state('gallery-edit');

							edit.get('library').add(state.get('selection').models);
							state.trigger('reset');
							controller.setState('gallery-edit');
						}
					}
				}
			}));
		}
	});

	plex.instance.GalleryFrameBuilder = {

		'build': function (shortcode, onUpdate) {

			var shortcode = wp.shortcode.next('gallery', shortcode),
					frameOptions = {};

			if (shortcode && shortcode.content) {

				shortcode = shortcode.shortcode;

				// init from shortcode
				var attachments = wp.media.gallery.attachments(shortcode);

				var selection = new wp.media.model.Selection(attachments.models, {
					props   : attachments.props.toJSON(),
					multiple: true
				});

				selection.gallery = attachments.gallery;

				frameOptions = {
					selection: selection,
					state    : 'gallery-edit'
				}

			}

			var frame = new plex.type.GalleryFrame(frameOptions);

			frame.state('gallery-edit').on("update", function (selection) {

				var shortCode = wp.media.gallery.shortcode(selection);
				onUpdate.call(this, shortCode, selection);

			});

			return frame;

		}

	}

})(jQuery);