(function ($) {

	"use strict";

	String.prototype.capitalize = function () {
		return this.charAt(0).toUpperCase() + this.slice(1);
	}


	/**
	 * ========================================================================
	 * Init namespaces
	 * ========================================================================
	 */

	if (!window.plex)                window.plex = {};
	if (!plex.instance)              plex.instance = {};
	if (!plex.type)                  plex.type = {};
	if (!plex.fh)                    plex.fh = {};
	if (!plex.local)                 plex.local = {};

	/**
	 * ========================================================================
	 * Include localization
	 * ========================================================================
	 */

	plex.local = {

		data: window.localization || {},

		'get': function (key) {
			if (this.data[key]) {
				return this.data[key];
			}
			else {
				return null;
			}
		}

	};


	/**
	 * ========================================================================
	 * Helper singleton classes
	 * ========================================================================
	 */


	plex.instance.MediaFrameBuilder = {

		defaults: {

			frame: 'select',

			multiple: false,

			title: plex.local.get('media_uploader_title'),

			library: {
				type: 'image'
			},

			button: {
				text: plex.local.get('select')
			}

		},

		build: function (settings, selectCallBack) {

			var frame = wp.media($.extend({}, this.defaults, settings));

			var select = function () {
				var attachment = frame.state().get('selection').first();
				selectCallBack.call(this, attachment);
			}

			frame.on('select', select, this);
			return frame;
		}

	}


	/**
	 * Static class that help to show forms in underscore templates
	 * @type {{formTemplates: {select: *}, checked: Function, select: Function}}
	 */
	plex.instance.FormHelpers = {

		formTemplates: {
			select: _.template("<select name='<%- name %>' <%= (styles) ? 'style=\"'+styles+'\"' : '' %>><% _.each(items, function(tit, value) { %><option value='<%= value %>' <%= (selected == value) ? 'selected=\"selected\"' : '' %>><%= tit %></option><% }); %> </select>")
		},

		checked: function (value, expected) {
			if (value == expected) {
				return "checked"
			}
			return "";
		},

		select: function (name, items, selected, styles) {
			var result = this.formTemplates.select({
				name    : name,
				items   : items,
				selected: selected,
				styles  : styles
			});
			return result;
		}

	};

	/**
	 * Short code for plex.instance.FormHelpers checked
	 * @param value
	 * @param expected
	 * @returns {*}
	 */
	plex.fh.checked = function (value, expected) {
		return plex.instance.FormHelpers.checked(value, expected);
	}

	/**
	 * Short code for plex.instance.FormHelpers select
	 * @param items
	 * @param selected
	 * @param styles
	 * @returns {*}
	 */
	plex.fh.select = function (items, selected, styles) {
		return plex.instance.FormHelpers.select(items, selected, styles);
	}

	// INIT COMMON UI ELEMENTS
	$(function () {

		$(document).on('click', '.datepicker', function (e) {

			var $this = $(e.target);

			if (!$this.hasClass('hasDatepicker')) {
				$this.datepicker();
				$this.datepicker("show");
			}

		});

		$(document).on('change', '.js-toggle-box-trigger', function (e) {

			var $this = $(e.target),
					$parent = $this.parents('.-context');

			$('#' + $this.data('box'), $parent).toggle();
		});


		$(document).on('click', '.-video__input', function (e) {

			var $this = $(e.target),
					$context = $this.parents('.-context');

			var frame = plex.instance.MediaFrameBuilder.build({

				title  : plex.local.get('select_video'),
				button : {
					text: plex.local.get('select')
				},
				library: {
					type: 'video'
				}

			}, function (attachment) {

				$this.val(attachment.get('url'));
				$('.-video__id', $context).val(attachment.get('id'));

			});

			frame.open();

		});

		$(document).on('click', '.-audio__input', function (e) {

			var $this = $(e.target),
					$context = $this.parents('.-context');

			var frame = plex.instance.MediaFrameBuilder.build({

				title  : plex.local.get('select_audio'),
				button : {
					text: plex.local.get('select')
				},
				library: {
					type: 'audio'
				}

			}, function (attachment) {
				$this.val(attachment.get('url'));
				$('.-audio__id', $context).val(attachment.get('id'));
			});

			frame.open();

		});


		$(document).on('click', '.-image__input, .-image__preview-box', function (e) {

			var $this = $(e.target),
					$context = $this.parents('.-context'),
					$attachId = $('.-image__att-id', $context);

			var frame = plex.instance.MediaFrameBuilder.build({

				title : plex.local.get('upload_img'),
				button: {
					text: plex.local.get('select')
				}

			}, function (attachment) {

				$attachId.val(attachment.get('id'));
				$('.-image__input', $context).val(attachment.get('url'));

				$('.-image__preview-box', $context).empty().append($(
						'<img src="' + attachment.get('url') + '" style="width:100%">'
				));

			});

			frame.open();

		});


		$(document).on('click', '.-icons-selection i', function (e) {

			var $this = $(e.target),
					$context = $this.parents('.-context');

			$('i', $context).removeClass('-selected');
			$('.-icon__input', $context).val($this.attr('class')).trigger('change');
			$this.addClass('-selected');

		});

		$('.-type-changable .wp-tab-bar a').click(function () {
			var $this = $(this),
					$context = $this.parents('.-block'),
					$videoType = $('.-type', $context);
			var videoTypeStr = $this.attr('href').slice(1);

			$videoType.val(videoTypeStr);
		});


		$(document).on('click', '.tabs-wrapper .wp-tab-bar a', function (e) {

			var $this = $(e.target),
					$context = $this.parents('.tabs-wrapper');
			$('.wp-tab-bar li', $context).removeClass('wp-tab-active');
			$this.closest('li').addClass('wp-tab-active');
			$('.wp-tab-panel', $context).hide();
			$($this.attr('href'), $context).show();
			event.preventDefault();

		});

		// init tooltips
		$(document).on('mouseenter mouseleave', '.-tooltip', function (e) {
			var $this = $(e.target);

			if (!$this.data('tooltip')) {

				$this.tooltip({
					animation: true,
					trigger  : 'hover',
					delay    : {
						show: 100,
						hide: 600
					},
					placement: 'right'
				});

				$this.tooltip('show');
			}
		});

		$(document).on('click', '.-gallery__create', function (e) {

			e.stopPropagation();

			var $this = $(e.target),
					$context = $this.parents('.-context'),
					shortcode = $('.-gallery__short-code', $context).val();

			var frame = plex.instance.GalleryFrameBuilder.build(shortcode, function (shortCode) {

				var additionalAttributes = $this.data('options');

				if (additionalAttributes) {
					$.extend(shortCode.attrs.named, additionalAttributes);
				}

				$context.find('.-gallery__short-code').val(shortCode.string());

			});

			frame.open();

		});


		// Service charge item
		(function () {


			var lastKey = $('.-service-charge-items').children().size() - 1;

			$('.-service-charge-meta-box').on('click', '.js-service-charge-item-add', function () {

				var $this = $(this),
						$parent = $this.parents('.-add-form'),
						$item = $parent.find('.-service-charge-input'),
						$err = $parent.find('.-err');


				if (!$item.val()) {
					$err.html(plex.local.get('service_charge_validation'));
					return false;
				}

				$err.empty();

				var tpl = _.template($('#plexServiceChargeItem').html());

				var $el = tpl({
					key : ++lastKey,
					item: $item.val()
				});

				$('.-service-charge-items').append($el);

				return false;

			});


			$('.-service-charge-meta-box').on('click', '.js-service-charge-item-del', function () {

				var $this = $(this),
						$row = $this.parents('.-item');

				$row.slideUp(500, function () {
					$(this).remove();
				});

				return false;

			});


		})();

		// Social list
		(function () {

			var getAmountOfSocialLinks = function () {
				return $('.-social-meta-box .-icon-list').children().size() -1;
			}

			var lastKey = getAmountOfSocialLinks();

			$('.-social-meta-box').on('click', '.js-social-add', function () {

				var $this = $(this),
						$parent = $this.parents('.-social-add-form'),
						$icon = $parent.find('.-icon__input'),
						$err = $parent.find('.-err');

				if (lastKey == null) {
					lastKey = getAmountOfSocialLinks();
				}

				if (!$icon.val()) {
					$err.html(plex.local.get('icon_validation'));
					return false;
				}

				$err.empty();

				var tpl = _.template($('#plexSocialElement').html());

				lastKey++;

				var $el = tpl({
					key : lastKey,
					icon: $icon.val(),
					link: $parent.find('.-link').val()
				});

				$('.-social-meta-box .-icon-list').append($el);

				return false;

			});


			$('.-social-meta-box').on('click', '.js-social-del', function () {

				var $this = $(this),
						$row = $this.parents('.-el');

				$row.slideUp(500, function () {
					$(this).remove();
				});

				return false;

			});


		})();


		$('.-cust-block').on('click', '.js-social-apply', function () {

			var $this = $(this),
					$context = $this.parents('.-cust-block'),
					data = $('.-icon-list', $context).serializeForm();
			$('.-setting', $context).val(JSON.stringify(data)).trigger('change');

		});


		$('.-color-scheme-cust-blk').on('click', '.js-el', function () {

			var $this = $(this),
					$context = $this.parents('.-cust-block'),
					$changeInput = $('.-color-scheme', $context),
					val = $this.data('key');
			$context.find('.-selected').removeClass('-selected');
			$this.addClass('-selected');
			$changeInput.val(val).trigger('change');

		});


		/**
		 * ========================================================================
		 * Short code builder
		 * ========================================================================
		 */


		$(document).on('change', '.js-synchronize', function () {
			var $this = $(this),
					$context = $this.parents('.-context');

			$context.find('.-content').val($this.val()).trigger('change');
		});


	});


})(jQuery);