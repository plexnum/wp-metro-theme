(function ($) {
        "use strict";


        var localization = window.localization || {};

        // carousel binder
        (function () {
                "use strict";
                // Extend default carouFredSel settings
                $.fn.carouFredSel.configs.transition = true;
                $.fn.carouFredSel.defaults.scroll.easing = 'easeOutCubic';

                $.extend($.fn.carouFredSel.defaults, {
                        auto      : {
                                timeoutDuration: 5000
                        },
                        width     : '100%',
                        height    : "variable",
                        responsive: true,
                        items     : {
                                height: "variable",
                                start : 0
                        },
                        scroll    : {
                                easing      : 'swing',
                                duration    : 500,
                                pauseOnHover: false,
                                event       : 'click',
                                queue       : false
                        },
                        swipe     : {
                                onTouch: true
                        },
                        next      : {
                                button: function () {
                                }
                        },
                        prev      : {
                                button: function () {
                                }
                        }
                });

                var JSCarouseleBuilder = {

                        build: function (el) {
                                this.$el = $(el);
                                this._resolveConfigs();
                                this._resolveProgressBar();
                                this._resolvePaddings();
                                this._resolvePagination();
                                this._resolveCtrlBtns();

                                this.$el.carouFredSel(this.config)
                                    .trigger('nextPage')
                                    .trigger("finish")
                                    .trigger("prevPage")
                                    .trigger("finish");
                        },

                        _resolveCtrlBtns   : function () {
                                /**
                                 * mod position
                                 *
                                 * add this class to wrapper carousel
                                 *
                                 * [
                                 *   .carousel_arrow-left-right-middle,
                                 *   .carousel_arrow-left-top,
                                 *   .carousel_arrow-left-bottom,
                                 *   .carousel_arrow-right-top,
                                 *   .carousel_arrow-right-bottom
                                 * ]
                                 */

                                this.config.buttons = this.config.buttons || {};

                                if (this.config.buttons.enable) {

                                        this.$el.parent().addClass('carousel_arrow-' + this.config.buttons.position);
                                        /**
                                         *     buttons.size
                                         *
                                         *   [mid, lage, big]
                                         */
                                        var sizeButton = this.config.buttons.size || "big"

                                        var $iconNext = $('<a href="#" class="icon-next icon_' + sizeButton + ' icon_in-bubble"></a>'),
                                                        $iconPrev = $('<a href="#" class="icon-prev icon_' + sizeButton + ' icon_in-bubble"></a>');

                                        this.$el.on('createCarouFredSel', function () {
                                                var th = $(this);

                                                $iconNext.insertAfter(th);
                                                $iconPrev.insertAfter(th);
                                        });

                                        this.config.next = this.config.next || {};

                                        this.config.next.button = function () {
                                                return $iconNext;
                                        }

                                        this.config.prev = this.config.prev || {};

                                        this.config.prev.button = function () {
                                                return $iconPrev;
                                        }
                                }

                        },
                        _resolveProgressBar: function () {

                                this.config.auto = this.config.auto || {};

                                if (this.config.auto.progress) {

                                        var $progress = $('<div>').addClass('progressBar');

                                        this.$el.on('createCarouFredSel', function () {
                                                var th = $(this).parent();
                                                th.append($progress);
                                        });

                                        delete this.config.auto.progress;

                                        this.config.auto.progress = {
                                                bar: $progress
                                        };

                                }

                        },
                        _resolvePagination : function () {

                                this.config.pagination = this.config.pagination || false;

                                if (this.config.pagination) {

                                        var $pagination = $('<div>').addClass('paginationCarusel')


                                        this.$el.on('createCarouFredSel', function () {
                                                var th = $(this).parent().parent().css('paddingBottom', 10);
                                                th.append($pagination);
                                        });
                                        this.config.pagination = {};
                                        $.fn.carouFredSel.pageAnchorBuilder = function (nr, item) {
                                                return '<a class="btn btn_grey" href="#' + nr + '"><div class="bg-main-bg"></div></a>';
                                        };
                                        this.config.pagination.container = $pagination;
                                }

                        },
                        _resolveConfigs    : function () {
                                var that = this,
                                                config = this.$el.data('config');
                                if (typeof(config) === "string") {
                                        config = $.parseJSON(config);
                                }

                                var override = {};
                                var context = this.$el.parents('.gallery-context');

                                if (context.length) {
                                        $.extend(true, config, context.data('gallery-override'));
                                }

                                this.config = config || {};

                                this.config.onCreate = function () {
                                        that.$el.trigger('createCarouFredSel', that);
                                }

                        },
                        _resolvePaddings   : function () {

                                this.config.style = this.config.style || {};

                                //Apply padding styles

                                if (this.config.style.paddingItem) {
                                        var padding = this.config.style.paddingItem;
                                        if ($.isNumeric(padding)) {
                                                this.$el.find("li").css("padding", +padding)
                                        }

                                        if ($.isArray(padding)) {
                                                var strong = padding.join("px ") + "px"
                                                this.$el.find("li").css({
                                                        "padding": strong
                                                });
                                        }
                                }

                        }
                }
                $('.js-carousel').each(function () {
                        var thet = this
                        $(thet).imagesLoaded().done(function () {

                                JSCarouseleBuilder.build(thet);
                        });
                });
        })();

        // proportion iframe
        (function () {
                "use strict";
                var wrap = $('.js-proportion-ifrem');
                wrap.each(function (index, item) {
                        var $item = $(item)
                        var ifrem = $item.find('iframe');
                        var width = ifrem.attr('width');
                        var height = ifrem.attr('height');
                        height = Number(height.replace('px', ''));
                        width = Number(width.replace('px', ''));
                        if ($item.data('not-height')) {
                                ifrem.css({
                                        'width': "100%"
                                });
                        } else {
                                if (width && height) {
                                        ifrem.css({
                                                'position': 'absolute',
                                                'height'  : "100%",
                                                'width'   : "100%"
                                        });
                                        ifrem
                                                        .wrap('<div class="proportion-ifrem__wrap">')
                                                        .parent().css('paddingTop', (height / width * 100) + '%');
                                }
                        }
                });

        })();

        // metrp index
        (function () {
                "use strict";

                var masonryOptions = {
                        itemSelector: '.js-tile',
                        columnWidth : 20,
                        gutter      : 20,
                        isAnimated  : false
                }
                var gridMetroDom = $('.js-grid-metro');
                gridMetroDom.masonry(masonryOptions)
                                .on('layoutComplete', function () {
                                        $('.js-animate').appear(function (e, a) {
                                                $(this).addClass("show");
                                        }, {
                                                accX: 0,
                                                accY: 0
                                        });
                                });
        })();

        // portfolio masonry
        (function () {
                var gridPorfolio = $('.js-grid-portfolio');

                gridPorfolio.masonry({
                        itemSelector : '.item',
                        gutter       : 0,
                        "columnWidth": ".grid-sizer",
                        isAnimated   : true
                });

                var gridPorfolioMasonry = gridPorfolio.data('masonry');

                $('.js-filter-portfolio a:not(.active)').click(function (e) {
                        e.preventDefault()
                        e.stopPropagation();

                        var $this = $(this);

                        $this.parent().addClass('active').siblings().removeClass("active");

                        var filter = $this.data("filter");


                        var itemsToReveal = $.map(gridPorfolio.find(filter + ":hidden"), function (item) {
                                return gridPorfolioMasonry.getItem(item);
                        });

                        gridPorfolioMasonry.reveal(itemsToReveal);
                        gridPorfolio.find('.item:not(' + filter + ')').hide();

                        gridPorfolioMasonry.layout();

                });
        })();


        // plugin image crop center
        $('.js-imgCentr').each(function (index, item) {
                var $item = $(item);
                var url = $item.attr('src')
                $item
                                .wrap('<div ' +
                                                'class="wrap-imag" ' +
                                                'style="background-image: url(' + url + ');' +
                                                '">')
        });

        // equalizing the height in blog
        (function () {
                "use strict";
                var selectAll = $('.js-team .js-cont,.js-team  h3,p:first-child');
                var team = $('.js-team');
                team.equalize({children: '.js-cont'})
                                .equalize({children: 'h3'})
                                .equalize({children: 'p:first-child'});
                $(window).resize($.debounce(100, function () {
                        selectAll.height("auto")
                        team.equalize({children: '.js-cont'})
                                        .equalize({children: 'h3'})
                                        .equalize({children: 'p:first-child'});
                }));
        })();

        // background full-sarine for index page
        (function () {
                var $fake = $('.fake-bg');
                var bg = $fake.css('background-image');
                if (bg) {
                        var src = bg.replace(/(^url\()|(\)$|[\"\'])/g, '');
                        $('<img>').attr('src', src).on('load', function () {
                                // do something, maybe:
                                $fake.addClass('show');
                        });
                }
        })();

        // ie9 and let insert placeholder
        $('input, textarea').placeholder();

        // custom selector
        $('select').uniform();

        //  custom scroll bar
        $('.scroller').baron({
                barOnCls: 'baron',
                bar     : '.scroller__bar',
                track   : '.scroller__track'
        });

        // menu hide and show
        (function () {
                "use strict";
                function hideMenu() {
                        $('.menu-box').removeClass('active');
                        $('.container-wrap').removeClass('left');
                        $('.js-sade-bar').removeClass('hide');
                        $('html').css({'overflow': 'visible'});
                }

                function showMenu() {
                        $('.menu-box').addClass('active');
                        $('.container-wrap').addClass('left');
                        $('.js-sade-bar').addClass('hide');
                        $('html').css({'overflow': 'hidden'});
                }

                function toggleMenu(e) {
                        e.preventDefault()
                        if ($('.menu-box').hasClass('active')) {
                                hideMenu();
                        } else {
                                showMenu();
                        }
                }

                function hideSade() {
                        $('.container-wrap').removeClass('right');
                        $('.js-sade-bar').removeClass('active');
                        $('.menu-box').removeClass('hide');
                        $('html').css({'overflow': 'visible'});
                }

                function showSade() {
                        $('.container-wrap').addClass('right');
                        $('.js-sade-bar').addClass('active');
                        $('.menu-box').addClass('hide');
                        $('html').css({'overflow': 'hidden'});
                }

                function toggleSade() {
                        if ($('.js-sade-bar').hasClass('active')) {
                                hideSade()
                        } else {
                                showSade()
                        }
                }

                $('body').click(function (e) {
                        var $el = $(e.target);
                        if (
                                        !$el.is('.menu-box') && !$el.parents('.menu-box').length && !$el.is('.js-sade-bar').length && !$el.parents('.js-sade-bar').length
                                        ) {
                                hideMenu();
                                hideSade();
                        }
                });

                $('.js-sade-bar-toggler').click(function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        toggleSade(e);
                });

                $('.js-menu-toggler').click(function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        toggleMenu(e);
                });
        })();

        // sub menu
        (function () {
                "use strict";
                var menuItems = $('.js-menu > li');

                menuItems.find('ul')
                                .parent()
                                .addClass('in-submenu')
                                .find('.active')
                                .parents('li')
                                .addClass('active');
                var menuItemsSubmenu = menuItems.filter('.in-submenu')
                menuItemsSubmenu.find('.item > a').oneTransitionEnd(
                                $.debounce(50,
                                                function () {
                                                        $('.scroller').trigger('sizeChange');
                                                })
                );

                $('> a', menuItems).click(function (e) {

                        var $this = $(this);
                        if (!($this.parents('li').is('.active') || !$this.parents('li').is('.in-submenu'))) {

                                $this.parents('li').addClass('active')
                                                .siblings().removeClass('active');

                                $('.scroller').trigger('sizeChange');

                                $('> a', menuItems).off("click.dubel");

                                return false;

                        }

                });

        })();

        // fancybox call
        (function () {
                "use strict";
                $('.js-media-el-trigger').click(function (e) {

                        var $this = $(this),
                                        $videContext = $this.parents('.media-context'),
                                        $fancyMedia = $('.js-fancybox-media', $videContext);

                        if ($fancyMedia.size() > 0) {
                                $fancyMedia.trigger('click');
                        }

                });

                $('.js-gallery-trigger').click(function (e) {

                        var $this = $(this),
                                        $galleryContext = $this.parents('.js-gallery-context'),
                                        $el = $galleryContext.find('li > a').first();

                        if ($el.size() > 0) {

                                if ($el.is('.js-fancybox')) {
                                        e.preventDefault();
                                        $el.trigger('click');
                                }
                                else {
                                        location.href = $el.attr('href');
                                }

                        }

                });

                $.fancybox.defaults.tpl.closeBtn = '<a class="fancybox-close icon-cancel" href="javascript:;"></a>';
                $.fancybox.defaults.tpl.next = '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span class="icon-next  icon_big icon_in-bubble"></span></a>';
                $.fancybox.defaults.tpl.prev = '<a title="Previous" class="fancybox-nav fancybox-prev " href="javascript:;"><span class="icon-prev icon_big icon_in-bubble"></span></a>';

                // defolt call fancybox
                $(".js-fancybox").fancybox({
                        autoResize: true,
                        beforeShow: function () {
                                $('.js-carousel').trigger("pause", true);
                                this.minWidth = $(this.element).data("width") || $(this.content).data("width") || 600;
                                this.minHeight = $(this.element).data("height") || $(this.content).data("height") || 'auto';
                        },
                        afterClose: function () {
                                $('.js-carousel').trigger("pause", [null, true]);
                        }
                });

                // call fancybox for media elements (audeo and video)
                $('.js-fancybox-media').fancybox({
                        openEffect : 'none',
                        closeEffect: 'none',
                        autoSize   : true,
                        autoResize : true,
                        height     : 'auto',
                        width      : 'auto',
                        minWidth   : 200,
                        maxWidth   : 1000,
                        scrolling  : 'visible',
                        helpers    : {
                                media: {}
                        },
                        beforeShow : function () {
                                this.minWidth = $(this.element).data("width") || $(this.content).data("width") || 600;
                                this.minHeight = $(this.element).data("height") || $(this.content).data("height") || 'auto';
                                $(window).resize()
                        },
                        afterLoad  : function () {
                                $.fancybox.update()
                        },
                        afterShow  : function () {
                                $.fancybox.update();
                                $(window).resize();
                        }

                });
        })();

        // coments wp
        (function () {
                "use strict";

                $('#respond').on('focusin', 'input, textarea', function () {
                        window.addComment.removeAttachedForm();
                });

                window.addComment = {

                        attachedForm: null,

                        cancelReplyLink: null,

                        moveFormToStandardPlace: function () {

                                if (this.attachedForm) {

                                        var that = this;

                                        this.attachedForm.fadeOut(500, function () {

                                                var $feedback = $('.feedback');

                                                $feedback.fadeIn(500);
                                                that.attachedForm.appendTo($feedback).fadeIn(500);
                                                that.attachedForm = null;

                                        });
                                }
                        },

                        createCancelReplyLink: function ($reply) {
                                $reply.removeClass('reply').addClass('cancel-reply');
                                $reply.find('.comment-reply-link').html('Cancel reply');
                                this.cancelReplyLink = $reply;
                        },

                        removeCancelReplyLink: function ($reply) {
                                $reply.removeClass('cancel-reply').addClass('reply');
                                $reply.find('.comment-reply-link').html('Reply');
                                this.cancelReplyLink = null;
                        },

                        moveForm: function (commId, parentId, respondId, postId) {

                                var $comment = $('#' + commId),
                                                $commentBody = $('#div-' + commId),
                                                $responde = $('#' + respondId),
                                                $commentParentInput = $('#comment_parent'),
                                                $postIdInput = $('#comment_post_ID'),
                                                postId = postId || false,
                                                that = this;


                                if (!$comment.size() || !$responde.size() || !parent)
                                        return;

                                if ($postIdInput.size() > 0 && postId) {
                                        $postIdInput.val(postId);
                                }

                                var $cancelReply = $commentBody.find('.cancel-reply');

                                // if click on cancel reply link than move form to standard place
                                if ($cancelReply.size() > 0) {
                                        $commentParentInput.val(0);
                                        this.removeCancelReplyLink(this.cancelReplyLink);
                                        this.moveFormToStandardPlace();
                                        return false;
                                }

                                // Set parent comment of existed comment
                                $commentParentInput.val(parentId);

                                if (this.cancelReplyLink) {
                                        this.removeCancelReplyLink(this.cancelReplyLink);
                                }

                                this.createCancelReplyLink(
                                                $commentBody.find('.reply')
                                );

                                if (this.attachedForm == null) {
                                        // Create attached form
                                        this.attachedForm = $responde;
                                }

                                // Value of each input of cloned form should be empty
                                this.attachedForm.find('textarea').each(function (i, el) {
                                        el.value = '';
                                });

                                $('.feedback').fadeOut(500, function () {
                                        that.attachedForm.insertAfter($commentBody).fadeIn(500);
                                });

                                return false;
                        }
                }

        })();

        // animate to scroll
        (function () {
                // start animate block
                if (!Modernizr.cssanimations) {
                        $('.js-animate').removeClass("js-animate")
                }

                var countAnimateStek = 1
                var intervalAnimate = window.intervalAnimate || 100
                $('.js-animate').appear(function (e, a) {
                        var $this = $(this);
                        var typeAnimate = $this.data('typeAnimate') || window.defaultTypeAnimate || 'fadeInUp'
                        countAnimateStek++
                        function animateStart() {
                                $this.imagesLoaded(function () {
                                        $this.oneAnimationEnd(function (e) {
                                                $(this)
                                                                .removeClass('animated')
                                                                .removeClass(typeAnimate)
                                                                .removeClass('js-animate')
                                        });
                                        $this.addClass("animated show " + typeAnimate)
                                });
                        }

                        if (countAnimateStek == 1) {
                                // start first animate
                                animateStart()
                                setTimeout(function () {
                                        countAnimateStek--
                                }, intervalAnimate);
                        } else {
                                // start second and more animate
                                setTimeout(function () {
                                        animateStart()
                                        countAnimateStek--
                                }, countAnimateStek * intervalAnimate);
                        }
                });
        })();

})(jQuery);