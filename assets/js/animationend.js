(function ($) {
    $.fn.oneAnimationEnd = function (fn, options) {
        var $this = $(this)
        $this.each(function (index, item) {
            $this = $(item)
//            var animationEndEventNames = [
//                    'webkitAnimationEnd',
//                    'oAnimationEnd',
//                    'msAnimationEnd',
//                    'animationend'
//                ],
//
//                transitionEndEventNames = [
//                    'webkitTransitionEnd',
//                    'oTransitionEnd',
//                    'MSTransitionEnd',
//                    'transitionend'
//                ]

//            if (Modernizr.csstransforms) {
//                for (var i = 0; i < transitionEndEventNames.length; i++) {
//                    var transitionEndEventName = transitionEndEventNames[i];
//                    $this.on(transitionEndEventName + ".custom", function (e) {
//                        callCalback(e);
//                        $this.off(transitionEndEventName + ".custom")
//                    })
//                }
//            } else {
//                var e = $.Event("transitionend", {'target': this})
//                callCalback(e);
//            }
            if (Modernizr.cssanimations) {
                $this.on(
                    'webkitAnimationEnd.custom ' +
                        'oanimationend.custom ' +
                        'msAnimationEnd.custom ' +
                        'animationend.custom', function (e) {
                        callCalback(e);
                        $this.off(
                            'webkitAnimationEnd.custom ' +
                                'oanimationend.custom ' +
                                'msAnimationEnd.custom ' +
                                'animationend.custom')
                    })
            } else {
                var e = $.Event("animationend", {'target': this});
                callCalback(e);
            }

            function callCalback(e) {
                fn.apply(item, e);
            }
        });
    };
    $.fn.oneTransitionEnd = function (fn, options) {
        var $this = $(this)
        $this.each(function (index, item) {
            $this = $(item)
            if (Modernizr.csstransitions) {
                $this.on(
                    'webkitTransitionEnd.custom ' +
                        'oTransitionEnd.custom ' +
                        'MSTransitionEnd.custom ' +
                        'transitionend.custom ' , function (e) {
                        callCalback(e);
                        $this.off(
                            'webkitTransitionEnd.custom ' +
                                'oTransitionEnd.custom ' +
                                'MSTransitionEnd.custom ' +
                                'transitionend.custom ' )
                    })
            } else {
                var e = $.Event("animationend", {'target': this});
                callCalback(e);
            }

            function callCalback(e) {
                fn.apply(item, e);
            }
        });
    };

})(jQuery)