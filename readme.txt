MetroBIZZ — Responsive Metro Styled Theme
MetroBIZZ — Suitable for B2B-oriented companies, self-presentation, design portfolio and directory services.


Features:
	Short Code Live Editor
	Easy to setup animation
	Unlimited color schemes
	Built-in color schemes
	23 custom Short Codes
	Advanced Wordpress Gallery builder
	255 vector icons
	11 page templates
	8 metro widgets
	Responsive Layout (Tested on iPad, iPhone, Android and Windows Mobile 8)
	Retina ready
	jQuery and CSS3 animation
	23 animation emergences
	Valid HTML5 code
	SEO Ready
	Crossbrowser compatible (IE8+)
	Drop-down navigation
	Widgets & Shortcodes
	Easy to customize
	Free Google Fonts (650+)
	Live preview fonts
	Working PHP contact form
	Layered PSD Included
	Documentation included
	Video documentation
	Support and updates
	Google Maps
	Social integration
	Twitter widget with new OAuth api

I’d be happy to customize the template according to your needs or make the unique webpages for a small additional payment.
Please, contact me by email form on our profile page.


Tags
metro, corporate, business, animation, fullscreen background, fxoffice, gallery, css animation, portfolio, projects, slider, responsive, retina ready
