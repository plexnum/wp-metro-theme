<?php

/**
 * ========================================================================
 * Search page
 * ========================================================================
 */

if ( ! defined( 'ABSPATH' ) ) exit;
?>

<?php plex_load_partial( 'view/templates/_partials/header' ); // Load header partial ?>

<?php

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();
$plex_view_transfer->set( 'page_header', __( 'Search', plex_get_trans_domain() ) );
$plex_view_transfer->set( 'not_founded', __( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', plex_get_trans_domain() ) );

// Set post icon
$plex_view_transfer->set( 'page_icon', 'icon-search' );
?>

<?php plex_load_partial( 'view/templates/_partials/page-header' ); ?>

	<ul class="blog-list mb30">
		<li class="blog-item">
			<div class="blog-item__content content">
				<div class="bg-main-bg"></div>
				<h2 class="page-header"><?php printf( __( 'Search Results for: "%s"', plex_get_trans_domain() ), get_search_query() ); ?></h2>

				<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<div class="row-fluid">
						<div class="span9">
							<input type="search" class="search-field input-fat" value="<?php echo get_search_query(); ?>" name="s" />
						</div>
						<div class="span3">
							<button type="submit" class="btn btn_bloc btn_big"><?php echo esc_attr__( 'Search', plex_get_trans_domain() ); ?></button>
						</div>
					</div>
				</form>

			</div>
		</li>
	</ul>

<?php plex_load_partial( 'view/templates/_content/search' ); // Load posts ?>

<?php plex_load_partial( 'view/templates/_partials/footer' ); // Load footer partial ?>