<?php

/**
 * ========================================================================
 * Single project
 * ========================================================================
 */

if ( ! defined( 'ABSPATH' ) ) exit;
?>

<?php plex_load_partial( 'view/templates/_partials/header' ); // Load header partial ?>

<?php

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();

$plex_view_transfer->set( 'page_header', get_the_title() );

// Set post icon
$plex_view_transfer->set( 'page_icon',
	plex_get_post_icon(
		get_the_ID(),
		plex_get_default_post_icon()
	)
);

$plex_view_transfer->set( 'page_excerpt', get_the_excerpt() );

?>

<?php plex_load_partial( 'view/templates/_partials/page-header' ); ?>
<?php plex_load_partial( 'view/templates/_content/single-project' ); ?>
<?php plex_load_partial( 'view/templates/_partials/footer' ); // Load footer partial ?>