<?php

/**
 * ========================================================================
 * Latest posts page
 * ========================================================================
 */

if ( ! defined( 'ABSPATH' ) ) exit;
?>

<?php plex_load_partial( 'view/templates/_partials/header' ); // Load header partial ?>

<?php

// Set page setting from page that decided as page for latest posts
if ( $plex_page_id = get_option( 'page_for_posts' ) ) {

	$plex_page = get_post( $plex_page_id );

	$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();

	$plex_view_transfer->set(
		'sidebars_settings',
		(array) get_post_meta( $plex_page_id, 'sidebars_settings', true )
	);

	if ( ! is_null( $plex_page ) ) {
		$plex_view_transfer->set( 'page_header', $plex_page->post_title );
		$plex_view_transfer->set(
			'page_icon',
			plex_get_post_icon( $plex_page_id, 'icon-Blog' )
		);
		$plex_view_transfer->set( 'page_excerpt', $plex_page->post_excerpt );
	}

}

?>

<?php plex_load_partial( 'view/templates/_partials/page-header' ); ?>

<?php plex_load_partial( 'view/templates/_content/posts' ); // Load posts ?>

<?php plex_load_partial( 'view/templates/_partials/footer' ); // Load footer partial ?>