<?php
/**
 * Template Name: Color settings
 */

/**
 * ========================================================================
 * Color setting
 * ========================================================================
 */

if ( ! defined( 'ABSPATH' ) ) exit;

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();
$plex_view_transfer->set( 'page_header', __( 'Color settings', plex_get_trans_domain() ) );
$plex_view_transfer->set( 'page_icon', 'icon-wand' );
$plex_view_transfer->set( 'page_excerpt', __( 'By using this page you can easily customize your theme colors', plex_get_trans_domain() ) );

?>

<?php plex_load_partial( 'view/templates/_partials/header' ); // Load header partial ?>

<?php plex_load_partial( 'view/templates/_partials/page-header' ); ?>

	<ul class="blog-list">
	<li class="blog-item">
	<div class="blog-item__content content">
	<div class="bg-main-bg"></div>


	<h2><?php _e( 'Typography', plex_get_trans_domain() ); ?></h2>
	<hr>

	<div class="row-fluid">
		<div class="span4">
			<h1>h1. Heading 1</h1>

			<h2>h2. Heading 2</h2>

			<h3>h3. Heading 3</h3>
			<h4>h4. Heading 4</h4>
			<h5>h5. Heading 5</h5>
			<h6>h6. Heading 6</h6>

		</div>
		<div class="span4">

			<p class="lead">
				text lead: Lorem ipsum dolor sit amet, consectetur adipisicing elit.
			</p>

			<p>
				text normal: Lorem ipsum dolor sit amet, consectetur adipisicing elit.
			</p>

			<p>
				<small>
					text small: Lorem ipsum dolor sit amet, consectetur adipisicing elit.
				</small>
			</p>

		</div>
		<div class="span4">
			<div class="wp-caption alignnone">

				<a href="#0">
					<img src="<?php echo plex_fix_url() . '/img.jpg'; ?>" alt="Image caption" width="1024" height="685">
				</a>

				<p class="wp-caption-text">Image caption</p>
			</div>
		</div>
	</div>
	<h2><?php _e( 'Buttons', plex_get_trans_domain() ); ?></h2>
	<hr>
	<a class="btn " href="#0">Default</a>
	<a class="btn  btn_col2" href="#0">Default color button</a>
	<a class="btn  btn_grey" href="#0">Opacity button
		<span class="bg-main-bg"></span></a>

	<h2><?php _e( 'Links', plex_get_trans_domain() ); ?></h2>
	<hr>

	<a href="#0">Link in the content</a>

	<h2><?php _e( 'Metro tiles', plex_get_trans_domain() ); ?></h2>
	<hr>

	<div class="row-fluid">
		<div class="span6">

			<div class=" tile_color1 ">
				<span class="tit">Careers</span>

				<div class="dis separator__wrap separator_right">

					<p class="separator__content">
						We are looking for professionals who can join to our team
					</p>
					<a href="#0" class=" separator">
						<i class="icon-next  icon_in-bubble icon_lage "></i>
					</a>

				</div>
			</div>
		</div>
		<div class="span6">
			<div class="tile_color2">
				<h4>
					MetroBizz LTD
				</h4>

				<p>
					Henry Street <br>
					Dublin 1, Co. Dublin, Ireland <br>
					02 12345678 <br>
				</p>

			</div>
		</div>
	</div>

	<h2><?php _e( 'Media elements', plex_get_trans_domain() ); ?></h2>
	<hr>

	<div class="row-fluid">
		<div class="span6">

			<div class="js-animate">
				<div style="width: 1200px; max-width: 100%;"><!--[if lt IE 9]>
					<script>document.createElement('video');</script><![endif]-->
					<video style="width: 100%; height: 100%;" class="wp-video-shortcode" width="1200" height="360" preload="metadata" controls="controls">
						<source type="video/mp4" src="<?php echo plex_fix_url() . '/video.mp4'; ?>" />
						<a href="<?php echo plex_fix_url() . '/video.mp4'; ?>"><?php echo plex_fix_url() . '/video.mp4'; ?></a>
					</video>
				</div>
			</div>

		</div>
		<div class="span6">
			<div class="js-animate"><!--[if lt IE 9]>
				<script>document.createElement('audio');</script><![endif]-->
				<audio class="wp-audio-shortcode" id="audio-703-1" preload="none" style="width: 100%" controls="controls">
					<source type="audio/mpeg" src="<?php echo plex_fix_url().'/test.mp3'; ?>" />
					<a href="<?php echo plex_fix_url().'/test.mp3'; ?>"><?php echo plex_fix_url().'/test.mp3'; ?></a>
				</audio>
			</div>
		</div>
	</div>


	<h2><?php _e( 'Testimonial', plex_get_trans_domain() ); ?></h2>
	<hr>

	<div data-type-animate="fadeInRight  ">
		<div class="testimonial__wrap">
			<div class="testimonial">
				<blockquote>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
				</blockquote>
				<i class="testimonial__arrow"></i>

				<div class="testimonial__person">
					<img src="<?php echo plex_fix_url() . '/img-min.jpg'; ?>" alt="" class="pull-left" />

					<h3>Ted Malone</h3>
					<h4>Director</h4>
				</div>
			</div>
		</div>
	</div>

	<h2><?php echo _e( 'Block quotes', plex_get_trans_domain() ); ?></h2>
	<hr>

	<div class="row-fluid">
		<div class="span4">

			<blockquote>
				<h2 class="author">John Werd</h2>

				<p>In dolore maluisset constituto eum, pro semper singulis te, ad tale velit definitionem qui.</p>
			</blockquote>
		</div>
		<div class="span4">

			<blockquote class="blockquote_icon">
				<i class="icon-quotes-left"></i>

				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
			</blockquote>
		</div>
		<div class="span4">

			<blockquote class="blockquote_icon">
				<i></i>

				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
			</blockquote>
		</div>
	</div>


	<h2><?php _e( 'Pagination', plex_get_trans_domain() ); ?></h2>
	<hr>

	<div class="aligncenter pagination-post">
		<a href="#0"><span>1</span></a>
		<a href="#0"><span>2</span></a>
		<a href="#0"><span>3</span></a>
		<a href="#0"><span>4</span></a>
		<span>5</span>
		<a href="#0"><span>6</span></a>
		<a href="#0"><span>7</span></a>
	</div>

	<h2><?php _e( 'Accordion', plex_get_trans_domain() ); ?></h2>
	<hr>

	<div class="accordion" id="accordion2">
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle" href="#0">
					<i class="icon-caret-down"></i> Collapsible Group Item #1
				</a>
			</div>
			<div class="accordion-body collapse in">
				<div class="accordion-inner">
					Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
					wolf
					moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
					eiusmod.
					Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
					shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
					ea
					proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw
					denim
					aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
				</div>
			</div>
		</div>
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle" href="#0">
					<i class="icon-caret-down"></i> Collapsible Group Item #2
				</a>
			</div>
			<div id="collapseTwo" class="accordion-body collapse">
				<div class="accordion-inner">
					Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
					wolf
					moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
					eiusmod.
					Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
					shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
					ea
					proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw
					denim
					aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
				</div>
			</div>
		</div>
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle" href="#0">
					<i class="icon-caret-down"></i> Collapsible Group Item #3
				</a>
			</div>
			<div id="collapseThree" class="accordion-body collapse">
				<div class="accordion-inner">
					Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
					wolf
					moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
					eiusmod.
					Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
					shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
					ea
					proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw
					denim
					aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
				</div>
			</div>
		</div>
	</div>
	<h2><?php _e( 'Tabs', plex_get_trans_domain() ); ?></h2>
	<hr>

	<ul class="nav nav-tabs">
		<li class="active"><a href="#0">Home</a></li>
		<li class=""><a href="#0">Profile</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in" id="home">
			<p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro
				synth
				master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro
				keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip
				placeat
				salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
		</div>
		<div class="tab-pane fade" id="profile">
			<p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1
				labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft
				beer
				twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl
				cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS
				salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit,
				sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester
				stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
		</div>
	</div>
	<h2><?php _e( 'Calendar', plex_get_trans_domain() ); ?></h2>
	<hr>

	<div class="row-fluid">
		<div class="span4 offset1">

			<div id="calendar_wrap">
				<table id="wp-calendar">
					<caption>October 2011</caption>
					<thead>
					<tr>
						<th scope="col" title="Monday">M</th>
						<th scope="col" title="Tuesday">T</th>
						<th scope="col" title="Wednesday">W</th>
						<th scope="col" title="Thursday">T</th>
						<th scope="col" title="Friday">F</th>
						<th scope="col" title="Saturday">S</th>
						<th scope="col" title="Sunday">S</th>
					</tr>
					</thead>

					<tfoot>
					<tr>
						<td colspan="3" id="prev"><a href="#0" title="View posts for January 2010">&laquo;
								Jan</a></td>
						<td class="pad">&nbsp;</td>
						<td colspan="3" id="next"><a href="#0" title="View posts for May 2013">May
								&raquo;</a></td>
					</tr>
					</tfoot>

					<tbody>
					<tr>
						<td colspan="5" class="pad">&nbsp;</td>
						<td>1</td>
						<td>2</td>
					</tr>
					<tr>
						<td>3</td>
						<td><a href="#0" title="Another post, woo">4</a></td>
						<td>5</td>
						<td>6</td>
						<td>7</td>
						<td>8</td>
						<td>9</td>
					</tr>
					<tr>
						<td>10</td>
						<td>11</td>
						<td>12</td>
						<td>13</td>
						<td>14</td>
						<td>15</td>
						<td>16</td>
					</tr>
					<tr>
						<td>17</td>
						<td id="today">18</td>
						<td>19</td>
						<td><a href="#0" title="Image in a post">20</a></td>
						<td>21</td>
						<td>22</td>
						<td>23</td>
					</tr>
					<tr>
						<td>24</td>
						<td>25</td>
						<td>26</td>
						<td>27</td>
						<td>28</td>
						<td>29</td>
						<td>30</td>
					</tr>
					<tr>
						<td>31</td>
						<td class="pad" colspan="6">&nbsp;</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<h2><?php _e( 'Progress bars', plex_get_trans_domain() ); ?></h2>
	<hr>

	<div class="row-fluid">
		<div class="span6">
			<div class="progress  show" data-type-animate="fadeInUpBig">
				<div class="bar" style="width: 60%;">
                  <span>
                      Text
                  </span>
				</div>
			</div>
		</div>
		<div class="span6">
		</div>
	</div>
	<h2><?php _e( 'Custom selector', plex_get_trans_domain() ); ?></h2>
	<hr>

	<div class="row-fluid">
		<div class="span6">
			<select name="archive-dropdown" class="archive-dropdown">
				<option value="">Select Month</option>
				<option class="level-0" value="#0"> May 2013</option>
				<option class="leval-1" value="#0"> October 2011</option>
				<option class="leval-1" value="#0"> January 2010</option>
				<option class="leval-2" value="#0"> December 2009</option>
				<option class="leval-3" value="#0"> November 2009</option>
				<option class="leval-0" value="#0"> October 2009</option>
			</select>
		</div>
	</div>
	</div>


	</li>
	</ul>

	<h2><?php echo _e( 'Metro widgets', plex_get_trans_domain() ); ?></h2>

	<div class="row grid-metro js-grid-metro">

	<a href="#0" class="span2 inch2 js-tile">
		<div class="tile_basis-linc">
			<div class="bg-main-bg"></div>

			<i class="icon-AboutUs"></i>
			<span>About Us</span>
		</div>
	</a>


	<div class="span4 inch2 js-tile">
		<div class="tile_text tile-type2">
			<div class="dis">
				<div class="scroller" data-baron-v="inited">
					<p>Pellentesque habitant morbi tristique senec tus et netus et</p>

					<div class="scroller__track">
						<div class="scroller__bar"></div>
					</div>
				</div>
			</div>
			<div class="bg-main-bg"></div>
		</div>
	</div>

	<div class="span4 inch2 js-tile">
		<div class="tile_text tile-type1">
			<div class="dis">
				<div class="scroller" data-baron-v="inited">
					<p>Pellentesque habitant morbi tristique senec tus et netus et</p>

					<div class="scroller__track">
						<div class="scroller__bar"></div>
					</div>
				</div>
			</div>
			<div class="bg-main-bg"></div>
		</div>
	</div>

	<div class="span4 inch2 js-tile">
		<div class="tile_text">
			<div class="bg-main-bg"></div>
			<div class="dis">
				<div class="scroller" data-baron-v="inited">
					<p>Pellentesque habitant morbi tristique senec tus et netus et</p>

					<div class="scroller__track">
						<div class="scroller__bar"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="span4 inch4 js-tile">
		<div class="tile_img-dis tile-type1">

			<img class="js-imgCentr" src="<?php echo plex_fix_url() . '/img.jpg'; ?>" width="1024" height="685">

			<div class="dis">
				<div class="bg-main-bg"></div>

				<time>
					9th December 2013 by admin
				</time>
				<h3>
					Lorem ipsum dolor esse
				</h3>

				<div class="separator__wrap separator_right">
					<div class="separator__content">
						Sed aliquam ultrices mauris. Integerer ante arcu, accumsan a, consectetuer... eget, viva posuere ut,
						mauris...
					</div>

					<a href="#0" class=" separator">
						<i class="icon-next  icon_in-bubble icon_lage "></i>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="span4 inch4 js-tile">
		<div class="tile_img-dis tile-type2">

			<img class="js-imgCentr" src="<?php echo plex_fix_url() . '/img.jpg'; ?>" width="1024" height="685"
					 alt="" />

			<a class="tile__icon-box" href="#0">
				<i class="icon-play-2  icon_in-bubble icon_big"></i>
			</a>

			<div class="dis">
				<div class="bg-main-bg"></div>

				<time>
					9th December 2013 by admin
				</time>
				<h3>
					Lorem ipsum dolor esse
				</h3>

				<div class="separator__wrap  separator_right">
					<p class="separator__content">Te cum doming placerat. Sea liber rectequer ne.</p>
					<a href="#0" class=" separator">
						<i class="icon-next  icon_in-bubble icon_lage "></i>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="span4 inch4 js-tile">
		<div class="tile_img-dis">
			<img class="js-imgCentr" src="<?php echo plex_fix_url() . '/img.jpg'; ?>" width="1024" height="685" alt="" />

			<div class="dis">
				<div class="bg-main-bg"></div>

				<time>
					9th December 2013 by admin
				</time>
				<h3>
					Lorem ipsum dolor esse
				</h3>

				<div class="separator__wrap  separator_right">
					<p class="separator__content">Te cum doming placerat. Sea liber rectequer ne.</p>
					<a href="#0" class=" separator">
						<i class="icon-next  icon_in-bubble icon_lage "></i>
					</a>
				</div>
			</div>
		</div>
	</div>

	<a href="#0" class="span2 inch2 js-tile">
		<div class="tile_date tile-type1">
			<span class="bg-main-bg"></span>
			<img class="js-imgCentr" src="<?php echo plex_fix_url() . '/img.jpg'; ?>" alt="" />

			<div class="box">
				<span class="bg-main-bg"></span>
				<time class="separator__wrap separator_right">
							<span class="separator__content">
									<span class="dey">28</span>
									<span class="mon">Jan</span>
							</span>

							<span class=" separator">
									<i class="icon-next  icon_in-bubble icon_lage "></i>
							</span>
				</time>
			</div>

		</div>
	</a>

	<a href="#0" class="span2 inch2 js-tile">

		<div class="tile_date tile-type2">
			<span class="bg-main-bg"></span>

			<div class="box">
				<span class="bg-main-bg"></span>

				<p class="dis">Vestibulum facilisis, es purus nec pulvinar </p>
				<time class="separator__wrap separator_right">
                            <span class="separator__content">
                                <span class="dey">28</span>
                                <span class="mon">Jan</span>
                            </span>

                            <span class=" separator">
                                <i class="icon-next  icon_in-bubble icon_lage "></i>
                            </span>
				</time>
			</div>

		</div>
	</a>

	<a href="#0" class="span2 inch2 js-tile">
		<div class="tile_date">
			<span class="bg-main-bg"></span>

			<img class="js-imgCentr" src="<?php echo plex_fix_url() . '/img.jpg'; ?>" alt="" />

			<div class="box">
				<span class="bg-main-bg"></span>
				<time class="separator__wrap separator_right">
                            <span class="separator__content">
                                <span class="dey">28</span>
                                <span class="mon">Jan</span>
                            </span>

                            <span class=" separator">
                                <i class="icon-next  icon_in-bubble icon_lage "></i>
                            </span>
				</time>
			</div>
		</div>
	</a>

	<div class="span4 inch2 js-tile tile_twitter">
		<div class="row-fluid">
			<div class="span2">
				<i class="icon-twitter icon_in-bubble icon_big"></i>
			</div>
			<div class="span10">
				<p>
					<a href="#0" class="author">
						@johnsmith
					</a>
					Lorem ipsum dolor sit amet, consectetur a nam numquam placeat reprehenderit sapiente.
					<a href="#0">http://t.co/m1u6f</a>
				</p>
				<time>1 day ago</time>
			</div>
		</div>
	</div>

	<a href="#0" class="span4 inch2 js-tile">
		<div class="tile_video">
			<img class="js-imgCentr" src="<?php echo plex_fix_url() . '/img.jpg'; ?>" alt="" />
        <span class="tile__icon-box">
            <i class="icon-play-2  icon_in-bubble icon_big"></i>
        </span>
		</div>
	</a>

	</div>

	<h2><?php _e( 'Posts', plex_get_trans_domain() ); ?></h2>
	<h3><?php _e( 'Simple post', plex_get_trans_domain() ); ?></h3>
	<ul class="blog-list">
		<li class="blog-item">
			<div class="blog-item__header">
				<div class="bg-main-bg"></div>

				<time>
					<span class="dey">28</span>
					<span class="mon">Jan</span>
				</time>
				<h3>
					Duis arcu tortor, suscipit eget.
				</h3>

				<div class="blog-item__prop">

					<i class="icon-user"></i>Admin <span class="split">|</span>
					<i class="icon-pencil"></i>Marketing, Economics<span class="split">|</span>
					<i class="icon-bubbles"></i>3 Comments
				</div>
			</div>
			<div class="blog-item__img">
				<img class="js-imgCentr" src="<?php echo plex_fix_url() . '/img.jpg'; ?>" alt="">
			</div>
			<div class="blog-item__content content">
				<div class="bg-main-bg"></div>

				<div class="separator__wrap  separator_right">
					<p class="separator__content">

						Phasellus nec sem in justo <a href="#0"> pellentesque facilisis</a>. Etiam imperdiet imperdiet orci.
						Nunc
						nec neque. Phasellus leo dolor, tempus non, auctor et, hendreriter quis, nisi. Curabitur ligula
						sapien,
						tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at
						massa.
						Sed cursus turpis vitae tortor. Donec posuere vulputate arcu.
					</p>
					<a href="#0" class=" separator">
						<i class="icon-next  icon_in-bubble icon_lage "></i>
					</a>
				</div>


			</div>
		</li>

		<h3><?php _e( 'Sticky post', plex_get_trans_domain() ); ?></h3>

		<li class="blog-item sticky">
			<div class="blog-item__header">
				<div class="bg-main-bg"></div>

				<time>
					<span class="dey">28</span>
					<span class="mon">Jan</span>
				</time>
				<h3>
					Duis arcu tortor, suscipit eget.
				</h3>

				<div class="blog-item__prop">

					<i class="icon-user"></i>Admin <span class="split">|</span>
					<i class="icon-pencil"></i>Marketing, Economics<span class="split">|</span>
					<i class="icon-bubbles"></i>3 Comments
				</div>
			</div>
			<div class="blog-item__img">
				<img class="js-imgCentr" src="<?php echo plex_fix_url() . '/img.jpg'; ?>" alt="">
			</div>
			<div class="blog-item__content content">
				<div class="bg-main-bg"></div>

				<div class="separator__wrap  separator_right">
					<p class="separator__content">

						Phasellus nec sem in justo <a href="#0"> pellentesque facilisis</a>. Etiam imperdiet imperdiet orci.
						Nunc
						nec neque. Phasellus leo dolor, tempus non, auctor et, hendreriter quis, nisi. Curabitur ligula
						sapien,
						tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at
						massa.
						Sed cursus turpis vitae tortor. Donec posuere vulputate arcu.
					</p>
					<a href="#0" class=" separator">
						<i class="icon-next  icon_in-bubble icon_lage "></i>
					</a>
				</div>


			</div>
		</li>
	</ul>
	<h2 class="page-header">Comments</h2>

	<div class="comment-box">
		<div class="bg-main-bg"></div>
		<ol class="commentlist">
			<li class="comment even thread-even depth-1 parent" id="comment-1">
				<div id="div-comment-1" class="comment-body">
					<div class="comment-author vcard">
						<img alt="" src="http://0.gravatar.com/avatar/?d=wavatar&amp;s=80"
								 class="avatar avatar-32 photo avatar-default"> <cite class="fn"><a href="http://wordpress.org/"
																																										rel="external nofollow"
																																										class="url">Mr WordPress</a></cite>
						<span class="says">says:</span>
					</div>


					<p>Hi, this is a comment.<br>
						To delete a comment, just log in and view the post's comments. There you will have the option to
						edit or delete them.</p>

					<div class="reply">
						<a class="comment-reply-link" href="#0"
							 onclick="return addComment.moveForm(&quot;div-comment-1&quot;, &quot;1&quot;, &quot;respond&quot;, &quot;1&quot;)">Reply</a>
					</div>
					<div class="comment-meta commentmetadata"><a href="#0">
							May 29, 2013 at 4:12 am</a>
					</div>
				</div>
				<ul class="children">
					<li class="comment byuser comment-author-admin bypostauthor even depth-3 parent" id="comment-434">
						<div id="div-comment-434" class="comment-body">
							<div class="comment-author vcard">
								<img alt="" src="http://0.gravatar.com/avatar/?d=wavatar&amp;s=80" class="avatar avatar-32 photo">
								<cite class="fn">admin</cite>
								<span class="says">says:</span></div>


							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aperiam asperiores, at consectetur, enim est fugit incidunt non quas quo, saepe tempora temporibus vel. Accusamus adipisci assumenda dolorum iusto sed?</p>

							<div class="reply">
								<a class="comment-reply-link" href="/hello-world/?replytocom=434#respond"
									 onclick="return addComment.moveForm(&quot;div-comment-434&quot;, &quot;434&quot;, &quot;respond&quot;, &quot;1&quot;)">Reply</a>
							</div>
							<div class="comment-meta commentmetadata"><a
										href="#0">
									July 11, 2013 at 2:11 pm</a></div>
						</div>

					</li>
				</ul>
			</li>
		</ol>


	</div>

<?php plex_load_partial( 'view/templates/_partials/footer' ); // Load footer partial ?>