<?php

/**
 * Template Name: Posts Magazine Type 3 Column
 */

/**
 * ========================================================================
 * Posts magazine template page
 * ========================================================================
 */

if ( ! defined( 'ABSPATH' ) ) exit;

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();
$plex_view_transfer->set( 'el_width', 4 );

plex_load_partial( 'posts-magazine-type-2-col.php' );
