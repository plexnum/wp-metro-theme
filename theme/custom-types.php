<?php

$plex_cm = plex_get_theme_conf_bag();

return array(


	array(
		'id'       => 'client',
		'settings' => array(
			'label'               => __( 'Clients', plex_get_trans_domain() ),
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_nav_menus'   => false,
			'query_var'           => false,
			'menu_icon'           => $plex_cm->get( 'urls[assets][fixtures]', false, true ) . '/icons/clients-menu.png',
			'rewrite'             => false,
			'supports'            => array(
				'title',
				'thumbnail'
			),
			'labels'              => array(
				'name'               => __( 'Clients', plex_get_trans_domain() ),
				'singular_name'      => __( 'Client', plex_get_trans_domain() ),
				'add_new'            => __( 'Add New Client', plex_get_trans_domain() ),
				'add_new_item'       => __( 'Add New Client', plex_get_trans_domain() ),
				'edit_item'          => __( 'Edit Client', plex_get_trans_domain() ),
				'new_item'           => __( 'New Client', plex_get_trans_domain() ),
				'view_item'          => __( 'View Client', plex_get_trans_domain() ),
				'search_items'       => __( 'Search Clients', plex_get_trans_domain() ),
				'not_found'          => __( 'No Clients Found', plex_get_trans_domain() ),
				'not_found_in_trash' => __( 'No Clients Found In Trash', plex_get_trans_domain() )
			)
		)
	),

	array(
		'id'       => 'feature',
		'settings' => array(
			'label'               => __( 'Features', plex_get_trans_domain() ),
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_nav_menus'   => false,
			'query_var'           => false,
			'menu_icon'           => $plex_cm->get( 'urls[assets][fixtures]', false, true ) . '/icons/features-menu.png',
			'rewrite'             => false,
			'supports'            => array(
				'title',
				'thumbnail',
				'editor'
			),
			'labels'              => array(
				'name'               => __( 'Features', plex_get_trans_domain() ),
				'singular_name'      => __( 'Feature', plex_get_trans_domain() ),
				'add_new'            => __( 'Add New Feature', plex_get_trans_domain() ),
				'add_new_item'       => __( 'Add New Feature', plex_get_trans_domain() ),
				'edit_item'          => __( 'Edit Feature', plex_get_trans_domain() ),
				'new_item'           => __( 'New Feature', plex_get_trans_domain() ),
				'view_item'          => __( 'View Feature', plex_get_trans_domain() ),
				'search_items'       => __( 'Search Features', plex_get_trans_domain() ),
				'not_found'          => __( 'No Features Found', plex_get_trans_domain() ),
				'not_found_in_trash' => __( 'No Features Found In Trash', plex_get_trans_domain() )
			)

		)
	),

	array(
		'id'       => 'testimonial',
		'settings' => array(
			'label'               => __( 'Testimonials', plex_get_trans_domain() ),
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_nav_menus'   => false,
			'query_var'           => false,
			'menu_icon'           => $plex_cm->get( 'urls[assets][fixtures]', false, true ) . '/icons/testimonials-menu.png',
			'rewrite'             => false,
			'supports'            => array(
				'title',
				'thumbnail'
			),
			'labels'              => array(
				'name'               => __( 'Testimonials', plex_get_trans_domain() ),
				'singular_name'      => __( 'Testimonial', plex_get_trans_domain() ),
				'add_new'            => __( 'Add New Testimonial', plex_get_trans_domain() ),
				'add_new_item'       => __( 'Add New Testimonial', plex_get_trans_domain() ),
				'edit_item'          => __( 'Edit Testimonial', plex_get_trans_domain() ),
				'new_item'           => __( 'New Testimonial', plex_get_trans_domain() ),
				'view_item'          => __( 'View Testimonial', plex_get_trans_domain() ),
				'search_items'       => __( 'Search Testimonials', plex_get_trans_domain() ),
				'not_found'          => __( 'No Testimonials Found', plex_get_trans_domain() ),
				'not_found_in_trash' => __( 'No Testimonials Found In Trash', plex_get_trans_domain() )
			)
		)
	),

	array(
		'id'       => 'service',
		'settings' => array(
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_nav_menus'   => false,
			'query_var'           => false,
			'menu_icon'           => $plex_cm->get( 'urls[assets][fixtures]', false, true ) . '/icons/services-menu.png',
			'rewrite'             => false,
			'supports'            => array(
				'title',
				'thumbnail',
				'editor',
				'excerpt'
			),
			'labels'              => array(
				'name'               => __( 'Services', plex_get_trans_domain() ),
				'singular_name'      => __( 'Service', plex_get_trans_domain() ),
				'add_new'            => __( 'Add New Service', plex_get_trans_domain() ),
				'add_new_item'       => __( 'Add New Service', plex_get_trans_domain() ),
				'edit_item'          => __( 'Edit Service', plex_get_trans_domain() ),
				'new_item'           => __( 'New Service', plex_get_trans_domain() ),
				'view_item'          => __( 'View Service', plex_get_trans_domain() ),
				'search_items'       => __( 'Search Services', plex_get_trans_domain() ),
				'not_found'          => __( 'No Services Found', plex_get_trans_domain() ),
				'not_found_in_trash' => __( 'No Services Found In Trash', plex_get_trans_domain() )
			)
		)
	),

	array(
		'id'       => 'team',
		'settings' => array(
			'label'               => __( 'Team', plex_get_trans_domain() ),
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_nav_menus'   => false,
			'query_var'           => false,
			'menu_icon'           => $plex_cm->get( 'urls[assets][fixtures]', false, true ) . '/icons/team-menu.png',
			'rewrite'             => false,
			'supports'            => array(
				'title',
				'thumbnail'
			),
			'labels'              => array(
				'name'               => __( 'Team', plex_get_trans_domain() ),
				'singular_name'      => __( 'Team', plex_get_trans_domain() ),
				'add_new'            => __( 'Add New Member', plex_get_trans_domain() ),
				'add_new_item'       => __( 'Add New Member', plex_get_trans_domain() ),
				'edit_item'          => __( 'Edit Member', plex_get_trans_domain() ),
				'new_item'           => __( 'New Member', plex_get_trans_domain() ),
				'view_item'          => __( 'View Member', plex_get_trans_domain() ),
				'search_items'       => __( 'Search Members', plex_get_trans_domain() ),
				'not_found'          => __( 'No Members Found', plex_get_trans_domain() ),
				'not_found_in_trash' => __( 'No Members Found In Trash', plex_get_trans_domain() )
			)
		)
	),

	array(
		'id'       => 'project',
		'settings' => array(
			'public'     => true,
			'query_var'  => 'project',
			'taxonomies' => array( 'project_category' ),
			'rewrite'    => array(
				'slug'       => 'projects',
				'with_front' => false
			),
			'menu_icon'           => $plex_cm->get( 'urls[assets][fixtures]', false, true ) . '/icons/projects-menu.png',
			'supports'   => array(
				'title',
				'thumbnail',
				'editor',
				'excerpt',
				'post-formats'
			),
			'labels'     => array(
				'name'               => __( 'Projects', plex_get_trans_domain() ),
				'singular_name'      => __( 'Project', plex_get_trans_domain() ),
				'add_new'            => __( 'Add New Project', plex_get_trans_domain() ),
				'add_new_item'       => __( 'Add New Project', plex_get_trans_domain() ),
				'edit_item'          => __( 'Edit Project', plex_get_trans_domain() ),
				'new_item'           => __( 'New Project', plex_get_trans_domain() ),
				'view_item'          => __( 'View Project', plex_get_trans_domain() ),
				'search_items'       => __( 'Search Projects', plex_get_trans_domain() ),
				'not_found'          => __( 'No Projects Found', plex_get_trans_domain() ),
				'not_found_in_trash' => __( 'No Projects Found In Trash', plex_get_trans_domain() )
			)
		)
	)

);