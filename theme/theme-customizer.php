<?php


return array(

	array(
		'id'      => 'title_tagline',
		'title'   => __( 'Site Title & Tagline', plex_get_trans_domain() ),
		'delete'  => array(
			'blogname',
			'blogdescription'
		),
		'options' => array(

			array(
				'id'                => 'blogname',
				'control'           => array(
					'label' => __( 'Site Title', plex_get_trans_domain() )
				),
				'setting'           => array(
					'transport' => 'postMessage',
					'type'      => 'option'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_TextField()
			),

			array(
				'id'                => 'blogdescription',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_TextArea', array(
					'label' => __( 'Tagline', plex_get_trans_domain() ),
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'type'      => 'option'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_HtmlData()
			),

		)
	),
	array(
		'id'      => 'colors',
		'title'   => __( 'Appearance', plex_get_trans_domain() ),
		'delete'  => array(
			'background_color'
		),
		'options' => array(

			array(
				'id'      => 'plex_color_scheme',
				'control' => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_ColorScheme', array(
					'label'    => __( 'Color Schemes', plex_get_trans_domain() ),
					'priority' => 0
				) ),
				'setting' => array(
					'transport' => 'postMessage',
					'default'   => 'custom',
					'schemes'   => array(
						'default' => array(
							'name'     => __( 'Default', plex_get_trans_domain() ),
							'color'    => '#003366',
							'settings' => array(
								'plex_header_font'              => 'Maven+Pro',
								'plex_h1_font_size'             => 35,
								'plex_h2_font_size'             => 30,
								'plex_h3_font_size'             => 20,
								'plex_h4_font_size'             => 16,
								'plex_h5_font_size'             => 12,
								'plex_h6_font_size'             => 10,
								'plex_site_title_c'							=> '#3096d3',
								'plex_text_font'                => 'Open+Sans',
								'plex_text_font_size'           => 12,
								'plex_main_c'                   => '#ffffff',
								'plex_site_bgc'                 => '#132235',
								'plex_site_sublayer_bgc'        => '#ffffff',
								'plex_site_sublayer_bgc_op'     => 20,
								'plex_btn_type1_bgc'            => '#003366',
								'plex_btn_type1_c'              => '#ffffff',
								'plex_btn_type2_bgc'            => '#2c86bc',
								'plex_btn_type2_c'              => '#ffffff',
								'plex_btn_op_c'                 => '#ffffff',
								'plex_btn_op_bgc_hover'         => '#2c86bc',
								'plex_link_c'                   => '#003366',
								'plex_link_c_in_content'				=> '#ffffff',
								'plex_twitter_bgc'              => '#2c86bc',
								'plex_twitter_c'                => '#ffffff',
								'plex_menu_c'                   => '#ffffff',
								'plex_menu_bgc_hover'           => '#2c86bc',
								'plex_menu_btn_c'               => '#ffffff',
								'plex_tile_type1_bgc'           => '#005487',
								'plex_tile_type1_c'             => '#ffffff',
								'plex_tile_type2_bgc'           => '#2c86bc',
								'plex_tile_type2_c'             => '#ffffff',
								'plex_tile_icon_c'              => '#ffffff',
								'plex_media_element_bgc'        => '#2c86bc',
								'plex_testimonial_bgc'          => '#2c86bc',
								'plex_testimonial_c'            => '#ffffff',
								'plex_testimonial_quotes_c'     => '#003366',
								'plex_blockquote_bgc'           => '#2c86bc',
								'plex_blockquote_quotes_c'      => '#003366',
								'plex_blockquote_author_c'      => '#003366',
								'plex_blockquote_icon_c'        => '#003366',
								'plex_pagination_c_hover'       => '#ffffff',
								'plex_pagination_bgc_hover'     => '#2c86bc',
								'plex_accordion_c'              => '#ffffff',
								'plex_accordion_bgc'            => '#2c86bc',
								'plex_tab_bgc'                  => '#2c86bc',
								'plex_tab_c'										=> '#ffffff',
								'plex_calendar_bgc'             => '#2c86bc',
								'plex_calendar_c'               => '#ffffff',
								'plex_calendar_today_bgc'       => '#ffffff',
								'plex_calendar_today_c'         => '#2c86bc',
								'plex_progress_bar_c'           => '#ffffff',
								'plex_progress_bar_bgc'         => '#2c86bc',
								'plex_selector_bgc'             => '#ffffff',
								'plex_selector_c'               => '#2c86bc',
								'plex_blog_header_date_bgc'     => '#2c86bc',
								'plex_blog_header_date_c'       => '#ffffff',
								'plex_sticky_post_bgc'          => '#2c86bc',
								'plex_sticky_post_date_bgc'     => '#003366',
								'plex_sticky_post_date_c'       => '#ffffff',
								'plex_bypostauthor_comment_bgc' => '#2c86bc'
							)
						),
						'turquoise' => array(
							'name'     => __( 'Turquoise', plex_get_trans_domain() ),
							'color'    => '#36ABA2',
							'settings' => array(
								'plex_accordion_bgc'            => "#127069",
								'plex_accordion_c'              => "#ffffff",
								'plex_blockquote_author_c'      => "#ff9c50",
								'plex_blockquote_bgc'           => "#127069",
								'plex_blockquote_icon_c'        => "#ff9c50",
								'plex_blockquote_quotes_c'      => "#ff9c50",
								'plex_blog_header_date_bgc'     => "#127069",
								'plex_blog_header_date_c'       => "#ffffff",
								'plex_btn_op_bgc_hover'         => "#ff9c50",
								'plex_btn_op_c'                 => "#ffffff",
								'plex_btn_type1_bgc'            => "#3e817c",
								'plex_btn_type1_c'              => "#ffffff",
								'plex_btn_type2_bgc'            => "#ff9c50",
								'plex_btn_type2_c'              => "#ffffff",
								'plex_bypostauthor_comment_bgc' => "#127069",
								'plex_calendar_bgc'             => "#127069",
								'plex_calendar_c'               => "#ffffff",
								'plex_calendar_today_bgc'       => "#ffffff",
								'plex_calendar_today_c'         => "#a7581b",
								'plex_h1_font_size'             => 35,
								'plex_h2_font_size'             => 30,
								'plex_h3_font_size'             => 20,
								'plex_h4_font_size'             => 16,
								'plex_h5_font_size'             => 12,
								'plex_h6_font_size'             => 10,
								'plex_site_title_c'							=> '#69d6ce',
								'plex_header_font'              => "Maven+Pro",
								'plex_link_c'                   => "#ff9c50",
								'plex_link_c_in_content'				=> '#ffffff',
								'plex_main_c'                   => "#ffffff",
								'plex_media_element_bgc'        => "#127069",
								'plex_menu_bgc_hover'           => "#ff9c50",
								'plex_menu_btn_c'               => "#ffffff",
								'plex_menu_c'                   => "#ffffff",
								'plex_pagination_bgc_hover'     => "#ff9c50",
								'plex_pagination_c_hover'       => "#ffffff",
								'plex_progress_bar_bgc'         => "#127069",
								'plex_progress_bar_c'           => "#ffffff",
								'plex_selector_bgc'             => "#ffffff",
								'plex_selector_c'               => "#127069",
								'plex_site_bgc'                 => "#127069",
								'plex_site_sublayer_bgc'        => "#ffffff",
								'plex_site_sublayer_bgc_op'     => 20,
								'plex_sticky_post_bgc'          => "#127069",
								'plex_sticky_post_c'            => "#ffffff",
								'plex_sticky_post_date_bgc'     => "#a7581b",
								'plex_sticky_post_date_c'       => "#ffffff",
								'plex_tab_bgc'                  => "#127069",
								'plex_tab_c'										=> '#ffffff',
								'plex_testimonial_bgc'          => "#127069",
								'plex_testimonial_c'            => "#ffffff",
								'plex_testimonial_quotes_c'     => "#ff9c50",
								'plex_text_font'                => "Open+Sans",
								'plex_text_font_size'           => 12,
								'plex_tile_icon_c'              => "#ffffff",
								'plex_tile_type1_bgc'           => "#127069",
								'plex_tile_type1_c'             => "#ffffff",
								'plex_tile_type2_bgc'           => "#69d6ce",
								'plex_tile_type2_c'             => "#ffffff",
								'plex_twitter_bgc'              => "#2c86bc",
								'plex_twitter_c'                => "#ffffff"
							)
						),
						'emerald' => array(
							'name'     => __( 'Emerald', plex_get_trans_domain() ),
							'color'    => '#0f8743',
							'settings' => array(
								'plex_accordion_bgc'            => "#4e42c9",
								'plex_accordion_c'              => "#ffffff",
								'plex_blockquote_author_c'      => "#ffff38",
								'plex_blockquote_bgc'           => "#4e42c9",
								'plex_blockquote_icon_c'        => "#ffff38",
								'plex_blockquote_quotes_c'      => "#ffff38",
								'plex_blog_header_date_bgc'     => "#4e42c9",
								'plex_blog_header_date_c'       => "#ffffff",
								'plex_btn_op_bgc_hover'         => "#ffff38",
								'plex_btn_op_c'                 => "#ffffff",
								'plex_btn_type1_bgc'            => "#4e42c9",
								'plex_btn_type1_c'              => "#ffffff",
								'plex_btn_type2_bgc'            => "#ffff38",
								'plex_btn_type2_c'              => "#0a0a0a",
								'plex_bypostauthor_comment_bgc' => "#a8a813",
								'plex_calendar_bgc'             => "#4e42c9",
								'plex_calendar_c'               => "#ffffff",
								'plex_calendar_today_bgc'       => "#ffffff",
								'plex_calendar_today_c'         => "#4e42c9",
								'plex_h1_font_size'             => 35,
								'plex_h2_font_size'             => 30,
								'plex_h3_font_size'             => 20,
								'plex_h4_font_size'             => 16,
								'plex_h5_font_size'             => 12,
								'plex_h6_font_size'             => 10,
								'plex_site_title_c'							=> '#4e42c9',
								'plex_header_font'              => "Maven+Pro",
								'plex_link_c'                   => "#4e42c9",
								'plex_link_c_in_content'				=> '#ffffff',
								'plex_main_c'                   => "#ffffff",
								'plex_media_element_bgc'        => "#4e42c9",
								'plex_menu_bgc_hover'           => "#4e42c9",
								'plex_menu_btn_c'               => "#ffffff",
								'plex_menu_c'                   => "#ffffff",
								'plex_pagination_bgc_hover'     => "#ffff38",
								'plex_pagination_c_hover'       => "#0a0a0a",
								'plex_progress_bar_bgc'         => "#4e42c9",
								'plex_progress_bar_c'           => "#ffffff",
								'plex_selector_bgc'             => "#ffffff",
								'plex_selector_c'               => "#4e42c9",
								'plex_site_bgc'                 => "#0f8743",
								'plex_site_sublayer_bgc'        => "#ffffff",
								'plex_site_sublayer_bgc_op'     => 20,
								'plex_sticky_post_bgc'          => "#4e42c9",
								'plex_sticky_post_c'            => "#ffffff",
								'plex_sticky_post_date_bgc'     => "#201685",
								'plex_sticky_post_date_c'       => "#ffffff",
								'plex_tab_bgc'                  => "#4e42c9",
								'plex_tab_c'										=> '#ffffff',
								'plex_testimonial_bgc'          => "#4e42c9",
								'plex_testimonial_c'            => "#ffffff",
								'plex_testimonial_quotes_c'     => "#ffff38",
								'plex_text_font'                => "Open+Sans",
								'plex_text_font_size'           => 12,
								'plex_tile_icon_c'              => "#ffffff",
								'plex_tile_type1_bgc'           => "#4e42c9",
								'plex_tile_type1_c'             => "#ffffff",
								'plex_tile_type2_bgc'           => "#201685",
								'plex_tile_type2_c'             => "#ffffff",
								'plex_twitter_bgc'              => "#2c86bc",
								'plex_twitter_c'                => "#ffffff"
							)
						)


					)
				)
			),

			array(
				'id'                => 'plex_header_font',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_Fonts', array(
					'label'    => __( 'Headers font', plex_get_trans_domain() ),
					'priority' => 5,
					'header'   => __( 'Headers', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => 'Maven+Pro',
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_FontsResolver()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Attr()
			),

			array(
				'id'                => 'plex_h1_font_size',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_Number', array(
					'label'    => __( 'H1 font size', plex_get_trans_domain() ),
					'priority' => 10
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => 35,
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_PixelResolver()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_AbsInt()
			),

			array(
				'id'                => 'plex_h2_font_size',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_Number', array(
					'label'    => __( 'H2 font size', plex_get_trans_domain() ),
					'priority' => 20
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => 30,
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_PixelResolver()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_AbsInt()
			),

			array(
				'id'                => 'plex_h3_font_size',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_Number', array(
					'label'    => __( 'H3 font size', plex_get_trans_domain() ),
					'priority' => 30
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => 20,
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_PixelResolver()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_AbsInt()
			),

			array(
				'id'                => 'plex_h4_font_size',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_Number', array(
					'label'    => __( 'H4 font size', plex_get_trans_domain() ),
					'priority' => 40
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => 16,
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_PixelResolver()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_AbsInt()
			),

			array(
				'id'                => 'plex_h5_font_size',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_Number', array(
					'label'    => __( 'H5 font size', plex_get_trans_domain() ),
					'priority' => 50
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => 12,
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_PixelResolver()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_AbsInt()
			),

			array(
				'id'                => 'plex_h6_font_size',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_Number', array(
					'label'    => __( 'H6 font size', plex_get_trans_domain() ),
					'priority' => 60
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => 10,
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_PixelResolver()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_AbsInt()
			),

			array(
				'id'                => 'plex_site_title_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Site title text color', plex_get_trans_domain() ),
					'priority' => 65,
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#3096d3'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_text_font',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_Fonts', array(
					'label'    => __( 'Text font', plex_get_trans_domain() ),
					'priority' => 70,
					'header'   => __( 'Text', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => 'Open+Sans',
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_FontsResolver()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Attr()
			),

			array(
				'id'                => 'plex_text_font_size',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_Number', array(
					'label'    => __( 'Text font size', plex_get_trans_domain() ),
					'priority' => 80
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => 12,
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_PixelResolver()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_AbsInt()
			),

			array(
				'id'                => 'plex_main_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Text color', plex_get_trans_domain() ),
					'priority' => 90
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_site_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Site main background', plex_get_trans_domain() ),
					'priority' => 100,
					'header'   => __( 'Site backgrounds', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#132235'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_site_sublayer_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Sub layer background', plex_get_trans_domain() ),
					'priority' => 110
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_site_sublayer_bgc_op',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_Number', array(
					'label'    => __( 'Sub layer opacity', plex_get_trans_domain() ),
					'priority' => 120,
					'min'      => 0,
					'max'      => 100
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => 20,
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_SubLayerOpacityResolver()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_AbsInt(),
			),

			array(
				'id'                => 'plex_btn_type1_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Button type 1 background', plex_get_trans_domain() ),
					'priority' => 130,
					'header'   => __( 'Buttons', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#003366',
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_MakeHoverDurker()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_btn_type1_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Button type 1 text color', plex_get_trans_domain() ),
					'priority' => 140
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_btn_type2_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Button type 2 background', plex_get_trans_domain() ),
					'priority' => 150
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc',
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_MakeHoverDurker()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),
			array(
				'id'                => 'plex_btn_type2_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Button type 2 text color', plex_get_trans_domain() ),
					'priority' => 160
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_btn_op_bgc_hover',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Opacity button background hover color', plex_get_trans_domain() ),
					'priority' => 170
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_btn_op_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Opacity button color', plex_get_trans_domain() ),
					'priority' => 180
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),


			array(
				'id'                => 'plex_link_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Links color', plex_get_trans_domain() ),
					'priority' => 190,
					'header'   => __( 'Links', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#003366',
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_MakeHoverDurker()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_link_c_in_content',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Links color in a content', plex_get_trans_domain() ),
					'priority' => 195,
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff',
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_MakeHoverDurker()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_twitter_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Twitter widget background color', plex_get_trans_domain() ),
					'priority' => 200,
					'header'   => __( 'Twitter widget', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_twitter_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Twitter widget text color', plex_get_trans_domain() ),
					'priority' => 205
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_menu_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Menu text color', plex_get_trans_domain() ),
					'priority' => 210,
					'header'   => __( 'Menu', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_menu_bgc_hover',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Menu hover background color', plex_get_trans_domain() ),
					'priority' => 215
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_menu_btn_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Menu button color', plex_get_trans_domain() ),
					'priority' => 218
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_tile_type1_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Tile background color type 1', plex_get_trans_domain() ),
					'priority' => 220,
					'header'   => __( 'Tile', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#005487'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_tile_type1_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Tile text color type 1', plex_get_trans_domain() ),
					'priority' => 230
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_tile_type2_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Tile background color type 2', plex_get_trans_domain() ),
					'priority' => 240
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_tile_type2_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Tile text color type 2', plex_get_trans_domain() ),
					'priority' => 250
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_tile_icon_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Tile icon color', plex_get_trans_domain() ),
					'priority' => 260
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_media_element_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Media element background color', plex_get_trans_domain() ),
					'priority' => 270,
					'header'   => __( 'Media element', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_testimonial_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Testimonials background color', plex_get_trans_domain() ),
					'priority' => 280,
					'header'   => __( 'Testimonials', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_testimonial_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Testimonials text color', plex_get_trans_domain() ),
					'priority' => 290
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_testimonial_quotes_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Testimonials quotes color', plex_get_trans_domain() ),
					'priority' => 300
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#003366'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_blockquote_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Block quote background color', plex_get_trans_domain() ),
					'priority' => 310,
					'header'   => __( 'Block quote', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_blockquote_quotes_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Block quote text color', plex_get_trans_domain() ),
					'priority' => 320
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#003366'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_blockquote_author_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Block quote author text color', plex_get_trans_domain() ),
					'priority' => 330
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#003366'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_blockquote_icon_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Block quote icon color', plex_get_trans_domain() ),
					'priority' => 330
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#003366'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_pagination_c_hover',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Pagination hover text color', plex_get_trans_domain() ),
					'priority' => 340,
					'header'   => __( 'Pagination', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_pagination_bgc_hover',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Pagination hover background color', plex_get_trans_domain() ),
					'priority' => 350
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_accordion_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Accordion background color', plex_get_trans_domain() ),
					'priority' => 360,
					'header'   => __( 'Accordion', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc',
					'resolver'  => new Plex_Component_ThemeCustomizer_Resolver_MakeHoverDurker()
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_accordion_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Accordion text color', plex_get_trans_domain() ),
					'priority' => 370
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),


			array(
				'id'                => 'plex_tab_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Tab background color', plex_get_trans_domain() ),
					'priority' => 380,
					'header'   => __( 'Tabs', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_tab_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Tab color', plex_get_trans_domain() ),
					'priority' => 385,
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_calendar_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Calendar background color', plex_get_trans_domain() ),
					'priority' => 390,
					'header'   => __( 'Calendar', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_calendar_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Calendar text color', plex_get_trans_domain() ),
					'priority' => 395
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_calendar_today_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Calendar today background color', plex_get_trans_domain() ),
					'priority' => 400
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_calendar_today_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Calendar today text color', plex_get_trans_domain() ),
					'priority' => 410
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_progress_bar_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Progress bar text color', plex_get_trans_domain() ),
					'priority' => 420,
					'header'   => __( 'Progress bar', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_progress_bar_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Progress bar background color', plex_get_trans_domain() ),
					'priority' => 430
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_selector_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Custom selector background color', plex_get_trans_domain() ),
					'priority' => 440,
					'header'   => __( 'Custom selector', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_selector_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Custom selector text color', plex_get_trans_domain() ),
					'priority' => 450
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_blog_header_date_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Posts header date background color', plex_get_trans_domain() ),
					'priority' => 460,
					'header'   => __( 'Posts', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_blog_header_date_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Posts header date text color', plex_get_trans_domain() ),
					'priority' => 470
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_sticky_post_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Sticky posts background color', plex_get_trans_domain() ),
					'priority' => 480
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_sticky_post_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Sticky posts text color', plex_get_trans_domain() ),
					'priority' => 485
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_sticky_post_date_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Sticky posts header date background color', plex_get_trans_domain() ),
					'priority' => 490
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#003366'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_sticky_post_date_c',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'Sticky posts header date text color', plex_get_trans_domain() ),
					'priority' => 500
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#ffffff'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			),

			array(
				'id'                => 'plex_bypostauthor_comment_bgc',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_SyncColor', array(
					'label'    => __( 'By post author comment background', plex_get_trans_domain() ),
					'priority' => 510,
					'header'   => __( 'Comments', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage',
					'default'   => '#2c86bc'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Color(),
			)

		)
	),

	array(
		'id'      => 'plex_background_image',
		'title'   => __( 'Main Page Background Image', plex_get_trans_domain() ),
		'options' => array(
			array(
				'id'      => 'background_image',
				'control' => plex_build_control(
					'WP_Customize_Background_Image_Control',
					array(
						'label' => _x( 'Background Image', 'control_title', plex_get_trans_domain() )
					)
				),
				'setting' => array(
					'transport' => 'postMessage'
				)
			)
		)
	),
	array(
		'id'       => 'plex_logo_section',
		'title'    => _x( 'Logo settings', 'title', plex_get_trans_domain() ),
		'desc'     => _x( 'Logo settings', 'description', plex_get_trans_domain() ),
		'priority' => 0,
		'options'  => array(
			array(
				'id'                => 'plex_logo_image',
				'control'           => plex_build_control( 'WP_Customize_Image_Control', array(
					'label' => _x( 'Logo image', 'control_title', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_Url()
			)
		)
	),
	array(
		'id'       => 'plex_social_section',
		'title'    => _x( 'Social', 'title', plex_get_trans_domain() ),
		'desc'     => _x( 'Social', 'description', plex_get_trans_domain() ),
		'priority' => 1001,
		'options'  => array(
			array(
				'id'             => 'plex_social',
				'control'        => plex_build_control(
					'Plex_Component_ThemeCustomizer_Social_Control',
					array(
						'label' => 'My Control'
					) ),
				'setting'        => array(
					'transport' => 'refresh'
				),
				'sanitize_rules' => array(
					'*plex_social' => array(
						'link' => 'url',
						'icon' => 'text_field'
					)
				)
			)

		)
	),
	array(
		'id'       => 'plex_copyright',
		'title'    => __( 'Copyrights', plex_get_trans_domain() ),
		'desc'     => __( 'Copyrights', plex_get_trans_domain() ),
		'priority' => 1020,
		'options'  => array(
			array(
				'id'                => 'plex_copyright',
				'control'           => plex_build_control( 'Plex_Component_ThemeCustomizer_Control_TextArea', array(
					'label' => __( 'Copyrights', plex_get_trans_domain() )
				) ),
				'setting'           => array(
					'transport' => 'postMessage'
				),
				'sanitize_strategy' => new Plex_Lib_Sanitizer_Strategies_HtmlData()
			)
		)
	)
);