<?php

// Register meta boxes
return array(
	new Plex_MetaBox_Icon(),
	new Plex_MetaBox_FeaturedVideo(),
	new Plex_MetaBox_FeaturedGallery(),
	new Plex_MetaBox_FeaturedAudio(),
	new Plex_MetaBox_Social(),
	new Plex_MetaBox_Team(),
	new Plex_MetaBox_SideBarsSettings(),
	new Plex_MetaBox_ExternalLink(),
	new Plex_MetaBox_ServiceCharge(),
	new Plex_MetaBox_Testimonial()
);