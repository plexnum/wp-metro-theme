<?php

return array(

	// Posts left sidebar
	array(
		'name'          => __( 'Posts Left Sidebar', plex_get_trans_domain() ),
		'id'            => 'posts-left-sidebar',
		'description'   => __( 'Posts Left Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>'
	),

	// Posts right sidebar
	array(
		'name'          => __( 'Posts Right Sidebar', plex_get_trans_domain() ),
		'id'            => 'posts-right-sidebar',
		'description'   => __( 'Posts Right Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),

	// Post left sidebar
	array(
		'name'          => __( 'Post Left Sidebar', plex_get_trans_domain() ),
		'id'            => 'post-left-sidebar',
		'description'   => __( 'Post Left Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),

	// Post right sidebar
	array(
		'name'          => __( 'Post Right Sidebar', plex_get_trans_domain() ),
		'id'            => 'post-right-sidebar',
		'description'   => __( 'Post Right Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),

	// Page left sidebar
	array(
		'name'          => __( 'Page Left Sidebar', plex_get_trans_domain() ),
		'id'            => 'page-left-sidebar',
		'description'   => __( 'Page Left Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),

	// Page right sidebar
	array(
		'name'          => __( 'Page Right Sidebar', plex_get_trans_domain() ),
		'id'            => 'page-right-sidebar',
		'description'   => __( 'Page Right Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),


	// Front page sidebar
	array(
		'name'          => __( 'Front page sidebar', plex_get_trans_domain() ),
		'id'            => 'front-page-sidebar',
		'description'   => __( 'Front page sidebar', plex_get_trans_domain() ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	),

	// Search left sidebar
	array(
		'name'          => __( 'Search Left Sidebar', plex_get_trans_domain() ),
		'id'            => 'search-left-sidebar',
		'description'   => __( 'Search Left Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),

	// Search right sidebar
	array(
		'name'          => __( 'Search Right Sidebar', plex_get_trans_domain() ),
		'id'            => 'search-right-sidebar',
		'description'   => __( 'Search Right Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),


	// Projects left sidebar
	array(
		'name'          => __( 'Projects Left Sidebar', plex_get_trans_domain() ),
		'id'            => 'projects-left-sidebar',
		'description'   => __( 'Projects Left Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),

	// Projects right sidebar
	array(
		'name'          => __( 'Projects Right Sidebar', plex_get_trans_domain() ),
		'id'            => 'projects-right-sidebar',
		'description'   => __( 'Projects Right Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),

	// Project left sidebar
	array(
		'name'          => __( 'Project Left Sidebar', plex_get_trans_domain() ),
		'id'            => 'project-left-sidebar',
		'description'   => __( 'Project Left Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),

	// Project right sidebar
	array(
		'name'          => __( 'Project Right Sidebar', plex_get_trans_domain() ),
		'id'            => 'project-right-sidebar',
		'description'   => __( 'Project Right Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),

	// Categories left sidebar
	array(
		'name'          => __( 'Filter by date, category, tag Left Sidebar', plex_get_trans_domain() ),
		'id'            => 'filter-left-sidebar',
		'description'   => __( 'Filter by date, category, tag Left Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	),

	// Categories right sidebar
	array(
		'name'          => __( 'Filter by date, category, tag Right Sidebar', plex_get_trans_domain() ),
		'id'            => 'filter-right-sidebar',
		'description'   => __( 'Filter by date, category, tag Left Sidebar', plex_get_trans_domain() ),
		'before_widget' => '<div class="content mb20 js-animate"><div class="bg-main-bg"></div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="mt0">',
		'after_title'   => '</h2>',
	)


);