<?php
// Define widgets which will be used in theme
return array(
	'Plex_Widget_TwitterFeedsWidget',
	'Plex_Widget_Metro_Image',
	'Plex_Widget_Metro_Icon',
	'Plex_Widget_Metro_VimeoVideo',
	'Plex_Widget_Metro_YoutubeVideo',
	'Plex_Widget_Gallery',
	'Plex_Widget_PopularPosts',
	'Plex_Widget_Metro_ShortCode',
	array( 'WP_Widget_Archives', 'Plex_Widget_Archives' ),
	array( 'WP_Widget_Categories', 'Plex_Widget_Categories' ),
	array( 'WP_Widget_Pages', 'Plex_Widget_Pages' ),
	array( 'WP_Widget_Recent_Comments', 'Plex_Widget_RecentComments' ),
	array( 'WP_Widget_Meta', 'Plex_Widget_Meta' ),
	array( 'WP_Widget_Recent_Posts', 'Plex_Widget_RecentPosts' ),
	array( 'WP_Widget_RSS', 'Plex_Widget_Rss' ),
	array( 'WP_Widget_Search', 'Plex_Widget_Search' ),
	array( 'WP_Widget_Tag_Cloud', 'Plex_Widget_TagCloud' ),
	array( 'WP_Widget_Calendar', 'Plex_Widget_Calendar' ),
	array( 'WP_Nav_Menu_Widget', 'Plex_Widget_Menu' ),
	array( 'Plex_Widget_Text', 'Plex_Widget_Text' )
	//'Plex_Widget_Gallery',
);