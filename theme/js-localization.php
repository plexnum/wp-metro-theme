<?php

return array(
	'admin' => array(
		'media_uploader_title'      => __( 'Media Uploader', plex_get_trans_domain() ),
		'select'                    => __( 'Select', plex_get_trans_domain() ),
		'select_video'              => __( 'Select video', plex_get_trans_domain() ),
		'select_audio'              => __( 'Select audio', plex_get_trans_domain() ),
		'upload_img'                => __( 'Upload image', plex_get_trans_domain() ),
		'service_charge_validation' => __( 'You should type Service charge', plex_get_trans_domain() ),
		'icon_validation'           => __( 'You should select icon first', plex_get_trans_domain() ),
		'short_code_live_editor'    => __( 'Short Code Live Editor', plex_get_trans_domain() ),
		'short_code_edit'           => __( 'Short code edit', plex_get_trans_domain() ),
		'insert'                    => __( 'Insert', plex_get_trans_domain() ),
		'cancel_editing'            => __( '&larr; Cancel Editing', plex_get_trans_domain() ),

	),
	'theme' => array(
		'assetUrl' => plex_get_assets_url()
	)
);