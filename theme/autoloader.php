<?php

if ( ! defined( 'ABSPATH' ) ) exit;
require PLEX_THEME_DIR . '/src/plex/auto-loader/loader.php';
$plex_autoloader = new Plex_AutoLoader_Loader();

// Register mapping of prefixes to directories
$plex_autoloader->register_prefixes( array(
	'Plex_' => PLEX_THEME_DIR . '/src'
) );

// Register autoloader
$plex_autoloader->register();