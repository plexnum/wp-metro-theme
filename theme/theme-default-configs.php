<?php
// default theme configurations
return array(

	// Theme common options
	'plex_metro_theme' => array(
	),

	// Twitter default options
	'plex_settings' => array(
		'twitter_consumer_key' => '',
		'twitter_consumer_secret' => '',
		'twitter_access_token'	=> '',
		'twitter_access_token_secret' => '',
		'twitter_cache_expire'	=> 3600,
		'google_api_key' => ''
	)

);