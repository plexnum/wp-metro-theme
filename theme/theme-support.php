<?php
return array(
	'post-thumbnails' 	=> array(),
	'post-formats'			=> array('image',
															 'gallery',
															 'video',
															 'audio')
);