<?php

$plex_cm              = plex_get_theme_conf_bag();
$plex_asset_js        = $plex_cm->get( 'urls[assets][js]', '', true );
$plex_asset_css       = $plex_cm->get( 'urls[assets][css]', '', true );
$plex_asset_icons     = $plex_cm->get( 'urls[assets][icons]', '', true );
$plex_asset_framework = $plex_cm->get( 'urls[assets][frameworks]', '', true );

/**
 *  All assets should be required in or after that hooks:
 *  1) 'wp_enqueue_scripts',
 *  2) 'admin_enqueue_scripts',
 *  3) 'customize_controls_enqueue_scripts',
 *  4) 'plex_short_code_preview_enquire_scripts'
 *
 *  if SCRIPT_DEBUG set to true than not minified version will be used
 *
 *
 */

return array(

	// STYLES
	'styles'  => array(

		// backend main css file
		'plex_admin'                    => array(
			'src'   => $plex_asset_css . '/admin/admin.css',
			'hooks' => array(
				'admin_enqueue_scripts',
				'customize_controls_print_styles'
			)
		),

		// icons
		'plex_icons'                    => array(
			'src'   => $plex_asset_icons . '/style.css',
			'hooks' => array(
				'customize_controls_print_styles'
			)
		),

		// frontend styles
		'plex_style'                    => array(
			'src'   => $plex_asset_css . '/style.css',
			'deps'  => array(
				'plex_style_settings'
			),
			'hooks' => array(
				'wp_enqueue_scripts',
				'plex_short_code_preview_enquire_scripts'
			)
		),

		// frontend styles settings should be loaded after frontend styles to customize them
		'plex_style_settings'           => array(
			'src'   => $plex_asset_css . '/style-settings.css'
		),

		// font selector for theme customizer
		'plex_fontselect_jquery_plugin' => array(
			'src'   => $plex_asset_js . '/libs/fontselect-jquery-plugin/fontselect.css',
			'hooks' => array(
				'customize_controls_print_styles'
			)
		),

		// iframe grid for short code preview
		'plex_iframe_grid'              => array(
			'src'   => $plex_asset_css . '/iframe-grid.css',
			'deps'  => array(
				'plex_style',
				'plex_style_settings'
			),
			'hooks' => array(
				'plex_short_code_preview_enquire_scripts_clients',
				'plex_short_code_preview_enquire_scripts_team',
				'plex_short_code_preview_enquire_scripts_row',
				'plex_short_code_preview_enquire_scripts_services',
				'plex_short_code_preview_enquire_scripts_testimonials'
			)
		),

		// libraries and plugins
		'jquery_ui_flick'               => array(
			'src' => $plex_asset_framework . '/jquery-ui/flick/jquery-ui-1.10.3.custom.min.css'
		)


	),

	// SCRIPTS
	'scripts' => array(

		// font select plugin for theme customizer
		'plex_fontselect_jquery_plugin'     => array(
			//'src'   => $plex_asset_js . '/libs/fontselect-jquery-plugin/jquery.fontselect.min.js',
			'src'   => $plex_asset_js . '/libs/fontselect-jquery-plugin/jquery.fontselect.js',
			'deps'  => array(
				'jquery'
			),
			'hooks' => array(
				'customize_controls_print_scripts'
			)
		),

		// theme customizer
		'plex_themecustomizer'              => array(
			'src'   => $plex_asset_js . '/admin/theme-customizer.min.js',
			'deps'  => array(
				'jquery',
				'plex_fontselect_jquery_plugin',
				'plex_jquery_ba-throttle-debounce'
			),
			'hooks' => array(
				'customize_controls_print_scripts'
			)
		),

		// theme customizer preview
		'plex_themecustomizer_preview'      => array(
			//'src'   => $plex_asset_js . '/admin/theme-customizer-preview.min.js',
			'src'   => $plex_asset_js . '/admin/theme-customizer-preview.js',
			'deps'  => array(
				'jquery',
				'underscore',
				'plex_admin',
				'customize-preview'
			),
			'hooks' => array(
				'customize_preview_init'
			)
		),

		// main script for backend
		'plex_admin'                        => array(
			'src'   => $plex_asset_js . '/admin/admin.min.js',
			'deps'  => array(
				'jquery',
				'jquery-ui-datepicker',
				'bootstrap_tooltip',
				'plex_jquery_ba-throttle-debounce',
				'underscore'
			),
			'hooks' => array(
				'admin_enqueue_scripts',
				'customize_controls_print_scripts'
			)
		),

		// main script for frontend
		'plex_main'                         => array(
			'src'   => $plex_asset_js . '/main.min.js',
			'deps'  => array(
				'jquery',
				'plex_jquery_fancybox',
				'plex_jquery_fancybox_media_helper',
				'plex_jquery_appear',
				'plex_plugins',
				'plex_carou-fred-sel',
				'plex_jquery_uniform',
				'plex_jquery_transit',
				'plex_jquery_mousewheel',
				'plex_jquery_touch-swipe',
				'plex_jquery_ba-throttle-debounce',
				'plex_jquery_masonry',
				'plex_placeholder',
				'plex_modernizer',
				'plex_images_loaded',
				'plex_jquery_equalize',
				'plex_bootstrap_transition',
				'plex_bootstrap_tab',
				'plex_bootstrap_alert',
				'plex_bootstrap_collapse',
				'plex_animationend',
				'plex_baron'
			),
			'hooks' => array(
				'wp_enqueue_scripts',
				'plex_short_code_preview_enquire_scripts'
			)
		),

		// gallery customizer
		'plex_admin_gallery_customizer'     => array(
			'src'   => $plex_asset_js . '/admin/gallery-customizer.min.js',
			'deps'  => array(
				'jquery',
				'plex_admin',
				'media-editor'
			),
			'hooks' => array(
				'admin_print_scripts-widgets.php',
				'admin_print_scripts-post.php',
				'admin_print_scripts-post-new.php'
			)
		),

		// short codes builder
		'plex_admin_short_codes_builder'    => array(
			'src'   => $plex_asset_js . '/admin/short-codes-builder.min.js',
			'deps'  => array(
				'jquery',
				'plex_admin',
				'media-editor',
				'plex_jquery_serialize_form'
			),
			'hooks' => array(
				'admin_print_scripts-post.php',
				'admin_print_scripts-post-new.php',
			)
		),

		'plex_admin_short_code_frame'       => array(
			'src'   => $plex_asset_js . '/admin/short-code-frame.min.js',
			'deps'  => array(
				'plex_admin_short_codes_builder',
				'plex_admin_gallery_customizer'
			),
			'hooks' => array(
				'admin_print_scripts-widgets.php'
			)
		),

		'plex_jquery_ba-throttle-debounce'  => array(
			'src'  => $plex_asset_js . '/libs/jquery.ba-throttle-debounce.min.js',
			'deps' => array(
				'jquery'
			)
		),

		// libraries
		'bootstrap_tooltip'                 => array(
			'src' => $plex_asset_framework . '/bootstrap/js/bootstrap-tooltip.min.js',
		),

		'plex_modernizer'                   => array(
			'src'       => $plex_asset_js . '/vendor/modernizr-2.6.2.min.js',
			'in_footer' => false
		),

		'plex_placeholder'                  => array(
			'src'  => $plex_asset_js . '/vendor/jquery.placeholder.min.js',
			'deps' => array(
				'jquery'
			)
		),

		'plex_jquery_masonry'               => array(
			'src'  => $plex_asset_js . '/jquery.masonry.min.js',
			'deps' => array(
				'jquery'
			)
		),

		'plex_jquery_touch-swipe'           => array(
			'src'  => $plex_asset_js . '/libs/jquery.touchSwipe.min.js',
			'deps' => array(
				'jquery'
			)
		),
		'plex_jquery_mousewheel'            => array(
			'src'  => $plex_asset_js . '/libs/jquery.mousewheel.min.js',
			'deps' => array(
				'jquery'
			)
		),
		'plex_jquery_transit'               => array(
			'src'  => $plex_asset_js . '/libs/jquery.transit.min.js',
			'deps' => array(
				'jquery'
			)
		),
		'plex_jquery_uniform'               => array(
			'src'  => $plex_asset_js . '/plugin/uniform/jquery.uniform.min.js',
			'deps' => array(
				'jquery'
			)
		),
		'plex_carou-fred-sel'               => array(
			'src'  => $plex_asset_js . '/plugin/carouFredSel/jquery.carouFredSel-6.2.1.min.js',
			'deps' => array(
				'jquery'
			)
		),

		'plex_plugins'                      => array(
			'src'  => $plex_asset_js . '/plugins.min.js',
			'deps' => array(
				'jquery'
			)
		),

		/*'plex_jquery_media-element'         => array(
			'src'       => $plex_asset_js . '/plugin/mediaelementjs/build/mediaelement-and-player.min.js',
			'deps'      => array(
				'jquery'
			),
			'in_footer' => false // include in header
		),*/

		'plex_jquery_fancybox'              => array(
			'src'  => $plex_asset_js . '/plugin/fancyBox/source/jquery.fancybox.min.js',
			'deps' => array(
				'jquery'
			)
		),

		'plex_jquery_fancybox_media_helper' => array(
			'src'  => $plex_asset_js . '/plugin/fancyBox/source/helpers/jquery.fancybox-media.min.js',
			'deps' => array(
				'plex_jquery_fancybox'
			)
		),


		'plex_jquery_appear'                => array(
			'src'  => $plex_asset_js . '/libs/jquery.appear.min.js',
			'deps' => array(
				'jquery'
			)
		),

		'plex_jquery_equalize'              => array(
			'src'  => $plex_asset_js . '/libs/equalize.min.js',
			'deps' => array(
				'jquery'
			)
		),

		'plex_images_loaded'                => array(
			'src'  => $plex_asset_js . '/libs/imagesloaded.min.js',
			'deps' => array(
				'jquery'
			)
		),

		'plex_jquery_serialize_form'        => array(
			'src'   => $plex_asset_js . '/libs/serializeForm.min.js',
			'hooks' => 'customize_controls_print_scripts'
		),

		'jquery-ui-sortable'                => array(
			'hooks' => 'admin_enqueue_scripts'
		),

		'jquery-ui-draggable'               => array(
			'hooks' => 'admin_enqueue_scripts'
		),

		'plex_bootstrap_transition'         => array(
			'src' => $plex_asset_framework . '/bootstrap/js/bootstrap-transition.min.js'
		),

		'plex_bootstrap_tab'                => array(
			'src' => $plex_asset_framework . '/bootstrap/js/bootstrap-tab.min.js'
		),

		'plex_bootstrap_alert'              => array(
			'src' => $plex_asset_framework . '/bootstrap/js/bootstrap-alert.min.js'
		),


		'plex_bootstrap_collapse'           => array(
			'src' => $plex_asset_framework . '/bootstrap/js/bootstrap-collapse.min.js'
		),

		'plex_animationend'                 => array(
			'src' => $plex_asset_js . '/animationend.min.js'
		),

		'google_maps_api'                   => array(
			'src' => 'http://maps.google.com/maps/api/js?sensor=false'
		),

		'plex_baron'                        => array(
			'src'  => $plex_asset_js . '/plugin/baron/baron.min.js',
			'deps' => array(
				'jquery'
			)
		)
	)
);