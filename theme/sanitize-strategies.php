<?php

return array(
	new Plex_Lib_Sanitizer_Strategies_AbsInt(), // abs_int
	new Plex_Lib_Sanitizer_Strategies_TextField(), // text_field
	new Plex_Lib_Sanitizer_Strategies_Url(), // url
	new Plex_Lib_Sanitizer_Strategies_HtmlData(), // html_data
	new Plex_Lib_Sanitizer_Strategies_Bool(), // bool
	new Plex_Lib_Sanitizer_Strategies_Date(), // date
	new Plex_Lib_Sanitizer_Strategies_InList(), // in_list
	new Plex_Lib_Sanitizer_Strategies_SqlOrderBy(), //sql_order_by
	new Plex_Lib_Sanitizer_Strategies_HtmlClass(), // html_class
	new Plex_Lib_Sanitizer_Strategies_Attr(), // attr
	new Plex_Lib_Sanitizer_Strategies_Without(), // without
	new Plex_Lib_Sanitizer_Strategies_Post()		// post
);