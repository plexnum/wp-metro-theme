<?php

return array(
	// Plex theme required dependencies
	new Plex_Component_ThumbnailSizesRegistery(),
	new Plex_Component_ThemeActivation(),
	new Plex_Component_ThemeCustomizer(),
	new Plex_Component_MenuLookCustomizer(),
	new Plex_Component_MetaBoxRegistry(),
	new Plex_Component_PostTypeRegistry(),
	new Plex_Component_SanitizeStrategiesRegistry(),
	new Plex_Component_JsLocalization(),
	new Plex_Component_GalleryCustomizer(),
	new Plex_Component_AssetsRegistry(),
	new Plex_Component_Page_MetroTheme(),
	new Plex_Component_Page_Settings(),
	new Plex_Component_ShortCodeRegistry(),
	new Plex_Component_RequiredPluginsRegistry(),
	new Plex_Component_ThemeResolver(),
	new Plex_Component_TaxonomiesRegistry(),
	new Plex_Component_TinyMceStyleResolver()
);