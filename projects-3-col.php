<?php

/**
 * Template Name: Projects 3 Column
 */

/**
 * ========================================================================
 * Projects template page
 * ========================================================================
 */

if ( ! defined( 'ABSPATH' ) ) exit;

$plex_view_transfer = Plex_Lib_ViewTransfer::get_transfer();
$plex_view_transfer->set( 'col', 3 );

plex_load_partial( 'projects-2-col.php' );